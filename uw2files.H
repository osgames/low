


#ifndef LOW_UW2FILES_H__
#define LOW_UW2FILES_H__

#include <stdlib.h>

namespace UW2 {




    class DataFile {
      private:
	size_t				len_;
	const unsigned char*		data_;

      public:
					DataFile(const char*);
					~DataFile();

	size_t				len(void)			{ return len_; };
	const unsigned char*		data(void)			{ return data_; };
	unsigned			u8(size_t i)			{ return data_[i]; };
	unsigned			u16(size_t i)			{ return u8(i) | (u8(i+1)<<8); };
	unsigned			u32(size_t i)			{ return u16(i) | (u16(i+2)<<16); };
    };

    /// i386 and x86_64 Linux ABI places bitfields in
    /// strict LSB first order.  Adapt as needed.
    struct ifTile {
	unsigned short			type:4,
					alt:4,
					light:1,
					unk1_:1,
					floortex:4,
					nomagic:1,
					door:1;
	unsigned short			walltex:6,
					ent:10;
    };

    struct ifEnt {
	unsigned short			id:9,
					flags:6,
					quantified:1;
	unsigned short			z:7,
					dir:3,
					y:3,
					x:3;
	unsigned short			quality:6,
					next:10;
	unsigned short			owner:6,
					special:10;
	unsigned char			mob[];
    };

    class ArkFile: protected DataFile {
      private:
	size_t				nblocks_;
	const unsigned char**		blocks_;
	size_t*				len_;

      public:
		    			ArkFile(const char*);
					~ArkFile();

	const unsigned char*		block(int m);
	size_t				len(int m);
    };

    class Map {
      private:
	const unsigned char*		data_;

      public:
					Map(const unsigned char* d): data_(d)	{ };
					Map(const Map& m): data_(m.data_)	{ };

					operator bool (void) const		{ return data_; };
	const ifTile&			tile(int x, int y)			{ return reinterpret_cast<const ifTile*>(data_)[x+64*(y)]; };
	const ifEnt&			ent(int id)				{ return *reinterpret_cast<const ifEnt*>(data_+
											(id<256? 0x4000+id*27: 0x5300+id*8)); };
    };

    class MapFile: protected ArkFile {
      public:
					MapFile(const char* fname): ArkFile(fname)	{ };

	Map				map(int m)				{ return block(m); };
	const unsigned char*		texmap(int m)				{ return block(m+80); };
    };

    class StringFile {
      private:
	struct Page {
	    size_t			    num;
	    const char**		    str;

	    				    Page(void): num(0), str(0)		{ };
					    ~Page();
	};
	size_t				nump_;
	Page*				pages_;

      public:
					StringFile(const char*);
					~StringFile();

	const char*			operator () (int, int);
    };

}; // namespace UW2

#endif // LOW_UW2FILES_H__
