

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "uw2files.H"


static const UW2::ifTile	solid = { }; // all 0

static void pillar(FILE* f, int x, int y, const char** tex)
{
    x *= 10;
    y *= 10;
    int	x1 = x + 10;
    int	y1 = y + 10;
    fprintf(f, "{\n");
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 -1.25 0 0 0\n",
	   x1,    y,     320,
	   x,     y,     320,
	   x,     y1,    320, tex[0]); // top
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	   x1,    y1,    320,
	   x,     y1,    320,
	   x1,    y1,    0,   tex[2]); // N
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 -1.25 1.25 0 0 0\n",
	   x1,    y1,    320,
	   x1,    y1,    0,
	   x1,    y,     320, tex[4]); // E
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 -1.25 0 0 0\n",
	   x,     y,     0,
	   x1,    y,     0,
	   x,     y1,    0,   tex[1]); // bot
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	   x,     y,     0,
	   x,     y,     320,
	   x1,    y,     0,   tex[3]); // S
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 -1.25 1.25 0 0 0\n",
	   x,     y,     0,
	   x,     y1,    0,
	   x,     y,     320, tex[5]); // W
    fprintf(f, "}\n");
}

static void brush(FILE* f, int x, int y, int bot, int tz, int tzx, int tzy, const char** tex)
{
    int top = tz;
    if(tzx > top)
	top = tzx;
    if(tzy > top)
	top = tzy;
    fprintf(f, "{\n");
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 -1.25 0 0 0\n",
	   80*(x+1), 80*y,     tzx,
	   80*x,     80*y,     tz,
	   80*x,     80*(y+1), tzy, tex[0]); // top
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	   80*(x+1), 80*(y+1), top,
	   80*x,     80*(y+1), top,
	   80*(x+1), 80*(y+1), bot, tex[2]); // N
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 -1.25 1.25 0 0 0\n",
	   80*(x+1), 80*(y+1), top,
	   80*(x+1), 80*(y+1), bot,
	   80*(x+1), 80*y,     top, tex[4]); // E
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 -1.25 0 0 0\n",
	   80*x,     80*y,     bot,
	   80*(x+1), 80*y,     bot,
	   80*x,     80*(y+1), bot, tex[1]); // bot
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	   80*x,     80*y,     bot,
	   80*x,     80*y,     top,
	   80*(x+1), 80*y,     bot, tex[3]); // S
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 -1.25 1.25 0 0 0\n",
	   80*x,     80*y,     bot,
	   80*x,     80*(y+1), bot,
	   80*x,     80*y,     top, tex[5]); // W
    fprintf(f, "}\n");
}

static void tbrush(FILE* f, int x, int y, int bot, int top, int angle, const char** tex)
{
    int	xx[4] = { 80*x, 80*x, 80*(x+1), 80*(x+1) };
    int yy[4] = { 80*y, 80*(y+1), 80*(y+1), 80*y };
    int	v[4][3] = {	{ 0, 1, 2 },
			{ 1, 2, 3 },
			{ 3, 0, 1 },
			{ 2, 3, 0 } };
    int t[4][2] = {	{ 4, 2, },
			{ 2, 5, },
			{ 3, 4, },
			{ 5, 3, } };

    fprintf(f, "{\n");
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 -1.25 0 0 0\n",
	    xx[v[angle][0]], yy[v[angle][0]], top,
	    xx[v[angle][1]], yy[v[angle][1]], top,
	    xx[v[angle][2]], yy[v[angle][2]], top, tex[0]);
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 -1.25 0 0 0\n",
	    xx[v[angle][2]], yy[v[angle][2]], bot,
	    xx[v[angle][1]], yy[v[angle][1]], bot,
	    xx[v[angle][0]], yy[v[angle][0]], bot, tex[0]);
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	    xx[v[angle][0]], yy[v[angle][0]], bot,
	    xx[v[angle][1]], yy[v[angle][1]], bot,
	    xx[v[angle][1]], yy[v[angle][1]], top, tex[t[angle][0]]);
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	    xx[v[angle][1]], yy[v[angle][1]], bot,
	    xx[v[angle][2]], yy[v[angle][2]], bot,
	    xx[v[angle][2]], yy[v[angle][2]], top, tex[t[angle][1]]);
    fprintf(f, " ( %d %d %d ) ( %d %d %d ) ( %d %d %d ) %s 0 0 0 1.25 1.25 0 0 0\n",
	    xx[v[angle][2]], yy[v[angle][2]], bot,
	    xx[v[angle][0]], yy[v[angle][0]], bot,
	    xx[v[angle][0]], yy[v[angle][0]], top, tex[1]);
    fprintf(f, "}\n");
}

static struct {
    int	n;
    const char* name;
    int x, y;
} mapinfo[] = {
	{ 1, "brit0", 16, 53 },
	{ 2, "brit1", 11, 42 },
	{ 3, "brit2", 14, 53 },
	{ 4, "brit3", 60, 22 },
	{ 5, "brit4", 44, 33 },
	{ 9, "tower0", 33, 32 },
	{ 10, "tower1", 28, 31 },
	{ 11, "tower2", 36, 35 },
	{ 12, "tower3", 27, 34 },
	{ 13, "tower4", 28, 31 },
	{ 14, "tower5", 28, 35 },
	{ 15, "tower6", 28, 31 },
	{ 16, "tower7", 37, 32 },
	{ 17, "killorn0", 27, 34 },
	{ 18, "killorn1", 42, 52 },
	{ 25, "ice0", 18, 40 },
	{ 26, "ice1", 6, 32 },
	{ 33, "talorus0", 31, 31 },
	{ 34, "talorus1", 15, 32 },
	{ 41, "academy0", 4, 39 },
	{ 42, "academy1", 1, 30 },
	{ 43, "academy2", 27, 29 },
	{ 44, "academy3", 1, 30 },
	{ 45, "academy4", 20, 41 },
	{ 46, "academy5", 24, 38 },
	{ 47, "academy6", 22, 29 },
	{ 48, "academy7", 26, 39 },
	{ 49, "tomb0", 32, 32 },
	{ 50, "tomb1", 32, 53 },
	{ 51, "tomb2", 43, 27 },
	{ 52, "tomb3", 30, 50 },
	{ 57, "pits0", 59, 19 },
	{ 58, "pits1", 33, 2 },
	{ 59, "pits2", 46, 56 },
	{ 65, "ether0", 26, 14 },
	{ 66, "ether1", 45, 19 },
	{ 67, "ether2", 8, 15 },
	{ 68, "ether3", 21, 13 },
	{ 69, "ether4", 32, 3 },
	{ 71, "unknown0", 24, 37 },
	{ 72, "unknown1", 19, 48 },
};

int main(int, char** argv)
{
    UW2::StringFile	str("../../uw2/data/strings.pak");
    UW2::MapFile	lev("../../uw2/data/lev.ark");

    for(int i=0; i<mapinfo[i].n; i++)
	if(UW2::Map map = lev.map(mapinfo[i].n)) {
	    char	fname[64];
	    sprintf(fname, "maps/%s.map", mapinfo[i].name);
	    FILE* f = fopen(fname, "w");
	    const unsigned char* tm = lev.texmap(mapinfo[i].n);
	    fprintf(f, "{\n"
		    "\"classname\" \"worldspawn\"\n"
		    "\"ambient\" \"10\"\n");
	    for(int y=-1; y<65; y++) {
		for(int x=-1; x<65; x++) {
		    const UW2::ifTile&	T = (y>=0 && y<64 && x>=0 && x<64)? map.tile(x, y): solid;
		    const UW2::ifTile&	N = (y>0)?  map.tile(x, y-1): solid;
		    const UW2::ifTile&	S = (y<63)? map.tile(x, y+1): solid;
		    const UW2::ifTile&	W = (x>0)?  map.tile(x-1, y): solid;
		    const UW2::ifTile&	E = (x<63)? map.tile(x+1, y): solid;

		    char	texs[6][80] = {
			"base_floor/clang_floor2",
			"base_ceiling/metceil1d",
			"gothic_wall/streetbricks11",
			"gothic_wall/streetbricks11",
			"gothic_wall/streetbricks11",
			"wall/marble_wall"
		    };
		    const char*	tex[6] = { texs[0], texs[1], texs[2], texs[3], texs[4], texs[5], };

#define MAPTEX(tnum) ((tm[tnum*2]|tm[tnum*2+1]<<8))
#define TEXNAME(n,texnum) \
		    if(const char* tn = str(10, texnum)) { \
			sprintf(texs[n], "uw2/%s_%d", tn+(tn[0]=='a'? (tn[1]==' '? 2: ((tn[1]=='n'&&tn[2]==' ')? 3: 0)): 0), texnum); \
			for(char* s=texs[n]; *s; s++) \
			if(*s==' ') \
			*s = '_'; \
		    } else \
		    sprintf(texs[n], "uw2/unk_%d", texnum)
#define TEX6(tnum) TEXNAME(0,MAPTEX(tnum)); for(int i=1; i<6; i++) strcpy(texs[i],texs[0])

		    int	ctex = 32;
		    for(int o=T.ent; o; o=map.ent(o).next) {
			const UW2::ifEnt&	e = map.ent(o);
			int			n;

			switch(e.id) {
			  case 0x160: // a_pillar
			    if(e.z==127)
				ctex = e.flags>1? e.flags-2: 32;
			    else {
				if(e.flags>1) {
				    TEX6(e.flags-2);
				} else {
				    TEX6(32);
				}
				pillar(f, x*8, y*8, tex);
			    }
			    break;
			  case 0x164: // a_bridge
			    if(e.z==127)
				ctex = e.flags>1? e.flags-2: 32;
			    else {
				if(e.flags>1) {
				    TEX6(e.flags-2);
				} else {
				    TEX6(32);
				}
				n = (e.z>>1)*5;
				brush(f, x, y, n, n+5, n+5, n+5, tex);
			    }
			    break;
			}
		    }
		    TEXNAME(0, MAPTEX(T.floortex));
		    TEXNAME(1, MAPTEX(ctex));
		    TEXNAME(2, MAPTEX(S.walltex));
		    TEXNAME(3, MAPTEX(N.walltex));
		    TEXNAME(4, MAPTEX(E.walltex));
		    TEXNAME(5, MAPTEX(W.walltex));
		    if(T.type) {
			int	alt = T.alt*20;
			switch(T.type) {
			  case 1:
			    brush(f, x, y, -5, alt, alt, alt, tex);
			    break;
			  case 2: // SE
			  case 3: // SW
			  case 4: // NE
			  case 5: // NW
			    TEXNAME(1, MAPTEX(T.walltex));
			    tbrush(f, x, y, -5, 320, T.type-2, tex);
			    tbrush(f, x, y, -5, alt, (T.type-2)^3, tex);
			    TEXNAME(1, MAPTEX(32));
			    break;
			  case 6:
			    brush(f, x, y, -5, alt, alt, alt+20, tex);
			    break;
			  case 7:
			    brush(f, x, y, -5, alt+20, alt+20, alt, tex);
			    break;
			  case 8:
			    brush(f, x, y, -5, alt, alt+20, alt, tex);
			    break;
			  case 9:
			    brush(f, x, y, -5, alt+20, alt, alt+20, tex);
			    break;
			}
			brush(f, x, y, 320, 325, 325, 325, tex);
		    } else if(N.type || N.ent || S.type || S.ent || E.type || E.ent || W.type || W.ent) {
			brush(f, x, y, -5, 320, 320, 320, tex);
		    }
		}
	    }
	    int px = mapinfo[i].x;
	    int py = mapinfo[i].y;
	    fprintf(f, "}\n");
	    strcat(fname, ".ents");
	    if(FILE* fi = fopen(fname, "r")) {
		char	pad[1024];
		while(fgets(pad, 1024, fi))
		    fputs(pad, f);
		fclose(fi);
	    }
	    fprintf(f, "{\n\"classname\" \"info_player_start\"\n\"origin\" \"%d.000000 %d.000000 %d.000000\"\n}\n",
		    int((0.5+px)*80), int((0.5+py)*80), (map.tile(px, py).alt*20+32));
	    fclose(f);
	}
}

