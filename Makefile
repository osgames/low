####
####  This file is Copyright (c) 2009 Marc A. Pelletier et. al. and is
####  made available under the Perl Artistic License version 2.0 (see
####  the file LICENCE.TXT in the root directory of this package, or
####  http://www.perlfoundation.org/artistic_license_2_0 for the terms).
####

CFLAGS		= -g3 -O0 -pipe
CFLAGS		+= `pkg-config --cflags OGRE`
OGRELIBS	= `pkg-config --libs OGRE`
OISLIBS		= `pkg-config --libs OIS`
BULLETLIBS	= -lBulletDynamics -lBulletCollision -lLinearMath
LIBS		= $(OGRELIBS) $(OISLIBS) $(BULLETLIBS)

Q3MAP2  = /opt/netradiant-395-x86_64/q3map2.x86_64

DEPDIR := .deps
DEPFILES = $(wildcard $(patsubst %,%/*.d,$(DEPDIR)))
MAPS     = academy0.map academy1.map academy2.map academy3.map \
	   academy4.map academy5.map academy6.map academy7.map \
	   brit0.map brit1.map brit2.map brit3.map brit4.map \
	   ether0.map ether1.map ether2.map ether3.map ether4.map \
	   ice0.map ice1.map \
	   killorn0.map killorn1.map \
	   pits0.map pits1.map pits2.map \
	   talorus0.map talorus1.map \
	   tomb0.map tomb1.map tomb2.map tomb3.map \
	   tower0.map tower1.map tower2.map tower3.map \
	   tower4.map tower5.map tower6.map tower7.map
MAPFILES = $(addprefix maps/,$(MAPS))
BSPFILES = $(addprefix maps/,$(addsuffix .bsp,$(basename $(MAPS))))

all:		walk pull_maps pull_tex

all-bsp:	$(BSPFILES)

%.o:		%.cc
		@echo "  CC      $<"
		@mkdir -p "$(*D)/$(DEPDIR)"
		@g++ $(CFLAGS) -Wp,-MMD,"$(*D)/$(DEPDIR)/$(*F).d",-MQ,"$@",-MP -c $(<) -o $*.o

$(MAPFILES):	pull_maps
		@echo "  GEN     maps"
		@./pull_maps

maps/%.bsp:	maps/%.map
		@echo "  BSP     $*"
		@$(Q3MAP2) -meta -v -skyfix -leaktest $< >maps/$*.log
		@echo "  VIS     $*"
		@$(Q3MAP2) -vis -v -merge $< >>maps/$*.log
		@echo "  LIGHT   $*"
		@$(Q3MAP2) -light -fast -samples 3 -shade -bounce 2 \
		  -bouncegrid -gamma 1.6 -compensate 4 $< >>maps/$*.log

walk:		walk.o game.o \
		ogre_video.o \
		simple_physics.o \
		gamesystem.o uw2files.o \
		md-unix.o
		@echo "  LD      $@"
		@g++ -g3 -o walk $^ $(LIBS)

pull_maps:	pull_maps.o uw2files.o md-unix.o
		@echo "  LD      $@"
		@g++ -g3 -o pull_maps $^

pull_tex:	pull_tex.o uw2files.o md-unix.o
		@echo "  LD      $@"
		@g++ -g3 -o pull_tex $^

textures/.stamp:pull_tex
		@echo "  GEN     textures"
		@./pull_tex
		@echo "  CONVERT textures"
		@(cd textures/uw2;for i in *.xpm;do convert $$i $$(basename $$i .xpm).tga;rm $$i;done)
		@touch textures/.stamp

include .deps/*.d
