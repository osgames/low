!!ARBfp1.0
OPTION ARB_precision_hint_fastest;

ATTRIB tc = fragment.texcoord[0];
ATTRIB eyev = fragment.texcoord[1];
ATTRIB lightv = fragment.texcoord[2];
PARAM lightc = program.env[0];
PARAM const = program.env[1];
PARAM nmm = { 2, 2, 2, -1 };
PARAM nma = { -1, -1, -1, 1 };
OUTPUT ocol = result.color;

TEMP tmp;
TEMP lv;
TEMP diff;
TEMP normal;
TEMP color;

TEX tmp, tc, texture[0], 2D;
MAD normal, tmp, nmm, nma;
TEX color, tc, texture[1], 2D;

DP3 tmp, eyev, eyev;

DP3 lv, lightv, lightv;
RSQ lv.w, lv.x;
MUL lv.xyz, lightv, lv.w;
SUB_SAT lv.w, lv.w, const.x;

DP3 diff, lv, normal;
MUL diff, lightc, diff;
MUL diff, diff, lv.w;

SUB tmp, const.z, tmp;
MAD_SAT tmp, const.w, tmp, nma.w;
MUL diff, diff, color;
LRP diff, color.a, color, diff;

MUL ocol, diff, tmp;

END
