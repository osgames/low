!!ARBfp1.0
OPTION ARB_precision_hint_fastest;

ATTRIB tc = fragment.texcoord[0];
ATTRIB eyev = fragment.texcoord[1];
ATTRIB lightv = fragment.texcoord[2];
PARAM lightc = program.env[0];
PARAM const = program.env[1];
PARAM nmm = { 2, 2, 2, -1 };
PARAM nma = { -1, -1, -1, 1 };
OUTPUT ocol = result.color;

TEMP tmp;
TEMP lv;
TEMP fog;
TEMP ev;
TEMP diff;
TEMP spec;
TEMP normal;
TEMP color;

TEX tmp, tc, texture[0], 2D;
MAD normal, tmp, nmm, nma;
TEX color, tc, texture[1], 2D;

DP3 fog, eyev, eyev;
RSQ tmp, fog.x;
MUL ev, eyev, tmp;

DP3 lv, lightv, lightv;
RSQ lv.w, lv.x;
MUL lv.xyz, lightv, lv.w;
SUB_SAT lv.w, lv.w, const.x;

DP3 tmp, lv, normal;

MUL spec, normal, -tmp.y;
MAD spec, nmm.x, spec, lv;
DP3 tmp.y, spec, ev;
MOV tmp.w, const.y;
LIT tmp.xyz, tmp;
MUL tmp.x, tmp.y, lv.w;
MUL diff, lightc, tmp.x;
MUL spec, diff, tmp.z;
MUL spec, spec, normal.a;
MUL diff, diff, tmp.y;

SUB fog, const.z, fog;
MAD_SAT fog, const.w, fog, nma.w;
MAD diff, diff, color, spec;
LRP diff, color.a, color, diff;

MUL ocol, diff, fog;

END
