////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include <stdio.h>
#include "game.H"


Game*		game = 0;		// the game singleton


static void test_tick(Game& g) {
    g.phy_simulate(1.0l/60);
};

const Game::Mode	nothing_mode = { 0, 0, 0, 0, 0, 0, test_tick, 0 };

Game::Game(void):
    mode(&nothing_mode)
{
    str_ = new UW2::StringFile("/home/marc/uw2/data/strings.pak");
    maps = new UW2::MapFile("/home/marc/uw2/data/lev.ark");
}

Game::~Game()
{
}


void Game::enter_level(int level)
{
    vid_prepare_world(level);
    phy_prepare_world(level);
    //aud_prepare_level();
    place_player(18, 48, 0, 116);
}

void Game::place_player(int x, int y, int face, int z)
{
    phy_place_player(0.5+x, 0.5+y, 0.75+(y/16.0), 90);
}

