x_skill(a, skill)
    a = 10001, increase skill by 1?
    a =  9999, return skill?
    	0 attack, 1 defense, 2 unarmed, 3 swords
	6 missile, 
	7 mana, 8 lore, 9 casting,
	10 disarm, 11 searching, 14 repair,
	15 charisma, 16 lockpick, 18 evaluation, 19 swimming,

int do_inv_create(n)
    creates and instance of object type 'n'
    returns some object identifier?

void x_obj_stuff(a, b, c, d, e, f, g, h, i)
    set object properties?  For which object?
    sewer key has f=28, h=1
    i is object identifier
    f is *special

void take_from_npc(n)
    takes an object of type 'n' from npc and give to avatar
    *must not fail* (drop object on ground if hands full?)

setup_to_barter()
    puts inventory in barter area

do_offer(a, b, c, d, e, f, g)

do_judgement()

do_demand(a, b, c)

do_decline()

show_inv(0, 0)

babl_hack(a, b)
    9 = merzan deal evaluation (b=525, then 0)

find_inv(a, b)
    look for item type 'b' in player inventory
    return id or truth?

