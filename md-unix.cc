

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "uw2files.H"


namespace UW2 {

    DataFile::DataFile(const char* fname):
	data_(0)
      {
	int fd = open(fname, O_RDONLY);
	if(fd >= 0) {
	    len_ = lseek(fd, 0, SEEK_END);
	    unsigned char* bytes = new unsigned char[len_];
	    if(lseek(fd, 0, SEEK_SET))
		throw errno;
	    if(read(fd, bytes, len_) != len_)
		throw errno;
	    close(fd);
	    data_ = bytes;
	    return;
	}
	throw errno;
      }

    DataFile::~DataFile()
      {
	if(data_)
	    delete[] data_;
      }


}; // namespace UW2

