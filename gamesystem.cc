
#include <unistd.h>
#include <stdio.h>

#include "game.H"
#include "ogre_video.H"
//#include "openal_audio.H"
#include "simple_physics.H"


class GameSystem:
    virtual public OgreVideo,
 // virtual public OpenALAudio,
    virtual public SimplePhysics
{
};


void Game::create_game_system(void)
{
    game = new GameSystem;
};

