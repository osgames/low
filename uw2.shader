textures/uw2/lavafall_60
{
    surfaceparm nolightmap
    surfaceparm nomarks
    q3map_surfacelight 666
    q3map_globaltexture
    {
      map textures/uw2/lavafall_60
	  tcMod turb 0 0.1 0 0.1
	  tcMod scroll 0.1 0.2
    }
    {
      map textures/uw2/lavafall_60
	  blendfunc fliter
	  tcMod turb 0 0.2 0 0.1
    }
    {
      map textures/uw2/lavafall_60
	  blendfunc add
	  tcMod scroll 0 0.5
    }

}

textures/uw2/fireplace_102
{
    surfaceparm nomarks
    q3map_surfacelight 666
    q3map_globaltexture
    qer_lightimage textures/uw2/lavafall_60
    {
      map $lightmap
    }
    {
      map textures/uw2/fireplace_102_lm
      blendfunc add
    }
    {
      map textures/uw2/fireplace_102
      blendfunc filter
    }
}

