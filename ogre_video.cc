////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>

#include "ogre_video.H"
#define OIS_DYNAMIC_LIB
#include <OgreFrameListener.h>
#include <OIS/OIS.h>
#include <OIS/OISKeyboard.h>
#include "events.H"

using namespace Ogre;

OgreVideo::OgreVideo(void):
    Game()
{
    root = new Root;

    root->setRenderSystem(root->getRenderSystemByName("OpenGL Rendering Subsystem"));
    root->addResourceLocation("/home/marc/work/low", "FileSystem");
    root->initialise(false);

    NameValuePairList	wopt;
    wopt["vsync"] = "true";
    window = root->createRenderWindow("Walkabout", 1200, 675, false, &wopt);

    smanager = root->createSceneManager("BspSceneManager");

    camera = smanager->createCamera("PlayerCamera"); 

    vport = window->addViewport(camera);
    vport->setBackgroundColour(ColourValue(1,1,1));

    size_t win_id = 0;
    OIS::ParamList plist;
    std::ostringstream win_id_str;
    window->getCustomAttribute("WINDOW", &win_id);
    win_id_str << (unsigned int)win_id;
    plist.insert(std::make_pair(std::string("WINDOW"), win_id_str.str()));
    plist.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
    plist.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
    plist.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
    plist.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
    imanager = OIS::InputManager::createInputSystem(plist);
    kbd = static_cast<OIS::Keyboard*>(imanager->createInputObject(OIS::OISKeyboard, true));
    mouse = static_cast<OIS::Mouse*>(imanager->createInputObject(OIS::OISMouse, true));
    kbd->setEventCallback(new OgreVideo::KeyListener(*this));
    mouse->setEventCallback(new OgreVideo::MouseListener(*this));
    root->addFrameListener(new OgreVideo::FrameListener(*this));

    //unsigned int ww, wh, wd;
    //int		 wt, wl;
    //window->getMetrics(ww, wh, wd, wl, wt);
}

OgreVideo::~OgreVideo()
{
    if(imanager) {
	if(mouse) imanager->destroyInputObject(mouse);
	if(kbd)	imanager->destroyInputObject(kbd);
	OIS::InputManager::destroyInputSystem(imanager);
    }
    delete root;
}

static struct {
    int	n;
    const char* name;
} mapinfo[] = {
	{ 1, "brit0", },
	{ 2, "brit1", },
	{ 3, "brit2", },
	{ 4, "brit3", },
	{ 5, "brit4", },
	{ 9, "tower0", },
	{ 10, "tower1", },
	{ 11, "tower2", },
	{ 12, "tower3", },
	{ 13, "tower4", },
	{ 14, "tower5", },
	{ 15, "tower6", },
	{ 16, "tower7", },
	{ 17, "killorn0", },
	{ 18, "killorn1", },
	{ 25, "ice0", },
	{ 26, "ice1", },
	{ 33, "talorus0", },
	{ 34, "talorus1", },
	{ 41, "academy0", },
	{ 42, "academy1", },
	{ 43, "academy2", },
	{ 44, "academy3", },
	{ 45, "academy4", },
	{ 46, "academy5", },
	{ 47, "academy6", },
	{ 48, "academy7", },
	{ 49, "tomb0", },
	{ 50, "tomb1", },
	{ 51, "tomb2", },
	{ 52, "tomb3", },
	{ 57, "pits0", },
	{ 58, "pits1", },
	{ 59, "pits2", },
	{ 65, "ether0", },
	{ 66, "ether1", },
	{ 67, "ether2", },
	{ 68, "ether3", },
	{ 69, "ether4", },
	{ 71, "unknown0", },
	{ 72, "unknown1", },
	{ 0, 0 }
};

void OgreVideo::vid_start(void)
{
    root->startRendering();
}

void OgreVideo::vid_prepare_world(int level)
{
    char	bspname[64] = { 0 };
    for(int i=0; mapinfo[i].n; i++)
	if(mapinfo[i].n == level) {
	    sprintf(bspname, "maps/%s.bsp", mapinfo[i].name);
	    break;
	}
    if(!bspname[0])
	throw "Invalid level number";

    smanager->clearScene();

    camera->setDirection(1, 0, 0);
    camera->roll(Degree(-90));
    camera->setFixedYawAxis(true, Vector3::UNIT_Z);
    camera->setPosition(1470, 3880, 290); // XXX: Hax!
    camera->setPosition(2270, 3880, 290); // XXX: Hax!
    camera->setNearClipDistance(4);
    camera->setFarClipDistance(4000);
    camera->setAspectRatio(16.0/9.0);
    camera->setFOVy(Degree(75));

    rgm = &ResourceGroupManager::getSingleton();
    rgm->linkWorldGeometryToResourceGroup(rgm->getWorldResourceGroupName(), bspname, smanager);
    rgm->initialiseAllResourceGroups();
    rgm->loadResourceGroup(rgm->getWorldResourceGroupName(), false, true);
}

void OgreVideo::vid_player_view(float x, float y, float z, float dir)
{
    camera->setDirection(1, 0, 0);
    camera->roll(Radian(dir));
    camera->setFixedYawAxis(true, Vector3::UNIT_Z);
    camera->setPosition(x, y, z);
}

bool OgreVideo::frame_started(const Ogre::FrameEvent&)
{
    return true;
}

bool forcequit = false;

bool OgreVideo::frame_ended(const Ogre::FrameEvent&)
{
    static timeval	tv_left = { 0, 0 };
    static unsigned	frames = 0;
    static timeval	tv_prev = { 0, 0 };
    timeval		tv_now;

    gettimeofday(&tv_now, 0);
    if(tv_prev.tv_sec) {
	tv_left.tv_sec += tv_now.tv_sec - tv_prev.tv_sec;
	tv_left.tv_usec += tv_now.tv_usec - tv_prev.tv_usec;
	if(tv_left.tv_usec < 0) {
	    tv_left.tv_usec += 1000000;
	    tv_left.tv_sec--;
	} else if(tv_left.tv_usec > 999999) {
	    tv_left.tv_usec -= 1000000;
	    tv_left.tv_sec++;
	}
    }
    tv_prev = tv_now;
    while(tv_left.tv_sec) {
	frames -= 60;
	tv_left.tv_sec--;
    }
    do {
	frames++;
	if(mode->tick)
	    (mode->tick)(*this);
    } while(frames < int(tv_left.tv_sec/16666));

    if(mouse) mouse->capture();
    if(kbd) kbd->capture();
    return !forcequit;
}

static const struct {
    OIS::KeyCode	ois;
    keyboard_syms_	low;
} keycodes[] = {
#include "ogre_ksym_table.h"
};

bool OgreVideo::key_pressed(const OIS::KeyEvent& ke)
{
    if(ke.key == OIS::KC_ESCAPE)
	forcequit = true;

    if(mode->keydown) {
	for(int i=0; keycodes[i].low!=K_No_Key; i++)
	    if(ke.key == keycodes[i].ois) {
		(mode->keydown)(*this, keycodes[i].low);
		break;
	    }
    }
    if(mode->keypress && ke.text) {
	char	utf8[16];

	utf8[0] = 0;
	if(ke.text < 127) {
	    utf8[0] = ke.text;
	    utf8[1] = 0;
	}
	if(utf8[0])
	    (mode->keypress)(*this, utf8);
    }
    return true;
}

bool OgreVideo::key_released(const OIS::KeyEvent& ke)
{
    return true;
}

bool OgreVideo::mouse_moved(const OIS::MouseEvent& me)
{
    return true;
}

bool OgreVideo::mouse_pressed(const OIS::MouseEvent& me, OIS::MouseButtonID mb)
{
    return true;
}

bool OgreVideo::mouse_released(const OIS::MouseEvent& me, OIS::MouseButtonID mb)
{
    return true;
}

