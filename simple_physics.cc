

#include "uw2files.H"
#include "simple_physics.H"


#if 0
static void brush(FILE* f, int x, int y, int bot, int tz, int tzx, int tzy)
{
    int top = tz;
    if(tzx > top)
	top = tzx;
    if(tzy > top)
	top = tzy;
    
    // convex hull of:
	   80*(x+1), 80*(y+1), bot,
	   80*(x+1), 80*(y+1), top,
	   80*(x+1), 80*y,     bot,
	   80*(x+1), 80*y,     top,
	   80*(x+1), 80*y,     tzx,
	   80*x,     80*(y+1), bot,
	   80*x,     80*(y+1), top,
	   80*x,     80*(y+1), tzy,
	   80*x,     80*y,     bot,
	   80*x,     80*y,     top,
	   80*x,     80*y,     tz,
}

static void tbrush(FILE* f, int x, int y, int bot, int top, int angle)
{
    int	xx[4] = { 80*x, 80*x, 80*(x+1), 80*(x+1) };
    int yy[4] = { 80*y, 80*(y+1), 80*(y+1), 80*y };
    int	v[4][3] = {	{ 0, 1, 2 },
			{ 1, 2, 3 },
			{ 3, 0, 1 },
			{ 2, 3, 0 } };

    // convex hull of
	    xx[v[angle][0]], yy[v[angle][0]], bot,
	    xx[v[angle][0]], yy[v[angle][0]], top,
	    xx[v[angle][1]], yy[v[angle][1]], bot,
	    xx[v[angle][1]], yy[v[angle][1]], top,
	    xx[v[angle][2]], yy[v[angle][2]], bot,
	    xx[v[angle][2]], yy[v[angle][2]], top,
}

#endif

void SimplePhysics::phy_prepare_world(int level)
{
    if(UW2::Map map = maps->map(level)) {
	for(int y=0; y<64; y++) {
	    for(int x=0; x<64; x++) {
		const UW2::ifTile& T = map.tile(x, y);
		for(int o=T.ent; o; o=map.ent(o).next) {
		    const UW2::ifEnt&	e = map.ent(o);
		    int			n;

		    switch(e.id) {
		      case 0x164: // a_bridge
			if(e.z!=127) {
			    n = (e.z>>1)*5;
			    // Add walkway
			    //brush(f, x, y, n, n+5, n+5, n+5, tex);
			}
			break;
		    }
		}
		int	alt = T.alt*20;
		switch(T.type) {
		  case 0:
		    // solid
		    //brush(f, x, y, -5, 320, 320, 320, tex);
		    break;
		  case 1:
		    // normal = { 0, 0, 1 };
		    //brush(f, x, y, -5, alt, alt, alt, tex);
		    break;
		  case 2: // SE
		  case 3: // SW
		  case 4: // NE
		  case 5: // NW
		    //tbrush(f, x, y, -5, 320, T.type-2);
		    //tbrush(f, x, y, -5, alt, (T.type-2)^3);
		    break;
		  case 6:
		    //brush(f, x, y, -5, alt, alt, alt+20, tex);
		    break;
		  case 7:
		    //brush(f, x, y, -5, alt+20, alt+20, alt, tex);
		    break;
		  case 8:
		    //brush(f, x, y, -5, alt, alt+20, alt, tex);
		    break;
		  case 9:
		    //brush(f, x, y, -5, alt+20, alt, alt+20, tex);
		    break;
		}
	    }
	}
    }
}



SimplePhysics::SimplePhysics(void)
{
}

SimplePhysics::~SimplePhysics()
{
}

void SimplePhysics::phy_simulate(double)
{
}

void SimplePhysics::phy_place_player(float, float, float, float)
{
}


