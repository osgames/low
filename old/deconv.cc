#include "setup.H"


static const unsigned char*	cnv = 0;

struct cnv_header_ {
    unsigned long	unknown1;
    unsigned long	size;
    unsigned short	unknown2;
    unsigned short	strings;
    unsigned short	gdepth;
    unsigned short	globals;
    unsigned char	conv[];
};

struct cnv_file_header_ {
    unsigned short	num;
    unsigned long	offs[0];

    const cnv_header_*	conv(int i) const;
} __attribute__((__packed__));

struct cnv_symbol_ {
    bool		func;
    char		type;
    char		id;
    char		name[29];
};

const cnv_header_* cnv_file_header_::conv(int i) const
  {
    if(!offs[i])
	return 0;
    int				ulen = *((int*)(cnv+offs[i]));
    unsigned char*		uc = new unsigned char[ulen+100];
    const unsigned char*	cc = cnv+offs[i]+4;

    unsigned char*		ue = uc + ulen;
    unsigned char*		up = uc;

    while(up<ue)
      {
	unsigned char	bits = *cc++;
	for(int i=0; i<8; i++, bits>>=1)
	  {
	    if(bits&1)
		*up++ = *cc++;
	    else
	      {
		int	m1 = *cc++;
		int	m2 = *cc++;

		m1 |= (m2&0xF0)<<4;
		if(m1&0x800)
		    m1 |= 0xFFFFF000;
		m2 = (m2&15) + 3;
		m1 += 18;
		if(m1 > (up-uc))
		    abort();
		while(m1 < (up-uc-0x1000))
		    m1 += 0x1000;
		while(m2--)
		    *up++ = uc[m1++];
	      }
	    if(up>=ue)
		break;
	  }
      }
    fprintf(stderr, "  done\n");
    return (cnv_header_*)uc;
  }

static const cnv_file_header_*	fh = 0;

static struct op_ {
    const char*		opcode;
    int			op;
}	ops[] = {
      /* 00 */	{ "nop", 0 },
      		{ "+", 0 },
		{ "*", 0 },
		{ "-", 0 },
		{ "/", 0 },
		{ "mod", 0 },
		{ "or", 0 },
		{ "and", 0 },
		{ "not", 0 },
		{ ">", 0 },
		{ ">=", 0 },
		{ "<", 0 },
		{ "<=", 0 },
		{ "=", 0 },
		{ "<>", 0 },
		{ "jmp", 1 },
      /* 10 */	{ "0branch", 2 },
      		{ "1branch", 2 },
		{ "branch", 2 },
		{ "(:)", 3 },
		{ "exec", 4 },
		{ "exit", 0 },
		{ "(lit)", 5 },
		{ "(local)", 6 },
		{ "drop", 0 },
		{ "swap", 0 },
		{ "depth", 0 },
		{ "sp!", 0 },
		{ "{", 0 },
		{ "}", 0 },
		{ "locals", 0 },
		{ "@", 0 },
      /* 20 */	{ "!", 0 },
		{ "array", 0 },
		{ "start", 0 },
		{ ">r", 0 },
		{ "r>", 0 },
		{ "strcmp", 0 },
		{ "quit", 0 },
		{ "emit", 0 },
		{ "query", 0 },
		{ "negate", 0 },
		{ "???", -1 },
};

int main(int, char**)
  {
    unsigned char*	c = new unsigned char[281657];
    cnv = c;
    fh = (const cnv_file_header_*)c;

    int	f = open("/home/marc/uw2/data/cnv.ark", O_RDONLY);
    read(f, c, 281657);
    close(f);

    printf("%d conversations\n", fh->num);

    for(int c=128; c<fh->num; c++)
      {
	if(const cnv_header_* ch=fh->conv(c))
	  {
	    printf("\n\n\nconversation %d: %d words, strings %d, gdepth %d, %d globals\n", c,
		    ch->size, ch->strings, ch->gdepth, ch->globals);
	    const unsigned char*	cd = ch->conv;

	    int			lbl[65536];
	    cnv_symbol_		s[ch->globals];
	    int			ns = 0;
	    for(int i=ch->globals; i; --i)
	      {
		int	fn = *cd++;
		memcpy(s[ns].name, ++cd, (fn<?28)>?0);
		s[ns].name[fn<?28] = 0;
		cd += fn;
		s[ns].id   = cd[0]; cd += 4;
		s[ns].func = cd[0]==17; cd += 2;
		if(s[ns].func)
		    printf(">>> %2x %s\n", s[ns].id, s[ns].name);
		s[ns++].type = cd[0]; cd += 2;
	      }

	    const unsigned short*	p = (const unsigned short*)cd;
	    for(int i=0; i<ch->size; i++)
		lbl[i] = 0;

	    for(int i=0; i<ch->size; i++)
	      {
		switch(p[0])
		  {
		    case 0x0F:
			if(p[1] != i+2)
			    lbl[p[1]] |= (p[1]<i)? 1: 2;
			break;
		    case 0x13:
			lbl[p[1]] |= 16;
			break;
		    case 0x10 ... 0x12:
			  {
			    int	d = i+1+(short)(p[1]);

			    lbl[d] |= (d<i)? 4: 8;
			  }
			break;
		  }
		op_&	op = ops[*p++ <? 0x2A];
		if(op.op>0)
		    p++, i++;
	      }
	    p = (unsigned short*)cd;

	    int	col = 0;

	    for(int i=0; i<ch->size; i++)
	      {
		if(p[0]==0x1A && p[1]==0x1C)
		  {
		    // idiom for (:)
		    if(col)
			printf("\n");
		    printf(": w%x\t", i);
		    p += 2;
		    i += 1;
		    col = 1;
		    continue;
		  }

		if(lbl[i])
		  {
		    if(col)
		      {
			printf("\n");
			col = 0;
		      }
		    printf(" (%x)\t", i); //, lbl[i]);
		    col = 1;
		  }
		if(!col)
		    printf("\t");

		if(p[0]==0x1D && p[1]==0x1B && p[2]==0x15)
		  {
		    // idiom for (;)
		    printf(";\n");
		    col = 0;
		    p += 3;
		    i += 2;
		    continue;
		  }

		op_&	op = ops[*p++ <? 0x2A];

		switch(op.op)
		  {
		    case 4:
			printf("_");
			for(int x=0; x<ns; x++)
			    if(s[x].func && s[x].id==*p)
			      {
				printf("%s(%d)", s[x].name, s[x].id);
				break;
			      }
			printf(" ");
			break;
		    case 5:
			printf("%hd ", *p);
			break;
		    case 6:
			printf("[%d] ", (short)*p);
			break;
		    case -1:
			printf("{%hx} ", p[-1]);
			break;
		    case 1:
			if(*p != i+2)
			    printf(">%x ", *p);
			break;
		    case 2:
			  {
			    int	d = ((short)(*p))+i+1;

			    switch(p[-1])
			      {
				case 0x0:	// 0branch
				    if(d>i+1)
				      {
					// ... forward, probably an 'if'
					printf("if ");
				      }
				    else
				      {
					// ... branch backwards, 'until'
					printf("until\n");
					col = -1;
				      }
				    break;
				default:
				    printf("%s>%x ", op.opcode, ((short)(*p))+i+1);
				    break;
			      }
			  }
			break;
		    case 3:
			printf("w%x ", *p);
			break;
		    default:
			printf("%s ", op.opcode);
			break;
		  }
		if(op.op>0)
		    p++, i++;
		if(++col==10)
		  {
		    printf("\n");
		    col = 0;
		  }
	      }
	    if(col)
		printf("\n");
	  }
      }
  }

