////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "map.H"
#include "entity.H"
#include "game.H"
#include "namedf.H"
#include "sound.H"
#include "textures.H"


static int doorbase(EIndex e)
  {
    switch(e->mat)
      {
	case 184:
	case 185:
	case 32:
            return 3;
	case 186:
            return 1;
        default:
            return 2;
      }
  }

static int doorwhen(EIndex e)
  {
    switch(e->mat)
      {
	case 184:
	case 185:
	case 32:
            return 5;
	case 186:
            return 45;
        default:
            return 85;
      }
  }


ENT_TICK_FUN(door)
  {
    if(e->int1==1) // opening
      {
	if(e->int2<5)
            Sound::sound(doorbase(e)+5, e->tx, e->ty, .5);
        if(e->int2<110)
            e->int2 += 5;
        else
            e->int1 = 2; // opened
      }
    else if(e->int1==3) // closing
      {
        if(e->int2>0)
          {
            if(e->int2<90 && player_occupies(e->tx, e->ty))
                e->int1 = 1;
            else
                e->int2 -= 5;
            if(e->int2 == doorwhen(e))
                Sound::sound(doorbase(e), e->tx, e->ty, .5);
          }
        else
            e->int1 = 0; // closed
      }
    if(e->int1==0 && !(e->link->flags&Entity::eSolid))
      {
	e->link->flags |= Entity::eSolid;
	Game::map(e->tx, e->ty).touched = true;
      }
  }

ENT_USE_FUN(door)
  {
    EIndex	lock = 0;

    for(EIndex e=o->f_contents; e; e=e->next)
	if(e->type==0x10F && !e->int1)	// a_lock
	  {
	    lock = e;
	    break;
	  }

    if(o->int1==0) // closed
      {
	if(lock && !force)
	  {
	    printf("lock: %d\n", lock->index());
	    msg("The door is locked.");
	    return false;
	  }
	o->int1 = 1;
	o->link->flags &= ~Entity::eSolid;
	Game::map(o->tx, o->ty).touched = true;
      }
    else
	o->int1 = 3;
    return true;
  }

ENT_USE_FUN(key)
  {
    if(force)
      {
	EIndex	lock = 0;

	for(EIndex e=target->f_contents; e; e=e->next)
	    if(e->type==0x10F)
	      {
		lock = e;
		break;
	      }

	if(!lock)
	    msg(Game::str(1, 3));
	else if(lock->special != o->special)
	    msg(Game::str(1, 2));
	else
	  {
	    lock->int1 = ~lock->int1;
	    msg(Game::str(1, lock->int1? 5: 4));
	  }
      }
    else
      {
	msg(Game::str(1, 7));
	do_target(o);
      }
    return true;
  }

extern void ur_render_wall(float, float, float, float, int, int, int, int, int, int, int, bool);

ENT_RENDER_PRE_FUN(rdoor)
  {
    float	bz = e->z;
    float	mz = bz - 0.5;
    float	tz = bz - 0.8125;
    float	tcr = .98;
    float	tcl = .02;
    float	tct = .82;
    float	tcm = .50;
    float	tcb = .01;

    if(e->mat<0x100) // secret door
      {
	tcr = .25;
	tcl = .75;
	tct = 0.1875 + bz;
	tcm = 0.5 + bz;
	tcb = 1.0 + bz;
      }

    glTranslatef(-.25, -.475, 0);
    glRotatef(e->int2, 0, 0, 1);
    glTranslatef(.25, .475, 0);

    glPushName(0);
    glTranslatef(0, 0, -.03125*e->tz);
    glNormal3f(0, 1, 0);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f( .5, tcm); glVertex3f(   0, -.475, mz);
	glTexCoord2f(tcr, tct); glVertex3f(-.25, -.475, tz);
	glTexCoord2f( .5, tct); glVertex3f(   0, -.475, tz);
	glTexCoord2f(tcl, tct); glVertex3f( .25, -.475, tz);
	glTexCoord2f(tcl, tcm); glVertex3f( .25, -.475, mz);
	glTexCoord2f(tcl, tcb); glVertex3f( .25, -.475, bz);
	glTexCoord2f( .5, tcb); glVertex3f(   0, -.475, bz);
	glTexCoord2f(tcr, tcb); glVertex3f(-.25, -.475, bz);
	glTexCoord2f(tcr, tcm); glVertex3f(-.25, -.475, mz);
	glTexCoord2f(tcr, tct); glVertex3f(-.25, -.475, tz);
    glEnd();

    if(e->mat<0x100) // secret door
      {
	tcr = .75;
	tcl = .25;
      }

    glNormal3f(0, -1, 0);
    glBegin(GL_TRIANGLE_FAN);
        glTexCoord2f( .5, tcm); glVertex3f(   0, -.500, mz);
	glTexCoord2f(tcl, tct); glVertex3f( .25, -.500, tz);
	glTexCoord2f( .5, tct); glVertex3f(   0, -.500, tz);
	glTexCoord2f(tcr, tct); glVertex3f(-.25, -.500, tz);
	glTexCoord2f(tcr, tcm); glVertex3f(-.25, -.500, mz);
	glTexCoord2f(tcr, tcb); glVertex3f(-.25, -.500, bz);
	glTexCoord2f( .5, tcb); glVertex3f(   0, -.500, bz);
	glTexCoord2f(tcl, tcb); glVertex3f( .25, -.500, bz);
	glTexCoord2f(tcl, tcm); glVertex3f( .25, -.500, mz);
	glTexCoord2f(tcl, tct); glVertex3f( .25, -.500, tz);
    glEnd();

    glBegin(GL_QUADS);
        glNormal3f(1, 0, 0);
	glTexCoord2f(.04, tcb); glVertex3f( 0.250, -0.475, bz);
	glTexCoord2f(.04, tcm); glVertex3f( 0.250, -0.475, mz);
	glTexCoord2f(.08, tcm); glVertex3f( 0.250, -0.500, mz);
	glTexCoord2f(.08, tcb); glVertex3f( 0.250, -0.500, bz);
	glTexCoord2f(.04, tcm); glVertex3f( 0.250, -0.475, mz);
	glTexCoord2f(.04, tct); glVertex3f( 0.250, -0.475, tz);
	glTexCoord2f(.08, tct); glVertex3f( 0.250, -0.500, tz);
	glTexCoord2f(.08, tcm); glVertex3f( 0.250, -0.500, mz);

        glNormal3f(-1, 0, 0);
	glTexCoord2f(.96, tcb); glVertex3f(-0.250, -0.500, bz);
	glTexCoord2f(.96, tcm); glVertex3f(-0.250, -0.500, mz);
	glTexCoord2f(.92, tcm); glVertex3f(-0.250, -0.475, mz);
	glTexCoord2f(.92, tcb); glVertex3f(-0.250, -0.475, bz);
	glTexCoord2f(.96, tcm); glVertex3f(-0.250, -0.500, mz);
	glTexCoord2f(.96, tct); glVertex3f(-0.250, -0.500, tz);
	glTexCoord2f(.92, tct); glVertex3f(-0.250, -0.475, tz);
	glTexCoord2f(.92, tcm); glVertex3f(-0.250, -0.475, mz);

	glNormal3f(0, 0, -1);
	glTexCoord2f(.25, .75); glVertex3f( 0.250, -0.475, tz);
	glTexCoord2f(.50, .75); glVertex3f( 0,     -0.475, tz);
	glTexCoord2f(.50, .80); glVertex3f( 0,     -0.500, tz);
	glTexCoord2f(.25, .80); glVertex3f( 0.250, -0.500, tz);
	glTexCoord2f(.50, .75); glVertex3f( 0,     -0.475, tz);
	glTexCoord2f(.75, .75); glVertex3f(-0.250, -0.475, tz);
	glTexCoord2f(.75, .80); glVertex3f(-0.250, -0.500, tz);
	glTexCoord2f(.50, .80); glVertex3f( 0,     -0.500, tz);
    glEnd();

    glPopName();
    return false;
  }

ENT_RENDER_PRE_FUN(xrdoor)
  {
    glTranslatef(0, .4825, 0);
    return rdoor_render_pre(e);
  }

ENT_RENDER_PRE_FUN(frame)
  {
    int		tz = Game::map(e->tx, e->ty).calt;
    int		rz = e->tz;
    int		bz = rz - 26;

    glPushName(0);
    glTranslatef(0, (e->type&1)? 0: .4825, -.03125*e->tz);
    glNormal3f(0, 1, 0);
    ur_render_wall(-0.500, -0.475,  0.500, -0.475, tz, bz, tz, bz, tz, bz, e->mat, false);
    ur_render_wall(-0.500, -0.475,  0.500, -0.475, bz, rz, bz, rz, bz, rz, e->mat, true);
    glNormal3f(0, -1, 0);
    ur_render_wall( 0.500, -0.500, -0.500, -0.500, tz, bz, tz, bz, tz, bz, e->mat, false);
    ur_render_wall( 0.500, -0.500, -0.500, -0.500, bz, rz, bz, rz, bz, rz, e->mat, true);
    tex(e->mat);
    glBegin(GL_QUADS);
        glNormal3f(1, 0, 0);
	glTexCoord2f(0,     0);    glVertex3f(-0.250, -0.475, e->z);
	glTexCoord2f(0,    .5);    glVertex3f(-0.250, -0.475, e->z-0.5);
	glTexCoord2f(.025, .5);    glVertex3f(-0.250, -0.500, e->z-0.5);
	glTexCoord2f(.025,  0);    glVertex3f(-0.250, -0.500, e->z);
	glTexCoord2f(.025, .5);    glVertex3f(-0.250, -0.475, e->z-0.5);
	glTexCoord2f(.025, .8125); glVertex3f(-0.250, -0.475, e->z-0.8125);
	glTexCoord2f(.050, .8125); glVertex3f(-0.250, -0.500, e->z-0.8125);
	glTexCoord2f(.050, .5);    glVertex3f(-0.250, -0.500, e->z-0.5);

        glNormal3f(-1, 0, 0);
	glTexCoord2f(0,     0);    glVertex3f( 0.250, -0.500, e->z);
	glTexCoord2f(0,    .5);    glVertex3f( 0.250, -0.500, e->z-0.5);
	glTexCoord2f(.025, .5);    glVertex3f( 0.250, -0.475, e->z-0.5);
	glTexCoord2f(.025,  0);    glVertex3f( 0.250, -0.475, e->z);
	glTexCoord2f(.025, .5);    glVertex3f( 0.250, -0.500, e->z-0.5);
	glTexCoord2f(.025, .8125); glVertex3f( 0.250, -0.500, e->z-0.8125);
	glTexCoord2f(.050, .8125); glVertex3f( 0.250, -0.475, e->z-0.8125);
	glTexCoord2f(.050, .5);    glVertex3f( 0.250, -0.475, e->z-0.5);

	glNormal3f(0, 0, 1);
	glTexCoord2f(.25, .75);    glVertex3f(-0.250, -0.475, e->z-0.8125);
	glTexCoord2f(.50, .75);    glVertex3f( 0,     -0.475, e->z-0.8125);
	glTexCoord2f(.50, .80);    glVertex3f( 0,     -0.500, e->z-0.8125);
	glTexCoord2f(.25, .80);    glVertex3f(-0.250, -0.500, e->z-0.8125);
	glTexCoord2f(.50, .75);    glVertex3f( 0,     -0.475, e->z-0.8125);
	glTexCoord2f(.75, .75);    glVertex3f( 0.250, -0.475, e->z-0.8125);
	glTexCoord2f(.75, .80);    glVertex3f( 0.250, -0.500, e->z-0.8125);
	glTexCoord2f(.50, .80);    glVertex3f( 0,     -0.500, e->z-0.8125);
    glEnd();
    glPopName();
    return false;
  }

ENT_USE_FUN(switch)
  {
    //bool	on = o->type&8;
    int		ss = 24;

    switch(o->type&7)
      {
	case 0 ... 2:
	    ss = 26;
	case 3 ... 4:
	    ss = 25;
      }

    Sound::sound(ss, o->tx, o->ty, .5);
    o->type ^= 8;

    return true;
  }

