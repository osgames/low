////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "manip.H"
#include "entity.H"
#include "textures.H"
#include "granny.H"

DollPanel::DollPanel(void):
	ManipulatorPanel(new InventoryManipulator,
		0,
		.74, .75, .25, .70,
		4, 2,
		.02, .02)
  {
  }

// caucasian skin: .81 .56 .45
// black skin: .51 .22 .09
// blond hair: .76 .58 .09
// red hair: .26 .07 .00
// brown hair: .51 .29 .09
// auburn: .53 .32 .03

static Granny::RenderModel	rms[2][5][4] = {
      { // Males
	  {
	      { 100,	{ .81, .56, .45, 1.0 } },
	      { 305,	{ .41, .22, .09, 1.0 } },
	  },
	  {
	      { 100,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 100,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 100,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 100,	{ .85, .70, .20, 1.0 } },
	  },
      },
      { // Females
	  {
	      { 131,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 131,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 131,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 131,	{ .85, .70, .20, 1.0 } },
	  },
	  {
	      { 131,	{ .85, .70, .20, 1.0 } },
	  },
      },
};

static struct {
    int			type;
    int			slot;
    Granny::RenderModel	rm;
} rme[] = {
      { 144, 10, { 242, { 1.0, 1.0, 1.0 } } },
      { 145, 10, { 240, { 1.0, 1.0, 1.0 } } },
      { 148, 10, { 243, { 1.0, 1.0, 1.0 } } },
      { 149, 10, { 241, { 1.0, 1.0, 1.0 } } },
      { 152,  9, { 207, { 1.0, 1.0, 1.0 } } },
      { 153,  9, { 208, { 1.0, 1.0, 1.0 } } },
      { 154,  9, { 209, { 1.0, 1.0, 1.0 } } },
      { 155,  9, { 210, { 1.0, 1.0, 1.0 } } },
      {  60, 10, { 206, { 1.0, 1.0, 1.0 } } },
      {  61, 10, { 205, { 1.0, 1.0, 1.0 } } },
      {  62, 10, { 201, { 1.0, 1.0, 1.0 } } },
      {  63, 10, { 203, { 1.0, 1.0, 1.0 } } },
      {  59, 10, { 200, { 1.0, 1.0, 1.0 } } },
      {  26, 10, { 237, { 1.0, 1.0, 1.0 } } },
      {   0,  9, { 211, { 1.0, 1.0, 1.0 } } },
      {   1,  9, { 220, { 1.0, 1.0, 1.0 } } },
      {   2,  9, { 211, { 1.0, 1.0, 1.0 } } },
      {   3,  9, { 214, { 1.0, 1.0, 1.0 } } },
      {   4,  9, { 227, { 1.0, 1.0, 1.0 } } },
      {   5,  9, { 228, { 1.0, 1.0, 1.0 } } },
      {   6,  9, { 229, { 1.0, 1.0, 1.0 } } },
      {   7,  9, { 212, { 1.0, 1.0, 1.0 } } },
      {   8,  9, { 215, { 1.0, 1.0, 1.0 } } },
      {   9,  9, { 224, { 1.0, 1.0, 1.0 } } },
      {  10,  9, { 213, { 1.0, 1.0, 1.0 } } },
      {  11,  9, { 211, { 1.0, 1.0, 1.0 } } },
      {  12,  9, { 226, { 0.4, 0.4, 0.4 } } },
      {  13,  9, { 226, { 1.0, 1.0, 1.0 } } },
      {  14,  9, { 224, { 1.0, 1.0, 1.0 } } },
      {  32, 12, { 410, { .40, .20, .02 } } },
      {  32, 12, { 411, { .40, .20, .02 } } },
      {  33, 12, { 404, { 1.0, 1.0, 1.0 } } },
      {  34, 12, { 412, { 1.0, 1.0, 1.0 } } },
      {  34, 12, { 415, { 1.0, 1.0, 1.0 } } },
      {  35, 11, { 409, { .40, .20, .02 } } },
      {  36, 11, { 403, { 1.0, 1.0, 1.0 } } },
      {  37, 11, { 414, { 1.0, 1.0, 1.0 } } },
      {  38, 14, { 408, { .40, .20, .02 } } },
      {  39, 14, { 416, { 1.0, 1.0, 1.0 } } },
      {  40, 14, { 413, { 1.0, 1.0, 1.0 } } },
      {  41, 13, { 417, { .40, .20, .02 } } },
      {  42, 13, { 417, { .95, .95, 1.0 } } },
      {  43, 13, { 417, { .95, .95, 1.0 } } },
      {  44, 15, { 407, { .40, .20, .02 } } },
      {  45, 15, { 402, { 1.0, 1.0, 1.0 } } },
      {  46, 15, { 405, { 1.0, 1.0, 1.0 } } },
      {  47, 13, { 418, { .10, .50, .20 } } },
      {  48, 15, { 413, { .90, .60, .10 } } },
      {  48, 15, { 422, { .90, .80, .10 } } },
      {  49, 15, { 422, { .90, .80, .10 } } },
      {  50, 14, { 422, { .90, .80, .10 } } },
      {  52, 15, { 422, { .90, .60, .10 } } },
      {  -1 },
};

static Granny::RenderModels doll;
static int dtick = 0;

static struct slot_show_t {
    float	x;
    float	y;
    float	px1, py1, px2, py2;
    bool	show;
} slt[24] = {
      { 0, 0 }, // not really shown
      { 0.02, 0.08, },
      { 0.07, 0.08, },
      { 0.12, 0.08, },
      { 0.17, 0.08, },
      { 0.02, 0.14, },
      { 0.07, 0.14, },
      { 0.12, 0.14, },
      { 0.17, 0.14, },
      { 0.02, 0.36, .05, .40, .09, .46, }, // right hand
      { 0.20, 0.36, .16, .40, .21, .46, }, // left hand
      { 0.11, 0.35, .09, .30, .16, .46, }, // leggings
      { 0.11, 0.47, .06, .46, .19, .57, }, // tunic
      { 0.11, 0.25, .09, .24, .16, .30, }, // boots
      { 0.02, 0.56,   0,   0,   0,   0, }, // gloves
      { 0.11, 0.58, .10, .57, .15, .63, }, // head
      { 0.02, 0.30, }, // right
      { 0.20, 0.30, }, // left
      { 0.19, 0.61, }, // back
};

void DollPanel::render(bool select)
  {
    rgump(select);

    for(int i=1; i<24; i++)
	slt[i].show = (i<9 || i>15 || Game::player.inv[i]);

    doll.num_rm = 0;
    if(Game::setup.paperdoll_model==100)
      {
	for(int i=0; i<4; i++)
	    if(rms[Game::player.stats.female][0][i].id)
		doll.rm[doll.num_rm++] = rms[Game::player.stats.female][0][i];
	for(int i=9; i<16; i++)
	    if(Game::player.inv[i])
		for(int m=0; rme[m].type>=0; m++)
		    if(rme[m].type==Game::player.inv[i]->type && i==rme[m].slot)
		      {
			doll.rm[doll.num_rm] = rme[m].rm;
			doll.rm[doll.num_rm].color[3] = 1.0;
			doll.rm[doll.num_rm++].left = i==10;
			slt[rme[m].slot].show = false;
		      }
      }
    else
      {
	doll.rm[doll.num_rm].id = Game::setup.paperdoll_model;
	doll.rm[doll.num_rm].color[0] =
	doll.rm[doll.num_rm].color[1] =
	doll.rm[doll.num_rm].color[2] =
	doll.rm[doll.num_rm++].color[3] = 1.0;
      }
    doll.scale = .25;
    doll.ani_loop = true;
    doll.ani_seq = Game::setup.paperdoll_anim;

    if(y<.8)
      {
	glPushMatrix();
	glTranslatef(.13, .25, .5);
	static float uuu=.01;
	uuu += .01;
	if(uuu>M_PI)
	    uuu = -M_PI;
	glRotatef(180+15*sin(uuu), 0, 1, 0);
	glRotatef(90, 1, 0, 0);
	glScalef(-.8, .8, .9);
	if(dtick%3)
	    dtick++;
	else
	  {
	    doll.tick();
	    dtick = 1;
	  }
	doll.render();
	glPopMatrix();
      }

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_CULL_FACE);
    for(int i=1; i<19; i++)
	if(slt[i].show)
	    rslot(slt[i].x, slt[i].y, i);
    glEnable(GL_CULL_FACE);

    glPopName();
    end_render();
  }

int DollPanel::slot(float mx, float my)
  {
    mx -= x;
    my -= y;
    for(int i=1; i<19; i++)
      {
	if(slt[i].show && mx>=slt[i].x && my>=slt[i].y && mx<(slt[i].x+BOX_XSIZE) && my<(slt[i].y+BOX_YSIZE))
	    return i;
      }
    for(int i=9; i<16; i++)
      {
	if(mx>=slt[i].px1 && my>=slt[i].py1 && mx<(slt[i].px2) && my<(slt[i].py2))
	  {
	    if((i==9 || i==10) && !slt[i].show && !Game::player.inv[i] && Game::player.inv[14])
		i = 14;
	    return i;
	  }
      }

    return -1;
  }

static int wslot(EIndex e)
  {
    if(e->type>52 && e->type<59)
	return 16;
    for(int m=0; rme[m].type>=0; m++)
	if(e->type == rme[m].type)
	    return rme[m].slot;
    return 0;
  }

bool InventoryManipulator::add(EIndex e)
  {
    if(int ws=wslot(e))
	if(!Game::player.inv[ws])
	  {
	    e->to_held(ws);
	    return true;
	  }

    for(int i=1; i<9; i++)
	if(!Game::player.inv[i])
	  {
	    e->to_held(i);
	    return true;
	  }

    return false;
  }

EIndex InventoryManipulator::swap(int slot, EIndex e)
  {
    if(slot>8 && slot<18)
      {
	int	ws = wslot(e);
	if(ws==14 && (slot==9 || slot==10))
	    slot = 14;
	if(ws==16 && slot==17)
	    ws = 17;
	else if(slot==14 && ws!=14)
	    slot = (ws==10)? 10: 9;
	if(slot!=9 && slot!=10 && ws!=slot)
	    return e;
      }

    if(EIndex b = Game::player.inv[slot])
      {
	b->unlist();
	e->to_held(slot);
	return b;
      }
    e->to_held(slot);
    return 0;
  }


