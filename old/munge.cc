////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

struct UWEntity // as parsed; not yet interpreted
  {
    int		id;
    int		flags;
    int		isqty;
    int		z;
    int		face;
    int		x;
    int		y;
    int		quality;
    int		next;
    int		unknown;
    int		link;
    int		mat;

    struct {
	bool		yes;
	int		level;
	int		x;
	int		y;
    }		world, dest;

    unsigned char minfo[19];
  };

enum Where {
    Limbo, Tile, Inside
};

struct LowEntity
  {
    int		id;
    int		qty;
    int		belongs;
    int		species;	// if mobile
    int		quality;	// ... of unknown use yet ...
    int		special;
    int		mat;		// material when applicable
    int		bits;		// original data flags

    bool	enchanted;
    bool	charged;

    int		spell;
    int		charges;

    Where	where;
    int		contents;
    int		link;

    int		next;

    // where == Inside
    int		container;

    // where == Tile
    int		facing;
    int		level;
    float	x;
    float	y;
    float	z;
  };

struct UWERec
  {
    int		ord;
    bool	dangles;
    UWEntity*	ent;
    LowEntity*	low;
  };

UWERec	er[16384];
int	ner = 0;

static int genlow(void)
  {
    for(int i=0; i<ner; i++)
	if(er[i].dangles)
	  {
	    if(er[i].ent)
	      {
		delete er[i].ent;
		er[i].ent = 0;
	      }
	    er[i].dangles = false;
	    return i;
	  }
    er[ner].low = new LowEntity;
    er[ner].dangles = false;
    return ner++;
  }

int seq(int ord)
  {
    if(ord == 0)
	return -1;
    // slow and simple
    for(int i=0; i<ner; i++)
	if(er[i].ord == ord)
	    return i;
    printf("eiii! evil ordinal %x\n", ord);
    return -1;
  }

static void skip(const char*& s)
  {
    while(*s > ' ')
	++s;
    while(*s==' ' || *s=='\t')
	++s;
  }

static int geti(const char*& s, int base=16)
  {
    int	i = strtol(s, 0, base);
    skip(s);
    return i;
  }


int main(int, char**)
  {
    FILE*	raw = fopen("save/entities.raw", "r");
    FILE*	ent = fopen("save/entities.dat", "w");
    char	line[1024];

    while(fgets(line, 1024, raw))
      {
	const char*	s = line;

	UWEntity*	e = new UWEntity;

	er[ner].ent = e;
	er[ner++].ord = geti(s);

	e->id = geti(s);
	e->mat = geti(s);
	e->flags = geti(s);
	e->isqty = geti(s);
	e->z = geti(s, 10);
	e->face = geti(s, 10);
	e->x = geti(s, 10);
	e->y = geti(s, 10);
	e->quality = geti(s);
	e->next = geti(s);
	e->unknown = geti(s);
	e->link = geti(s);

	while(*s>' ') switch(*s)
	  {
	    case 'w':
		skip(s);
		e->world.yes = true;
		e->world.level = geti(s, 10);
		e->world.x = geti(s, 10);
		e->world.y = geti(s, 10);
		skip(s);
		break;
	    case 'd':
		skip(s);
		e->dest.yes = true;
		e->dest.level = geti(s, 10);
		e->dest.x = geti(s, 10);
		e->dest.y = geti(s, 10);
		skip(s);
		break;
	    case 'm':
		skip(s);
		for(int i=0; i<19; i++)
		    e->minfo[i] = geti(s);
		skip(s);
		break;
	  }
      }
    fclose(raw);
    printf("read %d entities total\n", ner);

    for(int i=0; i<ner; i++)
      {
	UWEntity*	e = er[i].ent;
	LowEntity*	l = new LowEntity;

	memset(l, 0, sizeof(LowEntity));
	er[i].low = l;
	er[i].dangles = false;

	l->id = e->id;
	l->qty = 1;
	l->quality = e->quality;
	l->bits = e->flags;
	l->special = 0;
	if(l->id==0x192)
	    l->special = e->mat;

	l->next = seq(e->next);
	if(!e->isqty || (e->id>=0x180 && e->id<=0x1BF))
	    l->link = seq(e->link);
      }

    for(int i=0; i<ner; i++)
      {
	UWEntity*	e = er[i].ent;

	if(e->world.yes)
	  {
	    float	wx = float(e->world.x);
	    float	wy = float(e->world.y);

	    for(int j=i; j>-1; j=seq(er[j].ent->next))
	      {
		LowEntity*	l = er[j].low;

		if(l->where != Limbo)
		  {
		    printf("Oh no!  Ordinal %x is in more than one place at the same time!\n", er[j].ord);
		    break;
		  }
		l->where = Tile;
		l->facing = er[j].ent->face;
		l->level = e->world.level;
		l->x = wx + er[j].ent->y*.125 - .4375;
		l->y = wy + (7-er[j].ent->x)*.125 - .4375;
		l->z = (128-er[j].ent->z) * 0.03125;
		er[j].dangles = false;
	      }
	  }

	switch(e->id)
	  {
	    case 0x040 ... 0x08E:
	    case 0x098 ... 0x09F:
	    case 0x0AA:
	    case 0x15B:
	    case 0x15D:
	    case 0x15E:
		// all of those might have 'contents'
		if(!e->isqty)
		  {
		    for(int j=seq(er[i].ent->link); j>-1; j=seq(er[j].ent->next))
		      {
			er[j].dangles = false;
			LowEntity*	l = er[j].low;

			if(l->where != Limbo)
			  {
			    printf("Oh no!  Ordinal %x is in more than one place at the same time!\n", er[j].ord);
			    break;
			  }
			l->where = Inside;
			l->container = i;
		      }
		    er[i].dangles = false;
		  }
		break;
	    default:
		if(e->unknown)
		  {
		    // XXX: which objects are 'normal' objects and this have belong_to?
		    if(e->id>257 && e->id<271)
			er[i].low->special = e->unknown;
		    else
			er[i].low->belongs = e->unknown + 0x3F;
		  }
		if(e->isqty)
		  {
		    // okay, the cruddy part.
		    if(e->link & 512)
		      {
			if(e->flags&8) // enchantment
			  {
			    er[i].low->enchanted = true;
			    int	s = e->link & 255;

			    if(e->id<0x20)
			      {
				if(s>191)
				    s += 256;
			      }
			    else if(e->id<0x040)
			      {
				if(s>191)
				    s += 288;
			      }
			    else
			      {
				if(s<64)
				    s += 256;
				else
				    s += 144;
			      }

			    er[i].low->spell = s;
			  }
			else
			    er[i].low->special = e->link & 511;
		      }
		    else
			er[i].low->qty = e->link & 255;
		  }
		else for(int j=seq(er[i].ent->link); j>-1; j=seq(er[j].ent->next))
		      {
			er[j].dangles = false;
			LowEntity*	l = er[j].low;

			// some of those may be forcibly 'contained'

			switch(l->id)
			  {
			    case 0x1A1 ... 0x1A3:
			    case 0x1B1 ... 0x1B3:
			    case 0x188: // a_door trap
			    case 0x10F: // a_lock
				if(l->where != Limbo)
				  {
				    printf("Oh no!  Ordinal %x is in more than one place at the same time!\n", er[j].ord);
				    break;
				  }
				l->where = Inside;
				l->container = i;
				break;
			  }
		      }
		break;
	    case 0x180 ... 0x1bf:
		for(int j=seq(er[i].ent->link); j>-1; j=seq(er[j].ent->next))
		    er[j].dangles = false;
		break;
	  }
      }

    // fold spells back into their object
    for(int i=0; i<ner; i++)
	if(!er[i].dangles && er[i].low->id==0x120)
	  {
	    er[i].dangles = true;
	    if(!er[i].low->container)
		continue;
	    LowEntity*	l = er[er[i].low->container].low;
	    if(l->enchanted || l->charged)
	      {
		printf("enchanted /and/ a_spell?\n");
		continue;
	      }
	    l->enchanted = l->charged = true;
	    l->spell = er[i].low->spell;
	    if(l->spell<64)
		l->spell += 256;
	    else
		l->spell += 144;
	    l->charges = er[i].low->quality;
	  }

    // make object conversions as appropriate
    for(int i=0; i<ner; i++)
	if(!er[i].dangles)
	  {
	    UWEntity*	e = er[i].ent;
	    LowEntity*	l = er[i].low;

	    if(!e)
		continue;

	    switch(l->id)
	      {
		case 0x040 ... 0x07F:	// mobiles
		    if(e->minfo[18])
			l->species = e->minfo[18] + 0x41;
		    else
			l->species = e->id - 0x3F;
		    l->special = 3-(e->minfo[6]>>6);
		    l->facing = (e->face+4) & 7;
		    break;
		case 0x140 ... 0x14F:
		      {
			int		frame = genlow();
			LowEntity*	f = er[frame].low;

			l->id = (e->x<3 || e->y<3 || e->x>4 || e->y>4)? 321: 320;
			l->x = floor(l->x+.5);
			l->y = floor(l->y+.5);
			l->facing = (e->face+4) & 7;
			if((e->id&7) == 7)
			    l->mat = e->mat;
			else
			    l->mat = 0x100 + (e->id&7);
			if(e->id&8)
			    l->z += .75;
			*f = *l;
			l->link = frame;
			f->mat = e->mat;
			f->id += 2;
		      }
		    break;
		case 0x16E:
		case 0x164:
		    l->x = floor(l->x+.5);
		    l->y = floor(l->y+.5);
		case 0x160:
		    l->mat = e->mat;
		    break;
		case 0x163:
		    l->bits = 0;
		case 0x166:
		case 0x161 ... 0x162:
		case 0x16F ... 0x17F:
		    e->mat = 0x140+(e->flags%6);
		    if(l->facing&2)
			l->x = floor(l->x+.5);
		    else
			l->y = floor(l->y+.5);
		    l->mat = e->mat;
		    break;
		case 0x180 ... 0x19f:
		    l->special = e->z;
		    break;
		default:
		    break;
	      }
	  }

    for(int i=0; i<ner; i++)
	if(!er[i].dangles)
	  {
	    if(er[i].ent && er[i].ent->id==0x1FF)
		continue;
	    LowEntity*	l = er[i].low;

	    if(l->species)
	      {
		// do mobile code
		fprintf(ent, "%06d *%02x %d", i, l->species-1, l->special);
	      }
	    else
	      {
		fprintf(ent, "%06d %03x", i, l->id);
		if(l->qty>1)
		    fprintf(ent, " q%d", l->qty);
		if(l->belongs)
		    fprintf(ent, " b%x", l->belongs);
		if(l->enchanted)
		    fprintf(ent, " s%d", l->spell);
		if(l->charged)
		    fprintf(ent, " c%d", l->charges);
		if(l->mat)
		    fprintf(ent, " m%d", l->mat);
		if(l->special)
		    fprintf(ent, " x%d", l->special);
	      }
	    if(l->quality != 40)
		fprintf(ent, " Q%d", l->quality);
	    if(l->bits)
		fprintf(ent, " f%x", l->bits);
	    if(er[i].ent && er[i].ent->dest.y)
	      {
		fprintf(ent, " d{ %d %d %d %d }",
			er[i].ent->dest.level, er[i].ent->dest.x, er[i].ent->dest.y, 0);
	      }
	    if(l->link > 0)
		fprintf(ent, " l%d", l->link);
	    
	    switch(l->where)
	      {
		case Tile:
		    fprintf(ent, " w{ %d %g %g %g %d }", l->level, l->x, l->y, l->z, (4+l->facing)&7);
		    break;
		case Inside:
		    fprintf(ent, " i%d", l->container);
		    break;
		// no need for " h%d" held
	      };

	    fprintf(ent, "\n");
	  }
    fclose(ent);
  }

