////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "fileio.H"
#include "map.H"
#include "entity.H"
#include "light.H"

bool Level::save(const char* name)
  {
    char	fname[64];
    File	lfile;

    snprintf(fname, 64, "%s.lev", name);
    if(!(lfile = openlow(fname, false, true)))
	throw "Unable to save map file.";
    lfile.print("%d %d %d\n", level, w, h);
    for(int y=0; y<h; y++)
	for(int x=0; x<w; x++)
	    if(tile(x, y).mat)
	      {
		Tile&	t = tile(x, y);

		lfile.print("t%d,%d %d %d %d", x, y, t.mat, t.ciel, t.calt);
		bool		flat = true;
		unsigned char	alt = t.alt[0];
		for(int i=1; i<9; i++)
		    if(t.alt[i] != alt)
		      {
			flat = false;
			break;
		      }
		if(flat)
		    lfile.print(" %d", alt);
		else
		  {
		    lfile.print(" {");
		    for(int i=0; i<9; i++)
			lfile.print(" %d", t.alt[i]);
		    lfile.print(" }");
		  }
		lfile.print(" w{ %d %d %d }", t.wall[0].f2l, t.wall[0].c2c, t.wall[0].l2f);
		if(t.liquid)
		    lfile.print(" q%d %d %d %d", t.liquid, t.lq_lev, t.lq_slope, t.lq_zdiff);
		for(int i=0; i<8; i++)
		    if(t.wall[i].flags || t.wall[0].f2l!=t.wall[i].f2l
			    || t.wall[0].c2c!=t.wall[i].c2c || t.wall[0].l2f!=t.wall[i].l2f)
		      {
			lfile.print(" w%d{ %d %d", i, t.wall[i].flags, t.wall[i].f2l);
			if(t.wall[0].c2c!=t.wall[i].c2c || t.wall[0].l2f!=t.wall[i].l2f)
			    lfile.print(" %d %d", t.wall[i].c2c, t.wall[i].l2f);
			lfile.print(" }");
		      }
		if(t.light)
		    lfile.print(" l%ld", t.light);
		lfile.print("\n");
	      }
    for(Ambient* a=ambients; a; a=a->next)
      {
	lfile.print("a %d %f %d", a->num, a->gain, a->type);
	switch(a->type)
	  {
	    case 0:	// point sound
		lfile.print(" %g %g %g\n", a->coord[0], a->coord[1], a->coord[2]);
		break;
	    case 1: // ambient sound
		lfile.print("\n");
		break;
	    case 3: // sporadic sound
		lfile.print(" %d %d", a->ival[0], a->ival[1]);
		// no break
	    case 2: // area sound
		lfile.print(" %g %g %g %g %g %g\n",
			a->coord[0], a->coord[1], a->coord[2],
			a->coord[3], a->coord[4], a->coord[5]);
		break;
	  }
      }
    for(Light* l=lights; l; l=l->next)
      {
	lfile.print("l%ld %g %g %d %d %d %d %d %d",
		l->zones, l->x, l->y, int(32*l->z),
		int(255*l->color[0]), int(255*l->color[1]), int(255*l->color[2]), int(100*l->attenuate),
		l->kind);
	if(l->cutoff<180)
	    lfile.print(" s%d %d %d %d", int(l->cutoff), int(100*l->spot[0]), int(100*l->spot[1]), int(100*l->spot[2]));
	lfile.print("\n");
      }

    lfile.close();

    return false; // okay
  }

static void skip(const char*& s)
  {
    while(*s > ' ')
	++s;
    while(*s==' ' || *s=='\t')
	++s;
  }

static int geti(const char*& s, int base=10)
  {
    int	i = strtol(s, 0, base);
    skip(s);
    return i;
  }

static float getf(const char*& s)
  {
    float f = strtod(s, 0);
    skip(s);
    return f;
  }

bool Level::load_(const char* name)
  {
    char	fname[64];
    File	lfile;

    snprintf(fname, 64, "levels/%s.lev", name);
    if(!(lfile = openlow(fname)))
	throw "Unable to load map file that is supposed to exist!";

    char	pad[1024];
    int		a, b, c;	// temp values

    if(lfile.scan("%d %d %d\n", &a, &b, &c)!=3)
      {
	lfile.close();
	return true;
      }

    if(tiles)
	delete[] tiles;
    if(lights)
	delete lights;
    if(ambients)
	delete ambients;

    level = a;
    w = b;
    h = c;

    lights = 0;
    ambients = 0;
    tiles = new Tile[w*h];
    memset(tiles, 0, w*h*sizeof(Tile));

    while(lfile.readln(pad, 1023)) switch(pad[0])
      {
	case 'a':	// ambient sound
	      {
		const char*	s = pad;
		Ambient*	a = new Ambient;

		skip(s);
		a->num = geti(s);
		a->gain = getf(s);
		switch(a->type = geti(s))
		  {
		    case 0:
			a->coord[0] = getf(s);
			a->coord[1] = getf(s);
			a->coord[2] = getf(s);
			break;
		    case 1:
			break;
		    case 3:
			a->ival[0] = geti(s);
			a->ival[1] = geti(s);
			// no break
		    case 2:
			a->coord[0] = getf(s);
			a->coord[1] = getf(s);
			a->coord[2] = getf(s);
			a->coord[3] = getf(s);
			a->coord[4] = getf(s);
			a->coord[5] = getf(s);
			break;
		  }

		a->next = ambients;
		ambients = a;
	      }
	    break;
	case 'l':	// light
	      {
		const char*	s = pad+1;
		Light*		l = new Light;

		l->active = true;
		l->next = lights;
		lights = l;
		l->zones = 0;
		l->x = getf(s);
		l->y = getf(s);
		l->cx = l->x;
		l->cy = l->y;
		l->z = 0.03125 * geti(s);
		l->color[0] = getf(s);
		l->color[1] = getf(s);
		l->color[2] = getf(s);
		l->color[3] = 1.0;
		l->range = geti(s);
		l->when = 0;
		l->cutoff = 180;
		l->ent = 0;
		l->zone(*this);
		switch(l->kind = geti(s))
		  {
		    case 1:
		    case 2:
			l->scratch = l->color[1];
			break;
		  }
		if(*s=='s')
		  {
		    ++s;
		    l->cutoff = float(geti(s));
		    l->spot[0] = .01*geti(s);
		    l->spot[1] = .01*geti(s);
		    l->spot[2] = .01*geti(s);
		  }
	      }
	    break;
	case 't':	// tile
	    if(sscanf(pad, "t%d,%d ", &a, &b)==2)
	      {
		Tile&		t = tile(a, b);
		const char*	s = pad;

		skip(s);
		t.mat = geti(s);
		t.ciel = geti(s);
		t.calt = geti(s);
		t.light = 0;

		if(*s == '{')
		  {
		    skip(s);
		    for(int i=0; i<9; i++)
			t.alt[i] = geti(s);
		    skip(s);
		  }
		else
		  {
		    int	a = geti(s);
		    for(int i=0; i<9; i++)
			t.alt[i] = a;
		  }

		while(*s>=' ') switch(*s++)
		  {
		    case 'q':
			t.liquid = geti(s);
			t.lq_lev = geti(s);
			t.lq_slope = geti(s);
			t.lq_zdiff = geti(s);
			break;
		    case 'l':
			t.light |= geti(s);
			break;
		    case 'w':
			if(*s == '{')
			  {
			    skip(s);
			    t.wall[0].flags = 0;
			    t.wall[0].f2l = geti(s);
			    t.wall[0].c2c = geti(s);
			    t.wall[0].l2f = geti(s);
			    skip(s);
			    for(int i=1; i<8; i++)
				t.wall[i] = t.wall[0];
			  }
			else
			  {
			    a = geti(s);
			    t.wall[a].flags = geti(s);
			    t.wall[a].f2l = geti(s);
			    if(*s != '}')
			      {
				t.wall[a].c2c = geti(s);
				t.wall[a].l2f = geti(s);
			      }
			    skip(s);
			  }
			break;
		    default:
			skip(s);
			break;
		  }
	      }
	    break;
      }
    lfile.close();

    return false;
  }

