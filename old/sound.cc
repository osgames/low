////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "fileio.H"
#include "sound.H"
#include "game.H"
#include "panel.H"
#include "console.H"

#if HAVE_LIBSDL_MIXER

#include <SDL/SDL_mixer.h>

// XXX: Make this configurable
#define CHANS 16

namespace Sound {

    static Mix_Chunk*		sound_chunk[100];
    static Sound*		playing[CHANS];

#if HAVE_LIBVORBIS
    static char			music_name[32][256];
    static bool			music_keep[32];
    static Mix_Music*		music_mm[32];
    static int			volume_music = -1;
#endif

    static int			volume_sound = -1;

    Sound*	Sound::first = 0;
    Sound*	Sound::last = 0;

    Sound::Sound(void)
      {
	if((next = first))
	    next->prev = this;
	else
	    last = this;
	prev = 0;
	first = this;

	playing_on = -1;
	doomed = false;
	sample = 0;
      }

    Sound::~Sound()
      {
	(next? next->prev: last) = prev;
	(prev? prev->next: first) = next;
      }

    static void channel_finished(int chan)
      {
	Sound*	s = playing[chan];

	playing[chan] = 0;
	s->playing_on = -1;
	if(s->kind==Event)
	    delete s;
      }

#if HAVE_LIBVORBIS
    static int	mus_playing = 0;
    static int	mus_next = 0;

    static void music_start(int mus)
      {
	if(!music_mm[mus] && music_name[mus][0])
	    music_mm[mus] = Mix_LoadMUS(music_name[mus]);
	if(music_mm[mus])
	  {
	    mus_playing = mus;
	    Mix_VolumeMusic(volume_music);
	    Mix_PlayMusic(music_mm[mus], 0);
	  }
	else
	    mus_playing = 0;
      }

    int	mus_pl[3];

    static void	music_finished(void)
      {
	if(!music_keep[mus_playing])
	  {
	    Mix_FreeMusic(music_mm[mus_playing]);
	    music_mm[mus_playing] = 0;
	  }
	if(!mus_next)
	  {
	    if(mus_playing==mus_pl[0] && mus_pl[1])
		mus_next = mus_pl[1];
	    else if(mus_playing==mus_pl[1] && mus_pl[2])
		mus_next = mus_pl[2];
	    else
		mus_next = mus_pl[0];
	  }
	mus_playing = 0;
	if(mus_next)
	  {
	    music_start(mus_next);
	    mus_next = 0;
	  }
      }

#endif // HAVE_LIBVORBIS

    extern void playlist(int m1, int m2, int m3)
      {
#if HAVE_LIBVORBIS
	mus_pl[0] = m1;
	mus_pl[1] = m2;
	mus_pl[2] = m3;

	if(mus_playing && (mus_playing==m1))
	    return; // music that is playing is the "primary" music

	if(!mus_playing)
	  {
	    music_start(m1);
	    return;
	  }

	Mix_FadeOutMusic(1500);
	mus_next = 0;
#endif // HAVE_LIBVORBIS
      }

    static int volume(int vol)
      {
	return 128*vol*vol/10000;
      }

    extern void initialize(void)
      {
	Mix_AllocateChannels(CHANS);
	Mix_ChannelFinished(channel_finished);
	volume_sound = volume(Game::setup.sound_volume);

#if HAVE_LIBVORBIS
	Mix_HookMusicFinished(music_finished);
	volume_music = volume(Game::setup.music_volume);
#endif

	char        name[64];
	int         i;
	int         num = 0;
	File        fp = openlow("sounds.dat");

	if(fp)
	  {
	    while(fp.scan("%d %s\n", &i, name)==2)
	      {
		char    fname[256];

		snprintf(fname, 256, "%s/data/sounds/%s.wav", Game::setup.low_path?:".", name);
		sound_chunk[i] = Mix_LoadWAV(fname);
		if(!sound_chunk[i])
		  {
		    cprintf(Warning, "audio: unable to load sound %s: %s", name, strerror(errno));
		    continue;
		  }
		num++;
	      }
	    fp.close();

	    cprintf(Info, "audio: precached %d sounds", num);
	  }

#if HAVE_LIBVORBIS
	fp = openlow("music.dat");

	if(fp)
	  {
	    int		i;
	    char	pad[32];

	    while(fp.scan("%d %s\n", &i, pad)==2)
	      {
		music_keep[i] = (pad[0]=='+');
		sprintf(music_name[i], "%s/data/music/%s.mp3", Game::setup.low_path?:".", pad + (music_keep[i]? 1: 0));
	      }
	    fp.close();
	  }
#endif
      }
    
    // we fake things here in order to simplify handling of very distant
    // sources; taking loudness into account; distances between 2 .. 7
    // are mapped to 0 .. 255, distances between 7 .. 7.5 are mapped to
    // 255 if the sound is already playing, 256 otherwise and distances
    // over 5.5 are mapped to 256.  The tick code handles 256 to mean
    // that the source should not be played at all, this gives hysteresis
    // to distance clipping.

    int Sound::distance(void)
      {
	if(space==Ambient)
	    return 0;
	if(space==Area)
	  {
	    x = (Game::player.p_x >? minx) <? maxx;
	    y = (Game::player.p_y >? miny) <? maxy;
	  }
	float	dist = hypot(Game::player.p_x-x, Game::player.p_y-y)/loudness;

	if(dist>7.5)
	    return 256;
	if(dist>7.0)
	    return (playing_on<0)? 255: 256;
	if(dist<2.0)
	    return 0;
	return int(51*(dist-2.0));
      }

    extern Sound* sound(int snd, float x, float y, float z, float loud, Kind k)
      {
	if(!sound_chunk[snd])
	    return 0;

	Sound*		s = new Sound;

	s->sample = snd;
	s->kind = k;
	s->space = Point;
	s->loudness = loud;

	s->x = x;
	s->y = y;
	s->z = z;

	if(s->kind==Event && s->distance()>255)
	  {
	    delete s;	// that was quick.  :-)
	    return 0;
	  }

	return s;
      }

    extern void flush(void)
      {
	for(Sound* s = Sound::first; s; )
	  {
	    Sound*	next = s->next;
	    if(s->playing_on<0)
		delete s;
	    else
	      {
		s->doomed = true;
		s->kind = Event; // force selfdestruct
		Mix_FadeOutChannel(s->playing_on, 250);
	      }
	    s = next;
	  }
      }

    struct soundstat_ {
	Sound*		sound;
	int		dist;
	bool		playing;
    };

    static int ss_compare(const void* s1_, const void* s2_)
      {
	const soundstat_*	s1 = (const soundstat_*)s1_;
	const soundstat_*	s2 = (const soundstat_*)s2_;

	if(s1->dist==s2->dist)
	    return s1->playing? -1: s2->playing;
	return s2->dist - s1->dist;
      }

    extern void tick(void)
      {
	// every tick we decide which sounds we will play by sorting them
	// by (effective) distance, giving precedence to sounds which are
	// already playing.  We play the 'chan' closest sounds, stopping
	// the others as appropriate.  For most sounds, being stopped by
	// moving out of range is a death sentence.

	volume_sound	= volume(Game::setup.sound_volume);

	static soundstat_*	ss = 0;
	static int		nss = 0;
	int			ns = 0;

#if HAVE_LIBVORBIS
	int nvm		= volume(Game::setup.music_volume);
	if(nvm != volume_music)
	  {
	    volume_music = nvm;
	    Mix_VolumeMusic(nvm);
	  }
#endif

	// first, we collect and count the candidate sounds
	for(Sound* s=Sound::first; s; s=s->next)
	  {
	    if(s->doomed)	// doomed sounds are not candidates
		continue;

	    if(ns>=nss)
	      {
		if(ss)
		  {
		    soundstat_*	ssnew = new soundstat_[nss+=32];
		    memcpy(ssnew, ss, ns*sizeof(soundstat_));
		    delete[] ss;
		    ss = ssnew;
		  }
		else
		    ss = new soundstat_[nss=32];
	      }
	    ss[ns].sound = s;
	    ss[ns].dist=s->distance();
	    ss[ns++].playing = s->playing_on > -1;
	  }
	qsort(ss, ns, sizeof(soundstat_), ss_compare);

	// then we stop any sound which is marked as too far to play
	// either implicitely (because of index in the now-sorted array)
	// or explicitely (because the distance is >255)
	for(int i=0; i<ns; i++)
	    if(ss[i].playing && (i>CHANS || ss[i].dist>255))
	      {
		Mix_HaltChannel(ss[i].sound->playing_on);
		if(ss[i].sound->kind==Event)
		    ss[i].sound->doomed = true;
	      }

	// we now process every sound we want played, possibly starting
	// them if they weren't already being played

	int	nc = 0;	// next potential channel

	for(int i=0; i<CHANS && i<ns; i++)
	  {
	    Sound*	s = ss[i].sound;
	    if(!s->doomed && ss[i].dist<256)
	      {
		if(!ss[i].playing)
		  {
		    while(nc<CHANS && playing[nc])
			++nc;
		    if(nc>=CHANS) // oops.  We ran out of channels
			break;
		    Mix_PlayChannel(nc, sound_chunk[s->sample], (s->kind==Continuous)? -1: 0);
		    playing[nc] = s;
		    s->playing_on = nc++;
		    if(s->space==Random)
		      {
			s->x = s->minx + (s->maxx-s->minx)*drand48();
			s->y = s->miny + (s->maxy-s->miny)*drand48();
		      }
		  }
		Mix_Volume(s->playing_on, volume_sound);
		int	angle = 360-int(atan2(s->x-Game::player.p_x, Game::player.p_y-s->y)*180/M_PI - Game::player.p_h);
		while(angle<0)
		    angle += 360;
		Mix_SetPosition(s->playing_on, angle, ss[i].dist);
	      }
	  }
      }

}; // namespace Sound


#else // no LIBSDL_MIXER

namespace Sound {

    Sound::Sound(void)
      {
      }

    Sound::~Sound()
      {
      }

    extern void playlist(int, int, int)
      {
      }

    extern void initialize(void)
      {
      }
    
    extern Sound* sound(int, float, float, float, float, Kind)
      {
	return 0;
      }

    extern void flush(void)
      {
      }

    extern void tick(void)
      {
      }

}; // namespace Sound


#endif // no LIBSDL_MIXER

