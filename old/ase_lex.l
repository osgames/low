/*
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
  */


%{
#include "ase_parse.h"
%}

%option noyywrap
%option nounput

%%

\*3DSMAX_ASCIIEXPORT		{ return MAGIC; }
\*GEOMOBJECT[ \t\n]		{ return GEOMOBJECT; }
\*MESH[ \t\n]			{ return MESH; }
\*MESH_NUMVERTEX[ \t\n]		{ return MESH_NUMVERTEX; }
\*MESH_NUMFACES[ \t\n]		{ return MESH_NUMFACES; }
\*MESH_VERTEX_LIST[ \t\n]	{ return MESH_VERTEX_LIST; }
\*MESH_VERTEX[ \t\n]		{ return MESH_VERTEX; }
\*MESH_FACE_LIST[ \t\n]		{ return MESH_FACE_LIST; }
\*MESH_FACE[ \t\n]		{ return MESH_FACE; }
\*MESH_MTLID[ \t\n]		{ return MESH_MTLID; }
\*MESH_NUMTVERTEX[ \t\n]	{ return MESH_NUMTVERTEX; }
\*MESH_TVERTLIST[ \t\n]		{ return MESH_TVERTLIST; }
\*MESH_TVERT[ \t\n]		{ return MESH_TVERT; }
\*MESH_NUMTVFACES[ \t\n]	{ return MESH_NUMTVFACES; }
\*MESH_TFACELIST[ \t\n]		{ return MESH_TFACELIST; }
\*MESH_TFACE[ \t\n]		{ return MESH_TFACE; }
\*MESH_NORMALS[ \t\n]		{ return MESH_NORMALS; }
\*MESH_FACENORMAL[ \t\n]	{ return MESH_FACENORMAL; }
\*MESH_VERTEXNORMAL[ \t\n]	{ return MESH_VERTEXNORMAL; }
\*NODE_TM[ \t\n]		{ return NODE_TM; };
\*TM_ROTAXIS[ \t\n]		{ return TM_ROTAXIS; }
\*TM_ROTANGLE[ \t\n]		{ return TM_ROTANGLE; }
\*[A-Z_]+[ \t\n]		{ return UNKNOWN; }
\"[^"]*\"			{ return STRING; }
[A-Z]:				{ return LBL; }
-?[0-9]+\.[0-9]+		{ yylval.f = strtod(yytext, 0); return FLOAT; }
-?[0-9]+:?			{ yylval.i = strtol(yytext, 0, 10); return INT; }
\{				{ return BGIN; }
\}				{ return END; }
[ \t\n]+			;
.				;
