////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include <math.h>

#include "setup.H"
#include "worldg.H"
#include "map.H"
#include "objects.H"
#include "game.H"
#include "light.H"
#include "solids.H"

static const float	q = M_SQRT2;
static const float	v = cos(asinf(.25));
static const float	w = .25;

Planar	planes[] = {
#include "planes.cc"
};

Polygon* Geometry::addpoly(void)
  {
    int	a  = (np+31) & ~31;
    int na = (np+32) & ~31;

    if(na>a) {
	Polygon*	pp = new Polygon[na];
	if(p) {
	    memcpy(pp, p, np*sizeof(Polygon));
	    delete[] p;
	}
	memset(pp+np, 0, (na-np)*sizeof(Polygon));
	p = pp;
    }
    p[np].v = 0;
    p[np].n = 0;
    p[np].ts = 0;
    p[np].tt = 0;
    p[np].soffset = p[np].toffset = 0;
    return p + np++;
  }

// Having an addv in Geometry is sorta odd, but it allows us to
// coalesce vertices for use in vertex arrays later.
void Geometry::addv(Polygon* p, float x, float y, float z)
  {
    int	a, na;
    int	i;

    if(p->p)
	p->d = -(p->p->n[0]*x+p->p->n[1]*y+p->p->n[2]*z);

    for(i=0; i<nv; i++)
	if(v[i*3]==x && v[i*3+1]==y && v[i*3+2]==z)
	    break;

    if(i>=nv) {
	a  = (nv+31) & ~31;
	na = (nv+32) & ~31;
	if(na>a) {
	    float*	vv = new float[3*na];
	    if(v) {
		memcpy(vv, v, 3*nv*sizeof(float));
		delete[] v;
	    }
	    v = vv;
	}
	i = nv++;
	v[i*3]   = x;
	v[i*3+1] = y;
	v[i*3+2] = z;
    }

    a  = (p->n+31) & ~31;
    na = (p->n+32) & ~31;
    if(na>a) {
	short*	vv = new short[na];
	if(p->v) {
	    memcpy(vv, p->v, p->n*sizeof(int));
	    delete[] p->v;
	}
	p->v = vv;
    }

    p->v[p->n++] = i;
  }

Geometry::~Geometry()
  {
    if(p) {
	for(int i=0; i<np; i++)
	    if(p[i].v)
		delete[] p[i].v;
	delete[] p;
    }
    if(v)
	delete[] v;
  }

Polygon* Geometry::panel(int tex, int name, float x1, float x2, float y1, float y2, float z1, float z2, int plane, bool hack=false)
  {
    Polygon*	p = addpoly();
    p->p = planes+plane;
    p->tex = tex;
    p->glname = name;
    if(plane==4 || plane==8) {
	addv(p, x1, y1, z1);
	addv(p, x2, y2, z1);
	addv(p, x2, y2, z2);
	addv(p, x1, y1, z2);
    } else {
	addv(p, x1, y1, z1);
	addv(p, x2, y1, z1);
	addv(p, x2, y2, z2);
	addv(p, x1, y2, z2);
    }
    if(hack) {
	p->ts = new float[4];
	p->tt = new float[4];
	p->ts[0] = 0.0; p->ts[1] = 1.0; p->ts[2] = 1.0; p->ts[3] = 0.0;
	p->tt[0] = 0.0; p->tt[1] = 0.0; p->tt[2] = 1.0; p->tt[3] = 1.0;
    }
    return p;
  }

void Geometry::box(int tex, int name, float x1, float x2, float y1, float y2, float z1, float z2, int bits, bool hack)
  {
    if(bits&1)  panel(tex, name, x2, x1, y2, y1, z2, z2, 0, hack);
    if(bits&2)  panel(tex, name, x1, x2, y2, y1, z1, z1, 1, hack);
    if(bits&4)  panel(tex, name, x2, x1, y2, y2, z1, z2, 2, hack);
    if(bits&8)  panel(tex, name, x2, x2, y1, y2, z1, z2, 4, hack);
    if(bits&16) panel(tex, name, x1, x2, y1, y1, z1, z2, 6, hack);
    if(bits&32) panel(tex, name, x1, x1, y2, y1, z1, z2, 8, hack);
  }

struct span_ {
    int		tex;
    float	top;
    float	bot;
    float	to;
};

struct wall_ {
    int		ns;
    span_	s[8];
};

static wall_	walls[8];

static void wspan(int wal, int tex, float b, float t, float toffset=0.0)
  {
    if(b >= t)
	return;

    wall_&	w = walls[wal];
    for(int i=0; i<w.ns; i++) {
	if(w.s[i].bot >= t) continue;
	if(w.s[i].top <= b) continue;
	if(w.s[i].bot>=b && w.s[i].top<=t) {
	    w.s[i] = w.s[--w.ns];
	    i--;
	    continue;
	}
	if(w.s[i].top>=t && w.s[i].bot<=b) {
	    if(w.ns > 7)
		throw "wspan(): too many spans in wall (splitting)";
	    w.s[w.ns] = w.s[i];
	    w.s[w.ns++].top = b;
	    w.s[i].bot = t;
	    continue;
	}
	if(w.s[i].bot<=t && w.s[i].top>=t)
	    w.s[i].bot = t;
	if(w.s[i].top>=b && w.s[i].bot<=b)
	    w.s[i].top = b;
    }

    if(w.ns > 7)
	throw "wspan(): too many spans in wall";
    w.s[w.ns].top = t;
    w.s[w.ns].bot = b;
    w.s[w.ns].to = toffset;
    w.s[w.ns++].tex = tex;
  }

void Geometry::wpanel(int tex, int name, float x, float y, const float* mat, float z1, float z2,
	const int* pln, float to, const Tile& t)
  {
    #define xyx(xx,yy) x+(xx*mat[0])+(yy*mat[1])
    #define xyy(xx,yy) y+(xx*mat[2])+(yy*mat[3])
    #define xyxy(x1,y1,x2,y2) xyx(x1,y1), xyx(x2,y2), xyy(x1,y1), xyy(x2,y2)
    switch(tex) {
	case 0x66:
	    panel(tex, name, xyxy(.25, .0625, .78125, .0625), z1, z1+.78125, pln[0])->toffset = to;
	    panel(tex, name, xyxy(0, -.125, .28125, -.125), z1, z1+.78125, pln[0])->toffset = to;
	    panel(tex, name, xyxy(.78125, -.125, 1, -.125), z1, z1+.78125, pln[0])->toffset = to;
	    panel(tex, name, xyxy(0, 0, 0, -.125), z1, z2, pln[2])->toffset = to;
	    panel(tex, name, xyxy(1, -.125, 1, 0), z1, z2, pln[1])->toffset = to;
	    panel(tex, name, xyxy(.78125, .0625, .78125, -.125), z1, z1+.78125, pln[2])->toffset = to;
	    panel(tex, name, xyxy(.27125, -.125, .27125, .0625), z1, z1+.78125, pln[1])->toffset = to;
	    panel(tex, name, xyxy(0, -.125, 1, -.125), z1+.78125, z2, pln[0])->toffset = to;
	    panel(tex, name, xyxy(.78125, -.125, .27125, .0625), z1+.78125, z1+.78125, 1)->toffset = .21875;
	    break;
	default:
	    panel(tex, name, xyxy(0, 0, 1, 0), z1, z2, pln[0])->toffset = to;
	    break;
    }
  }

Geometry::Geometry(int x, int y, const Tile& t): nv(0), np(0), v(0), p(0)
  {
    Polygon*	p;
    int		ltt = x?    Game::map(x-1, y).type: 0;
    int		rtt = x<64? Game::map(x+1, y).type: 0;
    int		btt = y?    Game::map(x, y-1).type: 0;
    int		utt = y<64? Game::map(x, y+1).type: 0;
    int		lfh = x?    Game::map(x-1, y).height: 16;
    int		rfh = x<64? Game::map(x+1, y).height: 16;
    int		bfh = y?    Game::map(x, y-1).height: 16;
    int		ufh = y<64? Game::map(x, y+1).height: 16;
    //int		lch = 16;
    //int		rch = 16;
    //int		bch = 16;
    //int		uch = 16;

    if(ltt==0 || ltt==3 || ltt==5)
	lfh = 16;
    if(rtt==0 || rtt==2 || rtt==4)
	rfh = 16;
    if(utt==0 || utt==4 || utt==5)
	ufh = 16;
    if(btt==0 || btt==2 || btt==3)
	bfh = 16;

    // floor
    int		h1 = t.height;
    int		h2 = t.height;
    int		h3 = t.height;
    int		h4 = t.height;

    p = addpoly();
    p->glname = 0x10000 | t.floor_tex;
    p->tex = t.floor_tex;
    switch(t.type) {
	case 6:
	    p->p = planes+11;
	    h3 = h4 = t.height+1;
	    break;
	case 7:
	    p->p = planes+10;
	    h1 = h2 = t.height+1;
	    break;
	case 8:
	    p->p = planes+13;
	    h2 = h3 = t.height+1;
	    break;
	case 9:
	    p->p = planes+12;
	    h1 = h4 = t.height+1;
	    break;
	default:
	    p->p = planes+0;
    }
    addv(p, x  , y  , .25*h1);
    addv(p, x+1, y  , .25*h2);
    addv(p, x+1, y+1, .25*h3);
    addv(p, x  , y+1, .25*h4);

    // ceiling
    panel(t.ceil_tex, 0x10200, x+1, x, y, y+1, 4, 4, 1);

    for(int i=0; i<8; i++)
	walls[i].ns = 0;

    // north wall(s)
    if(t.type==1 || t.type==4 || t.type==5 || t.type>5)
	wspan(0, t.wall_tex, .25*t.height, .25*ufh);

    // east wall(s)
    if(t.type==1 || t.type==2 || t.type==4 || t.type>5)
	wspan(1, t.wall_tex, .25*t.height, .25*rfh);

    // south wall(s)
    if(t.type==1 || t.type==2 || t.type==3 || t.type>5)
	wspan(2, t.wall_tex, .25*t.height, .25*bfh);

    // west wall(s)
    if(t.type==1 || t.type==3 || t.type==5 || t.type>5)
	wspan(3, t.wall_tex, .25*t.height, .25*lfh);

    switch(t.type) {
	case 2:
	    p = addpoly();
	    p->glname = 0x10100 | t.wall_tex;
	    p->p = planes+5;
	    p->tex = t.wall_tex;
	    addv(p, x, y, .25*t.height);
	    addv(p, x+1, y+1, .25*t.height);
	    addv(p, x+1, y+1, 4);
	    addv(p, x, y, 4);
	    break;
	case 3:
	    p = addpoly();
	    p->glname = 0x10100 | t.wall_tex;
	    p->p = planes+7;
	    p->tex = t.wall_tex;
	    addv(p, x, y+1, .25*t.height);
	    addv(p, x+1, y, .25*t.height);
	    addv(p, x+1, y, 4);
	    addv(p, x, y+1, 4);
	    break;
	case 4:
	    p = addpoly();
	    p->glname = 0x10100 | t.wall_tex;
	    p->p = planes+3;
	    p->tex = t.wall_tex;
	    addv(p, x+1, y, .25*t.height);
	    addv(p, x, y+1, .25*t.height);
	    addv(p, x, y+1, 4);
	    addv(p, x+1, y, 4);
	    break;
	case 5:
	    p = addpoly();
	    p->glname = 0x10100 | t.wall_tex;
	    p->p = planes+9;
	    p->tex = t.wall_tex;
	    addv(p, x+1, y+1, .25*t.height);
	    addv(p, x, y, .25*t.height);
	    addv(p, x, y, 4);
	    addv(p, x+1, y+1, 4);
	    break;
    }

    static const int	pln[][6] = {
	  { 6, 4, 8, 2, 63, 63 },
	  { 8, 6, 2, 4, 63, 63 },
	  { 2, 8, 4, 6, 63, 63 },
	  { 4, 2, 6, 8, 63, 63 },
    };
    static float	mat[][4] = {
	  { 1, 0, 0, 1 },
	  { 0, 1,-1, 0 },
	  {-1, 0, 0,-1 },
	  { 0,-1, 1, 0 },
    };

    for(int on=t.objects; on; on=Game::map.o[on]->next) {
	Object&	o = *Game::map.o[on];
	if(o.flags&32 || o.mobile)
	    continue;
	float	ox = x+.125*o.tilex;
	float	oy = y+.125*o.tiley;
	float	oz = .03125*o.tilez;

	switch(o.type) {
	    case 0x140 ... 0x14f:
		//oz = .25*t.height;
		if(!(o.facing&2)) {
		    ox = x+.5;
		    box(t.wall_tex, 0x10100|t.wall_tex,  ox-.50, ox-.25, oy, oy+.125, oz,      4, 28);
		    box(t.wall_tex, 0x10100|t.wall_tex,  ox+.25, ox+.50, oy, oy+.125, oz,      4, 52);
		    box(t.wall_tex, 0x10100|t.wall_tex,  ox-.25, ox+.25, oy, oy+.125, oz+.875, 4, 22);
		} else {
		    oy = y+.5;
		    box(t.wall_tex, 0x10100|t.wall_tex,  ox, ox+.125, oy+.25, oy+.50, oz,      4, 56);
		    box(t.wall_tex, 0x10100|t.wall_tex,  ox, ox+.125, oy-.50, oy-.25, oz,      4, 44);
		    box(t.wall_tex, 0x10100|t.wall_tex,  ox, ox+.125, oy-.25, oy+.25, oz+.875, 4, 42);
		}
		break;
	    case 0x160:
		box(o.flags>1? Game::map.tpal[o.flags-2]: 0x640+o.flags, 0x10000|0x640+o.flags,ox,ox+.125,oy,oy+.125, oz, 4, 60);
		break;
	    case 0x164:
		box(o.flags>1? Game::map.tpal[o.flags-2]: 0x640+o.flags, 0x10000|0x640+o.flags, x, x+1, y, y+1, oz, oz+.0625);
		break;
	    case 0x16E:
	    case 0x16F:
		wspan(o.facing>>1, Game::map.tpal[o.owner], oz, oz+1, (oz+1)-trunc(oz));
		break;
	    // Strictly decorative (and therefore unmovable) items
	    default:
		if(!o.movable() && !(o.type>63 && o.type<128))
		    box(0x200+o.type, 0x20000+on, ox, ox+.125, oy, oy+.125, oz, oz+.125, 63, true);
		break;
	}
    }

    for(int wn=0; wn<4; wn++) {
	wall_&	w = walls[wn];

	for(int i=0; i<w.ns; i++) switch(wn) {
	    case 0: wpanel(w.s[i].tex, 0x10100|w.s[i].tex, x,  y+1,mat[wn], w.s[i].bot,w.s[i].top, pln[wn], w.s[i].to, t); break;
	    case 1: wpanel(w.s[i].tex, 0x10100|w.s[i].tex, x+1,y+1,mat[wn], w.s[i].bot,w.s[i].top, pln[wn], w.s[i].to, t); break;
	    case 2: wpanel(w.s[i].tex, 0x10100|w.s[i].tex, x+1,y,  mat[wn], w.s[i].bot,w.s[i].top, pln[wn], w.s[i].to, t); break;
	    case 3: wpanel(w.s[i].tex, 0x10100|w.s[i].tex, x,  y,  mat[wn], w.s[i].bot,w.s[i].top, pln[wn], w.s[i].to, t); break;
	}
    }

  }

