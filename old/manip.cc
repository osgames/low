////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "manip.H"
#include "entity.H"
#include "textures.H"

static const float	GUMP_XSCALE = 3.5;
static const float	GUMP_YSCALE = 2;


EntityManipulator::~EntityManipulator()
  {
  }

ContentManipulator::~ContentManipulator()
  {
    container->panel = 0;
  }

int ContentManipulator::number(void)
  {
    int	num = 0;
    for(EIndex e=container->f_contents; e; e=e->next)
	num++;
    return num;
  }

EIndex ContentManipulator::entity(int i)
  {
    for(EIndex e=container->f_contents; e; e=e->next)
	if(!i--)
	    return e;
    return 0;
  }

bool ContentManipulator::add(EIndex e)
  {
    e->to_inside(container);
    return true;
  }

EIndex ContentManipulator::swap(int i, EIndex e)
  {
    if(EIndex b=entity(i))
      {
	e->to_inside(container, b);
	b->unlist();
	return b;
      }
    else
      {
	e->to_inside(container);
	return 0;
      }
  }

InventoryManipulator::InventoryManipulator(void)
  {
  }

InventoryManipulator::~InventoryManipulator()
  {
  }

EIndex InventoryManipulator::entity(int i)
  {
    return Game::player.inv[i];
  }

ManipulatorPanel::ManipulatorPanel(EntityManipulator* em, short gump_,
	float x, float y, float gw, float gh,
	int w, int h,
	float xpad, float ypad):
	Panel(x, y, gw, gh)
  {
    shown = true;
    movable = true;
    gump = gump_;
    framed = gump==0;
    name = 2;
    man = em;
    xoffs = xpad;
    yoffs = ypad;
    mw = w;
    mh = h;
  }

ManipulatorPanel::~ManipulatorPanel()
  {
    delete man;
  }

static void ucol(bool up)
  {
    glDisable(GL_ALPHA_TEST);
    if(up)
	glColor4f(0.7, 0.7, 0.7, 0.6);
    else
	glColor4f(0.0, 0.0, 0.0, 0.6);
  }

void ManipulatorPanel::uframe(bool up, float x1, float y1, float x2, float y2)
  {
#if 0
    glLineWidth(1.5);
    glBegin(GL_LINE_STRIP);
      ucol(up);
      glVertex2f(x1, y1);
      glVertex2f(x1, y2);
      glVertex2f(x2, y2);
    glEnd();
    glBegin(GL_LINE_STRIP);
      ucol(!up);
      glVertex2f(x2, y2);
      glVertex2f(x2, y1);
      glVertex2f(x1, y1);
    glEnd();
#endif
    static const float	ox = .002;
    static const float	oy = .0015;

    glColor4f(0.0, 0.0, 0.0, 0.2);
    glBegin(GL_QUADS);
      glVertex2f(x1+ox, y1-oy);
      glVertex2f(x1+ox, y2);
      glVertex2f(x2, y2);
      glVertex2f(x2, y1-oy);
    glEnd();

    glLineWidth(1);
    ucol(!up);
    glBegin(GL_LINE_STRIP);
      glVertex2f(x2+ox, y2-oy);
      glVertex2f(x2+ox, y1);
      glVertex2f(x1, y1);
    glEnd();

    ucol(up);
    glBegin(GL_LINE_STRIP);
      glVertex2f(x2+ox, y2-oy);
      glVertex2f(x1, y2-oy);
      glVertex2f(x1, y1);
    glEnd();

  }

EIndex ManipulatorPanel::targets(float mx, float my)
  {
    int		s = slot(mx, my);

    if(s<0)
	return 0;
    return man->entity(s);
  }

int ManipulatorPanel::slot(float mx, float my)
  {
    float	rh = h/GUMP_YSCALE;
    float	xo = xoffs/GUMP_XSCALE;
    float	yo = yoffs/GUMP_YSCALE;

    mx -= x + xo + BOX_XMARGIN;
    my = ((rh-(my-y)) - yo) - BOX_YMARGIN;

    int	x = int(mx/BOX_XSPACE);
    int	y = int(my/BOX_YSPACE);

    if(x<0 || y<0 || y>=mh || (mx - x*BOX_XSPACE)>BOX_XSIZE || (my - y*BOX_YSPACE)>BOX_YSIZE)
	return -1;

    if(x==mw)
      {
	if(y==0)
	  {
	  }
	if(y==(mh-1))
	  {
	  }
	return -1;
      }

    return x + mw*y;
  }

void ManipulatorPanel::rgump(bool select)
  {
    float	rw = w/GUMP_XSCALE;
    float	rh = h/GUMP_YSCALE;

    begin_render(select);
    glPushName(int(this));
    glDisable(GL_CULL_FACE);
    if(gump)
      {
	tex(gump);
	glColor3f(1, 1, 1);
	glEnable(GL_ALPHA_TEST);
	glBegin(GL_QUADS);
	  glTexCoord2f(0, h); glVertex2d( 0,  0);
	  glTexCoord2f(w, h); glVertex2d(rw,  0);
	  glTexCoord2f(w, 0); glVertex2d(rw, rh);
	  glTexCoord2f(0, 0); glVertex2d( 0, rh);
	glEnd();
	tex_none();
      }
    else
      {
	tex_none();
	//render_frame();
	uframe(true, 0, 0, w, h);
      }
  }

void ManipulatorPanel::render(bool select)
  {
    float	rh = h/GUMP_YSCALE;
    float	xo = xoffs/GUMP_XSCALE;
    float	yo = yoffs/GUMP_YSCALE;

    rgump(select);

    for(int x=0; x<mw; x++)
	for(int y=0; y<mh; y++)
	  {
	    float	ix = xo+BOX_XMARGIN+BOX_XSPACE*x;
	    float	iy = (rh-yo)-(BOX_YMARGIN+BOX_YSPACE*(y+1));
	    int		ii = y*mw + x;

	    rslot(ix, iy, ii);
	  }

    glEnable(GL_CULL_FACE);
    glPopName();
    end_render();
  }

void ManipulatorPanel::rslot(float ix, float iy, int ii)
  {
    glEnable(GL_BLEND);
    uframe(false, ix, iy, ix+BOX_XSIZE, iy+BOX_YSIZE);
    ix += .002;
    iy += .002/.75;

    if(EIndex e=man->entity(ii))
      {
	glEnable(GL_TEXTURE_2D);
	glColor4f(1, 1, 1, 1);
	Entity::Entity::texture(e->type);
	glBegin(GL_QUADS);
	glTexCoord2d(0, 1); glVertex2d(ix, iy+BOX_YSIZE-.004);
	glTexCoord2d(0, 0); glVertex2d(ix, iy);
	glTexCoord2d(1, 0); glVertex2d(ix+BOX_XSIZE-.004, iy);
	glTexCoord2d(1, 1); glVertex2d(ix+BOX_XSIZE-.004, iy+BOX_YSIZE-.004);
	glEnd();
	glMatrixMode(GL_MODELVIEW);
	if(e->quantity>1)
	  {
	    glPushMatrix();
	    TextImage	ti;
	    ti.fmt("%d", e->quantity);
	    glTranslatef(ix+BOX_XSIZE-(.001+ti.width()), iy, 0);
	    ti.render(1, 1, 1, 1);
	    glPopMatrix();
	  }
	glDisable(GL_TEXTURE_2D);
      }
  }

