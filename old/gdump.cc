////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"

#include "granny.H"


struct cname_t {
    unsigned long	id;
    const char*		name;
} cnames[] = {

	{ 0xCA5E0103, "Object_Chunk" },
	{ 0xCA5E0200, "Text_Chunk" },
	{ 0xCA5E0F03, "Object_List" },
	{ 0xCA5E0F00, "Object_Object" },
	{ 0xCA5E0F05, "Object_Keys" },
	{ 0xCA5E0F01, "Object_Key" },
	{ 0xCA5E0F06, "Object_Value_Container" },
	{ 0xCA5E0F02, "Object_Value" },
	{ 0xCA5E0602, "Mesh_Chunk" },
	{ 0xCA5E0601, "Object_Mesh" },
	{ 0xCA5E0604, "Points" },
	{ 0xCA5E0603, "Point_Container" },
	{ 0xCA5E0801, "Point_Data" },
	{ 0xCA5E0802, "Normal_Data" },
	{ 0xCA5E0804, "TexMap_Container" },
	{ 0xCA5E0803, "TexMap_Data" },
	{ 0xCA5E0702, "Weights_Data" },
	{ 0xCA5E0901, "Polygon_Data" },
	{ 0xCA5E0F04, "Object_ID" },
	{ 0xCA5E0B01, "Bone_Name_List" },
	{ 0xCA5E0B00, "Bone_Name" },
	{ 0xCA5E0507, "Bone_Chunk" },
	{ 0xCA5E0505, "Skeleton" },
	{ 0xCA5E0508, "Bone_List" },
	{ 0xCA5E0506, "Bone_Data" },
	{ 0xCA5E0C01, "BoneTie_Chunk" },
	{ 0xCA5E0C00, "BoneTie_Group" },
	{ 0xCA5E0C07, "BoneId_Chunk" },
	{ 0xCA5E0C08, "BoneId_List" },
	{ 0xCA5E0C02, "BoneId" },
	{ 0xCA5E0C06, "BoneTie_Wrapper" },
	{ 0xCA5E0C03, "BoneTie_Ties" },
	{ 0xCA5E0C09, "BoneTie_List" },
	{ 0xCA5E0C0A, "BoneTie_BoneTie" },
	{ 0xCA5E0304, "Texture_Info_Chunk" },
	{ 0xCA5E0301, "Texture_Info_List" },
	{ 0xCA5E0305, "Texture_Container" },
	{ 0xCA5E0303, "Texture_Geometry" },
	{ 0xCA5E0E01, "Texture_Chunk" },
	{ 0xCA5E0E00, "Texture_Texture" },
	{ 0xCA5E0E07, "Texture_List" },
	{ 0xCA5E0E02, "Texture_Poly" },
	{ 0xCA5E0E03, "Texture_Poly_Datas" },
	{ 0xCA5E0E05, "Texture_Poly_Data" },
	{ 0xCA5E0E04, "Texture_Poly_Data_2" },
	{ 0xCA5E0E06, "Texture_Polys" },
	{ 0xCA5E1205, "Animation_Chunk" },
	{ 0xCA5E1200, "Animation_List" },
	{ 0xCA5E1201, "Animation_Data" },
	{ 0xCA5E1203, "Animation_Container" },
	{ 0xCA5E1204, "Animation" },
	{ 0, 0 },
};

void showchunk(int indent, Granny::Chunk* c)
  {
    cname_t*	cn = cnames;

    while(cn->name)
      {
	if(cn->id == c->id)
	    break;
	cn++;
      }

    if(cn->name)
	printf("%.*s%s", indent*2, "                  ", cn->name);
    else
	printf("%.*s%X", indent*2, "                  ", c->id);
    int len = c->estimate_len();
    if(len)
	printf(" %d@%d", len, c->start);
    if(len == 1)
	printf(" [%d]", c->dword(0));
    printf("\n");
    if(c->id!=0xCA5E0F03 && c->children)
	showchunk(indent+1, c->children);
    if(c->sibling)
	showchunk(indent, c->sibling);
  }

int main(int argc, char** argv)
  {
    if(argc>1)
      {
	Granny::File		mf(argv[1]);
	Granny::KeyPairs	keys(mf);
	Granny::Model		m(mf);

	showchunk(0, mf.chunks);

#if 0
	for(Granny::Object* o=keys.first; o; o=o->next)
	  {
	    printf("%3d:\n", o->id);
	    for(Granny::KeyPair* k=o->first; k; k=k->next)
		printf("\t'%s'='%s'\n", keys.s(k->key), keys.s(k->value));
	  }
#endif

      }
  }

bool Granny::FullModel::render(bool, int, float, float*)
  {
    return false;
  }

