////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "fileio.H"
#include "granny.H"
#include "console.H"


// WARNING:  This code is ugly as hell, contains black magic, and is
// probably going to be a nightmare to fiddle with.  It's also suspected
// to contain at least one buffer overflow, and to contribute to
// holes in the Ozone layer.

// I wish I wish I wish I could get rid of it.

namespace Granny {

    static const char*	model_files[] = {
	"models/%s.grn",
	0
    };

    static ::File open_possible(const char* fname)
      {
	char	file[256];

	for(const char** fmt=model_files; *fmt; fmt++)
	  {
	    snprintf(file, 255, *fmt, fname);
	    file[255] = 0;
	    ::File fp = openlow(file, true);
	    if(fp)
		return fp;
	  }

	return 0;
      }

    File::File(const char* fname)
      {
	::File	fp = open_possible(fname);

	if(fp)
	  {
	    len = fp.size()/4;
	    buf = new unsigned long[len];
	    fp.read(buf, 4*len);
	    fp.close();
	    chunks = new Chunk(*this, 16);
	  }
	else
	  {
	    len = 0;
	    chunks = 0;
	  }
      }

    File::~File()
      {
	if(len)
	    delete[] buf;
	if(chunks)
	    delete chunks;
      }

    unsigned long Chunk::subchildren(unsigned long base, unsigned long at)
      {
	Chunk*	child = new Chunk(file, at);
	child->numsub = child->subleft = child->dword(2);

	subleft--;
	child->start = base + (child->dword(1)>>2);
	if(last_child)
	    last_child->sibling = child;
	else
	    children = child;
	child->parent = this;
	last_child = child;
	at += 3;

	subleft -= child->subleft;
	while(child->subleft>0)
	    at = child->subchildren(base, at);
	return at;
      }

    Chunk::Chunk(File& f, unsigned long o): file(f)
      {
	unsigned long	at;

	sibling = 0;
	children = 0;
	last_child = 0;
	start = o;
	id = dword(0);
	len = 0;
	numsub = 0;

	switch(id)
	  {
	    case 0xCA5E0000:
		len = 8;
		at = start + 8;
		numsub = dword(1);
		for(unsigned int i=0; i<dword(1); i++)
		  {
		    Chunk*	child = new Chunk(file, at);

		    at += child->len;
		    if(last_child)
			last_child->sibling = child;
		    else
			children = child;
		    child->parent = this;
		    last_child = child;
		  }
		break;
	    case 0xCA5E0100 ... 0xCA5E01FF:
		int base = dword(2)>>2;
		at = base + 4;
		numsub = subleft = f.buf[base];

		while(subleft>0)
		    at = subchildren(base, at);
		len = 5;
		break;
	  }
      }

    Chunk::~Chunk()
      {
	if(children)
	    delete children;
	if(sibling)
	    delete sibling;
      }

    unsigned long Chunk::estimate_len(void)
      {
	Chunk*		next = 0;
	Chunk*		here = this;

	if(children)
	    return children->start - start;
	if(sibling)
	    return sibling->start - start;

	while( (here = here->parent) )
	    if( (next = here->sibling) )
		break;

	if(next)
	    return next->start - start;

	return 0;
      };

    Texture::Texture(Chunk* c)
      {
	num_tri = 0;
	tri_tc = 0;
	int	max = 0;

	if(Chunk* d = c->locate(1, Texture_Polys))
	  {
	    num_tri = d->dword(0);
	    tri_tc = new int[num_tri*3];
	    for(int i=0; i<num_tri; i++)
	      {
		max >?= tri_tc[i*3  ] = d->dword(i*4+2);
		max >?= tri_tc[i*3+1] = d->dword(i*4+3);
		max >?= tri_tc[i*3+2] = d->dword(i*4+4);
	      }
	  }
      }

    Mesh::Mesh(Chunk* c)
      {
	vertices = 0;
	deformed = 0;
	normals = 0;
	texcoords = 0;
	tri_verts = 0;

	num_v = 0;
	num_tri = 0;

	int	num;
	int	dim;

	while(c)
	  {
	    if(c->id == Object_Mesh)
	      {
		Chunk*	d = c->children;
		Chunk*	p;

		while(d)
		  {
		    switch(d->id)
		      {
			case Points:
			    if(!d->children || d->children->id != Point_Container)
				break;
			    p = d->children->children;

			    while(p)
			      {
				switch(p->id)
				  {
				    case Point_Data:
					num = p->estimate_len();
					num_v = num/3;
					vertices = new float[num];
					deformed = 0;
					for(int i=0; i<num; i++)
					    vertices[i] = p->flt(i);
					break;
				    case Normal_Data:
					num = p->estimate_len();
					normals = new float[num];
					for(int i=0; i<num; i++)
					    normals[i] = p->flt(i);
					break;
				    case TexMap_Container:
					dim = p->children->dword(0);
					num = (p->children->estimate_len()-1)/dim;
					texcoords = new float[num*2];
					for(int i=0; i<num; i++)
					  {
					    texcoords[i*2  ] = p->children->flt(i*dim+1);
					    texcoords[i*2+1] = p->children->flt(i*dim+2);
					  }
					break;
				  }
				p = p->sibling;
			      }
			    break;
			case Weights_Data:
			    if(int num=d->dword(0))
			      {
				int	p = 3;
				weights = new BoneWeights[num];

				for(int i=0; i<num; i++)
				  {
				    weights[i].num = d->dword(p++);
				    weights[i].bones = new Bone*[weights[i].num];
				    weights[i].weights = new float[weights[i].num];
				    for(int j=0; j<weights[i].num; j++)
				      {
					int	bid = d->dword(p++);
					weights[i].bones[j] = reinterpret_cast<Bone*>(bid);
					weights[i].weights[j] = d->flt(p++);
				      }
				  }
			      }
			    else
				weights = 0;
			    break;
			case Polygon_Data:
			    num_tri = d->estimate_len() / 6;
			    tri_verts = new int[num_tri*3];
			    tri_norm = new int[num_tri*3];
			    for(int i=0; i<num_tri; i++)
			      {
				tri_verts[i*3  ] = d->dword(i*6  );
				tri_verts[i*3+1] = d->dword(i*6+1);
				tri_verts[i*3+2] = d->dword(i*6+2);
				tri_norm[i*3  ] = d->dword(i*6+3);
				tri_norm[i*3+1] = d->dword(i*6+4);
				tri_norm[i*3+2] = d->dword(i*6+5);
			      }
			    break;
			case Object_ID:
			    break;
		      }
		    d = d->sibling;
		  }
	      }
	    c = c->sibling;
	  }
      }

    Mesh::~Mesh()
      {
	if(vertices)
	    delete[] vertices;
	if(normals)
	    delete[] normals;
	if(texcoords)
	    delete[] texcoords;
	if(tri_verts)
	    delete[] tri_verts;
	if(next)
	    delete next;
      }

    Meshes::Meshes(File& f): first(0)
      {
	Chunk*		o = f.locate(2, Object_Chunk, Mesh_Chunk);

	while(o)
	  {
	    if(o->id == Mesh_Chunk)
	      {
		Mesh*	m = new Mesh(o->children);
		m->next = first;
		first = m;
	      }
	    o = o->sibling;
	  }
      }

    Textures::Textures(File& f): tex(0)
      {
	if(Chunk* o = f.locate(5, Object_Chunk, Texture_Chunk, Texture_Texture, Texture_List, Texture_Poly))
	    tex = new Texture(o);
      }

    Bone::Bone(Chunk* c): parent(0), next(0), sibling(0), children(0)
      {
	pid = c->dword(0);
	xlate[0] = c->flt(1);
	xlate[1] = c->flt(2);
	xlate[2] = c->flt(3);
	rot[0] = c->flt(4);
	rot[1] = c->flt(5);
	rot[2] = c->flt(6);
	rot[3] = c->flt(7);
      }

    Bone* Bones::byid(unsigned int id)
      {
	for(Bone* b = bucket[id%17]; b; b = b->next)
	    if(b->id==id)
		return b;
	return 0;
      }

    Bone* Bones::byindex(unsigned int i)
      {
	for(int bn=0; bn<17; bn++)
	  {
	    for(Bone* b = bucket[bn]; b; b=b->next)
		if(b->index==i)
		    return b;
	  }
	return 0;
      }

    Bone* Bones::byname(const char* name)
      {
	for(int bn=0; bn<17; bn++)
	  {
	    for(Bone* b = bucket[bn]; b; b=b->next)
		if(b->name && !strcmp(b->name, name))
		    return b;
	  }
	return 0;
      }


    Bones::Bones(File& f)
      {
	Chunk*	o;

	for(int i=0; i<17; i++)
	    bucket[i] = 0;
	num = 0;

	if(!(o = f.locate(6, Object_Chunk, BoneTie_Chunk, BoneTie_Group, BoneId_Chunk, BoneId_List, BoneId)))
	    return;

	ids = new unsigned long[o->estimate_len()];
	memcpy(ids, f.buf + o->start, o->estimate_len()<<2);

	if( (o=f.locate(7, Object_Chunk, BoneTie_Chunk, BoneTie_Group,
			BoneTie_Wrapper, BoneTie_Ties, BoneTie_List, BoneTie_BoneTie)) )
	    bties = f.buf + o->start;

	if( (o=f.locate(4, Object_Chunk, Bone_Chunk, Skeleton, Bone_List)) )
	  {
	    o = o->children;
	    while(o)
	      {
		if(o->id == Bone_Data)
		  {
		    Bone*	b = new Bone(o);

		    b->id = ids[num++];
		    b->index = num;
		    b->pid = ids[b->pid];
		    b->next = bucket[b->id%17];
		    bucket[b->id%17] = b;
		    b->children = 0;
		  }
		o = o->sibling;
	      }

	    for(unsigned int i=0; i<num; i++)
		if(Bone* b = byindex(i))
		  {
		    if(b->pid != b->id)
		      {
			if(Bone* p = byid(b->pid))
			  {
			    b->parent = p;
			    b->sibling = p->children;
			    p->children = b;
			  }
		      }
		  }
	  }
      }

    Bones::~Bones()
      {
	for(int i=0; i<17; i++)
	    if(bucket[i])
		delete bucket[i];
      }

    Strings::Strings(Chunk* c)
      {
	unsigned long	bytes = 0;
	const char*	text = reinterpret_cast<char*>(c->file.buf + c->start + 2);

	num = c->dword(0);

	for(int i=0; i<num; i++)
	    bytes += strlen(text+bytes) + 1;
	str = new const char*[num];
	buf = new char[bytes];
	memcpy(buf, text, bytes);
	text = buf;
	for(int i=0; i<num; i++)
	  {
	    str[i] = text;
	    text += strlen(text) + 1;
	  }
      }

    KeyPair::KeyPair(Chunk* c): next(0)
      {
	key = c->dword(0);
	if(Chunk* v = c->locate(2, Object_Value_Container, Object_Value))
	    value = v->dword(1);
      }

    Object::Object(Chunk* c, KeyPairs* p): next(0), pairs(p), first(0)
      {
	n2 = c->dword(1);

	while(c && c->id != Object_Keys)
	    c = c->sibling;

	if(!c)
	    return;

	c = c->children;

	while(c)
	  {
	    if(c->id == Object_Key)
	      {
		KeyPair*	kp = new KeyPair(c);
		kp->next = first;
		first = kp;
	      }
	    c = c->sibling;
	  }
      }

    KeyPairs::KeyPairs(File& f): str(0), first(0), oid(0)
      {
	if(!f.chunks)
	    throw "3D models not found.  Are you sure you downloaded the 3D and Media archives?";
	Chunk*	o = f.chunks->children;

	while(o && o->id != Object_Chunk)
	    o = o->sibling;

	if(!o)
	    return;

	o = o->children;

	while(o)
	  {
	    if(o->id == Text_Chunk)
		this->str = new Strings(o);
	    else if(o->id == Object_List)
	      {
		Chunk*	l = o->children;

		while(l)
		  {
		    if(l->id==Object_Object && l->children)
		      {
			oid++;
			Object*	obj = new Object(l->children, this);

			obj->next = first;
			obj->id = oid;
			first = obj;
		      }
		    l = l->sibling;
		  }
	      }
	    o = o->sibling;
	  }
      }

    const char* Object::find(const char* key)
      {
	for(KeyPair* kp=first; kp; kp=kp->next)
	    if(!strcmp(key, pairs->s(kp->key)))
		return pairs->s(kp->value);
	return 0;
      }

    const char*	Object::name_key = "__ObjectName";

    Object* KeyPairs::find(const char* name)
      {
	for(Object* o=first; o; o=o->next)
	    if(!strcmp(o->name(), name))
		return o;
	return 0;
      }

    Object* KeyPairs::find(unsigned int id)
      {
	for(Object* o=first; o; o=o->next)
	    if(o->id == id)
		return o;
	return 0;
      }

    BoneAnim::BoneAnim(Chunk* c)
      {
	id = c->dword(0);
	xlate_time = new float[num_xlates = c->dword(6)];
	rot_time = new float[num_rots = c->dword(7)];
	unk_time = new float[num_unk = c->dword(8)];
	xlate = new float[num_xlates*3];
	rot = new float[num_rots*4];
	unk = new float[num_unk*3];

	int	p = 13;

	for(int i=0; i<num_xlates; i++)
	    xlate_time[i] = c->flt(p++);
	for(int i=0; i<num_rots; i++)
	    rot_time[i] = c->flt(p++);
	for(int i=0; i<num_unk; i++)
	    unk_time[i] = c->flt(p++);
	for(int i=0; i<num_xlates*3; i++)
	    xlate[i] = c->flt(p++);
	for(int i=0; i<num_rots*4; i++)
	    rot[i] = c->flt(p++);
	for(int i=0; i<num_unk*3; i++)
	    unk[i] = c->flt(p++);
      }

    BoneAnim::~BoneAnim()
      {
      }

    Animation::Animation(File& f): bones(f), keys(f), first(0)
      {
	if(Chunk* a=f.locate(5, Object_Chunk, Animation_Chunk, Animation_List, Animation_Container, Animation_Animation))
	    while(a && a->id==Animation_Animation)
	      {
		BoneAnim*	ba = new BoneAnim(a);

		ba->next = first;
		first = ba;
		a = a->sibling;
	      }
	if(Chunk* o = f.locate(6, Object_Chunk, BoneTie_Chunk, BoneTie_Group, BoneId_Chunk, BoneId_List, BoneId))
	  {
	    bids = new unsigned long[o->estimate_len()];
	    memcpy(bids, f.buf + o->start, o->estimate_len()<<2);
	  }
	if(Chunk* c = f.locate(2, Object_Chunk, Bone_Name_List))
	  {
	    int	b = 1;
	    Chunk*	bn = c->children;

	    while(bn)
	      {
		if(bn->id==Bone_Name && bn->children && bn->children->id==Object_ID)
		  {
		    Object*	o = keys.find(bn->children->dword(0));
		    if(o)
		      {
			Bone*	bone = bones.byid(b);
			
			if(bone)
			  {
			    bone->name = o->name();
			  }
		      }
		  }
		bn = bn->sibling;
		b++;
	      }
	  }


      }

    Model::Model(File& f): meshes(f), textures(f), bones(f), keys(f), tid(0)
      {
	if(!f.chunks)
	    throw "3D models not found.  Are you sure you downloaded the 3D and Media archives?";

	Chunk*	o = f.locate(1, Object_Chunk);

	if(Chunk* c = o->locate(3, Texture_Info_Chunk, Texture_Info_List, Object_ID))
	  {
	    Object*	o = keys.find(c->dword(0));
	    if(o)
		texture = o->name();
	  }

	if(Chunk* c = o->locate(1, Bone_Name_List))
	  {
	    int	b = 1;
	    Chunk*	bn = c->children;

	    while(bn)
	      {
		if(bn->id==Bone_Name && bn->children && bn->children->id==Object_ID)
		  {
		    Object*	o = keys.find(bn->children->dword(0));
		    if(o)
		      {
			Bone*	bone = bones.byid(b);
			
			if(bone)
			  {
			    bone->name = o->name();
			  }
		      }
		  }
		bn = bn->sibling;
		b++;
	      }
	  }

	for(Mesh* m=meshes.first; m; m=m->next)
	    for(int v=0; v<m->num_v; v++)
		if(m->weights)
		    for(int w=0; w<m->weights[v].num; w++)
		      {
			int		bt = reinterpret_cast<int>(m->weights[v].bones[w]);
			int		bid = bones.ids[bones.bties[bt*8]];
			Bone*	bone = bones.byid(bid);

			m->weights[v].bones[w] = bone;
		      }

	bones.root = bones.byname("__Root");

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	bones.root->reset();
	//meshes.first->apply_bones();
#ifndef TESTING
	//cprintf(Debug, "granny: model parsed succesfully");
#endif
      }

    Chunk* Chunk::vlocate(int num, va_list ap)
      {
	unsigned long	want = va_arg(ap, unsigned long);

	for(Chunk* c=children; c; c=c->sibling)
	    if(c->id == want)
	      {
		if(!num)
		    return c;
		else
		    return c->vlocate(num-1, ap);
	      }
	return 0;
      }

    Chunk* Chunk::locate(int num, ...)
      {
	va_list	ap;

	va_start(ap, num);
	Chunk* c = vlocate(num-1, ap);
	va_end(ap);

	return c;
      }

    Chunk* File::locate(int num, ...)
      {
	if(!chunks)
	    return 0;

	va_list ap;

	va_start(ap, num);
	Chunk* c = chunks->vlocate(num-1, ap);
	va_end(ap);

	return c;
      }

    void Mesh::apply_bones(void)
      {
	if(!weights)
	    return;

	float*	vert = vertices;
	float*	defs = deformed;

	for(int v=0; v<num_v; v++, vert+=3, defs+=3)
	  {
	    defs[0] = 0;
	    defs[1] = 0;
	    defs[2] = 0;
	    for(int w=0; w<weights[v].num; w++)
	      {
		defs[0] += weights[v].weights[w] *
				((weights[v].bones[w]->cmatrix[0] *vert[0]) +
				 (weights[v].bones[w]->cmatrix[4] *vert[1]) +
				 (weights[v].bones[w]->cmatrix[8] *vert[2]) +
				  weights[v].bones[w]->cmatrix[12]);
		defs[1] += weights[v].weights[w] *
				((weights[v].bones[w]->cmatrix[1] *vert[0]) +
				 (weights[v].bones[w]->cmatrix[5] *vert[1]) +
				 (weights[v].bones[w]->cmatrix[9] *vert[2]) +
				  weights[v].bones[w]->cmatrix[13]);
		defs[2] += weights[v].weights[w] *
				((weights[v].bones[w]->cmatrix[2] *vert[0]) +
				 (weights[v].bones[w]->cmatrix[6] *vert[1]) +
				 (weights[v].bones[w]->cmatrix[10]*vert[2]) +
				  weights[v].bones[w]->cmatrix[14]);
	      }
	  }
      }

    void Bone::transform(float* tm)
      {
	tm[0] = 1 - (rot[1]+rot[1])*rot[1] - (rot[2]+rot[2])*rot[2];
	tm[1] =     (rot[0]+rot[0])*rot[1] + (rot[2]+rot[2])*rot[3];
	tm[2] =     (rot[0]+rot[0])*rot[2] - (rot[1]+rot[1])*rot[3];
	tm[3] = 0;
	tm[4] =     (rot[0]+rot[0])*rot[1] - (rot[2]+rot[2])*rot[3];
	tm[5] = 1 - (rot[0]+rot[0])*rot[0] - (rot[2]+rot[2])*rot[2];
	tm[6] =     (rot[1]+rot[1])*rot[2] + (rot[0]+rot[0])*rot[3];
	tm[7] = 0;
	tm[8] =     (rot[0]+rot[0])*rot[2] + (rot[1]+rot[1])*rot[3];
	tm[9] =     (rot[1]+rot[1])*rot[2] - (rot[0]+rot[0])*rot[3];
	tm[10]= 1 - (rot[0]+rot[0])*rot[0] - (rot[1]+rot[1])*rot[1];
	tm[11]= 0;
	tm[12]= xlate[0];
	tm[13]= xlate[1];
	tm[14]= xlate[2];
	tm[15]= 1;
      }


    void invert(float* tm)
      {
	float	tmp[16];

	float det;
	float d12, d13, d23, d24, d34, d41;

	d12 = tm[ 2]*tm[ 7] - tm[ 3]*tm[ 6];
	d13 = tm[ 2]*tm[11] - tm[ 3]*tm[10];
	d23 = tm[ 6]*tm[11] - tm[ 7]*tm[10];
	d24 = tm[ 6]*tm[15] - tm[ 7]*tm[14];
	d34 = tm[10]*tm[15] - tm[11]*tm[14];
	d41 = tm[14]*tm[ 3] - tm[15]*tm[ 2];
	tmp[0] =  (tm[ 5]*d34 - tm[ 9]*d24 + tm[13]*d23);
	tmp[1] = -(tm[ 1]*d34 + tm[ 9]*d41 + tm[13]*d13);
	tmp[2] =  (tm[ 1]*d24 + tm[ 5]*d41 + tm[13]*d12);
	tmp[3] = -(tm[ 1]*d23 - tm[ 5]*d13 + tm[ 9]*d12);

	det = tm[0]*tmp[0] + tm[4]*tmp[1] + tm[8]*tmp[2] + tm[12]*tmp[3];

	float idet = 1.0 / det;

	tmp[0] *= idet;
	tmp[1] *= idet;
	tmp[2] *= idet;
	tmp[3] *= idet;

	tmp[4] = -(tm[ 4]*d34 - tm[ 8]*d24 + tm[12]*d23) * idet;
	tmp[5] =  (tm[ 0]*d34 + tm[ 8]*d41 + tm[12]*d13) * idet;
	tmp[6] = -(tm[ 0]*d24 + tm[ 4]*d41 + tm[12]*d12) * idet;
	tmp[7] =  (tm[ 0]*d23 - tm[ 4]*d13 + tm[ 8]*d12) * idet;

	d12 = tm[ 0]*tm[ 5] - tm[ 1]*tm[ 4];
	d13 = tm[ 0]*tm[ 9] - tm[ 1]*tm[ 8];
	d23 = tm[ 4]*tm[ 9] - tm[ 5]*tm[ 8];
	d24 = tm[ 4]*tm[13] - tm[ 5]*tm[12];
	d34 = tm[ 8]*tm[13] - tm[ 9]*tm[12];
	d41 = tm[12]*tm[ 1] - tm[13]*tm[ 0];

	tmp[8] =  (tm[ 7]*d34 - tm[11]*d24 + tm[15]*d23) * idet;
	tmp[9] = -(tm[ 3]*d34 + tm[11]*d41 + tm[15]*d13) * idet;
	tmp[10]=  (tm[ 3]*d24 + tm[ 7]*d41 + tm[15]*d12) * idet;
	tmp[11]= -(tm[ 3]*d23 - tm[ 7]*d13 + tm[11]*d12) * idet;
	tmp[12]= -(tm[ 6]*d34 - tm[10]*d24 + tm[14]*d23) * idet;
	tmp[13]=  (tm[ 2]*d34 + tm[10]*d41 + tm[14]*d13) * idet;
	tmp[14]= -(tm[ 2]*d24 + tm[ 6]*d41 + tm[14]*d12) * idet;
	tmp[15]=  (tm[ 2]*d23 - tm[ 6]*d13 + tm[10]*d12) * idet;

	for(int i=0; i<16; i++)
	    tm[i] = tmp[i];
      }

    static void mult(float* tm, const float* matrix)
      {
	float	tmp[16];

	for(int i=0; i<4; i++)
	  {
	    tmp[   i] = tm[i]*matrix[ 0] + tm[4+i]*matrix[ 1] + tm[8+i]*matrix[ 2] + tm[12+i]*matrix[ 3];
	    tmp[ 4+i] = tm[i]*matrix[ 4] + tm[4+i]*matrix[ 5] + tm[8+i]*matrix[ 6] + tm[12+i]*matrix[ 7];
	    tmp[ 8+i] = tm[i]*matrix[ 8] + tm[4+i]*matrix[ 9] + tm[8+i]*matrix[10] + tm[12+i]*matrix[11];
	    tmp[12+i] = tm[i]*matrix[12] + tm[4+i]*matrix[13] + tm[8+i]*matrix[14] + tm[12+i]*matrix[15];
	  }

	for(int i=0; i<16; i++)
	    tm[i] = tmp[i];
      }

    void Bone::reset(void)
      {
	float	mat[16];

	transform(mat);

	glMultMatrixf(mat);
	glGetFloatv(GL_MODELVIEW_MATRIX, mat);

	for(int i=0; i<16; i++)
	    matrix[i] = mat[i];
	invert(matrix);
	for(int i=0; i<16; i++)
	    cmatrix[i] = mat[i];
	mult(cmatrix, matrix);

	for(Bone* b=children; b; b=b->sibling)
	  {
	    glPushMatrix();
	    b->reset();
	    glPopMatrix();
	  }
      }

    void Bone::calculate(void)
      {
	float	mat[16];

	transform(mat);

	glMultMatrixf(mat);
	glGetFloatv(GL_MODELVIEW_MATRIX, mat);

	for(int i=0; i<16; i++)
	    cmatrix[i] = mat[i];
	mult(cmatrix, matrix);

	for(Bone* b=children; b; b=b->sibling)
	  {
	    glPushMatrix();
	    b->calculate();
	    glPopMatrix();
	  }
      }

    bool Bones::apply(Animation* anim, float when)
      {
	bool	end = true;

	if(anim) for(BoneAnim* ba=anim->first; ba; ba=ba->next)
	  {
	    Bone*	abone = anim->bones.byid(ba->id);
	    Bone*	b = byname(abone->name);

	    if(!b)
		continue;

	    int		xe = 1;
	    int		re = 1;
	    float	xwhen = when;
	    float	rwhen = when;

	    while(xe<(ba->num_xlates-1) && xwhen>ba->xlate_time[xe])
		xe++;
	    while(re<(ba->num_rots-1) && rwhen>ba->rot_time[re])
		re++;

	    int	xi = xe-1;
	    int	ri = re-1;

	    float	xbase = xe? ba->xlate_time[xi]: 0;
	    float	xfrac = (xwhen-xbase) / (ba->xlate_time[xe]-xbase);
	    float	rbase = re? ba->rot_time[ri]: 0;
	    float	rfrac = (rwhen-rbase) / (ba->rot_time[re]-rbase);

	    if(xfrac<1.0 && rfrac<1.0)
		end = false;

	    b->xlate[0] = ba->xlate[xe*3  ]*xfrac + ba->xlate[xi*3  ]*(1-xfrac);
	    b->xlate[1] = ba->xlate[xe*3+1]*xfrac + ba->xlate[xi*3+1]*(1-xfrac);
	    b->xlate[2] = ba->xlate[xe*3+2]*xfrac + ba->xlate[xi*3+2]*(1-xfrac);
	    b->rot[0]   = ba->rot[re*4  ]  *rfrac + ba->rot[ri*4  ]  *(1-rfrac);
	    b->rot[1]   = ba->rot[re*4+1]  *rfrac + ba->rot[ri*4+1]  *(1-rfrac);
	    b->rot[2]   = ba->rot[re*4+2]  *rfrac + ba->rot[ri*4+2]  *(1-rfrac);
	    b->rot[3]   = ba->rot[re*4+3]  *rfrac + ba->rot[ri*4+3]  *(1-rfrac);
	  }
	return end;
      }

    bool Model::apply(Animation* anim, float time)
      {
	bool end = bones.apply(anim, time);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	bones.root->calculate();
	meshes.first->apply_bones();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	return end;
      }






    FullModel*	FullModel::models[400];

    int FullModel::initialize(void)
      {
#ifndef TESTING
	cprintf(Debug, "granny: attempting to initialize from granny.dat");
#endif

	int	num = 0;
	char	pad[1024];
	::File	fp = openlow("granny.dat");

	if(!fp)
	    return 0;

	FullModel*	lm = 0;

	while(fp.readln(pad, 1024))
	    if(pad[0]>'0' && pad[0]<'5')
	      {
		int	id;
		char	name[64];

		if(sscanf(pad, "%d %s", &id, name)==2)
		  {
		    File	mf(name);
		    FullModel*	fm = new FullModel(mf);

		    models[id-100] = fm;
		    lm = fm;
		  }
	      }
	    else if(lm && pad[0]==' ')
	      {
		int	aid;
		char	name[64];

		name[0] = 0;
		if(sscanf(pad, "%d %s", &aid, name)>0)
		  {
		    if(aid > 0)
		      {
			File		af(name);
			AniModel*	am = new AniModel(af);

			am->next = lm->ani;
			am->id = aid;
			lm->ani = am;
			lm->self = true;
		      }
		    else if(models[(-aid)-100])
		      {
			lm->ani = models[(-aid)-100]->ani;
			lm->self = false;
		      }
		  }
	      }

	fp.close();
	return num;
      }

    static const float TICK = 1.0/25;

    void RenderModels::tick(void)
      {
	if(ani_done)
	  {
	    if(ani_loop)
		ani_time = 0.0;
	    else if(!ani_stop)
	      {
		ani_seq = ani_idle;
		ani_stop = false;
		ani_loop = true;
		ani_delay = 12;
	      }
	  }
	else
	    ani_time += TICK;
      }

}; //  namespace Granny

