#include "md.H"
#include "events.H"


static Cond	q_cond;

Event* Event::first = 0;
Event* Event::last = 0;

void Event::enqueue(void)
  {
    q_cond.lock();

    next = 0;
    if(first) {
	last = last->next = this;
	q_cond.unlock();
    } else {
	first = last = this;
	q_cond.unlock();
	q_cond.signal();
    }
  }

Event* Event::wait(void)
  {
    q_cond.lock();
    while(!first)
	q_cond.wait();
    Event* e = first;
    if(!(first = first->next))
	last = 0;
    e->next = 0;
    q_cond.unlock();
    return e;
  }

Event::Event(Type t, unsigned int w):
    next(0), type(t)
  {
    which = w;
    enqueue();
  }

Event::Event(Type t, float mx, float my):
    next(0), type(t)
  {
    motion.x = mx;
    motion.y = my;
    enqueue();
  }

