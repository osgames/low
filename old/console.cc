////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include <ctype.h>
#include "setup.H"

#include "game.H"
#include "map.H"
#include "panel.H"
#include "console.H"
#include "main.H"


extern FILE*	logfile;

void cprintf(CLevel level, const char* fmt, ...)
  {
    va_list	ap;

    va_start(ap, fmt);
    vfprintf(logfile, fmt, ap);
    fprintf(logfile, "\n");
    fflush(logfile);
    if(Game::console_p && level>=Info)
	Game::console_p->vprintf(0, fmt, ap);
    va_end(ap);
  }

enum VType {
    vtInt, vtBool, vtLevel, vtPercent, vtFloat, vtString,
};

static struct cvar_ {
    const char*		name;
    VType		type;
    void*		addr;
    int			min;
    int			max;

    bool		set(const char*);
    const char*		view(void);
}		cvars[] = {
      { "debug.grid",			vtBool,		&Game::setup.grid_debug },
      { "debug.martians",		vtBool,		&Game::setup.martians_debug },
      { "debug.paperdoll.anim",		vtInt,		&Game::setup.paperdoll_anim, 1000, 3000 },
      { "debug.paperdoll.model",	vtInt,		&Game::setup.paperdoll_model, 100, 199 },
      { "setup.detail.wall",		vtLevel,	&Game::setup.wall_detail },
      { "setup.detail.floor",		vtLevel,	&Game::setup.floor_detail },
      { "setup.detail.texture",		vtLevel,	&Game::setup.texture_detail },
      { "setup.detail.anim",		vtLevel,	&Game::setup.anim_detail },
      { "setup.audio.music_vol",	vtPercent,	&Game::setup.music_volume },
      { "setup.audio.sound_vol",	vtPercent,	&Game::setup.sound_volume },
      { "setup.audio.footsteps",	vtBool,		&Game::setup.footsteps },
      { "setup.video.framelock",	vtBool,		&Game::setup.framelock },
      { "setup.video.bumpmapping",	vtBool,		&Game::setup.bumpmapping },
      { "setup.video.sight",		vtInt,		&Game::setup.sight_depth, 4, 12 },
      { "setup.video.gamma",		vtFloat,	&Game::setup.gamma },
      { "setup.video.width",		vtInt,		&want_w, 320, 4026, },
      { "setup.video.height",		vtInt,		&want_h, 240, 2048, },
      { "setup.video.specular",		vtBool,		&Game::setup.specular_highlights },
      { "setup.video.fragment_programs",vtBool,		&Game::setup.fragment_programs },
      { "setup.video.shadows",		vtBool,		&Game::setup.do_shadows },
      { "setup.video.fullscreen",	vtBool,		&Game::fullscreen },
      { "setup.video.screen",		vtInt,		&want_screen, 0, 7, },
      { "setup.mouselook",		vtBool,		&Game::setup.mlook },
      { "setup.mouse.yreverse",		vtBool,		&Game::setup.yrev },
      { "setup.path.low",		vtString,	&Game::setup.low_path },
      { "setup.path.uw2",		vtString,	&Game::setup.uw2_path },
};

const char* cvar_::view(void)
  {
    static char		pad[256];
    static char*	levname[] = { "Low", "Medium", "High" };

    switch(type)
      {
	case vtInt:
	    sprintf(pad, "%d", *reinterpret_cast<int*>(addr));
	    return pad;
	case vtBool:
	    return (*reinterpret_cast<bool*>(addr))? "True": "False";
	case vtLevel:
	    return levname[int(*reinterpret_cast<Game::Setup::Level*>(addr))];
	case vtPercent:
	    sprintf(pad, "%d%%", *reinterpret_cast<int*>(addr));
	    return pad;
	case vtFloat:
	    sprintf(pad, "%g", *reinterpret_cast<float*>(addr));
	    return pad;
	case vtString:
	    sprintf(pad, "\"%.255s\"", *reinterpret_cast<char**>(addr) ?: "");
	    return pad;
      }
    return "(unknown)";
  }

static bool getnum(const char* s, int& i)
  {
    char*	ep;

    i = strtol(s, &ep, 0);
    if(ep != s)
	return true;
    return false;
  }

bool cvar_::set(const char* s)
  {
    int		i;

    switch(type)
      {
	case vtInt:
	    if(!getnum(s, i) || i<min || i>max)
		return true;
	    *reinterpret_cast<int*>(addr) = i;
	    break;
	case vtBool:
	    if(*s=='y' || *s=='Y' || *s=='t' || *s=='T' || *s=='1')
		*reinterpret_cast<bool*>(addr) = true;
	    else if(*s=='n' || *s=='N' || *s=='f' || *s=='F' || *s=='0')
		*reinterpret_cast<bool*>(addr) = false;
	    else
		return true;
	    break;
	case vtLevel:
	    if(*s=='l' || *s=='L' || *s=='0')
		*reinterpret_cast<Game::Setup::Level*>(addr) = Game::Setup::Low;
	    else if(*s=='m' || *s=='M' || *s=='1')
		*reinterpret_cast<Game::Setup::Level*>(addr) = Game::Setup::Medium;
	    else if(*s=='h' || *s=='H' || *s=='2')
		*reinterpret_cast<Game::Setup::Level*>(addr) = Game::Setup::High;
	    else
		return true;
	    break;
	case vtPercent:
	    if(!strncasecmp(s, "off", strlen(s)))
		*reinterpret_cast<int*>(addr) = 0;
	    else if(!getnum(s, i) || i<0 || i>100)
		return true;
	    *reinterpret_cast<int*>(addr) = i;
	    break;
	case vtFloat:
	    *reinterpret_cast<float*>(addr) = strtod(s, 0);
	    break;
	case vtString:
	    if(*reinterpret_cast<char**>(addr))
	    	delete[] *reinterpret_cast<char**>(addr);
	    if(s[0])
	      {
		*reinterpret_cast<char**>(addr) = new char[strlen(s)+1];
		strcpy(*reinterpret_cast<char**>(addr), s);
	      }
	    else
		*reinterpret_cast<char**>(addr) = 0;
	    break;
      }
    return false;
  }

static void cc_csave(int argc, char** argv)
  {
    const char*	fname;
    if(argc>1)
	fname = argv[1];
    else
	fname = "low.ini";
    FILE*	fp = fopen(fname, "w");
    if(fp)
      {
	for(unsigned int i=0; i<(sizeof(cvars)/sizeof(cvar_)); i++)
	    fprintf(fp, "set %s %s\n", cvars[i].name, cvars[i].view());
	fclose(fp);
	cprintf(Info, "setup: Settings saved to %s", fname);
      }
    return;
  }

static void cc_cload(int argc, char** argv)
  {
    const char*	fname;
    if(argc>1)
	fname = argv[1];
    else
	fname = "low.ini";
    rcfile(fname);
  }

static int  quiet = 0;

static void cc_set(int argc, char** argv)
  {
    if(argc<2)
      {
	for(unsigned int i=0; i<(sizeof(cvars)/sizeof(cvar_)); i++)
	    cprintf(Info, "%s: %s", cvars[i].name, cvars[i].view());
	return;
      }

    for(unsigned int i=0; i<(sizeof(cvars)/sizeof(cvar_)); i++)
      {
	bool		match = true;
	const char*	s = argv[1];
	const char*	n = cvars[i].name;

	// match; we need to match every part between '.'s in succession to
	// decide if we match at all.  We stop looking when we hit a '.'
	// in s and skip to the next '.' in n, but otherwise declare
	// failiure as soon as we have a mismatching character.  If we run
	// out of chars in s, we fail only if there remains '.'s in n.

	while(*s)
	  {
	    if(*s=='.')
	      {
		s++;
		while(*n && *n!='.')
		    n++;
		if(!*n)
		  {
		    match = false;
		    break;
		  }
		n++;
	      }
	    else if(tolower(*s) != *n)
	      {
		match = false;
		break;
	      }
	    else
	      {
		s++;
		n++;
	      }
	  }
	if(match && *n)
	  {
	    while(*n)
		if(*n++=='.')
		  {
		    match = false;
		    break;
		  }
	  }

	if(match)
	  {
	    if(argc>2)
	      {
		if(cvars[i].set(argv[2]))
		  {
		    cprintf(Warning, "'%s' is not a valid value for variable '%s'", argv[2], cvars[i].name);
		    return;
		  }
	      }
	    if(!quiet)
		cprintf(Info, "%s: %s", cvars[i].name, cvars[i].view());
	    return;
	  }
      }

    cprintf(Warning, "Unknown variable '%s'", argv[1]);
  }

static struct {
    const char*	name;
    void	(*func)(int, char**);
}		cmds[] = {
    	{ "set",	cc_set },
	{ "csave",	cc_csave },
	{ "cload",	cc_cload },
};

void ccommand(const char* cmd)
  {
    char	pad[1024];
    char*	argv[64];
    int		argc = 0;
    char*	d = pad;

    while(argc<64 && *cmd)
      {
	argv[argc++] = d;
	if(*cmd=='"')
	  {
	    cmd++;
	    while(*cmd && *cmd!='"')
		*d++ = *cmd++;
	    if(*cmd=='"')
		cmd++;
	  }
	else
	    while(*cmd && *cmd!=' ')
		*d++ = *cmd++;
	*d++ = 0;
	while(*cmd==' ')
	    cmd++;
      }

    if(argc<1)
	return;

    int	cl = strlen(argv[0]);

    for(unsigned int i=0; i<(sizeof(cmds)/sizeof(cmds[0])); i++)
	if(!strncmp(cmds[i].name, argv[0], cl))
	  {
	    cmds[i].func(argc, argv);
	    return;
	  }

    cprintf(Warning, "Unknown command '%s'", argv[0]);
  }

void rcfile(const char* fname)
  {
    FILE*	fp = fopen(fname, "r");
    char	pad[1024];

    quiet++;
    if(fp)
      {
	cprintf(Info, "setup: Loading settings from %s", fname);
	while(fgets(pad, 1023, fp))
	  {
	    char*	s = pad;
	    while(*s<=' ')
		s++;
	    char*	e = s+strlen(s);
	    while(e>s)
	      {
		--e;
		if(*e<=' ')
		    *e = 0;
		else
		    break;
	      }
	    ccommand(s);
	  }
	fclose(fp);
      }
    --quiet;
  }

