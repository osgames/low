////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"

#include "map.H"
#include "entity.H"
#include "game.H"
#include "namedf.H"
#include "sound.H"
#include "manip.H"

struct cconfig_t {
    short	id;
    short	gump;
    short	w;
    short	h;
    short	rows;
    short	cols;
    short	xo;
    short	yo;
};

static cconfig_t cconfig[] = {
      { 0x15b, 0x148, 106, 128, 3, 3, 20, 20, }, // barrel
      { 0x15d, 0x14a,  98, 128, 2, 3, 10, 54, }, // a chest
      { 0x080, 0x14b, 116, 128, 3, 3, 18, 24, }, // a sack
      { 0x081, 0x14b, 116, 128, 3, 3, 18, 24, }, // a sack
      { 0x082, 0x149, 128, 116, 2, 3, 28, 42, }, // a pack
      { 0x083, 0x149, 128, 116, 2, 3, 28, 42, }, // a pack
      { 0x084, 0x14c, 128, 116, 2, 4, 20, 46, }, // a box
      { 0x085, 0x14c, 128, 116, 2, 4, 20, 46, }, // a box
      { 0 },
};

ENT_USE_FUN(container)
  {
    static float	gumpx = .1;
    static float	gumpy = .5;

    if(!o->panel)
      {
	int	g;
	for(g=0; cconfig[g].id; g++)
	    if(cconfig[g].id == o->type)
		break;
	if(!cconfig[g].id)
	    g = 0;
	o->panel = new ManipulatorPanel(new ContentManipulator(o), cconfig[g].gump,
		gumpx, gumpy, cconfig[g].w/128.0, cconfig[g].h/128.0,
		cconfig[g].cols, cconfig[g].rows,
		cconfig[g].xo/128.0, cconfig[g].yo/128.0);
	gumpx += .1;
	gumpy -= .1;
	if(gumpx > .6)
	    gumpx = .1;
	if(gumpy < .1)
	    gumpy = .5;
      }
    return true;
  }

