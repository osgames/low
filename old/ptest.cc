////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#define WANT_SDL
#include "setup.H"

#include "particle.H"

Particle::Generator*	gens = 0;

void main_key(unsigned char k, int, int)
  {
    if(k==27)
	exit(0);
    glutPostRedisplay();
  }

void main_reshape(int w, int h)
  {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, float(w)/h, 0.01, 8.0);
  }

static void main_display(void)
  {
    static float       kol[]  = { 1.0, 1.0, 1.0, 1.0 };
    static float       lpos[] = { 1.0, -3.0, .7, 1.0 };
    static float       lcol[] = { 1.0, 1.0, 1.0, 1.0 };

    glClearColor(0.2, 0.1, 0.1, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(.5, 2, .5, 0, 0, .5, 0, 0, 1);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lcol);
    glLightfv(GL_LIGHT0, GL_POSITION, lpos);

    glDisable(GL_BLEND);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, lcol);
    glBegin(GL_TRIANGLE_STRIP);
      glVertex3f(-1, -1, 0);
      glVertex3f(-1, -1, 1);
      glVertex3f(+1, -1, 0);
      glVertex3f(+1, -1, 1);
    glEnd();

    if(gens)
	gens->render();

    glutSwapBuffers();
  }

void main_timer(int)
  {
    if(gens)
	gens->tick();
    glutPostRedisplay();
    glutTimerFunc(40, main_timer, 0);
  }

int main(int argc, char** argv)
  {
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutInit(&argc, argv);

    setvbuf(stdout, 0, _IONBF, 0);
    glutCreateWindow("Model test");

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glutDisplayFunc(main_display);
    glutReshapeFunc(main_reshape);
    glutKeyboardFunc(main_key);
    glutTimerFunc(40, main_timer, 0);

    glutPostRedisplay();

#if 0
    gens = new Particle::Generator(300, "smoke", "smoke");
    gens->col[0] = 1;
    gens->col[1] = 1;
    gens->col[2] = 1;
    gens->col[3] = .5;
    gens->xyz[0] = gens->xyz[1] = gens->xyz[2] = 0;
    gens->xrange = .1;
    gens->yrange = .02;
#else
    gens = new Particle::Generator(100, "splash", "splash");
    gens->col[0] = .5;
    gens->col[1] = .5;
    gens->col[2] = 1;
    gens->col[3] = .3;
    gens->xyz[0] = gens->xyz[1] = gens->xyz[2] = 0;
    gens->xrange = .02;
    gens->yrange = .02;
#endif

    glutMainLoop();
  }


