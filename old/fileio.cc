////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"

#include "fileio.H"
#include "game.H"


static char	fn[256];

File openlow(const char* fname, bool bin, bool write)
  {
    const char*	mode = write? "w": bin? "rb": "r";
    const char*	basename = strchr(fname, '/');

    if(basename)
	basename++;
    else
	basename = fname;

    if(!basename[0])
	return 0;

    sprintf(fn, "%s/save/%s", Game::setup.low_path ?: ".", basename);
    if(FILE* fp=fopen(fn, mode))
	return fp;
    if(write)
	return 0;
    sprintf(fn, "%s/data/%s", Game::setup.low_path ?: ".", fname);
    if(FILE* fp=fopen(fn, mode))
	return fp;
    return 0;
  }

File openuw2(const char* fname)
  {
    if(Game::setup.uw2_path)
      {
	sprintf(fn, "%s/%s", Game::setup.uw2_path, fname);
	if(FILE* fp=fopen(fn, "rb"))
	    return fp;
      }
    sprintf(fn, "%s/%s", Game::setup.low_path ?: ".", fname);
    if(FILE* fp=fopen(fn, "rb"))
	return fp;
    return 0;
  }

void File::close(void)
  {
    if(fp)
	fclose(fp);
    fp = 0;
  }

size_t File::size(void)
  {
    if(!fp)
	throw "fileio: bad size()";
    long	now = ftell(fp);
    size_t	len = 0;

    fseek(fp, 0, SEEK_END);
    len = ftell(fp);
    fseek(fp, now, SEEK_SET);
    return len;
  }

void File::seek(off_t off)
  {
    if(!fp)
	throw "fileio: bad seek()";
    fseek(fp, off, SEEK_SET);
  }

void File::read(void* ptr, size_t len)
  {
    if(!fp || fread(ptr, 1, len, fp) != len)
	throw "fileio: bad read()";
  }

void File::write(const void* ptr, size_t len)
  {
    if(!fp || fwrite(ptr, 1, len, fp) != len)
	throw "fileio: bad write()";
  }

bool File::readln(void* ptr, size_t max)
  {
    if(!fp)
	throw "fileio: bad readln()";
    return fgets((char*)ptr, max, fp);
  }

int File::scan(const char* fmt, ...)
  {
    if(!fp)
	throw "fileio: bad scan()";

    va_list	ap;

    va_start(ap, fmt);
    int ret = vfscanf(fp, fmt, ap);
    va_end(ap);

    return ret;
  }

void File::print(const char* fmt, ...)
  {
    if(!fp)
	throw "fileio: bad print()";

    va_list	ap;

    va_start(ap, fmt);
    vfprintf(fp, fmt, ap);
    va_end(ap);
  }

