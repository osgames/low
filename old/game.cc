////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"

#include "md.H"
#include "events.H"

#include "main.H"
#include "game.H"
#include "panel.H"
#include "sound.H"
#include "map.H"
#include "console.H"
#include "fileio.H"
#include "textures.H"
#include "strings.H"
#include "objects.H"

#include <sys/time.h>

// not including "solids.H" because ode pollutes our namespace greatly
extern void initialize_solids(void);

Game::Setup	Game::setup = {
    			1000,			// paperdoll_anim
			100,			// paperdoll_model

    			Game::Setup::High,	// wall_detail
    			Game::Setup::High,	// floor_detail
    			Game::Setup::High,	// texture_detail
    			Game::Setup::High,	// anim_detail
			true,			// fragment_programs
			true,			// specular_highlights
			true,			// do_shadows
			50,			// music_volume
			85,			// sound_volume
			14,			// sight_depth
			1.3,
			true,			// footsteps
			false,			// mouselook
			false,			// yrev

			false,			// grid_debug
			false,			// martians_debug
};

Game::Player	Game::player = {
    			1,			// level
			19.5, 48.5, 3.4,	// x, y, z
			270,			// heading
			0,			// mobile

			2,			// light (defaults to torch)

      			{
			  false,		// female
			  false,		// fighting
			  15, 15, 15,		// str, dex, int
			  50, 50,		// hp and max
			  50, 50,		// mp and max
			  false,		// poison
			  1,			// level
      			},
			0, 50,			// xp, next, level
			1,			// skill points
			
			"Coren",		// name

			// everything else at zero for now
};

float		Game::mousex = 0;
float		Game::mousey = 0;

unsigned int	Game::cursor_ii = 0;
int		Game::cursor_x = -1;
int		Game::cursor_y = -1;

int		Game::paused = 0;

Light*		Game::plight = 0;

Game::Mode*	Game::mode;

int		font = 0;


extern void check_memory_arena(void);

bool	Game::fullscreen = false;

static void render_function(float arw, float arh, float ar)
  {
    if(Game::mode && Game::mode->refresh)
	Game::mode->refresh(arw, arh, ar);
  }

bool Game::startup_video(void)
  {
    const VideoMode** vms = video_initialize(render_function, true);

    for(int i=0; vms[i]; i++) {
	if(   ( Game::fullscreen && vms[i]->fullscreen && vms[i]->width==want_w && vms[i]->height==want_h
		    && (!want_screen || want_screen==vms[i]->screen+1))
	   || (!Game::fullscreen && !vms[i]->fullscreen && vms[i]->width>=want_w && vms[i]->height>=want_h)) {
	    vms[i]->select(want_w, want_h);
	    break;
	}
    }

    //cprintf(Debug, "video: attempting to set gamma to %g", Game::setup.gamma);
    //SDL_SetGamma(Game::setup.gamma, Game::setup.gamma, Game::setup.gamma);

    //SDL_ShowCursor(SDL_DISABLE);
    //SDL_WM_SetCaption("Labyrinth of Worlds", "LoW");

    //cprintf(Info, "video: initialised %dx%d %s video mode", want_w, want_h, full? "fullscreen": "windowed");
    cprintf(Info, "video: %s OpenGL version %s", glGetString(GL_VENDOR), glGetString(GL_VERSION));
    cprintf(Info, "video: rendering with %s", glGetString(GL_RENDERER));

    return false;
  }

bool Game::startup_sound(int quality)
  {
    cprintf(Info, "audio: no audio support");

    return false;
  }

static bool	mlook = false;

void Game::mouselook(bool ml)
  {
    if(ml==mlook)
	return;
    mlook = ml;
  }

static Timer	tick_timer;

static void tick_function(void) {
    new Event(Event::GameEvent, 0);
}

int game(void)
  {
    Game::console_p = new ScrollPanel(0, .69, 1, 12);
    Game::console_p->shown = true;
    Game::console_p->console = true;
    strcpy(Game::console_p->inb, "> ");
    Game::console_p->inp = 2;

    rcfile("low.ini");

    if(Game::startup_video())
      {
	fprintf(stderr, "Unable to set video mode\n");
	return 1;
      }

    Textures::initialize();
    // at this point, we can start drawing to the screen.

    TextImage::initialize();
    initialize_solids();
    Game::startup_sound(0);
    if(Game::str.load("data/strings.pak"))
	throw "UW2 data files not found; check the README file.";
    Object::initialize();

    cprintf(Info, "Starting game... go! go! go!");

    Game::mode_load();

    Game::map.load(1);

    Game::start();
    tick_timer.start(40000, tick_function);

    bool	finished = false;
    bool	dorefresh = true;
    //bool	ignore_motion = true;
    while(!finished)
      {
	timeval	tv;

	gettimeofday(&tv, 0);
	if(Event* e = Event::wait()) switch(e->type)
	  {
	    case Event::Active:
		Game::paused--;
		break;

	    case Event::Inactive:
		Game::paused++;
		break;

	    case Event::KeyDown:
		if(Game::mode->keydown)
		    Game::mode->keydown(e->which, 0);
		break;

	    case Event::KeyHit:
		if(Game::mode->keypress)
		    Game::mode->keypress(e->which);
		break;

	    case Event::KeyUp:
		if(Game::mode->keyup)
		    Game::mode->keyup(e->which);
		break;

	    case Event::Motion:
		if(Game::mode->motion)
		    Game::mode->motion(e->motion.x, e->motion.y);
		break;

	    case Event::Position:
		Game::mousex = e->motion.x;
		Game::mousey = e->motion.y;
		if(Game::mode->hover)
		    Game::mode->hover();
		break;

	    case Event::ButtonDown:
		if(Game::mode->press)
		    Game::mode->press(e->which);
		break;

	    case Event::ButtonUp:
		if(Game::mode->release)
		    Game::mode->release(e->which);
		break;

	    case Event::Quit:
		if(Game::mode->quit)
		    Game::mode->quit();
		else
		    finished = true;
		break;

	    case Event::GameEvent:
		switch(e->which)
		  {
		    case 0:
			Sound::tick();
			if(Game::mode->tick)
			    Game::mode->tick();
			//if(Game::setup.framelock)
			    dorefresh = true;
			break;
		  }
		break;
	  };

	if(dorefresh) {
	    dorefresh = false;
	    video_post_repaint();
	}

      }

    return 0;
  }



Map			Game::map;
Strings::Strings	Game::str;

