////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"

#include "text.H"
#include "textures.H"
#include "fileio.H"

static unsigned int	font_tm[3];

struct Metric
  {
    int	tex_ln;         // line of char in texture
    int	tex_x;          // x in texture
    int	tex_w;          // width in texture
    int	alead;
    int	mlead;
    int	dlead;
    int	aspace;
    int	mspace;
    int	dspace;
  };

struct KPair
  {
    char c1;
    char c2;
    int  kern;
  };


static Metric           fmetric[96];
static KPair		kpair[64];
static int		kp = 0;
static const float      PIXEL = 1.0/256.0;
static const float	CELL = 3.5/128.0;

bool TextImage::initialize(void)
  {
    /*
    font_tm[0] = tex_named("font0a");
    font_tm[1] = tex_named("font0b");
    font_tm[2] = tex_named("font0c");
    */
    font_tm[0] = tex_named("font1");

    File	f = openlow("metrics1.dat");
    for(int i=0; i<96; i++)
	f.scan("%d %d %d %d %d %d %d %d %d\n",
		&fmetric[i].tex_ln, &fmetric[i].tex_x, &fmetric[i].tex_w,
		&fmetric[i].alead, &fmetric[i].mlead, &fmetric[i].dlead,
		&fmetric[i].aspace, &fmetric[i].mspace, &fmetric[i].dspace);
    while(f.scan("%c %c %d\n", &kpair[kp].c1, &kpair[kp].c2, &kpair[kp].kern)==3)
	kp++;
    f.close();

    return false;
  }

void TextImage::set(const char* str)
  {
    if(alloced)
	delete[] buf;
    alloced = false;
    buf = (char*)str;
    w = strlen(str);
  }

void TextImage::fmt(const char* fmt, ...)
  {
    va_list	ap;
    char	pad[128];

    va_start(ap, fmt);
    vsnprintf(pad, 127, fmt, ap);
    pad[127] = 0;
    va_end(ap);

    w = strlen(pad);
    buf = new char[w+1];
    strcpy(buf, pad);
    alloced = true;
  }

void TextImage::render(float scale, float r, float g, float b, float a, bool ital)
  {
    glColor4f(r, g, b, a);
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    int	ax = 0;
    int mx = 0;
    int dx = 0;

    char	last = 0;

    tex(font_tm[0]);
    glBegin(GL_QUADS);
    for(int i=0; i<w; i++)
      {
	if(buf[i]=='~')
	    continue;

	Metric&	m = fmetric[buf[i]-' '];
	int	x = ax >? mx >? dx;
	float	ctx = m.tex_x*PIXEL;
	float	cty = m.tex_ln*PIXEL;
	float	ctw = m.tex_w*PIXEL;
	float	cth = 32*PIXEL;

	while( (m.aspace<1 || (x+m.alead > ax))
	    && (m.mspace<1 || (x+m.mlead > mx))
	    && (m.dspace<1 || (x+m.dlead > dx)))
	    --x;

	for(int f=0; f<kp; f++)
	    if(last==kpair[f].c1 && buf[i]==kpair[f].c2)
		x += kpair[f].kern;

	float	gx = CELL/32*x;
	float	gw = CELL/32*m.tex_w;
	float	gi = gx;

	if(ital)
	  {
	    gi += CELL/3;
	    gx -= CELL/32*m.mlead;
	    gi -= CELL/32*m.mlead;
	  }

	glTexCoord2f(ctx, cty+cth);	glVertex2f(gx, 0.0);
	glTexCoord2f(ctx, cty);		glVertex2f(gi, CELL);
	glTexCoord2f(ctx+ctw, cty);	glVertex2f(gi+gw, CELL);
	glTexCoord2f(ctx+ctw, cty+cth);	glVertex2f(gx+gw, 0.0);

	if(m.aspace>0)
	    ax = x + m.alead + m.aspace + 2;
	if(m.mspace>0)
	    mx = x + m.mlead + m.mspace + 2;
	if(m.dspace>0)
	    dx = x + m.dlead + m.dspace + 2;

	last = buf[i];
      }
    glEnd();

    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
  }

float TextImage::width(const char* buf, int w)
  {
    if(w<1)
	return 0;

    int	ax = 0;
    int mx = 0;
    int dx = 0;

    char	last = 0;

    for(int i=0; i<w; i++)
      {
	if(buf[i]=='~')
	    continue;

	Metric&	m = fmetric[buf[i]-' '];
	int	x = ax >? mx >? dx;

	while( (m.aspace<1 || (x+m.alead > ax))
	    && (m.mspace<1 || (x+m.mlead > mx))
	    && (m.dspace<1 || (x+m.dlead > dx)))
	    --x;

	for(int f=0; f<kp; f++)
	    if(last==kpair[f].c1 && buf[i]==kpair[f].c2)
		x += kpair[f].kern;

	if(m.aspace>0)
	    ax = x + m.alead + m.aspace + 2;
	if(m.mspace>0)
	    mx = x + m.mlead + m.mspace + 2;
	if(m.dspace>0)
	    dx = x + m.dlead + m.dspace + 2;

	last = buf[i];
      }

    return CELL/32*(ax >? mx >? dx);
  }

