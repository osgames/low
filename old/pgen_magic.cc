////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "game.H"
#include "map.H"
#include "particle.H"


PART_PTICK_FUN(moongate)
  {
    return p.age>64 || p.xyz[2]>0 || p.xyz[2]<-.8;
  }

PART_GTICK_FUN(moongate)
  {
    if(g.max_p-g.act_p < 10)
	return;

    int	gmax = 10;
    int	age = random()&31;

    float	dir = (random()&1)? -1: 1;
    float	vec[3] = {
	(0.2 - 0.05*drand48())*dir,
	0.001 - 0.002*drand48(),
	0.005 - 0.010*drand48(),
    };
    float	xyz[3] = {
	3.1416*drand48(),
	g.xrange + g.yrange*drand48(),
	-.7 + .6*drand48(),
    };
    float	col[3] = {
	.5+.5*(random()&1),
	.5+.5*(random()&1),
	.5+.5*(random()&1),
    };

    while(gmax)
      {
	g.lighted = true;

	Particle::Particle&	p = g.parts[g.act_p++];

	p.age = age++;
	p.radial = true;
	for(int i=0; i<3; i++)
	  {
	    p.vec[i] = vec[i];
	    p.xyz[i] = xyz[i];
	    p.col[i] = col[i];
	  }
	xyz[0] -= .03*dir;
	p.col[3] = (gmax+1.0)/11;
	p.size = 8;
	--gmax;
      }
  }

