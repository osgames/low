////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "main.H"


int	want_w = 800;
int	want_h = 600;
int	want_screen = 0;

FILE*	logfile;

int main(int argc, char** argv)
  {
    logfile = stderr;
    //int	arg = 1;
    // parse options
    try {
	_exit(game());
    }
    catch(const char* em)
      {
	fprintf(stderr, "\n\n*** %s\n", em);
	perror("last error");
      }
  }

