
////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "font.xpm"

unsigned short getsl(int c, int l)
  {
    const char*		s = font_xpm[3+l]+(c*16);
    unsigned short	n = 0;

    for(int i=0; i<16; i++)
      {
	n <<= 1;
	n |= (s[i]=='.')? 1: 0;
      }
    return n;
  }

int main(int, char**)
  {
    unsigned short	sl = 0;

    for(int l=0; l<32; l++)
	fwrite(&sl, 2, 1, stdout);
    for(int c=1; c<173; c++)
	for(int l=0; l<32; l++)
	  {
	    sl = getsl(c-1, l);
	    fwrite(&sl, 2, 1, stdout);
	  }
  }

