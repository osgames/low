////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "render.H"
#include "map.H"
#include "textures.H"
#include "model.H"
#include "entity.H"
#include "granny.H"
#include "game.H"
#include "console.H"

namespace Granny {

#define MODEL(id)	(FullModel::models[(id)-100])

    ////
    //// Ze New and Improved model mesh deformation caching thing
    ////
    //
    //  What we do now:  the first time we encounter a pair [model,anim] we generate an array of arrays of deformed
    //  vertices at the desired granularity, filling them with bone-deformed coordinates that we precalculate all
    //  at once.  Then, we simply render the appropriate 'frame' on request.
    //
    //  We no longer rummage through all possible animations on render; we expect the code that selects the animation
    //  to have done the lookup (with possible fallback) once and then kept a /valid/ anim id.  This means we can now
    //  hash on predictable ids rather than pointers.
    //

    static const float	GRANULE = 25;
    static const int	CACHE_SIZE = 1024;	// must be power of two!

    struct Frame
      {
	float*		deformed;
	float		rh_matrix[16];
	float		lh_matrix[16];
      };

    struct Frames
      {
	Frames*		more;		// next with same hash key
	int		id_model;
	int		id_anim;
	int		num_frames;
	Frame*		frames;
      };

    static Frames*	fcache[CACHE_SIZE];

    static Frame* frame(bool& done, int model, int anim, float time)
      {
	int		hash = (model*37+anim*113) & (CACHE_SIZE-1);
	Frames*		f = fcache[hash];

	while(f && (f->id_model!=model || f->id_anim!=anim))
	    f = f->more;

	if(!f)
	  {
	    f = new Frames;

	    f->id_model = model;
	    f->id_anim = anim;
	    f->more = fcache[hash];
	    fcache[hash] = f;

	    Frame	def[1024];
	    int		fr = 0;
	    FullModel*	m = MODEL(model);
	    AniModel*	am;

	    for(am=m->ani; am; am=am->next)
		if(am->id == anim)
		    break;

	    if(!am)
		am = m->ani;

	    if(!am)
	      {
		f->frames = new Frame[1];
		f->frames[0].deformed = new float[m->meshes.first->num_v*3];
		memcpy(f->frames[0].deformed, m->meshes.first->vertices, m->meshes.first->num_v*3*sizeof(float));
		f->num_frames = 1;
	      }
	    else
	      {
		bool done = false;

		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		while(!done)
		  {
		    glLoadIdentity();

		    m->meshes.first->deformed = new float[m->meshes.first->num_v*3];

		    done = m->apply(am, float(fr)/GRANULE);
		    m->bones.root->calculate();
		    m->meshes.first->apply_bones();

		    if(Bone* b = m->bones.byname("CP_Grasp_Rhand"))
		      {
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadMatrixf(b->cmatrix);
			float	mat[16];
			memcpy(mat, b->matrix, 16*sizeof(float));
			invert(mat);
			glMultMatrixf(mat);
			glGetFloatv(GL_MODELVIEW_MATRIX, def[fr].rh_matrix);
			glPopMatrix();
		      }
		    if(Bone* b = m->bones.byname("CP_Grasp_Lhand"))
		      {
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glLoadMatrixf(b->cmatrix);
			float	mat[16];
			memcpy(mat, b->matrix, 16*sizeof(float));
			invert(mat);
			glMultMatrixf(mat);
			glGetFloatv(GL_MODELVIEW_MATRIX, def[fr].lh_matrix);
			glPopMatrix();
		      }

		    def[fr++].deformed = m->meshes.first->deformed;
		  }
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();

		f->frames = new Frame[fr];
		for(f->num_frames=0; f->num_frames<fr; f->num_frames++)
		    f->frames[f->num_frames] = def[f->num_frames];
	      }
	  }

	int		fn = int(time*GRANULE+.5);

	if(fn>=f->num_frames)
	    return &f->frames[f->num_frames-1];

	done = false;
	return &f->frames[fn];
      }

    void precache(int model)
      {
	if(!MODEL(model))
	    return;
	if(!MODEL(model)->tid)
	    MODEL(model)->tid = tex_named(MODEL(model)->texture);
	for(AniModel* am=MODEL(model)->ani; am; am=am->next)
	  {
	    bool	done = true;
	    float	time = 0.0;

	    do {
		done = true;
		frame(done, model, am->id, time);
		time += 1.0/GRANULE;
	    } while(!done);
	  }
      }

    void RenderModels::precache(void)
      {
	for(int mn=0; mn<num_rm; mn++)
	    Granny::precache(rm[mn].id);
      }

    void RenderModels::render(void)
      {
	static float	white[] = { 1.0, 1.0, 1.0, 1.0 };
	static float	black[] = { 0.0, 0.0, 0.0, 1.0 };
	float		umatrix[16];

	glMaterialfv(GL_FRONT, GL_SPECULAR, black);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glGetFloatv(GL_MODELVIEW_MATRIX, umatrix);
	glTranslatef(pos[0], pos[1], pos[2]);
	glRotatef(heading, 0, 0, 1);
	glScalef(scale, scale, -scale);

	ani_done = true;

	for(int mn=0; mn<num_rm; mn++)
	  {
	    FullModel*	m = MODEL(rm[mn].id);

	    glMatrixMode(GL_MODELVIEW);
	    glPushMatrix();

	    Frame*	fr = frame(ani_done, rm[mn].id, ani_seq, ani_time);
	    m->meshes.first->deformed = fr->deformed;
	    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, rm[mn].color);
	    glColor4fv(rm[mn].color);
	    if(rm[mn].color[3] < 1)
	      {
		glEnable(GL_BLEND);
		glDisable(GL_ALPHA_TEST);
	      }
	    else
	      {
		glDisable(GL_BLEND);
		glEnable(GL_ALPHA_TEST);
	      }
	    if(rm[mn].id<200 || rm[mn].id>299)
		m->render();
	    else
	      {
		fr = frame(ani_done, rm[0].id, ani_seq, ani_time);
		if(rm[mn].left)
		    glMultMatrixf(fr->lh_matrix);
		else
		    glMultMatrixf(fr->rh_matrix);
		m->render();
	      }

	    glMatrixMode(GL_MODELVIEW);
	    glPopMatrix();
	  }

	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, white);

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
      }

    bool FullModel::render(void)
      {
        Mesh*           m = meshes.first;
        Texture*        tx = textures.tex;

        if(!m || !tx)
            return false;

	if(!tid)
	    tid = tex_named(texture);

	if(tid)
	    tex(tid);

        glAlphaFunc(GL_GREATER, .3);

        //glTranslatef(-bones.root->xlate[0], -bones.root->xlate[1], -bones.root->xlate[2]);

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, m->deformed);

        glBegin(GL_TRIANGLES);
        for(int t=0; t<m->num_tri; t++)
          {
            for(int v=0; v<3; v++)
              {
                glNormal3fv(m->normals+m->tri_norm[t*3+v]*3);
                glTexCoord2fv(m->texcoords+tx->tri_tc[t*3+v]*2);
		glArrayElement(m->tri_verts[t*3+v]);
              }
          }
	glEnd();

	glDisableClientState(GL_VERTEX_ARRAY);

	return false;
      }

}; // namespace Granny


static int	rx[256];
static int	ry[256];
static int	rn = 0;

// Ugly hack: rendering the liquids will also render particle
// systems; we want this because this is the "right" time to
// render blended images after the depth buffer is all filled
// up.

void render_liquids(void)
  {
    static float    da[3] = { 0, 0, 1 };

    glDepthMask(0);
    glDisable(GL_ALPHA_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LIGHTING);
    glEnable(GL_POINT_SMOOTH);
#if defined(GL_DISTANCE_ATTENUATION_EXT)
#ifdef WIN32
    static PFNGLPOINTPARAMETERFVEXTPROC glPointParameterfvEXT = 0;
    if(!glPointParameterfvEXT)
	glPointParameterfvEXT = (PFNGLPOINTPARAMETERFVEXTPROC) SDL_GL_GetProcAddress("glPointParameterfvEXT");
    if(glPointParameterfvEXT)
#endif
    glPointParameterfvEXT(GL_DISTANCE_ATTENUATION_EXT, da);
#elif defined(GL_DISTANCE_ATTENUATION_ARB)
    glPointParameterfvARB(GL_DISTANCE_ATTENUATION_ARB, da);
#elif defined(GL_DISTANCE_ATTENUATION)
    glPointParameterfv(GL_DISTANCE_ATTENUATION, da);
#endif

    for(int i=0; i<rn; i++)
      {
	Tile&	t = Game::map(rx[i], ry[i]);

	ur_setup(rx[i], ry[i]);
	ur_render_liquid(rx[i], ry[i]);
	tex_none();
	glLoadName((rx[i]<<8)|ry[i]); glPushName(12);
	if(t.pgen)
	    t.pgen->render();
	glPopName(); glLoadName(0x10000 | (rx[i]<<8) | ry[i]); glPushName(0);
	for(EIndex e=t.f_ent; e; e=e->next)
	    if(e->parts)
	      {
		glLoadName(int(e));
		e->parts->xyz[0] = e->x - rx[i];
		e->parts->xyz[1] = e->y - ry[i];
		e->parts->xyz[2] = e->z;
		e->parts->render();
	      }
	glPopName();
      }
    rn = 0;

    glDisable(GL_BLEND);
    glEnable(GL_LIGHTING);
    glDepthMask(1);
  }

void render_tile(int x, int y)
  {
    // we remember which tiles we render so we can render the
    // liquids later.

    rx[rn] = x;
    ry[rn++] = y;

    Tile&	t = Game::map(x, y);

    glLoadName((x<<8)|y); glPushName(0);
    ur_render_tile(x, y);

    glPopName();
    glLoadName(0x10000 | (x<<8) | y);
    glPushName(0);
    for(EIndex e=t.f_ent; e; e=e->next)
      {
        glLoadName(int(e));
	e->render();
      }
    glPopName();
  }

