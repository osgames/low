////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"
#include "events.H"

#include "game.H"
#include "map.H"
#include "panel.H"
#include "sound.H"
#include "console.H"
#include "textures.H"
#include "worldg.H"
#include "solids.H"
#include "objects.H"

using namespace Game;

extern Mode	explore_mode;

static LabelPanel*	fps_p = 0;
static ScrollPanel*	text_p = 0;
static LabelPanel*	select_p = 0;
ScrollPanel*		Game::console_p = 0;

static float		console_y = 1.0;

//static float		viewz = 1.0;
static float		viewh = 0.0;
static float		plp;
static int		mz = 1;

static bool		hidepanels = false;
static bool		mlooking = false;
static bool		suppress_mlooking = false;

static Object*		target = 0;
static Object*		dtarget = 0;

enum _Doing { doNothing=0, doAim, doTarget, };
static _Doing		doing = doNothing;
static int		no_good = 0;

enum _move { mStand, mWalk, mRun, mWalkRelative, mStepLeft, mStepRight, mTurnLeft, mTurnRight, mBackPedal };

static _move	do_move = mStand;
static _move	do_mouse_move = mStand;
static bool	do_move_button;
static bool	keydown[512];

extern void render_render_world(void);

static void render_world(bool select=false)
  {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0.0, 0.0, 0.0, 0.0, 0.2, 0.0, 0.0, 0.0, 1.0);

    glRotatef(plp, 1.0, 0.0, 0.0);
    glRotatef(player.p_h, 0.0, 0.0, 1.0);
    glTranslatef(-player.p_x, -player.p_y, -(player.p_z+.25));

    glPushAttrib(GL_ENABLE_BIT);
    render_render_world();
    glPopAttrib();

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    glDisable(GL_LIGHTING);
    glDisable(GL_FOG);
  }

static void render_panels()
  {
    if(!hidepanels)
	Panel::render_all(false);
  }

static void render_cursor(void)
  {
    float	mx = Game::mousex;
    float	my = Game::mousey;
    float	d = 0;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);

    if(no_good)
      {
	no_good--;
	d = .01;
	tex(0x12B);
      }
    switch(doing)
      {
	case doNothing:
	    d = .007;
	    if(mlooking && !suppress_mlooking) {
		Game::mousex = mx = 2.0/3.0;
		Game::mousey = my = 0.5;
		tex(0x120);
	    }
	    else {
		mz = (mousey<.33)? 3: 0;
		mz += (mousex<(1.0/3.0))? 0: (mousex>1)? 2: 1;

		switch(mz) {
		    case 0: tex(0x127); break;
		    case 1: tex(0x121); break;
		    case 2: tex(0x128); break;
		    case 3: tex(0x125); break;
		    case 4: tex(0x122); break;
		    case 5: tex(0x126); break;
		}
	    }
	    break;
	case doAim:
	    d = .01;
	    tex(0x129);
	    break;
	case doTarget:
#if 0
		if(target_with)
		  {
		    tex(target_with->type + 0x200);
		    glColor3f(1, 1, 1);
		    glBegin(GL_QUADS);
		    glTexCoord2f(0, 1); glVertex3f(mx+.02-.03, my+.02-.03, 1);
		    glTexCoord2f(1, 1); glVertex3f(mx+.02+.03, my+.02-.03, 1);
		    glTexCoord2f(1, 0); glVertex3f(mx+.02+.03, my+.02+.03, 1);
		    glTexCoord2f(0, 0); glVertex3f(mx+.02-.03, my+.02+.03, 1);
		    glEnd();
		  }
#endif
	    d = .01;
	    tex(0x12A);
	    break;
      }

    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
        glTexCoord2f(0, 1); glVertex3f(mx+d-.03, my+d+.03, 0);
        glTexCoord2f(1, 1); glVertex3f(mx+d+.03, my+d+.03, 0);
        glTexCoord2f(1, 0); glVertex3f(mx+d+.03, my+d-.03, 0);
        glTexCoord2f(0, 0); glVertex3f(mx+d-.03, my+d-.03, 0);
    glEnd();

    glDisable(GL_BLEND);
  }


#if 0
static unsigned int	sel_buf[5000];
static int		sel_hits;
static int		select_major = 0;
static int		select_minor = 0;
#endif

static int		now;
static int		dtwhen;

static float		peangle;
static float		peratio;

void set_perspective(void)
  {
    gluPerspective(peangle, peratio, 0.01, 32.0);
  }

static void explore_render(float arw, float arh)
  {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);

    peangle = atan(arh) * 180/M_PI; // ... == sin(45o)
    peratio = arw/arh;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    set_perspective();

    render_world();

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho((2.0/3.0)-(arw/2), (2.0/3.0)+(arw/2), 0.5-(arh/2), 0.5+(arh/2), -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glColor3f(1, 1, 1);

    render_panels();
    render_cursor();
  }

static void explore_refresh(float arw, float arh, float)
  {
    console_p->y = console_y;

    explore_render(arw, arh);

#if 0
    if(ft[f])
	fps_p->ti.fmt("%5.1f fps", 15000.0/(ft[(f-1)&15]-ft[f]));
#endif

    if(select_p) {
	select_p->ti.fmt("%2d,%2d %08x", cursor_x, cursor_y, cursor_ii);
	select_p->w = .01*(select_p->ti.w+1);
	select_p->shown = true;
    }

    if((cursor_ii>>16)==2 && cursor_ii&65535)
	target = map.o[cursor_ii&65535];
    else
	target = 0;
  }

static void explore_init(void)
  {
    fps_p = new LabelPanel(0.01, .96, .15);
    fps_p->shown = true;
    fps_p->name = 1;
    select_p = new LabelPanel(0.01, .92, .2);
    select_p->shown = true;
    text_p = new ScrollPanel(0, 0, .8, 8);
    text_p->shown = true;
    text_p->small = true;
  }

static void startcon(void);
static void console_keypress(unsigned int key);

static inline bool vowel(char c)
  {
    return c=='a' || c=='e' || c=='i' || c=='o' || c=='u';
  }

void explore_press(int button)
  {
    switch(button) {
	case 3:
	    do_move_button = true;
	    break;
	case 1:
	    switch(cursor_ii>>16) {
		case 1: // level geometry
		    switch(cursor_ii&0xFF00) {
			case 0x0000: 
			    text_p->printf(200, "%s%s.", Game::str(1, 276), Game::str(10, 510-(cursor_ii&255)));
			    break;
			case 0x0100:
			    text_p->printf(200, "%s%s.", Game::str(1, 276), Game::str(10, cursor_ii&255));
			    break;
			case 0x0200:
			    text_p->printf(200, "%s%s.", Game::str(1, 276), Game::str(10, 511));
			    break;
		    }
		    break;
		case 2: // entity
		    if(target && target==dtarget && now<(dtwhen+500))
		      {
		      }
		    else if(doing==doTarget)
		      {
			doing = doNothing;
		      }
		    else
			target->look();
		    break;
	    }
    }
  }

void explore_mlook(float xmotion, float ymotion)
  {
    if(Game::setup.yrev)
	ymotion *= -1;

    plp += ymotion*30;
    if(plp<-60)
	plp = -60;
    if(plp>60)
	plp = 60;
    Game::player.p_h += xmotion*30;
    while(Game::player.p_h < 0)
	Game::player.p_h += 360;
    while(Game::player.p_h >= 360)
	Game::player.p_h -= 360;
    video_post_repaint();
  }

void explore_hover(void)
  {
  }

void explore_release(int button)
  {
    switch(button) {
	case 3:
	    do_move_button = false;
	    break;
    }
  }


void explore_keydown(unsigned int key, unsigned int mod)
  {
    keydown[key] = true;
    switch(key)
      {
	case K_Control_L:
	case K_Control_R:
	    video_mouselook_mode(mlooking = !Game::setup.mlook);
	    break;
	case K_m:
	    Game::player.p_z = 12;
	    break;
	case K_Space:
	case K_j:
	    if(Game::player.mob->grounded) {
		Game::player.mob->grounded = false;
		Game::player.mob->s[2] += (4.8-Game::player.p_z) <? 2.0;
	    }
	    break;
	case K_Escape:
	    if(mode==&explore_mode)
		new Event(Event::Quit);
	    break;
	case K_Tab:
	    hidepanels = !hidepanels;
	    break;
	case K_Grave:
	    startcon();
	    break;
      }
  }

void explore_keyup(unsigned int key)
  {
    keydown[key] = false;
    switch(key)
      {
	case K_Control_L:
	case K_Control_R:
	    video_mouselook_mode(mlooking = Game::setup.mlook);
	    break;
      }
  }

void console_tick(void)
  {
    if(console_y > .69)
	console_y = (console_y-.05) >? .69;
  }

void explore_tick(void)
  {
    do_mouse_move = mStand;
    if(do_move_button) {
	if(mlooking)
	    do_mouse_move = keydown[K_Shift_L]? mRun: mWalk;
	else {
	    switch(mz) {
		case 0:
		case 2:
		    Game::player.p_h += mz? 2: -2;
		case 1:
		    do_mouse_move = mWalkRelative;
		    break;
		case 3:
		    Game::player.p_h -= 5;
		    break;
		case 4:
		    do_mouse_move = mBackPedal;
		    break;
		case 5:
		    Game::player.p_h += 5;
		    break;
	    }
	}
    }

    if(keydown[K_w])
	do_move = mRun;
    else if(keydown[K_s])
	do_move = mWalk;
    else if(keydown[K_x])
	do_move = mBackPedal;
    else if(keydown[K_z])
	do_move = mStepLeft;
    else if(keydown[K_c])
	do_move = mStepRight;
    else
	do_move = do_mouse_move;

    if(keydown[K_a])
	Game::player.p_h += -5;
    else if(keydown[K_d])
	Game::player.p_h += 5;

    switch(do_move) {
	case mWalkRelative:
	    player.mob->t[0] = sin(player.p_h*M_PI/180)*(Game::mousey*4 <? 4.0);
	    player.mob->t[1] = cos(player.p_h*M_PI/180)*(Game::mousey*4 <? 4.0);
	    break;
	case mWalk:
	    player.mob->t[0] = sin(player.p_h*M_PI/180)*2;
	    player.mob->t[1] = cos(player.p_h*M_PI/180)*2;
	    break;
	case mRun:
	    player.mob->t[0] = sin(player.p_h*M_PI/180)*4;
	    player.mob->t[1] = cos(player.p_h*M_PI/180)*4;
	    break;
	case mBackPedal:
	    player.mob->t[0] = sin(player.p_h*M_PI/180)*-1.5;
	    player.mob->t[1] = cos(player.p_h*M_PI/180)*-1.5;
	    break;
	case mStepLeft:
	    player.mob->t[0] = sin((player.p_h-90)*M_PI/180)*2;
	    player.mob->t[1] = cos((player.p_h-90)*M_PI/180)*2;
	    break;
	case mStepRight:
	    player.mob->t[0] = sin((player.p_h+90)*M_PI/180)*2;
	    player.mob->t[1] = cos((player.p_h+90)*M_PI/180)*2;
	    break;
	case mTurnLeft:
	case mTurnRight:
	    Game::player.p_h += (do_move==mTurnLeft)? -5: 5;
	default:
	    player.mob->t[0] = player.mob->t[1] = player.mob->t[2] = 0;
	    break;
    }

    while(Game::player.p_h < 0)
	Game::player.p_h += 360;
    while(Game::player.p_h >= 360)
	Game::player.p_h -= 360;

    if(console_y < 1)
	console_y = (console_y+.05) <? 1;

    step_solids();

    Game::map.tick();

    tick++;
  }

Mode	explore_mode = {
    explore_keydown,
    0,
    explore_keyup,
    explore_hover,
    explore_press,
    explore_release,
    0,
    explore_tick,
    explore_refresh,
    explore_mlook
};

int	Game::tick = 0;

static Mode	console_mode = {
    0,
    console_keypress,
    explore_keyup,
    0,
    0,
    0,
    0,
    console_tick,
    explore_refresh,
    0
};

void Game::start(void)
  {
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_LIGHTING);
    glFrontFace(GL_CW);

    explore_init();
  }

static void load_screen(const char* what, int percent)
  {
#if 0
    int t = SDL_GetTicks();
    if(t < now+100)
	return;
    now = t;
#endif
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0, 1, 1, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glDisable(GL_LIGHTING);
    glDisable(GL_FOG);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_DEPTH_TEST);

    glColor4f(0, .5, 1, .8);
    glBegin(GL_QUADS);
      glVertex2f(.2, .88);
      glVertex2f(.2+.006*percent, .88);
      glVertex2f(.2+.006*percent, .9);
      glVertex2f(.2, .9);
    glEnd();
    glColor4f(1, 1, 1, .8);
    glBegin(GL_LINE_LOOP);
      glVertex2f(.2, .88);
      glVertex2f(.8, .88);
      glVertex2f(.8, .9);
      glVertex2f(.2, .9);
    glEnd();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
#if 0
    SDL_GL_SwapBuffers();
#endif
  }

void Game::mode_load(void)
  {
    load_screen("Loading...", 0);

    load_screen("Loading...", 10);

    plp = 0;
    viewh = 0;
    //viewz = pp.z;

    if(keydown[K_Control_L]||keydown[K_Control_R])
	video_mouselook_mode(mlooking = !Game::setup.mlook);
    else
	video_mouselook_mode(mlooking = Game::setup.mlook);
    mode = &explore_mode;
  }

void Game::mode_leave(void)
  {
    mode_load();
  }

static void console_keypress(unsigned int key)
  {
    if(key >= ' ' && key<'~' && key!='`')
      {
	if(console_p->inp < 80)
	    console_p->inb[console_p->inp++] = key;
      }
    switch(key)
      {
	case 8:
	case 127:
	    if(console_p->inp > 2)
		--console_p->inp;
	    break;
	case 13:
	    console_p->inb[console_p->inp] = 0;
	    ccommand(console_p->inb+2);
	    strcpy(console_p->inb, "> ");
	    console_p->inp = 2;
	    break;
	case 27:
	case '~':
	case '`':
	    suppress_mlooking = false;
	    mode = &explore_mode;
	    break;
      }
  }

static void startcon(void)
  {
    suppress_mlooking = true;
    mode = &console_mode;
  }

void msg(const char* fmt, ...)
  {
    va_list	ap;

    va_start(ap, fmt);
    text_p->vprintf(200+(strlen(fmt)/40)*100, fmt, ap);
    va_end(ap);
  }

