////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "render.H"
#include "map.H"
#include "entity.H"
#include "game.H"
#include "namedf.H"
#include "sound.H"

ENT_NAME_FUN(wall)
  {
    return Game::str(10, e->mat);
  }

ENT_NAME_FUN(bridge)
  {
    return Game::str(10, 510-e->mat);
  }

ENT_LOOK_FUN(plaque)
  {
    msg(Game::str(8, e->special));
  }

ENT_RENDER_PRE_FUN(hide)
  {
    return false;
  }

ENT_RENDER_PRE_FUN(decal)
  {
    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    switch(e->mat)
      {
	case 20:
	    glTranslatef(0, -.06*Game::tick, 0);
	    break;
      }
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glPushMatrix();
    render_decal(e->facing, e->z, e->mat);
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    return false;
  }


ENT_TICK_FUN(moongate)
  {
    e->int1 += 1;
  }

static bool found[256];
ENT_RENDER_PRE_FUN(moongate)
  {
    float	r = 1.0;
    float	g = 1.0;
    float	b = 1.0;

    switch(e->special&255)
      {
	case 33: // red
	    b = g = 0.0;
	    break;
	case 79: // blue?
	    r = g = 0.0;
	    break;
	case 16: // yellow
	    g = 0.75;
	    b = 0.0;
	    break;
	case 90:
	case 91: // purple
	case 45:
	    g = 0.0;
	    break;
	case 194: // white
	    break;
	default:
	    if(!found[e->special&255])
	      {
		printf("gate: %d\n", e->special&255);
		found[e->special&255] = true;
	      }
	    break;
      }

    float	o = sin(.1*e->int1);

    float	emm[4] = { r, g, b, 0.8 };
    float	dif[4] = { 0, 0, 0, 1.0 };
    //float	pos[4] = { 0, o, .4, 1 };

    texture_hook(248);

    glMaterialfv(GL_FRONT, GL_SPECULAR, dif);
    glMaterialfv(GL_FRONT, GL_EMISSION, emm);
    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, dif);

    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glRotatef(e->int1, 0, 1, 0);
    glScalef(.3, .3, .3);
    glRotatef(120+o*40, 1, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    return true;
  }

ENT_RENDER_POST_FUN(moongate)
  {
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
  }

ENT_RENDER_PRE_FUN(none)
  {
    return false;
  }

ENT_RENDER_PRE_FUN(pillar)
  {
    float	tz = Game::map(e->tx, e->ty).calt * .03125;
    float	bz = (e->tz+4) * .03125;
    float	cz = bz - .25;

    glTranslatef(0, 0, -e->z);
    texture_hook(e->mat);
    while(bz > tz)
      {
	glBegin(GL_QUADS);
	  glNormal3f(0, 1, 0);
	  glTexCoord2f(  0, cz); glVertex3f(-.03,  .03,  cz);
	  glTexCoord2f(.06, cz); glVertex3f( .03,  .03,  cz);
	  glTexCoord2f(.06, bz); glVertex3f( .03,  .03,  bz);
	  glTexCoord2f(  0, bz); glVertex3f(-.03,  .03,  bz);
	  glNormal3f(0, -1, 0);
	  glTexCoord2f(.06, cz); glVertex3f( .03, -.03,  cz);
	  glTexCoord2f(  0, cz); glVertex3f(-.03, -.03,  cz);
	  glTexCoord2f(  0, bz); glVertex3f(-.03, -.03,  bz);
	  glTexCoord2f(.06, bz); glVertex3f( .03, -.03,  bz);
	  glNormal3f(1, 0, 0);
	  glTexCoord2f(.06, cz); glVertex3f( .03,  .03,  cz);
	  glTexCoord2f(  0, cz); glVertex3f( .03, -.03,  cz);
	  glTexCoord2f(  0, bz); glVertex3f( .03, -.03,  bz);
	  glTexCoord2f(.06, bz); glVertex3f( .03,  .03,  bz);
	  glNormal3f(-1, 0, 0);
	  glTexCoord2f(  0, cz); glVertex3f(-.03, -.03,  cz);
	  glTexCoord2f(.06, cz); glVertex3f(-.03,  .03,  cz);
	  glTexCoord2f(.06, bz); glVertex3f(-.03,  .03,  bz);
	  glTexCoord2f(  0, bz); glVertex3f(-.03, -.03,  bz);
	glEnd();
	bz = cz;
	cz = (cz-.5) >? tz;
      }

    return false;
  }

