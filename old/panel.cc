////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"

#include "game.H"
#include "panel.H"


Panel*	Panel::first = 0;
Panel*	Panel::last = 0;


Panel::Panel(float x_, float y_, float w_, float h_)
  {
    shown = false;
    movable = false;
    framed = true;
    x = x_;
    y = y_;
    w = w_;
    h = h_;
    name = 0;

    if((next = first))
	first->prev = this;
    else
	last = this;
    first = this;
    prev = 0;
  }

Panel::~Panel()
  {
    if(prev)
	prev->next = next;
    else
	first = next;
    if(next)
	next->prev = prev;
    else
	last = prev;
  }

void Panel::begin_render(bool select)
  {
    int		vport[4];

    if(select)
      {
	glGetIntegerv(GL_VIEWPORT, vport);
	//gluPickMatrix(Game::mousex, Game::screen_h-Game::mousey, 3, 3, vport);
	glLoadName(0xE0000|name);
      }

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    glTranslatef(x, y, 0);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  }

void Panel::end_render(void)
  {
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
  }

void Panel::render_frame(void)
  {
    if(!framed)
	return;
    glColor4f(0.0, 0.0, 0.0, 0.4);
    glBegin(GL_QUADS);
        glVertex2f(0, 0);
	glVertex2f(0, h);
	glVertex2f(w, h);
	glVertex2f(w, 0);
    glEnd();
  }

void Panel::render(bool select)
  {
    begin_render(select);
    render_frame();
    end_render();
  }

void Panel::render_all(bool select)
  {
    for(Panel* p=first; p; p=p->next)
	if(p->shown)
	    if(!select || p->name)
		p->render(select);
  }

void LabelPanel::render(bool select)
  {
    begin_render(select);
    render_frame();
    glColor4f(0.5, 0.5, 0.5, 1.0);
    glBegin(GL_LINE_LOOP);
        glVertex2f(0, 0);
	glVertex2f(0, h);
	glVertex2f(w, h);
	glVertex2f(w, 0);
    glEnd();
    glTranslatef(w/2 - (ti.w*.005), .005, 0);
    ti.render(1, 1, 1, 1);
    end_render();
  }

ScrollPanel::ScrollPanel(float x, float y, float w, int nl):
	Panel(x, y, w, nl*.025+.01)
  {
    l = new Line[num = nl];
    for(int i=0; i<nl; i++)
      {
	l[i].ti.set("");
	l[i].fade = 0;
	l[i].old = false;
	l[i].col = 0;
	l[i].ital = false;
      }
    small = false;
    console = false;
    inp = 0;
  }

ScrollPanel::~ScrollPanel()
  {
    if(l)
	delete[] l;
  }

void ScrollPanel::render(bool select)
  {
    if(console)
      {
	inb[inp] = '_';
	inb[inp+1] = 0;
	l[num-1].ti.w = inp+1;
	l[num-1].ti.buf = inb;
      }
    begin_render(select);
    for(int i=0; i<num; i++)
	if(l[i].ti.buf[0] || console)
	  {
	    h = .01 + (num-i)*.025;
	    render_frame();
	    break;
	  }
    glTranslatef(.005, .005, 0);
    for(int i=num-1; i>=0; i--)
      {
	float	alpha = 1.0;

	if(l[i].fade)
	  {
	    if(l[i].fade < 50)
		alpha = .02 * l[i].fade;
	    if(!--l[i].fade)
		l[i].ti.set("");
	  }
	if(console && i<num-1)
	    l[i].ti.render(1, 1, 1, .5, alpha, l[i].ital);
	else
	    l[i].ti.render(1, 1, 1, 1, alpha, l[i].ital);
	glTranslatef(0, .025, 0);
      }
    end_render();
  }

void ScrollPanel::printf(int fade_, const char* fmt, ...)
  {
    va_list	ap;

    va_start(ap, fmt);
    vprintf(fade_, fmt, ap);
    va_end(ap);
  }

void ScrollPanel::vprintf(int fade_, const char* fmt, va_list ap)
  {
    char	pad[128];
    int		last = console? num-2: num-1;

    vsnprintf(pad, 127, fmt, ap);
    pad[128] = 0;

    l[0].ti.set("");
    for(int i=0; i<last; i++)
	l[i] = l[i+1];
    l[last].ti.buf = new char[128];
    strcpy(l[last].ti.buf, pad);
    l[last].ti.w = strlen(pad);
    l[last].ti.alloced = true;
    l[last].fade = fade_;
  }

