////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2005 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#ifndef LOW_OBJECTS_H__
#define LOW_OBJECTS_H__


struct Mobile;

struct Object_ondisk {
    unsigned short	type;
    unsigned short	link;		// for real
    unsigned short	property;
    unsigned short	enchantment;	// for real
    unsigned char	quantity;	// for real
    unsigned char	quality;
    unsigned short	next;
    unsigned long	flags: 6,
			facing: 3,
			tilex: 3,
			tiley: 3,
			tilez: 7,
			owner: 6;
};

struct OProp {
    unsigned long	height: 8,
			radius: 3,
			unk_1: 1,
			mass: 12,
			unk_2: 4,
			decal: 1,
			movable: 1,
			unk_3: 1,
			container: 1;
    unsigned short	value;
    unsigned short	unk_4: 2,
			qual: 2,
			unk_5: 5,
			kind: 4,
			unk_6: 2,
			ownable: 1;
    unsigned char	unk_7;
    unsigned char	unk_8;
    unsigned char	qualtype: 4,
			printable: 1,
			unk_9: 3;

    struct Melee {
	signed char	slash;
	signed char	bash;
	signed char	stab;
	char		unk_1[3];
	unsigned char	skill;
	unsigned char	durability;
    };

    struct Armor {
	unsigned char	protection;
	unsigned char	durability;
	unsigned char	unk_1;
	unsigned char	slot;
    };

    struct Container {
	unsigned char	capacity;
	unsigned char	accept;
	unsigned char	unk_1;
    };

    struct Light {
	unsigned char	brightness;
	unsigned char	duration;
    };
};

struct Object: public Object_ondisk {
    enum Location {
	Limbo, Tile, Inside,		// in the "level"
	Held, HeldInside,		// with the player
    };

    static OProp		oprop[512];
    static OProp::Melee		mprop[16];
    static OProp::Armor		aprop[32];
    static OProp::Container	cprop[16];
    static OProp::Light		lprop[16];

    unsigned char*		mob;
    unsigned short		num;
    Light*			light;
    Mobile*			mobile;
    Location			where;
    union {
	struct {
	    unsigned char		x;
	    unsigned char		y;
	}			    tile;
	unsigned short		    container;
	unsigned short		    slot;
    };


    static void			initialize(void);

    				Object(void);
				~Object();

    void			render(void);
    const char*			name(bool art, bool plur=false) const;
    void			tick(void);
    void			think(void);

    void			unlist(void);
    void			close_gumps(void);
    void			to_world(float x, float y, float z, int face);
    void			to_held(int slot);
    //void			to_inside(EIndex, EIndex before=0);

    void			consume(int replace);
    void			die(bool hard=false);

    void			act(void);
    void			pickup(void);
    void			use(bool f=false);
    void			talk(void);
    void			look(void) const;

    bool			movable(void) const		{ return oprop[type].movable; };
    bool			alive(void) const		{ return type>63 && type<128 /* && !dead */; };
    bool			trigger(void) const		{ return type>=0x1A0 && type<=0x1BF; };
    bool			trap(void) const		{ return type>=0x180 && type<=0x19F; };
};

#endif
