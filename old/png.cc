////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "png.H"

void PNG_reader::reader(png_structp pp, png_bytep buf, unsigned int len)
  {
    PNG_reader*		r = reinterpret_cast<PNG_reader*>(png_get_io_ptr(pp));

    r->read((char*)buf, (size_t)len);
  }

PNG_Context::PNG_Context(PNG_reader* rdr)
  {
    char	head[8];

    if(!rdr->valid())
      {
	delete rdr;
	throw PNG_Error("Unable to read file", errno);
      }
    reader = rdr;
    rdr->read(head, 8);
    if(png_sig_cmp((png_byte*)head, 0, 8))
      {
	delete rdr;
	throw PNG_Error("Not a PNG file", 0);
      }

    pp = 0;
    ip = 0;
    ep = 0;

    pp = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, _error_, 0);
    if(!pp)
      {
	delete rdr;
	throw PNG_Error("Unable to allocate read struct", 0);
      }
    ip = png_create_info_struct(pp);
    ep = png_create_info_struct(pp);
    if(!ip || !ep)
      {
	delete rdr;
	png_destroy_read_struct(&pp, ip? &ip: 0, ep? &ep: 0);
	throw PNG_Error("Unable to allocate info structs", 0);
      }
    png_set_sig_bytes(pp, 8);
    png_set_read_fn(pp, reader, PNG_reader::reader);
  }

PNG_Image* PNG_Context::read(void)
  {
    PNG_Image*		img = 0;
    unsigned char**	rows = 0;

	int	depth;
	int	color;

	img = new PNG_Image;

	png_read_info(pp, ip);
	png_get_IHDR(pp, ip, (png_uint_32*)&img->w, (png_uint_32*)&img->h, &depth, &color, 0, 0, 0);

	if(color == PNG_COLOR_TYPE_PALETTE)
	  {
	    png_set_palette_to_rgb(pp);
	    color = PNG_COLOR_TYPE_RGB;
	  }
	if(depth == 16)
	    png_set_strip_16(pp);
	if(color==PNG_COLOR_TYPE_GRAY || color==PNG_COLOR_TYPE_GRAY_ALPHA)
	  {
	    png_set_gray_to_rgb(pp);
	    color = (color==PNG_COLOR_TYPE_GRAY)? PNG_COLOR_TYPE_RGB: PNG_COLOR_TYPE_RGB_ALPHA;
	  }
	if(color==PNG_COLOR_TYPE_RGB)
	    png_set_filler(pp, 0xFF, PNG_FILLER_AFTER);

	png_read_update_info(pp, ip);

	rows = new unsigned char* [img->h];
	img->rgba = new unsigned char [img->w * ip->rowbytes + 1];
	img->rgba[img->w*ip->rowbytes] = 123;

	for(int i=0; i<img->h; i++)
	    rows[i] = img->rgba + ip->rowbytes * i;

	if(img->rgba[img->w*ip->rowbytes] != 123)
	    throw img;

	png_read_image(pp, rows);
	png_read_end(pp, ep);

    if(rows)
	delete[] rows;
    return img;
  }


PNG_Context::~PNG_Context()
  {
    png_destroy_read_struct(&pp, &ip, &ep);
  }

