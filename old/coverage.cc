////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "coverage.H"
#include "map.H"
#include "entity.H"

static struct bydist_ {
    int		x;
    int		y;
} bydist[708] = {
#include "by_dist.H"
};

static int	maxdist[16] = {
      0,   4,  12,  28,
     48,  80, 112, 148,
    196, 252, 316, 376,
    440, 528, 612, 708,
};

static unsigned char	hbuf[3600];

static const double	rad2deg = 1800.0 / M_PI;

struct coverage_ {
    short	from;
    short	num;
};

static coverage_& coverage(double dx, double dy, bool conservative)
  {
    static coverage_	c;

    double	k[4] = {
	atan2(dx-.5, -dy-.5),
	atan2(dx+.5, -dy-.5),
	atan2(dx-.5, -dy+.5),
	atan2(dx+.5, -dy+.5),
    };
    double	min = M_PI;
    double	max = -M_PI;

    for(int i=0; i<4; i++)
      {
	min <?= k[i];
	max >?= k[i];
      }

    if(max-min > M_PI)
      {
	min = 2*M_PI;
	max = 0;

	for(int i=0; i<4; i++)
	  {
	    if(k[i]<0)
		k[i] += 2*M_PI;
	    min <?= k[i];
	    max >?= k[i];
	  }
      }

    if(min<0)
      {
	min += 2*M_PI;
	max += 2*M_PI;
      }

    if(conservative)
      {
	c.from = int(min*rad2deg+1);
	c.num = int((max-min)*rad2deg-1);
      }
    else
      {
	c.from = int(min*rad2deg);
	c.num = int((max-min)*rad2deg+.999);
      }

    return c;
  }

static inline void cover(coverage_& c)
  {
    int		n = c.num;
    int		f = c.from;

    while(n--)
      {
	hbuf[f++] = 1;
	if(f>3599)
	    f = 0;
      }
  }

static inline bool check(coverage_& c)
  {
    int		n = c.num;
    int		f = c.from;

    while(n--)
      {
	if(!hbuf[f++])
	    return false;
	if(f>3599)
	    f = 0;
      }
    return true;
  }

void Coverage::add_tile(int tx, int ty)
  {
    x[n] = tx;
    y[n++] = ty;
  }

Coverage* covered_set(Level& map, int range, float px, float py)
  {
    int		tx = int(px+.5);
    int		ty = int(py+.5);
    int		num = maxdist[range];
    Coverage*	cov = new Coverage;

    cov->x = new int[num];
    cov->y = new int[num];
    cov->n = 0;

    // we always render the tile we are sitting on
    cov->add_tile(tx, ty);

    memset(hbuf, 0, 3600);

    for(bydist_* d=bydist; num--; d++)
      {
	int x = tx+d->x;
	int y = ty+d->y;

	if(x<0 || x>=map.w || y<0 || y>=map.h)
	    continue;

	double	cdx = float(x)-px;
	double	cdy = py-float(y);

	if(!map(x, y).mat)
	  {
	    cover(coverage(cdx, cdy, true));
	    continue;
	  }

	if(check(coverage(cdx, cdy, false)))
	    continue;

	for(EIndex e=map(x, y).f_ent; e; e=e->next)
	    if(e->type==322 || e->type==323) // doorframes
		if(e->flags & Entity::eSolid)
		  {
		    cov->add_tile(x, y);
		    cover(coverage(cdx, cdy, true));
		    continue;
		  }

	cov->add_tile(x, y);
      }

    return cov;
  }
