#include "setup.H"
#include <malloc.h>

#ifndef RELEASE
#include <mcheck.h>

struct Blook
  {
    bool		free;
    Blook*		prev;
    unsigned long	len;
    unsigned long	magic1;
    unsigned long	magic2;
  };

static Blook* blooks = 0;

static const unsigned long	magic1_ = 0xDEADBEEF;
static const unsigned long	magic2_ = 0xF00DFACE;


static void my_check(Blook* b);
static void* my_new(size_t len)
  {
    Blook*		b = (Blook*)malloc(len+sizeof(Blook)+8);
    unsigned long*	e = (unsigned long*)(((char*)b)+sizeof(Blook)+len);

    memset(b, 0x55, len+sizeof(Blook)+8);
    b->len = len;
    b->free = false;
    b->magic1 = magic1_;
    b->magic2 = magic2_;
    e[0] = magic2_;
    e[1] = magic1_;

    b->prev = blooks;
    blooks = b;

    my_check(b);

    void*	rv = ((char*)b)+sizeof(Blook);
    return rv;
  }

static void my_bad(const char*)
  {
    abort();
  }

static void my_check(Blook* b)
  {
    while(b)
      {
	unsigned long*	e = (unsigned long*)(((char*)b)+sizeof(Blook)+b->len);

	if(b->magic1!=magic1_ || b->magic2!=magic2_)
	    my_bad("never alloced?");
	else if(e[0]!=magic2_ || e[1]!=magic1_)
		my_bad("end trample!");
	b = b->prev;
	break;
      }
  }

#endif


void check_memory_arena(void)
  {
#ifndef RELEASE
    my_check(blooks);
#endif
  }

#ifndef RELEASE
static void my_delete(void* ptr)
  {
    if(ptr)
      {
	Blook*		b = (Blook*)(((char*)ptr)-sizeof(Blook));

	if(b->free)
	    my_bad("double free!");
	my_check(b);
	memset(ptr, 0xAA, b->len);
	//free(b);
      }
  }

void* operator new(size_t len)
  {
    return my_new(len);
  }

void* operator new[](size_t len)
  {
    return my_new(len);
  }

void operator delete(void* ptr)
  {
    my_delete(ptr);
  }

void operator delete[](void* ptr)
  {
    my_delete(ptr);
  }
#endif
