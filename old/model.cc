////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "fileio.H"
#include "model.H"
#include "console.H"

extern int yyparse(void);

LoadModel	model;
extern FILE*	yyin;
extern int	yydebug;

bool LoadModel::Load(const char* mname)
  {
    char	fname[64];
    File	fp;
    bool	parse = true;

    if(model.meshes)
	delete model.meshes;
    model.meshes = 0;

    snprintf(fname, 64, "models/%s.ase", mname);
    if( (fp = openlow(fname)) )
      {

	yyin = fp.fp;

	yydebug = 0;
	parse = yyparse();
	fp.close();
      }
    if(parse)
	cprintf(Warning, "models: unable to parse model %s", mname);
    return true;
  }

void LoadMesh::numv(int n)
  {
    v = new LoadVertex[nv_ = n];
    memset(v, 0, sizeof(LoadVertex)*nv_);
  }

void LoadMesh::numf(int n)
  {
    f = new LoadFace[nf_ = n];
    memset(f, 0, sizeof(LoadFace)*nf_);
  }

void LoadMesh::numtv(int n)
  {
    t = new LoadTVert[nt_ = n];
    memset(t, 0, sizeof(LoadTVert)*nt_);
  }

void LoadMesh::numtf(int)
  {
  }

void LoadModel::complete(void)
  {
  }

void LoadModel::mknm(void)
  {
    for(LoadMesh* m=meshes; m; m=m->next)
	m->mknm();
  }

void LoadModel::glrender(bool)
  {
    for(LoadMesh* m=meshes; m; m=m->next)
	m->glrender(false);
  }

void LoadModel::newmesh(void)
  {
    LoadMesh*	m = new LoadMesh;

    m->next = meshes;
    meshes = m;
  }

