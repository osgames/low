////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"

#include "fileio.H"
#include "programs.H"


namespace Programs
  {
    struct Program
      {
	unsigned int	id;
	bool		error;
      };

    static Program	vprogs[32];
    static Program	fprogs[64];

    void load_program(Program& p, int id, int type)
      {
	char	fname[128];

	snprintf(fname, 128, "shaders/%d.%cp", id, type==GL_VERTEX_PROGRAM_ARB? 'v': 'f');

	try
	  {
	    File	pfile = openlow(fname, false);
	    size_t	plen = pfile.size();
	    char	prog[plen];

	    pfile.read(prog, plen);
	    pfile.close();

	    int ep;
	    glGenProgramsARB(1, &p.id);
	    glBindProgramARB(type, p.id);
	    glProgramStringARB(type, GL_PROGRAM_FORMAT_ASCII_ARB, plen, prog);
	    glGetIntegerv(GL_PROGRAM_ERROR_POSITION_ARB, &ep);
	    if(ep >= 0) {
		glDeleteProgramsARB(1, &p.id);
		throw "program error";
	    }
	  }
	catch(...)
	  {
	    p.id = 0;
	    p.error = true;
	  }
      }

    void vp(int id)
      {
	if(vprogs[id].id)
	    glBindProgramARB(GL_VERTEX_PROGRAM_ARB, vprogs[id].id);
	else if(!vprogs[id].error)
	    load_program(vprogs[id], id, GL_VERTEX_PROGRAM_ARB);
      }

    void fp(int id)
      {
	if(fprogs[id].id)
	    glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, fprogs[id].id);
	else if(!fprogs[id].error)
	    load_program(fprogs[id], id, GL_FRAGMENT_PROGRAM_ARB);
      }


  }

