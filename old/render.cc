////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"

#include "map.H"
#include "textures.H"
#include "game.H"
#include "console.H"
#include "worldg.H"
#include "programs.H"
#include "light.H"
#include "fileio.H"
#include "model.H"
#include "objects.H"

using namespace Game;

void init_renderer(void)
  {
  }

void Geometry::render(const Light* l, const float* amb)
  {
    bool prog0 = true;

    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, v);

    if(l) {
	float	lp[] = { l->cur.x, l->cur.y, l->cur.z, 1.0 };
	float	ep[] = { player.p_x, player.p_y, player.p_z+.25, 1.0 };
	float	vd = setup.sight_depth;
	glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, 0, ep);
	glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, 1, lp);
	glProgramEnvParameter4fvARB(GL_FRAGMENT_PROGRAM_ARB, 0, l->cur.diffuse);
	glProgramEnvParameter4fARB(GL_FRAGMENT_PROGRAM_ARB, 1, 1.0/l->cur.radius, 40, (vd*vd)/2, 2/(vd*vd));
	if(amb)
	    glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, 5, amb);
    }

    Planar*	lastp = 0;
    for(int pn=0; pn<np; pn++) {
	Polygon&	p = this->p[pn];

	if(Game::setup.grid_debug) {
	    glColor3f(0.1, 0.1, 0.1);
	    glPushName(p.glname);
	    glDrawElements(GL_POLYGON, p.n, GL_UNSIGNED_SHORT, p.v);
	    glPopName();
	    glLineWidth(3);
	    glColor3f(1, 1, 0);
	    glDrawElements(GL_LINE_LOOP, p.n, GL_UNSIGNED_SHORT, p.v);
	    continue;
	}

	if(l) {
	    glActiveTexture(GL_TEXTURE1);
	    tex(p.tex);
	    glActiveTexture(GL_TEXTURE0);
	    if(p.tex<0x100 && !tex(p.tex+0x500)) tex(0x558); // known flat

	    if(p.p != lastp) {
		glMatrixMode(GL_MATRIX0_ARB);
		glLoadMatrixf((lastp = p.p)->T);
		glMatrixMode(GL_MODELVIEW);
		glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, 2, p.p->s);
		glProgramEnvParameter4fvARB(GL_VERTEX_PROGRAM_ARB, 3, p.p->t);
	    }

	    glProgramEnvParameter4fARB(GL_VERTEX_PROGRAM_ARB, 4, p.soffset, p.toffset, 0, 0);
	}

	glPushName(p.glname);
	if(p.ts) {
	    if(prog0) {
		Programs::vp(1);
		prog0 = false;
	    }
	    glBegin(GL_POLYGON);
	    for(int i=0; i<p.n; i++) {
		glTexCoord2f(p.ts[i], p.tt[i]);
		glArrayElement(p.v[i]);
	    }
	    glEnd();
	} else {
	    if(!prog0) {
		Programs::vp(0);
		prog0 = true;
	    }
	    glDrawElements(GL_POLYGON, p.n, GL_UNSIGNED_SHORT, p.v);
	}
	glPopName();
    }

    glDisableClientState(GL_VERTEX_ARRAY);
  }


static const float	black[] = { 0, 0, 0, 0 };

void render_tile(Tile& t, const Light* light, const float* amb) {
    Programs::vp(0);
    t.geometry->render(light, amb);
    if(t.objects) {
	Programs::vp(1);
	for(unsigned short on = t.objects; on; on=map.o[on]->next) {
	    Object&	o = *map.o[on];

	    if((o.flags&32)==0) {

	    }
	}
    }
}

void render_render_world(void)
  {
    glEnable(GL_CULL_FACE);
    glFrontFace(GL_CCW);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);

    PVS::vis	vi[257];
    int		nv = 1;
    int		va = int(trunc((player.p_h-180)*64/180))&127;
    int		vd = setup.sight_depth;

    vi[0].x = int(trunc(player.p_x));
    vi[0].y = int(trunc(player.p_y));
    if(PVS* p=map(vi[0].x, vi[0].y).pvs) {
	for(int i=14; i>3; --i)
	    if(p->last[i] < p->last[15]) {
		vd = i+1;
		break;
	    }
	if(vd>setup.sight_depth)
	    vd = setup.sight_depth;
	vd = setup.sight_depth;
	for(int i=0; i<p->last[vd]; i++) {
	    Tile&	t = map(p->v[i].x, p->v[i].y);
	    int		mi = (p->v[i].minv+5)&127;
	    int		ma = (p->v[i].maxv+5)&127;
	    if(t.type == 0)
		continue;
	    if(mi<ma) {
		if(va<mi || va>ma)
		    continue;
	    } else if(va>ma && va<mi)
		continue;
	    vi[nv++] = p->v[i];
	}
    }

    for(int i=0; i<nv; i++) {
	const PVS::vis&	v = vi[i];
	Tile&		t = map(v.x, v.y);

	if(!t.geometry || (t.flags&Tile::Dirty)) {
	    if(t.geometry)
		delete t.geometry;
	    t.geometry = new Geometry(v.x, v.y, t);
	    t.flags &= ~Tile::Dirty;
	}
    }

    if(!Game::setup.grid_debug) {
	glActiveTexture(GL_TEXTURE0);

	Programs::vp(0);
	Programs::fp(setup.specular_highlights? 3: 1);
	glEnable(GL_VERTEX_PROGRAM_ARB);
	glEnable(GL_FRAGMENT_PROGRAM_ARB);
    }

    glEnable(GL_FOG);
    glFogi(GL_FOG_MODE, GL_LINEAR);
    glFogf(GL_FOG_END, vd-1);
    glFogf(GL_FOG_START, vd-3);
    glFogfv(GL_FOG_COLOR, black);

    if(plight) {
	plight->cur.x = player.p_x;
	plight->cur.y = player.p_y;
	plight->cur.z = player.p_z+.2;
	plight->cur.radius = 2;
	plight->cur.diffuse[0] = .7;
	plight->cur.diffuse[1] = .4;
	plight->cur.diffuse[2] = .2;
    }

    static const float	black[] = { 0.02, 0.02, 0.02, 0.00 };
    static const float	brit[]  = { 0.08, 0.05, 0.04, 0.00 };

    for(int i=0; i<nv; i++) {
	Tile&		t = map(vi[i].x, vi[i].y);
	const float*	amb = brit;

	if(t.ambient_two)
	    amb = black;
	render_tile(t, plight, amb);
    }

    if(!Game::setup.grid_debug) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	Programs::fp(setup.specular_highlights? 2: 0);

	for(Light* l=map.lights; l; l=l->next)
	  {
	    if(l==plight)
		continue;

	    int	lx = int(trunc(l->cur.x));
	    int	ly = int(trunc(l->cur.y));

	    PVS*	lpvs = map(lx, ly).pvs->intersection(lx, ly, int(round(l->cur.radius)), vi, nv);

	    if(lpvs) {
		if(setup.do_shadows) {
		    if(lpvs && !l->shadows) {
			l->shadows = new Geometry(l);
		    }

		    glActiveTexture(GL_TEXTURE1);
		    glDisable(GL_TEXTURE_2D);
		    glActiveTexture(GL_TEXTURE0);
		    glDisable(GL_TEXTURE_2D);
		    glDisable(GL_VERTEX_PROGRAM_ARB);
		    glDisable(GL_FRAGMENT_PROGRAM_ARB);

		    glDepthMask(GL_FALSE);
		    glDisable(GL_CULL_FACE);

		    glEnable(GL_STENCIL_TEST);
		    glEnable(GL_STENCIL_TEST_TWO_SIDE_EXT);
		    glActiveStencilFaceEXT(GL_BACK);
		    glStencilFunc(GL_ALWAYS, 0, ~0);
		    glStencilOp(GL_KEEP, GL_DECR_WRAP, GL_KEEP);
		    glActiveStencilFaceEXT(GL_FRONT);
		    glStencilFunc(GL_ALWAYS, 0, ~0);
		    glStencilOp(GL_KEEP, GL_INCR_WRAP, GL_KEEP);
		    glDepthFunc(GL_LESS);
		    glColorMask(0, 0, 0, 0);
		    glClear(GL_STENCIL_BUFFER_BIT);

		    if(lpvs && l->shadows) {
			l->shadows->render(0);
		    }
		    glDisable(GL_STENCIL_TEST_TWO_SIDE_EXT);
		    glStencilFunc(GL_EQUAL, 0, ~0);
		    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

		    glColorMask(1, 1, 1, 1);
		    glEnable(GL_CULL_FACE);
		    glDepthMask(255);
		    glDepthFunc(GL_LEQUAL);

		    glActiveTexture(GL_TEXTURE1);
		    glEnable(GL_TEXTURE_2D);
		    glActiveTexture(GL_TEXTURE0);
		    glEnable(GL_TEXTURE_2D);
		    glEnable(GL_BLEND);
		    glEnable(GL_VERTEX_PROGRAM_ARB);
		    glEnable(GL_FRAGMENT_PROGRAM_ARB);
		}

		for(int i=0; i<=lpvs->last[14]; i++)
		    render_tile(map(lpvs->v[i].x, lpvs->v[i].y), l, 0);

		if(setup.do_shadows)
		    glDisable(GL_STENCIL_TEST);
	    }
	  }

	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glDisable(GL_VERTEX_PROGRAM_ARB);
	glDisable(GL_FRAGMENT_PROGRAM_ARB);
    }
  
    unsigned int	selb[512];

    glSelectBuffer(512, selb);
    glRenderMode(GL_SELECT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    video_select_at(Game::mousex, Game::mousey);
    set_perspective();

    for(int i=0; i<nv; i++) {
	Tile&		t = map(vi[i].x, vi[i].y);
	const float*	amb = brit;

	if(t.ambient_two)
	    amb = black;
	glPushName((vi[i].x<<8) | (vi[i].y));
	render_tile(t, 0, 0);
	glPopName();
    }

    int	nhits = glRenderMode(GL_RENDER);
    int	h = 0;

    unsigned int	minz  = 0xFFFFFFFF;
    unsigned int	hitxy = 0;
    unsigned int	hitii = 0;

    while(nhits-- > 0) {
	if(selb[h]>0 && selb[h+1]<minz) {
	    minz = selb[h+1];
	    hitxy = selb[h+3];
	    hitii = (selb[h]>1)? selb[h+4]: 0;
	}
	h += selb[h]+3;
    }

    Game::cursor_x = -1;
    Game::cursor_y = -1;
    Game::cursor_ii = 0;
    if(minz < 0xFFFFFFFF) {
	Game::cursor_x = (hitxy>>8) & 0xFF;
	Game::cursor_y = hitxy & 0xFF;
	Game::cursor_ii = hitii;
    }
  }

