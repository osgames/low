////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"

#include "fileio.H"
#include "strings.H"

namespace Strings {


    char HuffTree::snarf(BitStream& b)
      {
	Node*	n = nodes+root;

	while((n->left&n->right)!=0xFF)
	    n = nodes + (b.bit()? n->right: n->left);

	return n->sym;
      }

    Strings::Strings(void)
      {
	for(int i=0; i<17; i++)
	    hash[i] = 0;
      }

    Strings::~Strings()
      {
	for(int i=0; i<17; i++)
	    if(hash[i])
		delete hash[i];
      }

    bool Strings::Strings::load(const char* fname)
      {
#ifdef NO_STRINGS
	return false;
#endif
	File		sf = openuw2(fname);

	if(!sf)
	    return true;

	long	len = sf.size();

	unsigned char	buf[len];

	sf.read(buf, len);
	sf.close();

	HuffTree	ht;

	ht.num = *reinterpret_cast<short*>(buf);
	ht.root = ht.num-1;
	ht.nodes = reinterpret_cast<HuffTree::Node*>(buf+2);

#if 0
	for(int i=0; i<ht.num; i++)
	    printf("%d: %d <- '%c' -> %d\n", i, ht.nodes[i].left, ht.nodes[i].sym, ht.nodes[i].right);
#endif
	int	ndir = *reinterpret_cast<short*>(buf+2+4*ht.num);
	struct  dir_ {
	    unsigned short	id;
	    unsigned long	offs;
	} __attribute__ ((packed)) const *dir = reinterpret_cast<dir_*>(buf+4+4*ht.num);

	for(int i=0; i<ndir; i++)
	  {
	    Block*	b = new Block;

	    b->id = dir[i].id;
	    b->next = hash[b->id%17];
	    hash[b->id%17] = b;

	    const short*	blk = reinterpret_cast<short*>(buf+dir[i].offs);

	    b->num = blk[0];
	    b->str = new const char*[b->num];

	    for(int s=0; s<b->num; s++)
	      {
		BitStream	bs(reinterpret_cast<const unsigned char*>(blk+b->num+1)+blk[s+1]);
		char		pad[1024];
		char*		d = pad;

		while((*d = ht.snarf(bs)) != '|')
		  {
		    switch(*d)
		      {
			case 13:
			case 10:
			    *d = '~';
			    break;
		      }
		    d++;
		  }
		*d = 0;

		if(d-pad > 1023)
		    abort();

		if(pad[0])
		  {
		    char* str = new char[strlen(pad)+1];

		    strcpy(str, pad);
		    b->str[s] = str;
#if STRINGOUT
		    printf("%d/%d(%x): '%s'\n", b->id, s, s, str);
#endif
		  }
		else
		    b->str[s] = 0;
	      }
	  }
	return false;
      }

    Block& Strings::block(int i)
      {
	for(Block* b = hash[i%17]; b; b=b->next)
	    if(b->id==i)
		return *b;
	throw this;
      }

}; // namespace Strings

#if STRINGOUT

const unsigned char	test[] = { 0xFA, 0x50 };

int main(int, char**)
  {
    Strings::Strings	s;

    s.load("data/strings.pak");
  }

#endif
