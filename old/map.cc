////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2005 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"
#include "map.H"
#include "objects.H"
#include "fileio.H"
#include "worldg.H"
#include "light.H"
#include "game.H"
#include "solids.H"

PVS* PVS::intersection(int x, int y, int d, const vis* lv, int nv)
  {
    PVS*	u = new PVS;
    int		nu = 0;

    u->v = new vis[nv+1];
    for(int i=0; i<nv; i++) {
	if(lv[i].x==x && lv[i].y==y) {
	    u->v[nu].x = x;
	    u->v[nu++].y = y;
	} else for(int j=0; j<=last[d]; j++)
	    if(lv[i].x==v[j].x && lv[i].y==v[j].y) {
		u->v[nu++] = lv[i];
		break;
	    }
    }

    if(nu) {
	u->last[14] = nu-1;
	return u;
    }

    delete u;
    return 0;
  }


void Map::load(int lev)
  {
    char	fname[32];

    sprintf(fname, "levels/l%d.map", lev);
    File mf = openlow(fname, true);
    size_t		len = mf.size();
    unsigned char*	buf = new unsigned char[len];
    mf.read(buf, len);

    try
      {
	if(len<4 || buf[0]!='L' || buf[1]!='o' || buf[2]!='W' || buf[3]!='m')
	    throw "File for map is not a map file";
	buf += 4;
	len -= 4;
	while(len>1) {
	    switch(buf[0]) {
		case 0: // map data
		    w = buf[2];
		    h = buf[3];
		    t = new Tile[w*h];
		    memset(t, 0, w*h*sizeof(Tile));
		    buf += 4;
		    len -= 4;
		    memcpy(tpal, buf, 128);
		    buf += 128;
		    len -= 128;
		    for(int y=0; y<h; y++)
			for(int x=0; x<w; x++) {
			    int	l = buf[0];

			    memcpy(t+x+y*w, buf+1, sizeof(Tile_ondisk));

			    buf += l;
			    len -= l;
			}
		    break;
		case 1: // PVS
		    if(PVS* p = new PVS) {
			t[buf[2]+buf[3]*w].pvs = p;
			for(int i=2; i<16; i++)
			    p->last[i] = buf[i*2] + (buf[i*2+1]<<8);
			buf += 32;
			len -= 32;
			PVS::vis* v = new PVS::vis[p->last[15]];
			p->v = v;
			for(int i=0; i<p->last[15]; i++) {
			    v[i].x = buf[0];
			    v[i].y = buf[1];
			    v[i].minv = buf[2];
			    v[i].maxv = buf[3];
			    buf += 4;
			    len -= 4;
			}
		    }
		    break;
		case 2: // Objects
		    buf += 2;
		    len -= 2;
		    o = new Object*[1024];
		    memset(o, 0, 1024*sizeof(Object*));
		    while(len>2) {
			unsigned short	n = buf[0]+256*buf[1];

			if(!n)
			    break;

			Object*		ob = new Object;

			memcpy(ob, buf+2, sizeof(Object_ondisk));
			ob->mob = 0;
			o[n] = ob;
			buf += sizeof(Object_ondisk)+2;
			len -= sizeof(Object_ondisk)+2;
			if(n<256) {
			    ob->mob = new unsigned char[19];
			    memcpy(ob->mob, buf, 19);
			    buf += 19;
			    len -= 19;
			}
		    }
		    buf += 2;
		    len -= 2;
		    for(int i=2; i<1024; i++)
			if(o[i])
			    o[i]->num = i;

		    for(int x=0; x<64; x++)
			for(int y=0; y<64; y++) {
			    Tile& tt = t[x+y*w];
			    for(unsigned short on=tt.objects; on && o[on]; on=o[on]->next) {
				o[on]->where = Object::Tile;
				o[on]->tile.x = x;
				o[on]->tile.y = y;
				if(o[on]->movable()) {
				    Mobile* m = o[on]->mobile = new Mobile;
				    m->r = .0625;
				    m->x = .125*o[on]->tilex + .0625 + x;
				    m->y = .125*o[on]->tiley + .0625 + y;
				    m->z = .03125*o[on]->tilez + m->r;
				    m->stopped = false;
				    m->legs = 0.0;
				    m->grounded = false;
				    m->inert = true;
				    m->floating = false;
				    m->two_spheres = false;
				    m->s[0] = m->s[1] = m->s[2] = 0.0;
				    m->object = o[on];
				}
			    }
			}
		    break;
		default:
		    throw "Map file contains strange stuff";
	    }
	}

	lights = 0;
	Game::plight = new Light(*this, 0.0, 0.0, 0.0, 0, 2, .7, .4, .2);

	for(int i=0; i<64*64; i++) {
	    int	x = i&63;
	    int y = i>>6;
	    t[i].flags |= Tile::Dirty;
	    for(int on = t[i].objects; on; on=o[on]->next) {
		Object& o = *this->o[on];
		if(o.flags & 32)
		    continue;
		float ox = x+.125*o.tilex;
		float oy = y+.125*o.tiley;
		float oz = .03125*o.tilez;
		switch(o.type) {
		    case 0x095:
			new Light(*this, ox+.0625, oy+.0625, oz+.1, 1, 4, .7, .5, .3); break;
			break;
		    case 0x096:
			new Light(*this, ox+.0625, oy+.0625, oz+.1, 1, 3, .5, .35, .15); break;
			break;
		    case 0x16E:
		    case 0x16F:
			if(tpal[o.owner] == 0x66) switch(o.facing>>1) {
			    case 0: new Light(*this, x+.5, y+.95, oz+.35, 2, 6, .9, .7, .3); break;
			    case 1: new Light(*this, x+.95, y+.5, oz+.35, 2, 6, .9, .7, .3); break;
			    case 2: new Light(*this, x+.5, y+.05, oz+.35, 2, 6, .9, .7, .3); break;
			    case 3: new Light(*this, x+.05, y+.5, oz+.35, 2, 6, .9, .7, .3); break;
			}
			break;
		}
	    }
	    if(t[i].wall_tex == 0x66) {
		int	ltt = x?    t[i-1 ].type: 0;
		int	rtt = x<63? t[i+1 ].type: 0;
		int	btt = y?    t[i-64].type: 0;
		int	utt = y<63? t[i+64].type: 0;
		int	lfh = x?    t[i-1 ].height: 16;
		int	rfh = x<63? t[i+1 ].height: 16;
		int	bfh = y?    t[i-64].height: 16;
		int	ufh = y<63? t[i+64].height: 16;

		if(ltt==0 || ltt==3 || ltt==5) lfh = 16;
		if(rtt==0 || rtt==2 || rtt==4) rfh = 16;
		if(utt==0 || utt==4 || utt==5) ufh = 16;
		if(btt==0 || btt==2 || btt==3) bfh = 16;

		if(ufh>t[i].height && (t[i].type==1 || t[i].type==4 || t[i].type==5 || t[i].type>5))
		    new Light(*this, x+.5, y+.95, .25*t[i].height+.35, 2, 5, .8, .6, .4);
		if(rfh>t[i].height && (t[i].type==1 || t[i].type==2 || t[i].type==4 || t[i].type>5))
		    new Light(*this, x+.95, y+.5, .25*t[i].height+.35, 2, 5, .8, .6, .4);
		if(bfh>t[i].height && (t[i].type==1 || t[i].type==2 || t[i].type==3 || t[i].type>5))
		    new Light(*this, x+.5, y+.05, .25*t[i].height+.35, 2, 5, .8, .6, .4);
		if(lfh>t[i].height && (t[i].type==1 || t[i].type==3 || t[i].type==5 || t[i].type>5))
		    new Light(*this, x+.05, y+.5, .25*t[i].height+.35, 2, 5, .8, .6, .4);
	    }
	}
      }
    catch(...)
      {
	delete[] buf;
	throw;
      }
  }


void Map::tick(void)
  {
    for(Light* l=lights; l; l=l->next)
	l->tick();
  }

