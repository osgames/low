
#include "md.H"
#include "events.H"

#include "console.H"

#include <unistd.h>
#include <sys/poll.h>
#include <string.h>

#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/extensions/xf86vmode.h>
#include <X11/extensions/Xinerama.h>
#include <GL/glx.h>

#include <stdio.h>
#include <stdlib.h>

// X isn't quite properly multithreaded-- at least not on most
// platforms.  Easy way around: use a mutex to make sure exactly
// one thread at a time uses X.

static Mutex		x_mux;

static Display*			dpy;
static Window			top;
static Window			win;
static Colormap			cmap;
static GLXContext		glxc;
static Atom			delete_window;
static Thread			event_th;
static Thread			repaint_th;

static XineramaScreenInfo*	screens_;
static int			num_screens_ = 0;
static float*			screens_ar_;
static VideoMode**		vmodes;
struct fs_info_ {
    XF86VidModeModeInfo*	vmmi;
    int				screen;
    unsigned short		rates[4];
};
static fs_info_			res[16];
static int			num_res;
static repaint_func_t*		repaint_func;

static bool	do_mouselook = false;
static bool	mouse_snapped = false;
static int	mouse_x;
static int	mouse_y;
static int	win_w = 0;
static int	win_h = 0;
static float	win_ar;
static float	win_ar_w;
static float	win_ar_h;


static float aspectratio(int width, int height)
  {
    // Because monitors (and OSes) rarely report correct physical measurements
    // of the screen, we will be rounding aspects to 9:16, 3:4, 4:3, and 16:9.
    // Aspect ratios outside [1:2 .. 2:1] will be left alone, however.  Maybe
    // there is someone out there that has an anamorphic projector.  :-)

    float bar = float(width)/float(height);

    if(bar<0.5000)
	return bar;
    if(bar<0.6563)
	return 0.5625;
    if(bar<1.0417)
	return 0.7500;
    if(bar<1.55555555)
	return 1.33333333;
    if(bar<2.0)
	return 1.77777777;
    return bar;
  }

const VideoMode** video_initialize(repaint_func_t* repaint, bool stencil, bool alpha, bool dblbuf)
  {
    dpy = XOpenDisplay(0);
    if(!dpy)
	throw "Unable to open X Display";

    int scratch;

    if(!XineramaQueryExtension(dpy, &scratch, &scratch) || !(screens_ = XineramaQueryScreens(dpy, &num_screens_))) {
	static XineramaScreenInfo single_screen = { 0, 0, 0 };
	static float single_screen_ar = aspectratio(WidthMMOfScreen(DefaultScreenOfDisplay(dpy)),
		HeightMMOfScreen(DefaultScreenOfDisplay(dpy)));
	single_screen.width = WidthOfScreen(DefaultScreenOfDisplay(dpy));
	single_screen.height = HeightOfScreen(DefaultScreenOfDisplay(dpy));
	single_screen_ar /= float(single_screen.width)/single_screen.height;
	screens_ = &single_screen;
	screens_ar_ = &single_screen_ar;
	num_screens_ = 1;
    } else {
	screens_ar_ = new float[num_screens_];
	for(int i=0; i<num_screens_; i++)
	    screens_ar_[i] = aspectratio(
		WidthMMOfScreen(DefaultScreenOfDisplay(dpy)) * screens_[i].width / WidthOfScreen(DefaultScreenOfDisplay(dpy)),
		HeightMMOfScreen(DefaultScreenOfDisplay(dpy)) * screens_[i].height / HeightOfScreen(DefaultScreenOfDisplay(dpy)))
		/ (float(screens_[i].width)/screens_[i].height);
    }

    if(XF86VidModeQueryExtension(dpy, &scratch, &scratch)) {
	XF86VidModeModeInfo**	modes;
	int			num_modes;

	for(int sn=0; sn<num_screens_; sn++) {
	    if(XF86VidModeGetAllModeLines(dpy, sn, &num_modes, &modes)) {
		for(int i=0; i<num_modes; i++) {
		    int rate = res[i].vmmi? int(1000*(float(res[i].vmmi->dotclock)/res[i].vmmi->htotal)/res[i].vmmi->vtotal): 0;
		    if(modes[i]->hdisplay > screens_[sn].width
			    || modes[i]->vdisplay > screens_[sn].height) {
			// Some drivers implement a "pseudo-Xinerama" over a single frame buffer.
			// this confuses the *bleep* out of xf86vidmode.
			goto novm;
		    }
		    for(int j=0; j<num_res; j++) {
			if(res[j].screen==sn && res[j].vmmi->hdisplay == modes[i]->hdisplay) {
			    if(res[j].vmmi->vdisplay < modes[i]->vdisplay) {
				*res[j].vmmi = *modes[i];
				res[sn].rates[0] = res[sn].rates[1] = res[sn].rates[2] = res[sn].rates[3] = 0;
			    }
			    if(res[j].vmmi->vdisplay == modes[i]->vdisplay) {
				for(int k=0; k<3; k++)
				    if(!res[sn].rates[k]) {
					res[sn].rates[k] = rate;
					goto skip;
				    }
				if(res[sn].rates[3] < rate)
				    res[sn].rates[3] = rate;
			    }

			    goto skip;
			}
		    }
		    res[num_res].screen = sn;
		    res[num_res].vmmi = new XF86VidModeModeInfo;
		    *res[num_res++].vmmi = *modes[i];
skip: ;
		}
		XFree(modes);
	    }
	}
    } else {
novm:
	for(int sn=0; sn<num_screens_; sn++) {
	    res[sn].screen = sn;
	    res[sn].vmmi = 0;
	    res[sn].rates[0] = res[sn].rates[1] = res[sn].rates[2] = res[sn].rates[3] = 0;
	}
	num_res = num_screens_;
    }

    int nxvi;
    XVisualInfo* xvi = XGetVisualInfo(dpy, VisualNoMask, 0, &nxvi);
    int wantclass = TrueColor;

// We prefer DirectColor if available because that garantees we
// can do color correction
    for(int i=0; i<nxvi; i++)
	if(xvi[i].c_class==DirectColor) {
	    int val;
	    if(!glXGetConfig(dpy, xvi+i, GLX_USE_GL, &val) && val) {
		wantclass = DirectColor;
		break;
	    }
	}

    int	nvmodes = 0;
    int avmodes = 32;
    vmodes = new VideoMode*[avmodes];

    for(int i=0; i<nxvi; i++) {
	if(xvi[i].c_class!=wantclass)
	    continue;
	bool best = true;
	int val;
	if(glXGetConfig(dpy, xvi+i, GLX_USE_GL, &val) || !val)
	    continue;
	if(glXGetConfig(dpy, xvi+i, GLX_RGBA, &val) || !val)
	    continue;
	if(glXGetConfig(dpy, xvi+i, GLX_LEVEL, &val) || val)
	    continue;
	if(glXGetConfig(dpy, xvi+i, GLX_ALPHA_SIZE, &val))
	    continue;
	if(alpha && val<4)
	    continue;
	if(!alpha && val>0)
	    best = false;
	if(glXGetConfig(dpy, xvi+i, GLX_DOUBLEBUFFER, &val) || bool(val)!=dblbuf)
	    continue;
	if(glXGetConfig(dpy, xvi+i, GLX_DEPTH_SIZE, &val) || val<12)
	    continue;
	if(glXGetConfig(dpy, xvi+i, GLX_STENCIL_SIZE, &val) || val<8)
	    continue;
	if(!glXGetConfig(dpy, xvi+i, GLX_CONFIG_CAVEAT, &val) && val==GLX_SLOW_CONFIG)
	    best = false;
	if(stencil && (glXGetConfig(dpy, xvi+i, GLX_STENCIL_SIZE, &val) || val<8))
	    continue;

	if(nvmodes+(2+num_res) > avmodes) {
	    VideoMode** nv = new VideoMode*[avmodes <<= 1];
	    memcpy(nv, vmodes, sizeof(VideoMode*)*nvmodes);
	    delete[] vmodes;
	    vmodes = nv;
	}

	VideoMode* vm = vmodes[nvmodes++] = new VideoMode;
	vm->fullscreen = false;
	vm->width = WidthOfScreen(DefaultScreenOfDisplay(dpy));
	vm->height = HeightOfScreen(DefaultScreenOfDisplay(dpy));
	vm->depth = xvi[i].depth;
	vm->rates[0] = vm->rates[1] = vm->rates[2] = vm->rates[3] = 0;
	vm->screen = 0;
	vm->suboptimal = vm->slow = false;
	vm->visual = xvi+i;
	vm->aratio = screens_ar_[0];

	for(int i=0; i<num_res; i++) {
	    vm = vmodes[nvmodes++] = new VideoMode;
	    vm->fullscreen = true;
	    vm->width = res[i].vmmi->hdisplay;
	    vm->height = res[i].vmmi->vdisplay;
	    for(int j=0; j<3; j++)
		vm->rates[j] = res[i].rates[j];
	    vm->screen = res[i].screen;
	    vm->depth = xvi[i].depth;
	    vm->visual = xvi+i;
	    vm->suboptimal = vm->slow = false;
	    vm->aratio = screens_ar_[0];
	}
    }

    vmodes[nvmodes] = 0;

    repaint_func = repaint;
    return const_cast<const VideoMode**>(vmodes);
  }

static bool keydown[256];

static int x_to_key(KeySym ks) {
    switch(ks) {
	case 0x0020 ... 0x007E:
	    return ks;

	case 0xFF01 ... 0xFF1B:
	case 0xFF7F ... 0xFFFF:
	    return ks & 0xFF;

	case 0xFF50 ... 0xFF6B:
	    return (ks-0x10) & 0xFF;

	default:
	    return 0;
    }
}

static Cond post_repaint_cond;
static bool post_repaint;
static bool suspend_repaint = false;

static void* x_repaint_handler(void*)
  {
    for(;;)
      {
	post_repaint_cond.lock();
	while(!post_repaint)
	    post_repaint_cond.wait();
	post_repaint = false;
	bool suspend = suspend_repaint;
	post_repaint_cond.unlock();

	if(!suspend) {
	    x_mux.lock();
	    if(repaint_func)
		repaint_func(win_ar_w, win_ar_h, win_ar);
	    glXSwapBuffers(dpy, win);
	    x_mux.unlock();
	}
      }
  }

void video_post_repaint(void)
  {
    post_repaint_cond.lock();
    post_repaint = true;
    post_repaint_cond.unlock();
    post_repaint_cond.signal();
  }

void video_select_at(float mx, float my)
  {
    int	mousex = win_w/2 + int((mx-(2.0/3.0))*win_w/win_ar_w);
    int mousey = win_h/2 + int((my-0.5)*win_h/win_ar_h);
    int vport[4];
    glGetIntegerv(GL_VIEWPORT, vport);
    gluPickMatrix(mousex, mousey, 3, 3, vport);
  }

float video_vertical_ratio(void) { return win_ar_h; };
float video_horizontal_ratio(void) { return win_ar_w; };

static void snap_mouse(void)
  {
    XWarpPointer(dpy, win, win, 0, 0, win_w, win_h, mouse_x = win_w/2, mouse_y = win_h/2);
    mouse_snapped = true;
  }

void video_mouselook_mode(bool mlook)
  {
    if(mlook && !do_mouselook) {
	x_mux.lock();
	snap_mouse();
	x_mux.unlock();
    }
    do_mouselook = mlook;
  }

static void* x_event_handler(void*)
  {
    x_mux.lock();

    for(;;)
      {
	XEvent	ev;

	// We need to work around the fact that only one thread gets to mess with
	// X11 at the same time.  This means we cannot use XNextEvent() unless
	// we have the lock... which would be bad if it sleeps.  So what we do is
	// check that we have an event first.  If we don't we poll the actual
	// file descriptor after we release the lock.

	XFlush(dpy);
	while(XEventsQueued(dpy, QueuedAlready)) {
	    XNextEvent(dpy, &ev);

	    switch(ev.type) {
		case ConfigureNotify:
		    if(ev.xconfigure.window == top) {
			float ar = win_ar * win_w / win_h;
			win_ar_w = 4.0/3.0;
			win_ar_h = 1.0;
			if(ar < win_ar_w)
			    win_ar_h = win_ar_w / ar;
			else if(ar > win_ar_w)
			    win_ar_w = ar;
			if(win && ev.xconfigure.width!=win_w || ev.xconfigure.height!=win_h) {
			    post_repaint_cond.lock();
			    suspend_repaint = true;
			    post_repaint_cond.unlock();
			    win_w = ev.xconfigure.width;
			    win_h = ev.xconfigure.height;
			    XResizeWindow(dpy, win, win_w, win_h);
			}
		    } else if(ev.xconfigure.window == win) {
			if(ev.xconfigure.width==win_w && ev.xconfigure.height==win_h) {
			    glViewport(0, 0, win_w, win_h);
			    post_repaint_cond.lock();
			    suspend_repaint = false;
			    post_repaint = true;
			    post_repaint_cond.unlock();
			    if(do_mouselook)
				snap_mouse();
			    post_repaint_cond.signal();
			}
		    }
		    break;

		case MotionNotify:
		    if(mouse_snapped && (ev.xmotion.x!=mouse_x || ev.xmotion.y!=mouse_y))
			break;
		    mouse_snapped = false;
		    if(ev.xmotion.x==mouse_x && ev.xmotion.y==mouse_y)
			break;
		    // no break;
		case ButtonPress:
		case ButtonRelease:
		    if(do_mouselook) {
			new Event(Event::Motion, (ev.xmotion.x-mouse_x)*0.0078125, (ev.xmotion.y-mouse_y)*0.0078125);
			snap_mouse();
		    } else {
			new Event(Event::Position,
				(ev.xmotion.x-win_w/2)*win_ar_w/win_w + (2.0/3.0),
				(win_h/2-ev.xmotion.y)*win_ar_h/win_h + 0.5);
		    }
		    if(ev.type == ButtonPress)
			new Event(Event::ButtonDown, ev.xbutton.button);
		    if(ev.type == ButtonRelease)
			new Event(Event::ButtonUp, ev.xbutton.button);
		    break;

		case EnterNotify:
		    if(ev.xcrossing.detail==NotifyNonlinear)
			XSetInputFocus(dpy, win, RevertToPointerRoot, CurrentTime);
		    break;

		case ClientMessage:
		    if(ev.xclient.format==32 && Atom(ev.xclient.data.l[0])==delete_window)
			new Event(Event::Quit);
		    break;

		case Expose:
		    if(!ev.xexpose.count)
			video_post_repaint();
		    break;

		case MappingNotify:
		    switch(ev.xmapping.request) {
			case MappingModifier:
			case MappingKeyboard:
			    XRefreshKeyboardMapping(&ev.xmapping);
			    break;
		    }
		    break;

		case KeymapNotify:
		      {
			int	kmin;
			int	kmax;
			int	kpk;

			XDisplayKeycodes(dpy, &kmin, &kmax);
			KeySym* kmap = XGetKeyboardMapping(dpy, kmin, kmax-kmin+1, &kpk);

			for(int i=kmin; i<=kmax; i++) {
			    KeySym ks = kmap[(i-kmin)*kpk];
			    if(ks == NoSymbol)
				continue;
			    if(int k = x_to_key(ks)) {
				if(keydown[k] && !(ev.xkeymap.key_vector[i>>3] & (1<<(i&7)))) {
				    new Event(Event::KeyUp, k);
				    keydown[k] = false;
				} else if(!keydown[k] && (ev.xkeymap.key_vector[i>>3] & (1<<(i&7)))) {
				    new Event(Event::KeyDown, k);
				    keydown[k] = true;
				}
			    }
			}
		      }
		    break;

		case KeyPress:
		      {
			char			buf[8];
			KeySym			ks;
			static XComposeStatus	xcs;

			if(int len=XLookupString(&ev.xkey, buf, 8, &ks, &xcs)) {
			    for(int i=0; i<len; i++)
				new Event(Event::KeyHit, buf[i]);
			}

			if(int k = x_to_key(XLookupKeysym(&ev.xkey, 0))) {
			    if(!keydown[k]) {
				new Event(Event::KeyDown, k);
				keydown[k] = true;
			    }
			}
		      }
		    break;

		case KeyRelease:
		    if(int k = x_to_key(XLookupKeysym(&ev.xkey, 0))) {
			if(keydown[k]) {
			    new Event(Event::KeyUp, k);
			    keydown[k] = false;
			}
		    }
		    break;
	    }

	}

	// No pending events; we'll poll the file descriptor.
	pollfd	ufd = { ConnectionNumber(dpy), POLLIN|POLLERR|POLLHUP };

	// We are relying on the idea that no X11 call except the X*Event() family
	// reads from the server connection.  If that does not hold true, we have
	// a (small) window for a race condition between unlock() and poll() below
	// where some waiting thread could cause the socket to be drained.
	//
	// This is not catastrophic at any rate, since we will get woken up as soon
	// as something else occurs (any mouse movement, keypress, etc).  It still
	// sucks.

	XFlush(dpy);
	x_mux.unlock();
	if(poll(&ufd, 1, -1)<0 || (ufd.revents & (POLLERR|POLLHUP))) {
	    throw "connection to X server has been lost";
	    // handle this more gracefuly?
	}
	x_mux.lock();

	XEventsQueued(dpy, QueuedAfterReading);
      }
  }

static bool video_cleanup_(void)
  {
    glXMakeCurrent(dpy, None, 0);
    XSync(dpy, 0);

    if(win) {
	XUnmapWindow(dpy, win);
	XSync(dpy, 0);
	XDestroyWindow(dpy, win);
	win = 0;
    }
    if(top) {
	XUnmapWindow(dpy, top);
	XSync(dpy, 0);
	XDestroyWindow(dpy, win);
	win = 0;
    }
    if(glxc) {
	glXDestroyContext(dpy, glxc);
	glxc = 0;
    }

    XSync(dpy, true);

    return false;
  }

bool VideoMode::select(int wantw, int wanth) const
  {
    XSetWindowAttributes	attr;

    attr.event_mask = KeyPressMask        | KeyReleaseMask       | KeymapStateMask
		    | ButtonPressMask     | ButtonReleaseMask    | PointerMotionMask
		    | EnterWindowMask     | LeaveWindowMask
		    | StructureNotifyMask | VisibilityChangeMask
		    | ExposureMask
		    | FocusChangeMask
		    ;
    if(fullscreen) {
	wantw = width;
	wanth = height;
    }

    attr.override_redirect = true;
    attr.background_pixel = BlackPixel(dpy, DefaultScreen(dpy));
    attr.border_pixel = attr.background_pixel;

    x_mux.lock();
    video_cleanup_();

    try {
	glxc = glXCreateContext(dpy, reinterpret_cast<XVisualInfo*>(visual), 0, true);
	if(glxc <= 0)
	    throw "unable to create glX context";

	attr.colormap = cmap = XCreateColormap(dpy, DefaultRootWindow(dpy),
		reinterpret_cast<XVisualInfo*>(visual)->visual, AllocNone);
	if(cmap <= 0)
	    throw "unable to create color map";

	top = XCreateWindow(dpy, DefaultRootWindow(dpy),
		screens_[screen].x_org, screens_[screen].y_org,
		wantw, wanth, 0,
		CopyFromParent, InputOutput, CopyFromParent,
		CWEventMask | CWBackPixel | CWBorderPixel | (fullscreen? CWOverrideRedirect: 0),
		&attr);
	if(top <= 0)
	    throw "unable to create managed window";

	win_w = wantw;
	win_h = wanth;
	win = XCreateWindow(dpy, top,
		0, 0, wantw, wanth, 0,
		reinterpret_cast<XVisualInfo*>(visual)->depth, InputOutput, reinterpret_cast<XVisualInfo*>(visual)->visual,
		CWEventMask | CWBackPixel | CWBorderPixel | CWColormap,
		&attr);
	if(win <= 0)
	    throw "unable to create rendering window";

	//static Cursor win_cur;
	//if(!win_cur)
	    //win_cur = XCreateFontCursor(dpy, 0);
	XDefineCursor(dpy, win, 0);

	if(fullscreen) {
	    // KDE likes to be told we want to keep our fullscreen window on top
	    XEvent	ev;
	    long	mask;

	    memset(&ev, 0, sizeof(XEvent));
	    ev.xclient.type = ClientMessage;
	    ev.xclient.window = DefaultRootWindow(dpy);
	    ev.xclient.format = 32;
	    ev.xclient.data.l[0] = top;
	    ev.xclient.data.l[1] = CurrentTime;
	    ev.xclient.message_type = XInternAtom(dpy, "KWM_KEEP_ON_TOP", false);
	    mask = SubstructureRedirectMask;
	    XSendEvent(dpy, DefaultRootWindow(dpy), false, mask, &ev);
	}

	XWMHints* hints = XAllocWMHints();
	hints->input = true;
	hints->flags = InputHint;
	XSetWMHints(dpy, top, hints);
	XSetWMHints(dpy, win, hints);
	XFree(hints);

	XStoreName(dpy, top, "Labyrinth Of Worlds");

	XClassHint* chint = XAllocClassHint();
	chint->res_name = "LoW";
	chint->res_class = "glint";
	XSetClassHint(dpy, top, chint);
	XFree(chint);

	// We want to be asked politely to close the window
	delete_window = XInternAtom(dpy, "WM_DELETE_WINDOW", false);
	XSetWMProtocols(dpy, top, &delete_window, 1);

	win_ar = aratio;

	XMapWindow(dpy, top);
	XMapWindow(dpy, win);
	glXMakeCurrent(dpy, win, glxc);
    } catch(const char* msg) {
	video_cleanup_();
	x_mux.unlock();
	cprintf(Warning, "video: unable switch to mode: %s", msg);
	return false;
    }

    x_mux.unlock();

    event_th.start(x_event_handler, 0);
    repaint_th.start(x_repaint_handler, 0);

    return true;
  }

