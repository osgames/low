////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

////  *********
////  This program reads the UW2 levels.ark file and create LoW's map files
////  from it.  This is an expensive operation because of the line-of-sight
////  precalculations that are stored in the map files.
////  *********

#include "setup.H"
#include "geom.H"
#include "map.H"
#include "objects.H"

using namespace Geom;

struct aTile {
    unsigned short	type: 4,
			height:4,
			unk_light_q:1,
			unk_unk_q:1,
			floor_tex:4,
			nomagic:1,
			door:1;
    unsigned short	wall_tex:6,
			objects:10;
};

struct aObject {
    unsigned short	id:9,
			flags:6,
			multiple:1;
    unsigned short	z:7,
			facing:3,
			y:3,
			x:3;
    unsigned short	quality:6,
			next:10;
    unsigned short	owner:6, // often y
			link:10;
    unsigned char	more[0];
};

unsigned char*	file;
int		flen;

static inline int byte(int i)
  {
    return file[i];
  }

static inline int word(int i)
  {
    return byte(i) | (byte(i+1)<<8);
  }

static inline int lword(int i)
  {
    return word(i) | (word(i+2)<<16);
  }

static inline int offset(int i)
  {
    return lword(i*4+2);
  }

static inline int size(int i)
  {
    int	base = offset(i++);
    while(i<word(0))
	if(int next = offset(i++))
	    return next-base;
    return flen - base;
  }

unsigned char* unpack(int base)
  {
    if(!base)
	return 0;

    int			len = lword(base);
    unsigned char*	buf = new unsigned char[len+100];
    unsigned char*	up = buf;

    base += 4;

    while(up < buf+len)
      {
	int		bits = byte(base++);
	for(int r=0; r<8; r++)
	  {
	    if(bits&1)
		*up++ = byte(base++);
	    else
	      {
		int	o = byte(base++);
		int	c = byte(base++);

		o |= (c&0xF0)<<4;
		c = (c&15) + 3;
		o = o+18;
		if(o > (up-buf))
		    o -= 0x1000;
		while(o < (up-buf-0x1000))
		    o += 0x1000;
		while(c--)
		    *up++ = buf[o++];
	      }
	    bits >>= 1;
	  }
      }

    return buf;
  }

unsigned char	solid[64][64];
unsigned char	visible[64][64];


static bool obscured(float vx, float vy, L2& vl, L2& ll, L2& rl, const P2& p1, const P2& p2)
  {
    const S2	os(p1, p2);

    if(rl*os.m<=0 && ll*os.n<=0)
	return true;
    if(rl*os.n<=0 && ll*os.m<=0)
	return true;
    if(rl*os.m<=0 && rl*os.n>0)
	rl = L2(P2(vx, vy), os.n);
    else if(rl*os.n<=0 && rl*os.m>0)
	rl = L2(P2(vx, vy), os.m);
    else if(ll*os.n<=0 && ll*os.m>0)
	ll = L2(os.m, P2(vx, vy));
    else if(ll*os.m<=0 && ll*os.n>0)
	ll = L2(os.n, P2(vx, vy));
    if(rl*(ll%vl) <= 0)
	return true;
    return false;
  }

static bool obscured(int x, int y, int ex, int ey, const S2& s, float vx, float vy)
  {
    L2	vl(s);
    L2	ll(s.n, P2(vx, vy));
    L2	rl(P2(vx, vy), s.m);
    S2	os;

    //for(int yy=(y<?ey); yy<=(y>?ey); yy++)
	//for(int xx=(x<?ex); xx<=(x>?ex); xx++) {
	    //if((xx==x&&yy==y) || (xx==ex&&yy==ey))
		//continue;
	    //switch(solid[xx][yy]) {
		//case 0:
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy), P2(xx, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy+1), P2(xx+1, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy+1), P2(xx+1, yy))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy), P2(xx, yy))) return true;
		    //break;
		//case 2:
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy), P2(xx, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy+1), P2(xx+1, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy+1), P2(xx, yy))) return true;
		    //break;
		//case 3:
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy+1), P2(xx+1, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy+1), P2(xx+1, yy))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy), P2(xx, yy+1))) return true;
		    //break;
		//case 4:
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy), P2(xx, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy+1), P2(xx+1, yy))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy), P2(xx, yy))) return true;
		    //break;
		//case 5:
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx, yy), P2(xx+1, yy+1))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy+1), P2(xx+1, yy))) return true;
		    //if(obscured(vx, vy, vl, ll, rl, P2(xx+1, yy), P2(xx, yy))) return true;
		    //break;
	    //}
	//}
    return false;
  }

static bool obscured(int x, int y, int xx, int yy)
  {
    S2	vs;

    if(xx==x) {
	if(yy<y)
	    vs = S2(P2(xx+1, yy+1), P2(xx, yy+1));
	else
	    vs = S2(P2(xx, yy), P2(xx+1, yy));
    } else if(xx<x) {
	if(yy<y)
	    vs = S2(P2(xx+1, yy), P2(xx, yy+1));
	else if(yy==y)
	    vs = S2(P2(xx+1, yy), P2(xx+1, yy+1));
	else
	    vs = S2(P2(xx, yy), P2(xx+1, yy+1));
    } else { // xx>x
	if(yy<y)
	    vs = S2(P2(xx+1, yy+1), P2(xx, yy));
	else if(yy==y)
	    vs = S2(P2(xx, yy+1), P2(xx, yy));
	else
	    vs = S2(P2(xx, yy+1), P2(xx+1, yy));
    }

    if(xx<x)
	for(float k=y; k<=y+1; k+=.031250)
	    if(!obscured(x, y, xx, yy, vs, x, k))
		return false;
    if(xx>x)
	for(float k=y; k<=y+1; k+=.031250)
	    if(!obscured(x, y, xx, yy, vs, x+1, k))
		return false;
    if(yy<y)
	for(float k=x; k<=x+1; k+=.031250)
	    if(!obscured(x, y, xx, yy, vs, k, y))
		return false;
    if(yy>y)
	for(float k=x; k<=x+1; k+=.031250)
	    if(!obscured(x, y, xx, yy, vs, k, y+1))
		return false;

    return true;
  }

struct PVT {
    int			x, y;
    float		d;
    float		minv;
    float		maxv;
};

PVT	pvs[64*64];

extern "C" int pvt_compar(const void* p1_, const void* p2_)
  {
    if(static_cast<const PVT*>(p1_)->d == static_cast<const PVT*>(p2_)->d)
	return int(128*(static_cast<const PVT*>(p1_)->minv - static_cast<const PVT*>(p2_)->maxv));
    return (static_cast<const PVT*>(p1_)->d<static_cast<const PVT*>(p2_)->d)? -1: 1;
  }

static inline double clip(double a) {
    if(a<-M_PI)
	return a + 2*M_PI;
    if(a>=M_PI)
	return a - 2*M_PI;
    return a;
}

static inline double mina(double a, double b) {
    if(fabs(a-b)>M_PI)
	return a>b? a: b;
    return a>b? b: a;
}

static inline double maxa(double a, double b) {
    if(fabs(a-b)>M_PI)
	return a<b? a: b;
    return a<b? b: a;
}

static bool		oflist[1024];
static int		mapx[1024];
static int		mapy[1024];

int main(int argc, char** argv)
  {
    FILE*	fp = fopen("lev.ark", "rb");

    fseek(fp, 0, SEEK_END);
    flen = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    file = new unsigned char[flen];
    fread(file, 1, flen, fp);
    fclose(fp);

    int		dirsize = word(0);

    for(int i=0; i<1024; i++)
	mapx[i] = mapy[i] = 0;

    for(int ln=0; ln<dirsize/4; ln++)
      {
	unsigned char*	lmap = unpack(lword(2+ln*4));
	unsigned short*	ltex = reinterpret_cast<unsigned short*>(file+lword(2+ln*4+dirsize));

	if(!lmap)
	    continue;

	if(argc>1 && ln!=atoi(argv[1]))
	    continue;

	const aTile*		tiles = reinterpret_cast<aTile*>(lmap);
	const unsigned char*	mobiles = lmap+0x4000;
	const aObject*		objects = reinterpret_cast<aObject*>(lmap+0x5b00);
	const short*		mfree = reinterpret_cast<short*>(lmap+0x7300);
	const short*		ofree = reinterpret_cast<short*>(lmap+0x74fc);
	const short&		nmfree = *reinterpret_cast<short*>(lmap+0x7c02);
	const short&		nofree = *reinterpret_cast<short*>(lmap+0x7c04);
	char			fname[64];

	sprintf(fname, "data/levels/l%d.map", ln);
	printf("Level %d:\n", ln);

	unsigned char	pad[256] = "LoWm";
	FILE* mf = fopen(fname, "wb");
	fwrite(pad, 1, 4, mf);
	pad[0] = 0; pad[1] = 0; pad[2] = 64; pad[3] = 64;
	fwrite(pad, 1, 4, mf);
	fwrite(ltex, 2, 64, mf);
	for(int y=0; y<64; y++) {
	    for(int x=0; x<64; x++) {
		const aTile&	t = tiles[y*64+x];
		int		tl = 1;
		Tile_ondisk	tod;
		solid[x][y] = tod.type = t.type;
		tod.floor_tex = ltex[t.floor_tex];
		tod.wall_tex = ltex[t.wall_tex];
		tod.ceil_tex = ltex[32];
		tod.height = t.height;
		tod.nomagic = t.nomagic;
		tod.door = t.door;
		tod.ambient_two = t.unk_light_q;
		tod.unknown = t.unk_unk_q;
		tod.objects = t.objects;
		if(t.objects>0 && t.objects<1024) {
		    mapx[t.objects] = x;
		    mapy[t.objects] = y;
		}
		memcpy(pad+tl, &tod, sizeof(Tile_ondisk));
		tl += sizeof(Tile_ondisk);
		pad[0] = tl;
		fwrite(pad, 1, tl, mf);
	    }
	}

	for(int i=0; i<1024; i++)
	    oflist[i] = false;
	for(int i=0; i<=nmfree; i++)
	    oflist[mfree[i]] = true;
	for(int i=0; i<=nofree; i++)
	    oflist[ofree[i]] = true;

fprintf(stderr, "--- objects for level %d ---\n", ln);
	printf("  Dumping objects... "); fflush(stdout);
	int nob = 0;
	pad[0] = 2;
	pad[1] = 0;
	fwrite(pad, 1, 2, mf);
	for(int i=2; i<1024; i++) if(!oflist[i]) {
	    aObject		ao;
	    Object_ondisk	o;

	    memset(&o, 0, sizeof(Object_ondisk));
	    if(i<256) {
		memcpy(&ao, mobiles+i*27, 8);
	    } else {
		ao = objects[i-256];
	    }

	    o.type = ao.id;
	    o.flags = ao.flags;
	    o.facing = ao.facing;
	    o.tilez = ao.z;
	    o.tiley = ao.y;
	    o.tilex = ao.x;
	    o.quality = ao.quality;
	    o.next = ao.next;
	    o.owner = ao.owner;
	    if(ao.multiple && (ao.id<0x180 || ao.id>0x1BF)) {
		if(ao.link < 512) {
		    o.link = 0;
		    o.quantity = ao.link;
		} else {
		    o.property = ao.link - 512;
		    o.link = 0;
		    o.quantity = 1;
		}
	    } else {
		o.link = ao.link;
		o.quantity = 1;
	    }
	    pad[0] = i&255;
	    pad[1] = i>>8;
	    fwrite(pad, 1, 2, mf);
	    fwrite(&o, sizeof(Object_ondisk), 1, mf);
	    if(i<256)
		fwrite(mobiles+i*27+8, 19, 1, mf);

fprintf(stderr, "%d: ", i);
if(mapx[i]||mapy[i]) {
    fprintf(stderr, " (at %d,%d)", mapx[i], mapy[i]);
    solid[mapx[i]][mapy[i]] = 1;
}
switch(o.type) {
#include "names"
    default: fprintf(stderr, " a_#%d", o.type);
}
if(o.link) fprintf(stderr, " link=%d", o.link);
if(o.property) fprintf(stderr, " property=%d", o.property);
if(o.enchantment) fprintf(stderr, " enchantment=%d", o.enchantment);
if(o.quantity!=1) fprintf(stderr, " quantity=%d", o.quantity);
if(o.quality) fprintf(stderr, " quality=%d", o.quality);
if(o.next) fprintf(stderr, " next=%d", o.next);
if(o.flags) fprintf(stderr, " flags=%d", o.flags);
if(o.owner) fprintf(stderr, " owner=%d", o.owner);
if(o.facing) fprintf(stderr, " facing=%d", o.facing);
fprintf(stderr, " [%d,%d,%d]", o.tilex, o.tiley, o.tilez);
fprintf(stderr, " |");
  for(int i=0; i<8; i++)
      fprintf(stderr, " %02X", int(((const unsigned char*)&ao)[i]));
fprintf(stderr, "\n");

	    nob++;
	}
	pad[0] = 0;
	pad[1] = 0;
	fwrite(pad, 1, 2, mf);
	printf(" %d used\n", nob);
	
	printf("  Calculating pvs... "); fflush(stdout);
	int	total8 = 0;
	int	total12 = 0;
	int	total14 = 0;
	int	max8 = 0;
	int	max12 = 0;
	int	max14 = 0;
	int	total = 0;
	for(int y=0; y<64; y++) {
	    for(int x=0; x<64; x++) {
		for(int yy=0; yy<64; yy++) {
		    for(int xx=0; xx<64; xx++) {
			visible[xx][yy] = 0;
		    }
		}
		if(solid[x][y] == 0)
		    continue;
		for(int yy=0; yy<64; yy++) {
		    for(int xx=0; xx<64; xx++)
		      {
			int	dist = int(trunc(sqrt((xx-x)*(xx-x)+(yy-y)*(yy-y))));
			if(dist<2) {
			    visible[xx][yy] = dist;
			    continue;
			}
			if(dist>14)
			    continue;
			if(x==xx && y==yy)
			    continue;
			if(solid[xx][yy]==0)
			    continue;
			if(!obscured(x, y, xx, yy))
			    visible[xx][yy] = dist;
		      }
		}
		total++;
		int	count8 = 0;
		int	count12 = 0;
		int	count14 = 0;
		int	nv = 0;
		for(int xx=0; xx<64; xx++)
		    for(int yy=0; yy<64; yy++)
			if(visible[xx][yy]) {
			    pvs[nv].x = xx;
			    pvs[nv].y = yy;
			    pvs[nv].d = sqrt((xx-x)*(xx-x)+(yy-y)*(yy-y));

			    float	vx[4] = { xx-x-.5, xx-x-.5, xx-x+.5, xx-x+.5 };
			    float	vy[4] = { yy-y+.5, yy-y-.5, yy-y-.5, yy-y+.5 };

			    for(int i=0; i<4; i++) {
				double	dx = vx[i];
				double	dy = vy[i];
				double	d = sqrt(dx*dx+dy*dy);
				double	tetha = atan2(dx, dy);
				double	alpha = acos(sqrt(.5)/d);
				double	right = clip(tetha+alpha-(M_PI*3/4));
				double	left  = clip(tetha-alpha+(M_PI*3/4));
				if(i) {
				    pvs[nv].minv = mina(pvs[nv].minv, right);
				    pvs[nv].maxv = maxa(pvs[nv].maxv, left);
				} else {
				    pvs[nv].minv = right;
				    pvs[nv].maxv = left;
				}
			    }

			    nv++;

			    count14++;
			    if(visible[xx][yy]<13)
				count12++;
			    if(visible[xx][yy]<9)
				count8++;
			}

		qsort(pvs, nv, sizeof(PVT), pvt_compar);
		unsigned short	last[16];
		int		ll = 2;

		for(int i=0; i<nv; i++) {
		    int	d = int(trunc(pvs[i].d));
		    if(d>ll)
			last[ll++] = i;
		}
		while(ll<16)
		    last[ll++] = nv;
		last[15] = nv;
		pad[0] = 1; pad[1] = 0; pad[2] = x; pad[3] = y;
		fwrite(pad, 1, 4, mf);
		fwrite(last+2, 2, 14, mf);
		for(int i=0; i<nv; i++) {
		    float	min = (M_PI+pvs[i].minv)*64/M_PI;
		    float	max = (M_PI+pvs[i].maxv)*64/M_PI;
		    pad[0] = pvs[i].x;
		    pad[1] = pvs[i].y;
		    pad[2] = int(floor(min))&127;
		    pad[3] = int(ceil(max))&127;
		    fwrite(pad, 1, 4, mf);
		}
		if(count8 > max8)
		    max8 = count8;
		if(count12 > max12)
		    max12 = count12;
		if(count14 > max14)
		    max14 = count14;
		total8 += count8;
		total12 += count12;
		total14 += count14;
	    }
	    printf("\r  Calculating pvs... %3d%%", ((y+1)*100)/64); fflush(stdout);
	}
	printf("\n    Dist Max   Avg\n---- --- -----\n");
	printf("      14 %3d %5.1f\n", max14, float(total14)/total);
	printf("      12 %3d %5.1f\n", max12, float(total12)/total);
	printf("       8 %3d %5.1f\n", max8, float(total8)/total);
	fclose(mf);
      }
  }

