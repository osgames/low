////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "game.H"
#include "map.H"
#include "particle.H"


static void liquid_stick(Particle::Particle& p)
  {
    if(p.xyz[2] > 0)
      {
	p.xyz[2] = 0;
	p.stuck = true;
      }
  }

PART_PTICK_FUN(splash)
  {
    liquid_stick(p);
    if(!p.stuck)
	p.vec[2] += .001;
    p.col[0] *= .95;
    p.col[1] *= .95;
    if(p.stuck)
	p.col[3] *= .8;
    else
	p.col[3] *= .95;
    return p.col[3]<.02;
  }

PART_GTICK_FUN(splash)
  {
    int	gmax = 2;

    while(gmax-- && g.act_p < g.max_p)
      {
	Particle::Particle&	p = g.parts[g.act_p++];

	p.age = random()&63;
	p.stuck = false;
	p.vec[0] = -.02 + .04*drand48();
	p.vec[1] = -.02 + .04*drand48();
	p.vec[2] = -.01 - 0.005*drand48();
	p.xyz[0] = -g.xrange/2 + g.xrange*drand48();
	p.xyz[1] = -g.yrange/2 + g.yrange*drand48();
	p.xyz[2] = 0;
	p.col[0] = g.col[0];
	p.col[1] = g.col[1];
	p.col[2] = g.col[2];
	p.col[3] = g.col[3];
	p.size = 5 + 5.0*drand48();
      }
  }

