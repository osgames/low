////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include <math.h>

#include "setup.H"
#include "worldg.H"
#include "map.H"
#include "game.H"


static const float	q = sqrt(.5);
static const float	v = cos(asinf(.25));
static const float	w = .25;

Planar	planes[] = {
      /*  0 */ { { 0, 0, 1 }, { 1, 0, 0 }, { 0, 1, 0 } }, // up
      /*  1 */ { { 0, 0,-1 }, { 1, 0, 0 }, { 0,-1, 0 } }, // down

      /*  2 */ { { 0, 1, 0 }, {-1, 0, 0 }, { 0, 0,-1 } }, // north
      /*  3 */ { { q, q, 0 }, {-1, 0, 0 }, { 0, 0,-1 } }, // northeast
      /*  4 */ { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0,-1 } }, // east
      /*  5 */ { { q,-q, 0 }, { 0, 1, 0 }, { 0, 0,-1 } }, // southeast
      /*  6 */ { { 0,-1, 0 }, { 1, 0, 0 }, { 0, 0,-1 } }, // south
      /*  7 */ { {-q,-q, 0 }, { 1, 0, 0 }, { 0, 0,-1 } }, // southwest
      /*  8 */ { {-1, 0, 0 }, { 0,-1, 0 }, { 0, 0,-1 } }, // west
      /*  9 */ { {-q, q, 0 }, { 0,-1, 0 }, { 0, 0,-1 } }, // northwest

      /* 10 */ { { 0, w, v }, { 1, 0, 0 }, { 0, 1, 0 } }, // north slope
      /* 11 */ { { 0,-w, v }, { 1, 0, 0 }, { 0, 1, 0 } }, // south slope
      /* 12 */ { { w, 0, v }, { 1, 0, 0 }, { 0, 1, 0 } }, // east slope
      /* 13 */ { {-w, 0, v }, { 1, 0, 0 }, { 0, 1, 0 } }, // west slope
};


int main(int, char**)
  {
    for(int m=0; m<14; m++) {
	Planar&	f = planes[m];

	printf("      {\t{ %8.5f, %8.5f, %8.5f },\n", f.n[0], f.n[1], f.n[2]);
	printf("\t{ %8.5f, %8.5f, %8.5f },\n", f.s[0], f.s[1], f.s[2]);
	printf("\t{ %8.5f, %8.5f, %8.5f },\n", f.t[0], f.t[1], f.t[2]);
	printf("\t{");

	f.s[0] = f.t[1]*f.n[2] - f.t[2]*f.n[1];
	f.s[1] = f.t[2]*f.n[0] - f.t[0]*f.n[2];
	f.s[2] = f.t[0]*f.n[1] - f.t[1]*f.n[0];

	if(m<2 || m>9) {
	    //f.t[1] = -f.t[1];
	    f.s[0] = -f.s[0];
	}


	f.T[ 0]=f.s[0]; f.T[ 4]=f.s[1]; f.T[ 8]=f.s[2]; f.T[12]=0.0;
	f.T[ 1]=f.t[0]; f.T[ 5]=f.t[1]; f.T[ 9]=f.t[2]; f.T[13]=0.0;
	f.T[ 2]=f.n[0]; f.T[ 6]=f.n[1]; f.T[10]=f.n[2]; f.T[14]=0.0;
	f.T[ 3]=0.0;    f.T[ 7]=0.0;    f.T[11]=0.0;    f.T[15]=1.0;

	for(int i=0; i<16; i++) {
	    printf(" %8.5f,", f.T[i]);
	    if((i&3) == 3)
		printf("\n\t ");
	}
	printf("} },\n");
    }
  }
