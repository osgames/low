////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"

#include "fileio.H"
#include "gr.H"

namespace Gr {


    Palettes*	pals = 0;

    Palettes::Palettes(const char* fname_p, const char* fname_a)
      {
	::File	fp = openuw2(fname_p);

	if(!fp)
	    throw "Unable to access the UW2 data files.  Check the README file.";

	char	pad[256*3];

	fp.read(pad, 3*256);
	fp.close();
	for(int i=0; i<256; i++)
	    for(int j=0; j<3; j++)
		color[i][j] = (int(pad[i*3+j])*255)/63;

	fp = openuw2(fname_a);

	if(!fp)
	    throw "Unable to access the UW2 data files.  Check the README file.";

	for(int i=0; i<31; i++)
	  {
	    fp.read(pad, 16);
	    for(int j=0; j<16; j++)
		pal[i][j] = pad[j];
	  }
	fp.close();
      }

    Image::Image(int w_, int h_): data(0)
      {
	ow = w_;
	oh = h_;
	w = 1;
	h = 1;
	while(w<w_)
	    w<<=1;
	while(h<h_)
	    h<<=1;
	data = new unsigned char[w*h*4+1];
	memset(data, 0, w*h*4);
	data[w*h*4] = 123;
      }

#ifndef RELEASE
    void Image::check(void)
      {
#if 0
	if( ((unsigned long*)this)[3]!=0xf00dface || ((unsigned long*)this)[4]!=0xdeadbeef )
	    abort();
#endif
      }
#endif

    File::File(const char* fname): file(0), size(0)
      {
	if(!pals)
	    pals = new Palettes("data/pals.dat", "data/allpals.dat");

	::File	fp = openuw2(fname);

	if(!fp)
	    return;

	size_t flen = fp.size();

	file = new unsigned char[flen];
	fp.read(file, flen);
	fp.close();

	num = *reinterpret_cast<short*>(file+1);

	texid = new unsigned int[num];
	memset(texid, 0, sizeof(int)*num);
      }

    File::~File()
      {
	if(file)
	    delete[] file;
      }

    static bool			half;
    static unsigned char	lo;

    static inline int get4(const unsigned char*& s)
      {
	if(half)
	  {
	    half = false;
	    return lo;
	  }
	half = true;
	lo = *s & 15;
	return *s++ >> 4;
      }

    static int getcnt(const unsigned char*& s)
      {
	int	cnt = get4(s);

	if(cnt)
	    return cnt;
	cnt  = get4(s)<<4;
	cnt |= get4(s);
	if(cnt)
	    return cnt;
	cnt  = get4(s)<<8;
	cnt |= get4(s)<<4;
	cnt |= get4(s);
	return cnt;
      }

    static inline void set8(Image* i, int& x, int& y, int c, unsigned char alpha)
      {
	if(y >= i->h)
	    return;

	unsigned char*	d = i->data + (((i->h-y-1)*i->w + x)<<2);

	if(d>(i->data+(i->w*i->h*4)))
	    throw i;
	if(c)
	  {
	    d[0] = pals->color[c][0];
	    d[1] = pals->color[c][1];
	    d[2] = pals->color[c][2];
	    d[3] = alpha;
	  }
	else for(int i=0; i<4; i++)
	    d[i] = 0;
	if(++x >= i->ow)
	  {
	    x = 0;
	    y++;
	  }

	i->check();
      }

    static inline void set4(Image* i, int& x, int& y, int p, int c, unsigned char alpha)
      {
	set8(i, x, y, pals->pal[p][c], alpha);
      }

    Image* File::fetch(int n, unsigned char alpha)
      {
	if(n<0 || n>=num)
	    return 0;
	const unsigned char*	s = file + reinterpret_cast<long*>(file+3)[n];

	int		t = *s++;
	int		w = *s++;
	int		h = *s++;
	Image*		i = new Image(w, h);
	int		x = 0;
	int		y = 0;
	int		c = w*h;
	int		p;

	i->check();

	switch(t)
	  {
	    case 4:	// 8 bit uncompressed
		s += 2;
		for(int n=0; n<c; n++)
		    set8(i, x, y, *s++, alpha);
		break;
	    case 10:	// 4 bit uncompressed
		p = *s;
		s += 3;
		for(int n=0; n<c; n+=2)
		  {
		    set4(i, x, y, p, (*s)>>4, alpha);
		    set4(i, x, y, p, (*s++)&15, alpha);
		  }
		break;
	    case 8:	// 4 bit rle
		p = *s;
		s += 3;
		half = false;
		while(c>0)
		  {
		    // repeat record
		    int rpt=1;
		    int	cnt, nib;
		    while(rpt--)
		      {
			cnt = getcnt(s);
			if(cnt==1)
			    break;
			if(cnt==2)
			  {
			    rpt = getcnt(s);
			    continue;
			  }

			c -= cnt;
			nib = get4(s);
			while(cnt--)
			    set4(i, x, y, p, nib, alpha);
		      }
		    if(c<1)
			break;
		    // run record
		    cnt = getcnt(s);
		    c -= cnt;
		    while(cnt--)
			set4(i, x, y, p, get4(s), alpha);
		  }
		break;
	  }

	if(i->data[i->w*i->h*4] != 123)
	    throw "Ouch!  Overrun in fetch()";

	return i;
      }

    void File::texture(int n, unsigned char alpha)
      {
	if(n<0 || n>=num)
	    return;

	Image*	img = fetch(n, alpha);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->w, img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->data);
	delete img;
      }

}; // namespace Gr

