/*  
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
  */


%{

#include <stdlib.h>
#include "model.H"

#define YYDEBUG 1

static int	lface;
//static int	vnum;

static void yyerror(const char* msg)
  {
    throw ModelError(msg);
  }

extern int yylex(void);

%}

%union {
	float	f;
	long	i;
}

%token		GEOMOBJECT MESH MESH_NUMVERTEX MESH_NUMFACES MESH_VERTEX_LIST LBL
%token		MESH_VERTEX MESH_FACE_LIST MESH_FACE MESH_MTLID MESH_NUMTVERTEX MAGIC
%token		MESH_TVERTLIST MESH_TVERT MESH_NUMTVFACES MESH_TFACELIST MESH_TFACE
%token		MESH_NORMALS MESH_FACENORMAL UNKNOWN STRING BGIN END MESH_VERTEXNORMAL
%token		NODE_TM TM_ROTAXIS TM_ROTANGLE
%token <f>	FLOAT
%token <i>	INT
%type <i>	lint

%start		file

%%

anything		: FLOAT			{ }
			| INT			{ }
			| STRING
			;

anything_list		: anything
			| anything_list anything
			;

maybe_something		:
			| anything_list
			;

unknown			: UNKNOWN BGIN unknown_list END
			| UNKNOWN maybe_something
			;

unknown_list		: unknown
			| unknown_list unknown
			;

in_vertex_list		: MESH_VERTEX INT FLOAT FLOAT FLOAT
				{ model.meshes->vi($2).v($3, $4, $5); }
			| unknown
			;

list_vertex_list	: in_vertex_list
			| list_vertex_list in_vertex_list
			;

lint			: LBL INT	{ $$ = $2; }
			;

in_face_list		: MESH_FACE INT lint lint lint lint lint lint
				{ model.meshes->fi(lface = $2).v($3, $4, $5); }
			| MESH_MTLID INT
				{ model.meshes->fi(lface).mat = $2; }
			| unknown
			;

list_face_list		: in_face_list
			| list_face_list in_face_list
			;

in_tvert_list		: MESH_TVERT INT FLOAT FLOAT FLOAT
				{ model.meshes->ti($2).set($3, -$4, $5); }
			| unknown
			;

list_tvert_list		: in_tvert_list
			| list_tvert_list in_tvert_list
			;

in_tface_list		: MESH_TFACE INT INT INT INT
				{ model.meshes->fi($2).t($3, $4, $5); }
			| unknown
			;

list_tface_list		: in_tface_list
			| list_tface_list in_tface_list
			;

in_normals		: MESH_FACENORMAL INT FLOAT FLOAT FLOAT
				{ model.meshes->fi(lface = $2).n($3, $4, $5); }
			| MESH_VERTEXNORMAL INT FLOAT FLOAT FLOAT
				{ model.meshes->fi(lface).vn($2, $3, $4, $5); }
			| unknown
			;

list_normals		: in_normals
			| list_normals in_normals
			;

in_mesh			: MESH_NUMVERTEX INT
				{ model.meshes->numv($2); }
			| MESH_NUMFACES INT
				{ model.meshes->numf($2); }
			| MESH_VERTEX_LIST BGIN list_vertex_list END
			| MESH_FACE_LIST BGIN list_face_list END
			| MESH_NUMTVERTEX INT
				{ model.meshes->numtv($2); }
			| MESH_TVERTLIST BGIN list_tvert_list END
			| MESH_NUMTVFACES INT
				{ model.meshes->numtf($2); }
			| MESH_TFACELIST BGIN list_tface_list END
			| MESH_NORMALS BGIN list_normals END
			| unknown
			;

list_mesh		: in_mesh
			| list_mesh in_mesh
			;

in_tm			: TM_ROTAXIS FLOAT FLOAT FLOAT	{ model.meshes->rx=$2; model.meshes->ry=$3; model.meshes->rz=$4; }
			| TM_ROTANGLE FLOAT		{ model.meshes->ra=$2; }
			| unknown
			;

list_tm			: in_tm
			| list_tm in_tm
			;

in_geom			: NODE_TM BGIN list_tm END
			| MESH BGIN list_mesh END
			| unknown
			;

list_geom		: in_geom
			| list_geom in_geom
			;

in_file			: GEOMOBJECT BGIN { model.newmesh(); } list_geom END
			| unknown
			;

list_file		: in_file
			| list_file in_file
			;

file			: MAGIC INT list_file	{ model.complete(); }
			;

%%


