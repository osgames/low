

#include "geom.H"

namespace Geom {

    void L2::fromseg(const P2& m, const P2& n)
      {
	b = m.x - n.x;
	a = n.y - m.y;
	c = -(a*m.x + b*m.y);
      }

    P2 L2::operator % (const L2& l) const
      {
	float	d = a*l.b - l.a*b;
	return P2( (-c*l.b - -l.c*b)/d, (a*-l.c-l.a*-c)/d );
      }


}; // namespace Geom

