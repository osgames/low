
#include "md.H"
#include <signal.h>
#include <sys/time.h>

Mutex::Mutex(void)
  {
#ifdef WIN32
    pthread_mutex_init(&mux, 0);
#else
    pthread_mutexattr_t	muxattr;

    pthread_mutexattr_init(&muxattr);
    pthread_mutexattr_settype(&muxattr, PTHREAD_MUTEX_ERRORCHECK_NP);
    pthread_mutex_init(&mux, &muxattr);
#endif
  }

Mutex::~Mutex()
  {
    if(pthread_mutex_destroy(&mux))
	throw "pthread_mutex_destroy() failed";
  }

void Mutex::lock(void)
  {
    if(pthread_mutex_lock(&mux))
	throw "pthread_mutex_lock() failed";
  }

void Mutex::unlock(void)
  {
    if(pthread_mutex_unlock(&mux))
	throw "pthread_mutex_unlock() failed";
  }

Cond::Cond(void)
  {
    pthread_cond_init(&cond, 0);
  }

Cond::~Cond()
  {
    pthread_cond_destroy(&cond);
  }

void Cond::signal(void)
  {
    pthread_cond_signal(&cond);
  }

void Cond::broadcast(void)
  {
    pthread_cond_broadcast(&cond);
  }

void Cond::wait(void)
  {
    pthread_cond_wait(&cond, &mux);
  }

bool Thread::running(void)
  {
    lock();
    bool run = active;
    unlock();
    return run;
  }

void Thread::start(void* (*func)(void*), void* arg)
  {
    lock();
    if(active) {
	unlock();
	throw "thread already running";
    }
    pthread_create(&th, 0, func, arg);
    active = true;
    unlock();
  }

void* Thread::stop(void)
  {
    lock();
    if(!active) {
	unlock();
	return 0;
    }
    pthread_cancel(th);
    void* rv;
    pthread_join(th, &rv);
    active = false;
    unlock();
    return rv;
  }

