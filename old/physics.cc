////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "physics.H"
#include "map.H"
#include "entity.H"
#include "game.H"

extern void liquid_alt(int*, Tile&);

namespace Physics {

    static Altitude*		alc[3][3];
    static Altitude		fake_alt[3][3];
    static EIndex		ale[3][3];
    static unsigned char	alciel[3][3];
    static int			alc_bx;
    static int			alc_by;

    static inline int aaalt(int x, int y)
      {
	return
	    alc[((x+alc_bx)>>3)+1][((y+alc_by)>>3)+1]?
	    alc[((x+alc_bx)>>3)+1][((y+alc_by)>>3)+1]->alt[(x+alc_bx)&7][(y+alc_by)&7]:
	    0;
      }

    static inline bool aaciel(int x, int y)
      {
	return alciel[((x+alc_bx)>>3)+1][((y+alc_by)>>3)+1];
      }

    static inline bool aablock(int x, int y, int a, int h)
      {
	return aaalt(x, y)<a
	    || aaciel(x, y)>aaalt(x, y)-h;
      }

    static const double	deg_rad = M_PI/180;

    static void add(float& s1, float& h1, float s2, float h2)
      {
	float	x = s1*sin(h1*deg_rad) + s2*sin(h2*deg_rad);
	float	y = s1*cos(h1*deg_rad) + s2*cos(h2*deg_rad);

	h1 = atan2(x, y) / deg_rad;
	s1 = hypot(x, y);

	while(h1 < 0)
	    h1 += 360;
	while(h1 >= 360)
	    h1 -= 360;
      }

    static inline int aahit(int x, int y, int a, int h, int dist)
      {
	int		u = 0;
	int		l = 0;
	int		d = 0;
	int		r = 0;

	for(int D=0; D<=dist; D++)
	  {
	    for(int xx=-dist; xx<=dist; xx++)
	      {
		if(aablock(x+xx, y-dist, a, h))
		    u++;
		if(aablock(x+xx, y+dist, a, h))
		    d++;
	      }
	    for(int yy=-dist; yy<=dist; yy++)
	      {
		if(aablock(x-dist, y+yy, a, h))
		    l++;
		if(aablock(x+dist, y+yy, a, h))
		    r++;
	      }
	    if(u || l || d || r)
		break;
	  }

	int		max = u >? r >? d >? l;

	if(!max)
	    return 0;

	if(u==max)
	  {
	    if(r==max)
		return 2;
	    else if(l==max)
		return 8;
	    else
		return 1;
	  }
	else if(d==max)
	  {
	    if(r==max)
		return 4;
	    else if(l==max)
		return 6;
	    else
		return 5;
	  }
	else if(r==max)
	    return 3;
	else
	    return 7;
      }

    static void aaprep(int x, int y, int zztop)
      {
	int		tx = x>>3;
	int		ty = y>>3;

	alc_bx = x&7;
	alc_by = y&7;
	for(int xx=0; xx<3; xx++)
	    for(int yy=0; yy<3; yy++)
		if(tx+xx>0 && ty+yy>0 && tx+xx<=Game::map.w && ty+yy<=Game::map.h)
		  {
		    Tile&	t = Game::map(tx+xx-1, ty+yy-1);

		    alc[xx][yy] = t.altitude();
		    alciel[xx][yy] = t.calt;
		    ale[xx][yy] = t.f_ent;
		    for(EIndex e=t.f_ent; e; e=e->next)
			if(e->type==0x164) // bridge/walkway/platform
			  {
			    if(zztop < (int(e->z*32)-1))
			      {
				alc[xx][yy] = &fake_alt[xx][yy];
				for(int x=0; x<8; x++)
				    for(int y=0; y<8; y++)
					fake_alt[xx][yy].alt[x][y] = int(e->z*32)-2;
			      }
			    else
				alciel[xx][yy] = int(e->z*32)-1;
			  }
		  }
		else
		  {
		    alc[xx][yy] = 0;
		    alciel[xx][yy] = 0;
		    ale[xx][yy] = 0;
		  }
      }

    static float reflect(float h, float ra)
      {
	float nh = ra - (h-ra) + 180;

	while(nh<0)
	    nh += 360;
	while(nh>=360)
	    nh -= 360;

	if(fabs(h-nh)<1)
	  {
	    nh = ra-180;
	    if(nh<0)
		nh += 360;
	  }
	return nh;
      }

    Collision& Ballistic::update(bool effects)
      {
	static Collision	col;
	int			climb = climboid? 10: 0;
	int			rad = radius >? 1;

	col.what = Collision::Nothing;
	col.trig = 0;

	if(!moving)
	    return col;

	if(flying)
	    grounded = false;

retry:
	int		stz = int(z*32);
	int		sx = int(x*8+4);
	int		sy = int(y*8+4);
	float		destx = x + speed * .0625 * sin(h/180*M_PI);
	float		desty = y - speed * .0625 * cos(h/180*M_PI);
	int		dx = int(destx*8+4);
	int		dy = int(desty*8+4);
	int		steps = abs(dx-sx) >? abs(dy-sy);
	float		xf = float(dx-sx)/float(steps);
	float		yf = float(dy-sy)/float(steps);
	int		s;
	int		hits = 0;

	//if(vspeed>.1)
	    //climb = 0;

	aaprep(sx, sy, stz-height);

	int	newz = 255;

	for(int xx=-rad; xx<=rad; xx++)
	    for(int yy=-rad; yy<=rad; yy++)
		newz <?= aaalt(xx, yy);

	float	depth = 0;

	floating = false;
	if(Game::map(int(x+.5), int(y+.5)).liquid)
	  {
	    int		ll[9];

	    liquid_alt(ll, Game::map(int(x+.5), int(y+.5)));

	    float		xfrac = (x+.5) - int(x+.5);
	    float		yfrac = (y+.5) - int(y+.5);
	    float		xtop = xfrac*ll[2] + (1-xfrac)*ll[0];
	    float		xbot = xfrac*ll[8] + (1-xfrac)*ll[6];
	    float		lev = yfrac*xbot + (1-yfrac)*xtop;

	    depth = int(z*32-lev);

	    if(depth>0)
		climb += int(depth+1);

	    if(depth > 8)
		floating = true;
	  }

	if(grounded && !floating)
	    z = newz * 0.03125;
	else if(!flying)
	  {
boink:
	    z += vspeed/10;

	    if(int(z*32)-height < aaciel(0, 0))
	      {
		col.what = Collision::Cieling;
		col.force = -vspeed;

		z = float(aaciel(0, 0) + height)*0.03125;
		vspeed = -vspeed * bounce;
	      }
	    if(!floating && z >= newz*0.03125)
	      {
		col.what = Collision::Floor;
		col.force = vspeed;

		vspeed = -vspeed * bounce;
		if(fabs(vspeed)<.25)
		  {
		    vspeed = 0;
		    grounded = true;
		    speed /= 4;
		    z = newz*0.03125;
		  }
		else
		    goto boink;
	      }
	    if(floating)
	      {
		// we want equilibrium at depth 14, so:
		if(depth>14)
		  {
		    z -= 0.03125*(depth-14);
		    vspeed = 0;
		  }
		else
		    vspeed += gravity/2;
	      }
	    else
		vspeed += gravity;
	  }
	
	for(s=0; s<(steps>?1); s++)
	  {
	    float		ldist = 0.1;
	    EIndex	lent = 0;

	    for(int xx=0; xx<=2; xx++)
		for(int yy=0; yy<=2; yy++)
		    for(EIndex e=ale[xx][yy]; e; e=e->next)
		      {
			if(&e->phys == this)
			    continue;
			if((e->flags&Entity::eSolid)==0)
			    continue;
			if((e->flags&Entity::eFixed))
			    continue;
			if(z < e->z-.03125*e->t().height)
			    continue;
			if(z-.03125*height > e->z)
			    continue;

			float	dx = x + (speed * .0625 * (s+1) * sin(h/180*M_PI)) / steps;
			float	dy = y - (speed * .0625 * (s+1) * cos(h/180*M_PI)) / steps;
			float	dist = hypot(e->x-dx, e->y-dy) - .125*(rad+e->t().radius);
			float	sdist = hypot(e->x-x, e->y-y) - .125*(rad+e->t().radius);

			if(dist<0 && sdist>dist && dist<ldist)
			  {
			    ldist = dist;
			    lent = e;
			  }
		      }
	    if(!steps)
		break;

	    if(lent)
	      {
		if(lent->trigger())
		    col.trig = lent;
		else
		  {
		    col.what = Collision::Entity;
		    col.which = int(lent);
		    col.force = speed;

		    float	ra = atan2(lent->x-x, y-lent->y);
		    //float	rd = .125*(1.0+rad+lent->t().rad);

		    ra *= 180.0/M_PI;
		    if(ra<0)
			ra += 360;

		    if(lent->mobile())
		      {
			float	tmass = mass + lent->phys.mass;
			float	self = lent->phys.mass/tmass;

			if(effects)
			    add(lent->phys.speed, lent->phys.h, speed*(1-self), ra);
			lent->phys.moving = true;
			speed *= bounce * self;
			h = reflect(h, ra);

			x += (speed * .0625 * s * sin(h/180*M_PI)) / steps;
			y -= (speed * .0625 * s * cos(h/180*M_PI)) / steps;
			goto retry;
		      }
		  }
	      }

	    // ... then with level geometry
	    int		tx = int(xf*(s+1));
	    int		ty = int(yf*(s+1));

	    if( (hits = aahit(tx, ty, stz-climb, height, rad)) )
		break;
	  }

	if(hits)
	  {
	    float	frac = float(steps-s)/steps;

	    col.what = Collision::Wall;
	    col.which = hits;
	    col.force = speed;

	    if(!(grounded||flying))
		speed *= bounce;
	    x += (speed * .0625 * s * sin(h/180*M_PI)) / steps;
	    y -= (speed * .0625 * s * cos(h/180*M_PI)) / steps;

	    float	nh = reflect(h, 45.0 * (hits-1));

	    if(grounded || floating || flying)
	      {
		speed *= frac/2;
		add(speed, h, speed, nh);
		h = int((h+22.5)/45)*45.0;
		goto retry;
	      }
	    else
	      {
		h = nh;
		speed *= bounce;
		vspeed *= bounce;
	      }
	  }
	else
	  {
	    if(grounded && !climboid)
	      {
		speed /= 2;
		if(speed < .2)
		    speed = 0;
	      }
	    x = destx;
	    y = desty;
	  }

	aaprep(int(x*8+4), int(y*8+4), stz-height);

	newz = 255;

	for(int xx=-rad; xx<=rad; xx++)
	    for(int yy=-rad; yy<=rad; yy++)
		newz <?= aaalt(xx, yy);

	if(newz > stz+climb)
	    grounded = false;

	moving = speed > .1;

	if(!moving)
	    speed = 0;

	if(flying)
	    grounded = false;

	if(floating || !grounded)
	    moving = true;

	return col;
      }

    Collision& Propelled::update(void)
      {
	Collision& c = Ballistic::update();

	if(grounded || floating || flying)
	  {
	    float	sd = aim_speed - speed;
	    if(sd<-.1 || sd>.1)
		speed += sd/3;
	    else
		speed = aim_speed;
	    if(aim_speed > 0)
		h = aim_h;

	    moving = speed > .1;
	  }
	else
	    moving = true;

	return c;
      }

    Ballistic::Ballistic(void)
      {
	x = y = z = 0;
	height = 8;
	radius = 0;
	climboid = false;
	gravity = .05;
	speed = h = vspeed = 0;
	mass = 1;
	bounce = 0.1;
	grounded = false;
	floating = false;
	moving = true;
      }

    Propelled::Propelled(void): aim_speed(0), aim_h(0)
      {
	climboid = true;
	gravity = .15;
      }


}; // namepsace Physics
