////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "eindex.H"

#ifndef LOW_LIGHT_H__
#define LOW_LIGHT_H__

struct Map;
struct Light
  {
    Light*			next;

    struct {
	float			    x, y, z;
	float			    radius;
	float			    diffuse[4];
	float			    specular[4];
    }				base, cur;
    int				kind;
    Geometry*			shadows;

				Light(Map& map, float x, float y, float z, int kind_, float radius_, float r, float g, float b);
    void			tick(void);

    static void			all_ticks(void);
  };


#endif
