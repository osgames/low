////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "map.H"
#include "light.H"
#include "entity.H"
#include "namedf.H"
#include "manip.H"

namespace Entity {

    Entity	Entity::entities[16384];
    static int	low_free = 1;
    short	Entity::used_ents = 1;

    EIndex Entity::create(int t)
      {
	bool	around = false;

	while(entities[low_free].used)
	  {
	    ++low_free;
	    if(low_free==used_ents)
	      {
		if(around)
		  {
		    if(used_ents<16384)
		      {
			low_free = used_ents++;
			break;
		      }
		    throw "Too many objects in the universe!";
		  }
		around = true;
		low_free = 1;
	      }
	  }

	entities[low_free].is_a(t);

	return low_free;
      }

    void Entity::destroy(void)
      {
	unlist();
	if(parts)
	    delete parts;
	if(panel)
	    delete panel;
	if(index()<low_free)
	    low_free = index();
	used = false;
      }

    void Entity::is_a(int typ)
      {
	memset(this, 0, sizeof(Entity));

	used_ents >?= index()+1;
	used = true;
	type = typ;
	quantity = 1;
	quality = 40;
	mat = t().mat;
	flags = t().flags;

	if(mobile())
	  {
	    phys.mass = t().mass;
	    phys.height = t().height;
	    phys.radius = t().radius;
	    phys.bounce = t().bounce;
	    phys.climboid = true;
	    phys.gravity = .15;
	    phys.flying = flags&mFlying;
	  }
	else if(object())
	  {
	    phys.mass = t().mass;
	    phys.height = t().height;
	    phys.radius = t().radius;
	    phys.bounce = t().bounce;
	    phys.climboid = false;
	    phys.gravity = .05;
	    phys.flying = false;
	  }

	if(t().pg_max_p)
	  {
	    parts = new Particle::Generator(t().pg_max_p);
	    parts->ptick = t().pg_ptick;
	    parts->gtick = t().pg_gtick;
	    parts->xrange = t().pg_xrng;
	    parts->yrange = t().pg_yrng;
	    for(int i=0; i<4; i++)
		parts->col[i] = t().pg_col[i];
	  }
      }

    void Entity::close_gumps(void)
      {
	if(panel)
	    delete panel;
	for(EIndex e=f_contents; e; e=e->next)
	    e->close_gumps();
      }

    void Entity::unlist(void)
      {
	close_gumps();

	if(where==Tile && level==Game::player.p_level)
	  {
	    ::Tile&	t = Game::map(tx, ty);

	    if(light && light->active)
	      {
		light->active = false;
		light->dezone(Game::map);
	      }
	    (next? next->prev: t.l_ent) = prev;
	    (prev? prev->next: t.f_ent) = next;
	  }
	if(where==Inside)
	  {
	    if(object())
		for(EIndex co = container; co; co=co->container)
		    if(co->object())
			    co->mass -= mass;
	    (next? next->prev: container->l_contents) = prev;
	    (prev? prev->next: container->f_contents) = next;
	  }
	if(where==Held && object())
	  {
	    Game::player.carry -= mass;
	    // remove handling goes here
	  }
	Game::player.inv[slot] = 0;
	where = Limbo;
      }

    void Entity::to_world(float x_, float y_, float z_, int face_)
      {
	unlist();
	where = Tile;
	tx = int((x=x_)+.5);
	ty = int((y=y_)+.5);
	tz = int((z=z_)*32);
	facing = face_;
	level = Game::player.p_level;

	if(object())
	  {
	    phys.x = x_;
	    phys.y = y_;
	    phys.z = z_;
	  }

	::Tile&	t = Game::map(tx, ty);
	((next = t.f_ent)? t.f_ent->prev: t.l_ent) = index();
	t.f_ent = index();
	prev = 0;

	t.touched = true;
	if(light)
	  {
	    light->active = true;
	    light->zone(Game::map);
	  }
      }

    void Entity::to_held(int slot_)
      {
	unlist();
	where = Held;
	slot = slot_;
	Game::player.inv[slot] = index();
	if(object())
	    Game::player.carry += mass;
	// wear handling goes here
      }

    void Entity::to_inside(EIndex cont, EIndex before)
      {
	unlist();

	bool	inalready = false;
	if(before) for(EIndex e=cont->f_contents; e; e=e->next)
	    if(e==before)
	      {
		inalready = true;
		((prev = e->prev)? e->prev->next: cont->f_contents) = index();
		e->prev = index();
		next = e;
		break;
	      }

	if(!inalready)
	  {
	    ((prev = cont->l_contents)? cont->l_contents->next: cont->f_contents) = index();
	    cont->l_contents = index();
	    next = 0;
	  }

	container = cont;
	where = Inside;

	if(object())
	    for(EIndex co = cont; co; co=co->container)
		if(co->object())
		    co->mass += mass;
      }

    void Entity::consume(int replace)
      {
	Where	was = where;

	unlist();
	if(replace>=0)
	  {
	    is_a(replace);
	    switch(was)
	      {
		case Tile:
		    to_world(x, y, z, facing);
		    break;
		case Held:
		    to_held(slot);
		    break;
		case Inside:
		    to_inside(container);
		    break;
		default:
		    // nothing special to do
		    break;
	      }
	  }
      }

    const char* Entity::name(bool art) const
      {
	return name(art, false);
      }

    // I know.  Returing a pointer to static data is Bad, but I'm being
    // careful to use it quickly and it's much, much less cruddy when used
    // int vararg context.

    static const char* tname(bool art, bool plur, int id, bool& coll)
      {
	static char	pad[128];
	const char*	raw = Game::str(4, id);

	coll = false;
	if(plur)
	  {
	    if(char* p=strchr(raw, '&'))
		return p+1;
	    else
		art = false;
	  }
	strcpy(pad, raw);
	if(char* p=strchr(pad, '&'))
	    *p = 0;
	if(plur)
	    strcat(pad, "s");
	if(char* u=strchr(pad, '_'))
	  {
	    if(art)
		*u = ' ';
	    else
		return u+1;
	  }
	else
	    coll = true;
	return pad;
      }

    const char* Entity::name(bool art, bool plur) const
      {
	bool	coll;

	if(t().name)
	    return t().name(index());

	if(object() && !mobile())
	  {
	    int	tt = type;
	    if((tt&0x1F0)==0x140 && mat<0x100)
		tt = 0x14F;

	    bool		coll;
	    static char		pad[128];
	    char*		d = pad + 3;
	    const char*	bname = tname(false, quantity!=1, tt, coll);

	    if(coll)
		art = false;
	    if(art && quantity!=1)
	      {
		if(quantity)
		  {
		    sprintf(d, "%d ", quantity);
		    d += strlen(d);
		  }
		else
		  {
		    strcpy(d, "no ");
		    d += 3;
		  }
		art = false;
	      }

	    if(dead)
	      {
		strcpy(d, "dead ");
		d += 5;
	      }

	    // quality adjective
	    //       0 broken
	    // 01-   1 badly damaged
	    //   -   2 damaged
	    //   -   3 strudy (include 28)
	    //   -   4 massive
	    //   -   5 "flawless"
	    if(t().qc<15)
	      {
		int	qa = 5;

		if(quality==0)
		    qa = 0;
		else if(quality<0x10)
		    qa = 1;
		else if(quality<0x20)
		    qa = 2;
		else if(quality<0x30)
		    qa = 3;
		else if(quality<0x3F)
		    qa = 4;
		strcpy(d, Game::str(5, qa+t().qc*6));
		d += strlen(d);
		*d++ = ' ';
	      }

	    // noun
	    strcpy(d, bname);
	    d += strlen(d);

	    // enchantment
	    if(enchanted)
	      {
		sprintf(d, " of %s (%d)", Game::str(6, spell), spell);
		d += strlen(d);
	      }
	    if(charged)
	      {
		sprintf(d, " with %d charges", charges);
		d += strlen(d);
	      }

	    // belongs
	    if(belongs_to)
	      {
		strcpy(d, " belonging to");
		strcat(d, Game::str(1, belongs_to+307));
		d += strlen(d);
	      }

	    d = pad + 3;
	    if(art)
	      {
		*--d = ' ';
		if(pad[3]=='a' || pad[3]=='e' || pad[3]=='i' || pad[3]=='o' || pad[3]=='u')
		    *--d = 'n';
		*--d = 'a';
	      }
	    return d;
	  }

	if(mobile())
	  {
	    static char	pad[128];
	    char	nam[128];
	    const char*	raw = 0;

	    if(s().npc_type && !s().name[0])
		raw = Game::str(7, s().npc_type-0x30);
	    if(!raw)
		raw = Game::str(4, type);

	    strcpy(nam, raw);
	    if(char* p=strchr(nam, '&'))
		*p = 0;
	    char* n = strchr(nam, '_');
	    char* a = "a ";
	    if(n)
	      {
		*n = 0;
		n = n+1;
	      }
	    else
		n = nam;
	    if(!art)
		a = "";
	    else if(int(mood)>1)
		a = "an ";

	    if(s().name[0])
		sprintf(pad, "%s%s %s named %s", a, Game::str(5, 99-mood), n, s().name);
	    else
		sprintf(pad, "%s%s %s", a, Game::str(5, 99-mood), n);

	    return pad;
	  }

	return tname(art, plur, type, coll);
      }

    void Entity::tick(void)
      {
	if(parts)
	    parts->tick();

	if(t().tick)
	    t().tick(index());

	if(models.num_rm)
	    models.tick();

	if(dead)
	  {
	    if(decay>100)
		for(int i=0; i<models.num_rm; i++)
		    models.rm[i].color[3] *= .99;
	    if(++decay > 300)
	      {
		consume(-1);
		return;
	      }
	  }


	if(object() || mobile())
	  {
	    if(where != Tile)
		return;

	    if(!phys.grounded || phys.moving)
	      {
		phys.update();
		x = phys.x;
		y = phys.y;
		z = phys.z;
	      }

	    int     ntx = int(x+.5);
	    int     nty = int(y+.5);

	    if(ntx!=tx || nty!=ty)
		to_world(x, y, z, int(phys.h/45+.5));
	  }
      }

    void Entity::look(void) const
      {
	if(t().look)
	    t().look(index());
	else
	  {
	    char	pad[128];

	    strcpy(pad, Game::str(1, 276));
	    strcat(pad, name(true));
	    msg(pad);
	  }
	Game::trigger(index(), 0x1A3);
      }


    void Entity::pickup(void)
      {
	if((flags&oTakable)==0)
	  {
	    msg(Game::str(1, 109));
	    return;
	  }
	if(Game::player.inv[0])
	    throw "Manipulating two objects at the same time?";
	belongs_to = 0;
	to_held(0);
      }

    void Entity::use(bool force)
      {
	if(t().use)
	  {
	    if(t().use(index(), force))
		Game::trigger(index(), 0x1A2);
	  }
      }

    void Entity::is_mobile(int spc)
      {
	is_a(species[spc].type);

	spec = spc;
	for(int i=0; i<s().num_rm; i++)
	    models.rm[i] = s().rm[i];
	models.num_rm = s().num_rm;
	models.scale = s().scale;
	models.ani_seq = models.ani_idle = (flags&mFlying)? 1000: 1000;
	models.ani_loop = true;
      }


    void all_ticks(void)
      {
	for(int i=0; i<16384; i++)
	  {
	    Entity&	e = Entity::entities[i];

	    if(e.used && ( (e.where==Tile && e.level==Game::player.p_level)
			|| (e.where==Held)))
		e.tick();
	  }
      }

    void populate(void)
      {
	FOR_EVERY_ENT(e)
	    if(e->where==Tile && e->level==Game::player.p_level)
	      {
		e->where = Limbo;
		e->to_world(e->x, e->y, e->z, e->facing);
	      }
      }

}; // namespace Entity

