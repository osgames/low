////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "worldg.H"
#include "light.H"
#include "game.H"
#include "worldg.H"
#include "map.H"

Geometry::Geometry(const Light* l): nv(0), np(0), v(0), p(0)
  {
    Polygon*	p;
    PVS*	pvs = Game::map(int(trunc(l->cur.x)), int(trunc(l->cur.y))).pvs;

    if(!pvs)
	return;

    for(int tn=-1; tn<=pvs->last[int(ceil(l->cur.radius))]; tn++) {
	Tile&		t = Game::map(tn<0? int(trunc(l->cur.x)): pvs->v[tn].x, tn<0? int(trunc(l->cur.y)): pvs->v[tn].y);
	//Tile&		t = Game::map(pvs->v[tn].x, pvs->v[tn].y);

	if(!t.geometry || t.flags&Tile::Dirty) {
	    if(t.geometry)
		delete t.geometry;
	    t.geometry = new Geometry(tn<0? int(trunc(l->cur.x)): pvs->v[tn].x, tn<0? int(trunc(l->cur.y)): pvs->v[tn].y, t);
	    t.flags &= ~Tile::Dirty;
	}

	if(!t.geometry)
	    continue;

	// We start with 2 because the first two polygons in a tile geometry
	// are always the final floor and final cieling, which cannot project
	// shadows on anything.  (Actually, strictly speaking, the floor could
	// but its shadow would always fall completely within those of the
	// lower tile walls and would therefore always be redundant).
	for(int tpn=2; tpn<t.geometry->np; tpn++) {
	    Polygon&	tp = t.geometry->p[tpn];
	    if(l->cur.x*tp.p->n[0] + l->cur.y*tp.p->n[1] + l->cur.z*tp.p->n[2] < -tp.d)
		continue;
	    float nx[tp.n]; float ny[tp.n]; float nz[tp.n];
	    float fx[tp.n]; float fy[tp.n]; float fz[tp.n];
	    bool overfar = false;
	    for(int i=0; i<tp.n; i++) {
		nx[i] = t.geometry->v[tp.v[i]*3];
		ny[i] = t.geometry->v[tp.v[i]*3+1];
		nz[i] = t.geometry->v[tp.v[i]*3+2];
		float	dx = nx[i]-l->cur.x;
		float	dy = ny[i]-l->cur.y;
		float	dz = nz[i]-l->cur.z;
		float	di = sqrt(dx*dx+dy*dy+dz*dz);
		if(di>l->cur.radius) {
		    overfar = true;
		    break;
		}
		float	ra = l->cur.radius/di;
		fx[i] = l->cur.x+dx*ra;
		fy[i] = l->cur.y+dy*ra;
		fz[i] = l->cur.z+dz*ra;
	    }
	    if(overfar)
		continue;

	    for(int i=0; i<tp.n; i++) {
		p = addpoly();
		addv(p, nx[i], ny[i], nz[i]);
		addv(p, fx[i], fy[i], fz[i]);
		addv(p, fx[(i+1)%tp.n], fy[(i+1)%tp.n], fz[(i+1)%tp.n]);
		addv(p, nx[(i+1)%tp.n], ny[(i+1)%tp.n], nz[(i+1)%tp.n]);
	    }
	    p = addpoly();
	    for(int i=tp.n-1; i>=0; --i)
		addv(p, fx[i], fy[i], fz[i]);
	    p = addpoly();
	    for(int i=0; i<tp.n; i++)
		addv(p, nx[i], ny[i], nz[i]);
	}
    }
  }

Light::Light(Map& map, float x, float y, float z, int kind_, float radius_, float r, float g, float b)
  {
    next = map.lights;
    map.lights = this;
    kind = kind_;
    base.x = x;
    base.y = y;
    base.z = z;
    base.radius = radius_;
    base.diffuse[0] = base.specular[0] = r;
    base.diffuse[1] = base.specular[1] = g;
    base.diffuse[2] = base.specular[2] = b;
    cur = base;
  }


void Light::tick(void)
  {
    bool	moved = false;
    float	r1, r2;

    switch(kind) {
	case 0: break;
	case 1:
	case 2:
	    if(random()&3) break;
	    r1 = drand48()*drand48();
	    r2 = r1 + r1*drand48()*.25;
	    cur.diffuse[0] = base.diffuse[0] * (1.0+r2*.2);
	    cur.diffuse[1] = base.diffuse[1] * (1.0+r1*.2);
	    cur.diffuse[2] = base.diffuse[2] * (1.0+r1*.2);
	    if(random()&3) break;
	    cur.z = base.z + .05*r2;
	    r1 = drand48()*drand48();
	    r2 = drand48()*drand48();
	    cur.x = base.x -.01 +.02*r1;
	    cur.y = base.y -.01 +.02*r2;
	    moved = true;
	    break;
    }

    if(moved && shadows) {
	delete shadows;
	shadows = 0;
    }
  }

