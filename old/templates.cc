////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "fileio.H"
#include "map.H"
#include "light.H"
#include "entity.H"
#include "namedf.H"

namespace Entity {


    Template	Entity::templates[512];
    Species     Entity::species[320];

    static void skip(const char*& s)
      {
	while(*s > ' ')
	    ++s;
	while(*s==' ' || *s=='\t')
	    ++s;
      }

    static int geti(const char*& s, int base=10)
      {
	int i = strtol(s, 0, base);
	skip(s);
	return i;
      }

    static float getf(const char*& s)
      {
	float f = strtod(s, 0);
	skip(s);
	return f;
      }

    static const char* getw(const char*& s)
      {
	static char	pad[32];
	int		i = 0;

	while(*s>' ')
	  {
	    if(i<32)
		pad[i++] = *s;
	    s++;
	  }
	pad[i] = 0;
	while(*s==' ' || *s=='\t')
	    ++s;
	return pad;
      }

    Template::Template(void)
      {
	flags = 0;
	model = 511;
	r_scale = color[0] = color[1] = color[2] = color[3] = 1.0;
	r_rot = r_xlate[0] = r_xlate[1] = r_xlate[2] = 0.0;

	radius = 1;
	height = 4;
	value = 0;
	mass = 1;
	bounce = .1;

	name = 0;
	look = 0;
	tick = 0;
	wear = 0;
	remove = 0;
	use = 0;
	render_pre = 0;
	render_post = 0;


	l_range = 0;

	pg_max_p = 0;
	pg_ptick = 0;
	pg_gtick = 0;
      }

    Species::Species(void)
      {
	name[0] = 0;
	gender = 0;
	type = 0x40;
	num_rm = 1;
	scale = 0.4;
	rm[0].id = 122;
	rm[0].color[0] = rm[0].color[1] = rm[0].color[2] = rm[0].color[3] = .5;
      }

    bool initialize(void)
      {
	for(int i=0x40; i<0x80; i++)
	  {
	    Entity::templates[i].radius = 1;
	    Entity::templates[i].height = 24;
	  }

	File		fp = openlow("templates.dat");
	char		pad[1024];

	while(fp.readln(pad, 1023))
	  {
	    const char*		s = pad;
	    while(*s==' ')
		s++;
	    int			i = geti(s);
	    Template&		t = Entity::templates[i];

	    t.flags = geti(s, 16);
	    t.qc = geti(s, 16);
	    t.radius = geti(s);
	    t.height = geti(s);
	    t.mass = geti(s);

	    while(*s>' ') switch(*s++)
	      {
		case 'v':
		    t.value = geti(s);
		    break;
		case 'b':
		    t.bounce = getf(s);
		    break;
		case 'L':
		    skip(s);
		    t.l_range = geti(s);
		    t.l_type = geti(s);
		    t.l_col[0] = getf(s);
		    t.l_col[1] = getf(s);
		    t.l_col[2] = getf(s);
		    skip(s);
		    break;
		case 'm':
		    t.model = geti(s);
		    break;
		case 'c':
		    skip(s);
		    t.color[0] = getf(s);
		    t.color[1] = getf(s);
		    t.color[2] = getf(s);
		    if(*s!='}')
			t.color[3] = getf(s);
		    else
			t.color[3] = 1.0;
		    skip(s);
		    break;
		case 'x':
		    skip(s);
		    t.r_xlate[0] = getf(s);
		    t.r_xlate[1] = getf(s);
		    t.r_xlate[2] = getf(s);
		    if(*s!='}')
			t.r_rot = getf(s);
		    skip(s);
		    break;
		case 's':
		    skip(s);
		    for(int i=0; i<8; i++)
			t.solid[i] = geti(s, 16);
		    t.flags |= eFixed;
		    skip(s);
		    break;
		case 'G':
		    skip(s);
		    t.pg_max_p = geti(s);
		    while(*s>' ' && *s !='}') switch(*s++)
		      {
			case 'p':
			    t.pg_ptick = FL_part_ptick::find(getw(s));
			    break;
			case 'g':
			    t.pg_gtick = FL_part_gtick::find(getw(s));
			    break;
			case 'c':
			    skip(s);
			    for(int i=0; i<4; i++)
				t.pg_col[i] = getf(s);
			    skip(s);
			    break;
			case 'n':
			    skip(s);
			    for(int i=0; i<3; i++)
				t.pg_nrm[i] = getf(s);
			    skip(s);
			    break;
			case 'x':
			    t.pg_xrng = getf(s);
			    break;
			case 'y':
			    t.pg_yrng = getf(s);
			    break;
			default:
			    skip(s);
			    break;
		      }
		    skip(s);
		    break;
		case 'n':
		    t.name = FL_ent_name::find(getw(s));
		    break;
		case 'l':
		    t.look = FL_ent_look::find(getw(s));
		    break;
		case 't':
		    t.tick = FL_ent_tick::find(getw(s));
		    break;
		case 'w':
		    t.wear = FL_ent_wear::find(getw(s));
		    break;
		case 'W':
		    t.remove = FL_ent_remove::find(getw(s));
		    break;
		case 'u':
		    t.use = FL_ent_use::find(getw(s));
		    break;
		case 'r':
		    t.render_pre = FL_ent_render_pre::find(getw(s));
		    break;
		case 'R':
		    t.render_post = FL_ent_render_post::find(getw(s));
		    break;
		default:
		    skip(s);
		    break;
	      }
	  }
	fp.close();

	fp = openlow("species.dat");

	while(fp.readln(pad, 1023))
	  {
	    if(pad[0]=='#')
		continue;

	    const char*	s = pad;
	    int		id;
	    Species&	t = Entity::species[id=geti(s, 16)];
	    int		n;

	    t.num_rm = 0;
	    t.npc_type = geti(s, 16);
	    t.scale = getf(s);
	    t.type = geti(s, 16);

	    if(id>0x3F)
	      {
		Species&	b = Entity::species[t.type-0x40];
		t.num_rm = b.num_rm;
		for(int i=0; i<t.num_rm; i++)
		    t.rm[i] = b.rm[i];
	      }
	    while(*s > ' ') switch(*s)
	      {
		case '{':
		    skip(s);
		    t.num_rm = 0;
		    while(*s>' ' && *s!='}') switch(*s)
		      {
			case '#':
			    s++;
			    n = geti(s, 16);
			    t.rm[t.num_rm-1].color[0] = float((n>>16)&255)/255.0;
			    t.rm[t.num_rm-1].color[1] = float((n>>8)&255)/255.0;
			    t.rm[t.num_rm-1].color[2] = float(n&255)/255.0;
			    break;
			case '.':
			    skip(s);
			    break;
			default:
			    t.rm[t.num_rm].color[0] = 1.0;
			    t.rm[t.num_rm].color[1] = 1.0;
			    t.rm[t.num_rm].color[2] = 1.0;
			    t.rm[t.num_rm].color[3] = 1.0;
			    t.rm[t.num_rm++].id = geti(s);
			    break;
		      }
		    skip(s);
		    break;
		case 'm':
		    t.gender = 1;
		    skip(s);
		    break;
		case 'f':
		    t.gender = 2;
		    skip(s);
		    break;
		case '"':
		    s++;
		    for(char* d=t.name; *s!='"'; )
		      {
			*d++ = *s++;
			*d = 0;
		      }
		    skip(s);
		    break;
		default:
		    skip(s);
		    break;
	      }
	  }
	fp.close();

	return false;
      }

    bool load(void)
      {
	char		pad[1024];
	File		fp = openlow("entities.dat");

	if(!fp)
	    return true;

	while(fp.readln(pad, 1023))
	  {
	    const char*		s = pad;

	    if(*s != '0')
		continue;

	    EIndex	e(geti(s));

	    if(*s=='*')
	      {
		s++;
		e->is_mobile(geti(s, 16));
		e->mood = Mood(geti(s));
	      }
	    else
		e->is_a(geti(s, 16));

	    while(*s>' ') switch(*s++)
	      {
		case 'q':
		    e->quantity = geti(s);
		    break;
		case 'b':
		    e->belongs_to = geti(s, 16);
		    break;
		case 's':
		    e->enchanted = true;
		    e->spell = geti(s);
		    break;
		case 'c':
		    e->charged = true;
		    e->charges = geti(s);
		    break;
		case 'm':
		    e->mat = geti(s);
		    break;
		case 'x':
		    e->special = geti(s);
		    break;
		case 'h':
		    e->to_held(geti(s));
		    break;
		case 'l':
		    e->link = geti(s);
		    break;
		case 'i':
		    e->where = Suspended;
		    e->container = geti(s);
		    break;
		case 'f':
		    e->bits = geti(s);
		    break;
		case 'Q':
		    e->quality = geti(s, 10);
		    break;
		case 'd':
		    skip(s);
		    for(int v=0; v<8 && *s!='}'; v++)
			e->val[v] = geti(s);
		    while(*s != '}')
			skip(s);
		    skip(s);
		    break;
		case 'w':
		    skip(s);
		    e->where = Tile;
		    e->level = geti(s);
		    e->phys.x = e->x = getf(s);
		    e->phys.y = e->y = getf(s);
		    e->phys.z = e->z = getf(s);
		    e->facing = geti(s);
		    e->phys.grounded = true;
		    e->phys.moving = false;
		    e->phys.speed = 0;
		    e->phys.vspeed = 0;
		    if(e->mobile())
		      {
			e->phys.aim_speed = 0;
			e->phys.aim_h = 45*e->facing;
		      }
		    skip(s);
		    break;
		default:
		    skip(s);
		    break;
	      }
	  }
	fp.close();

	FOR_EVERY_ENT(e)
	  {
	    if(e->where==Suspended)
	      {
		e->where = Limbo;
		e->to_inside(e->container);
	      }
	  }

	return false;
      }

}; // namespace Entity


FL_ent_name*            FL_ent_name::list       = 0;
FL_ent_look*            FL_ent_look::list       = 0;
FL_ent_tick*            FL_ent_tick::list       = 0;
FL_ent_wear*            FL_ent_wear::list       = 0;
FL_ent_remove*          FL_ent_remove::list     = 0;
FL_ent_use*             FL_ent_use::list        = 0;
FL_ent_render_pre*      FL_ent_render_pre::list = 0;
FL_ent_render_post*     FL_ent_render_post::list= 0;

