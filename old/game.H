////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "strings.H"

#ifndef LOW_GAME_H__
#define LOW_GAME_H__

struct ScrollPanel;
struct Map;
struct Mobile;
struct Light;

namespace Game {


    struct Mode
      {
	void		(*keydown)(unsigned int sym, unsigned int mod);
	void		(*keypress)(unsigned int key);
	void		(*keyup)(unsigned int sym);
	void		(*hover)(void);
	void		(*press)(int button);
	void		(*release)(int button);
	void		(*quit)(void);
	void		(*tick)(void);
	void		(*refresh)(float, float, float);
	void		(*motion)(float xmotion, float ymotion);
      };

    extern bool		fullscreen;
    extern float	mousex;
    extern float	mousey;

    extern int		paused;

    extern Mode*	mode;

    extern bool startup_video(void);
    extern bool	startup_sound(int quality);

    extern void	event(int code, void* parm);
    extern void start(void);
    extern void menu(void);

    extern void	mode_leave(void);	// switch to leave_level mode
    extern void	mode_load(void);	// switch to mode_load mode

    extern void	mouselook(bool);

    extern ::ScrollPanel*	console_p;

    struct Setup
      {
	enum Level {
	    Low=0, Medium=1, High=2,
	};

	int		paperdoll_anim;
	int		paperdoll_model;

	Level		wall_detail;
	Level		floor_detail;	// cieling also
	Level		texture_detail;
	Level		anim_detail;
	bool		fragment_programs;
	bool		specular_highlights;
	bool		do_shadows;
	int		music_volume;
	int		sound_volume;
	int		sight_depth;
	float		gamma;
	bool		framelock;
	bool		bumpmapping;
	bool		footsteps;
	bool		mlook;
	bool		yrev;

	bool		grid_debug;
	bool		martians_debug;

	char*		low_path;
	char*		uw2_path;
      };

    enum Skills {
	Skill_Attack, Skill_Defense,
	Skill_Unarmed, Skill_Swords, Skill_Axes, Skill_Maces, Skill_Missiles,
	Skill_Lore, Skill_Casting,
	Skill_Traps, Skill_Search, Skill_Track, Skill_Stealth,
	Skill_Repair, Skill_Charisma, Skill_Acrobat, Skill_Appraise,
	Skill_Swimming,
    };

    struct Stats // player or mobile
      {
	bool		female;
	bool		fighting;
	unsigned char	st_str;
	unsigned char	st_dex;
	unsigned char	st_int;
	unsigned char	hp, hp_max;
	unsigned char	mp, mp_max;
	unsigned char	poison;
	unsigned char	xp_lev;
	unsigned char	sk_lev[20];
      };

    struct Player
      {
	// geometry of player
	int		p_level;
	float		p_x, p_y, p_z, p_h;
	Mobile*		mob;

	int		light;		// light generation

	// character stats
	Stats		stats;
	unsigned short	xp, xp_next;
	unsigned char	sk_points;	// to spend

	char		name[16];

	// active magic
	unsigned short	active_tk;
	unsigned short	active_ww;
	unsigned short	active_jump;
	unsigned short	active_bounce;
	unsigned short	active_ff;
	unsigned short	active_lev;
	unsigned short	active_fly;
	unsigned short	active_flameproof;
	unsigned short	active_stoneskin;
	unsigned short	active_ironskin;
	unsigned short	active_mprotect;
	unsigned short	active_light;
	unsigned short	active_sunlight;

	int		carry;

	// quest flags and friends
	char		qflag[256];

	// inventory
	unsigned short	inv[25];	// 0 - temp holding 1-8 inv 9+ worn/held 19-24 barter
      };

    extern float	view[16];

    extern int		tick;

    extern Map		map;
    extern Player	player;
    extern Setup	setup;
    extern Light*	plight;

    extern unsigned int	cursor_ii;
    extern int		cursor_x;
    extern int		cursor_y;

    extern Strings::Strings	str;

} // namespace Game

extern void		msg(const char* fmt, ...);
extern void		set_perspective(void);

#endif
