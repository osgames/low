////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#include "map.H"
#include "entity.H"
#include "namedf.H"
#include "manip.H"

namespace Entity {


    void Entity::think(void)
      {
	if(where!=Tile)
	    return;

	/*Physics::Collision	c =*/ phys.update();

	x = phys.x;
	y = phys.y;
	z = phys.z;

	int	ntx = int(x+.5);
	int	nty = int(y+.5);

	if(ntx!=x || nty!=y)
	    to_world(x, y, int(z*32), int(phys.aim_h/45+.5));

	models.tick();
      }

    void Entity::die(bool)
      {
	if(!mobile())
	    return;
	dead = true;

	phys.grounded = false;
	phys.moving = true;
	phys.flying = false;
	phys.climboid = false;

	models.ani_seq = 1300;
	models.ani_loop = false;
	models.ani_done = false;
	models.ani_stop = true;
	models.ani_time = 0;

	while(EIndex o=f_contents)
	  {
	    o->to_world(x, y, z-.2, facing);
	    o->phys.speed = 1+.5*drand48();
	    o->phys.vspeed = 1+.3*drand48();
	    o->phys.moving = true;
	    o->phys.grounded = false;
	    o->phys.h = 359*drand48();
	  }
      }


}; // namespace Entity

