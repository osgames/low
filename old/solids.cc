////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include <math.h>

#include "setup.H"
#include "worldg.H"
#include "map.H"
#include "game.H"
#include "worldg.H"
#include "solids.H"
#include "objects.H"

struct collision_detail {
    Mobile*		m;
    Mobile*		victim;
    const Polygon*	poly;
    double		w;		// point at which the collision occured
    float		n[3];		// collision normal
};

struct mobile_detail {
    Mobile*		m;
    float		x, y, z;
    float		r;
    float		v[3];		// impulse
};


bool sphere_hit_lseg(float sx1, float sy1, float sz1,
		     float sx2, float sy2, float sz2,
		     const mobile_detail& m,
		     collision_detail& col)
  {
    float	dx = sx2-sx1;
    float	dy = sy2-sy1;
    float	dz = sz2-sz1;

    float	ilw = ((m.x-sx1)*dx + (m.y-sy1)*dy + (m.z-sz1)*dz)
		    / (dx*dx + dy*dy + dz*dz);

    if(ilw<0.0) ilw = 0.0;
    if(ilw>1.0) ilw = 1.0;

    float	xi = ilw*dx + sx1;
    float	yi = ilw*dy + sy1;
    float	zi = ilw*dz + sz1;

    // Solving (x+w*dx-xi)^2 + (y+w*dy-yi)^2 + (z+w*dz-zi)^2 - r^2 = 0 for
    // w gives us the point along w[dx dy dz] where our sphere of radius
    // r hits [xi yi zi]; if the point lies outside [0 .. 1] then we
    // are not hitting it.  Nan means it cannot be hit along that vector
    // at all.

    xi -= m.x;
    yi -= m.y;
    zi -= m.z;

    double a = m.v[0]*m.v[0] + m.v[1]*m.v[1] + m.v[2]*m.v[2];
    double b = -2*m.v[0]*xi - 2*m.v[1]*yi - 2*m.v[2]*zi;
    double c = (xi*xi + yi*yi + zi*zi - m.r*m.r);

    double w = ( -b - sqrt(b*b - 4*a*c) ) / (2*a);

    if(w>=0 && w<=1 && w<col.w) {
	col.w = w;
	col.m = m.m;
	col.victim = 0;
	col.n[0] = w*(m.v[0]-xi);
	col.n[1] = w*(m.v[1]-yi);
	col.n[2] = w*(m.v[2]-zi);
	double r = sqrt(col.n[0]*col.n[0] + col.n[1]*col.n[1] + col.n[2]*col.n[2]);
	col.n[0] /= r;
	col.n[1] /= r;
	col.n[2] /= r;
	return true;
    }

    return false;
  }

static void detect_collision(collision_detail& col, const mobile_detail& m, const Polygon& p, const Geometry& g)
  {
    double	x, y, z;

    // 1. Does mobile sphere cross the polygon plane at all?
    double	ds = p.p->n[0]*m.x + p.p->n[1]*m.y + p.p->n[2]*m.z + p.d;
    double	de = p.p->n[0]*(m.x+m.v[0]) + p.p->n[1]*(m.y+m.v[1]) + p.p->n[2]*(m.z+m.v[2]) + p.d;

    //    no possibility
    if((ds>m.r*1.1 && de>m.r*1.1) || (ds<-m.r*1.1 && de<-m.r*1.1) || (de>ds))
	return;

    double	w = (m.r-ds)/(de-ds);
    if(w>=0 && w<=1) {
	// we touch the plane from the front this impulse; check that the contact point
	// lies within the polygon: quick way out, bounce against plane normal.
	// Since most of our polygons are on axis-aligned planes, and we have already
	// determined the contact point lies within the plane, we only check against
	// the bounding box.  This is incorrect for slanted polygons, but not by
	// enough so that you'd notice.  :-)

	x = m.x+w*m.v[0]-m.r*p.p->n[0];
	y = m.y+w*m.v[1]-m.r*p.p->n[1];
	z = m.z+w*m.v[2]-m.r*p.p->n[2];
	double minx = g.v[p.v[0]*3], miny = g.v[p.v[0]*3+1], minz = g.v[p.v[0]*3+2];
	double maxx = g.v[p.v[0]*3], maxy = g.v[p.v[0]*3+1], maxz = g.v[p.v[0]*3+2];
	for(int i=1; i<p.n; i++) {
	    minx <?= g.v[p.v[i]*3];
	    miny <?= g.v[p.v[i]*3+1];
	    minz <?= g.v[p.v[i]*3+2];
	    maxx >?= g.v[p.v[i]*3];
	    maxy >?= g.v[p.v[i]*3+1];
	    maxz >?= g.v[p.v[i]*3+2];
	}
	if(x>=minx && x<=maxx && y>=miny && y<=maxy && z>=minz && z<=maxz) {
	    if(w<col.w) {
		col.m = m.m;
		col.victim = 0;
		col.poly = &p;
		col.w = w;
		col.n[0] = p.p->n[0];
		col.n[1] = p.p->n[1];
		col.n[2] = p.p->n[2];
	    }
	    return;
	}

    }

    // Here we check against polygon edges.
    for(int i=0; i<p.n; i++)
	if(sphere_hit_lseg(g.v[p.v[i]*3], g.v[p.v[i]*3+1], g.v[p.v[i]*3+2],
		    g.v[p.v[(i+1)%p.n]*3], g.v[p.v[(i+1)%p.n]*3+1], g.v[p.v[(i+1)%p.n]*3+2],
		    m, col))
	    col.poly = &p;

    // We haven't caught all cases, apparently, because it's still
    // possible to slip through geometry.  But it's hard to do, even
    // on purpose, and I haven't been able to reproduce it reliably.

    // *sigh*
  }

static Mobile*	mobiles = 0;

Mobile::Mobile(void)
  {
    next = mobiles;
    mobiles = this;
  }

void initialize_solids(void)
  {
    Mobile*	p = new Mobile;

    Game::player.mob = p;
    p->r = .1;
    p->dx = 0;
    p->dy = 0;
    p->dz = .25;
    p->r2 = .1;
    p->legs = .45;
    p->stopped = false;
    p->grounded = 0;
    p->inert = false;
    p->floating = false;
    p->two_spheres = true;
    p->s[0] = p->s[1] = p->s[2] = p->t[0] = p->t[1] = p->t[2] = 0;
    p->accel = .25;
    p->next = 0;
    p->object = 0;
  }

void apply_impulses(double time)
  {
    for(Mobile* m=mobiles; m; m=m->next) {
	if(m->stopped)
	    continue;
	m->x += time*m->s[0];
	m->y += time*m->s[1];
	m->z += time*m->s[2];
	if(m->object)
	    m->object->to_world(m->x, m->y, m->z, m->object->facing);
	if(m->grounded) {
	    Tile&	t = Game::map(int(trunc(m->x)), int(trunc(m->y)));
	    m->grounded = 0;
	    if(!t.geometry)
		throw "Ack! No geometry at destination?!";
	    for(int pn=0; pn<t.geometry->np; pn++)
		if(t.geometry->p[pn].p->n[2] > .5) {
		    Polygon&	p = t.geometry->p[pn];
		    double fh = m->z - ((m->x*p.p->n[0] + m->y*p.p->n[1] + p.d) / -p.p->n[2]);
		    //printf("fh = %lf\n", fh);
		    if(fh>=-.05 && fh<=(m->legs+.3)) {
			m->grounded = &p;
			break;
		    }
		}
	}
    }
  }

void step_solids(double time)
  {
    // First, do accelerations
    for(Mobile* m=mobiles; m; m=m->next) {
	if(!m->inert && m->grounded) {
	    // go to target speed
	    double	wx = m->t[0]-m->s[0];
	    double	wy = m->t[1]-m->s[1];
	    double	wa = sqrt(wx*wx+ wy*wy);
	    double	a = (m->accel/wa) <? 1.0;
	    m->s[0] += wx*a;
	    m->s[1] += wy*a;
	}
	if(m->stopped)
	    continue;
	if(m->grounded && m->inert) {
	    // simplistic friction
	    m->s[0] /= 2;
	    m->s[1] /= 2;
	    m->s[2] /= 2;
	    if((m->s[0]*m->s[0] + m->s[1]*m->s[1] + m->s[2]*m->s[2]) < .0025) { // ~ .05**2
		m->s[0] = m->s[1] = m->s[2] = 0;
		m->stopped = true;
		continue;
	    }
	}
	if(!m->grounded && !m->floating) {
	    // Apple a la newton
	    m->s[2] += time*-4.5;
	    if(m->s[2] < -5)
		m->s[2] = -5;
	}
	if(!m->inert && m->grounded) {
	    double fh = (m->x*m->grounded->p->n[0] + m->y*m->grounded->p->n[1] + m->grounded->d)
		/ -m->grounded->p->n[2];
	    if(m->z != fh+m->legs) {
		double d = fh+m->legs-m->z;
		if(fabs(d)<.05)
		    m->z = fh+m->legs;
		else
		    m->z += d-(d/2 <? .2);
	    }
	    m->s[2] = 0;
	}
    }

    // Accelerations have been applied; now we iterate over time until all collisions have been resolved.

    while(time > .001) {
	collision_detail	col = { 0, 0, 0, 1.0 };
	for(Mobile* m=mobiles; m; m=m->next) {
	    mobile_detail	md = { m, m->x, m->y, m->z, m->r, { m->s[0]*time, m->s[1]*time, m->s[2]*time } };
	    mobile_detail	md2= { m, m->x+m->dx, m->y+m->dy, m->z+m->dz, m->r2, { m->s[0]*time, m->s[1]*time, m->s[2]*time } };

	    for(int tx=int(trunc(m->x-.5)); tx<=int(trunc(m->x+.5)); tx++)
		for(int ty=int(trunc(m->y-.5)); ty<=int(trunc(m->y+.5)); ty++) {
		    Tile&	t = Game::map(tx, ty);
		    if(!t.geometry || (t.flags&Tile::Dirty)) {
			if(t.geometry)
			    delete t.geometry;
			t.geometry = new Geometry(tx, ty, t);
			t.flags &= ~Tile::Dirty;
		    }
		    for(int pn=0; pn<t.geometry->np; pn++) {
			detect_collision(col, md, t.geometry->p[pn], *t.geometry);
			if(m->two_spheres)
			    detect_collision(col, md2, t.geometry->p[pn], *t.geometry);
		    }
		}
	}

	if(col.w < 1.0) {
	    if(col.w > 0)
		apply_impulses(time*col.w);
	    time *= (1-col.w);
	    double ndot = 2 * (col.n[0]*col.m->s[0] + col.n[1]*col.m->s[1] + col.n[2]*col.m->s[2]);
	    col.m->s[0] = (col.m->s[0] - col.n[0]*ndot)*.5;
	    col.m->s[1] = (col.m->s[1] - col.n[1]*ndot)*.5;
	    col.m->s[2] = (col.m->s[2] - col.n[2]*ndot)*.5;
	    if(col.n[2]>.5 && (fabs(col.m->s[2])<.1 || !col.m->inert)) {
		col.m->s[2] = 0;
		col.m->grounded = col.poly;
	    }
	} else {
	    apply_impulses(time);
	    return;
	}
    }
  }


void step_solids(void)
  {
    if(Game::player.mob) {
	Game::player.mob->x = Game::player.p_x;
	Game::player.mob->y = Game::player.p_y;
	Game::player.mob->z = Game::player.p_z;
    }
    step_solids(.040);
    if(Game::player.mob) {
	Game::player.p_x = Game::player.mob->x;
	Game::player.p_y = Game::player.mob->y;
	Game::player.p_z = Game::player.mob->z;
    }
  }

