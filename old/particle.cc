////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "particle.H"

FL_part_ptick*	FL_part_ptick::list	= 0;
FL_part_gtick*	FL_part_gtick::list	= 0;

namespace Particle {


    Particle::Particle(void)
      {
	radial = polar = stuck = false;
      }

    Generator::Generator(int max)
      {
	next = 0;
	max_p = max;
	act_p = 0;
	parts = new Particle[max];
	ptick = 0;
	gtick = 0;
	lighted = false;
      }

    Generator::~Generator()
      {
	delete[] parts;
	if(next)
	    delete next;
      }

    void Generator::tick(void)
      {
	if(gtick)
	    gtick(*this);
	for(int p=0; p<act_p; )
	  {
	    if(!parts[p].stuck)
	      {
		parts[p].xyz[0] += parts[p].vec[0];
		parts[p].xyz[1] += parts[p].vec[1];
		parts[p].xyz[2] += parts[p].vec[2];
	      }
	    parts[p].age++;
	    if(parts[p].age==255 || ptick(parts[p]))
		parts[p] = parts[--act_p];
	    else
		p++;
	  }
	if(next)
	    next->tick();
      }

    void Generator::render(void)
      {
	float		ls = 0;
	float		pos[3];

	if(lighted)
	    glDisable(GL_LIGHTING);
	else
	    glEnable(GL_LIGHTING);
	glBegin(GL_POINTS);
	for(int p=0; p<act_p; p++)
	  {
	    pos[0] = xyz[0];
	    pos[1] = xyz[1];
	    pos[2] = xyz[2];
	    if(ls != parts[p].size)
	      {
		glEnd();
		glPointSize(ls=parts[p].size);
		glBegin(GL_POINTS);
	      }
	    if(lighted)
		glColor4fv(parts[p].col);
	    else
		glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, parts[p].col);
	    if(parts[p].radial)
	      {
		pos[0] += sin(parts[p].xyz[0])*parts[p].xyz[1];
		pos[1] += cos(parts[p].xyz[0])*parts[p].xyz[1];
		pos[2] += parts[p].xyz[2];
	      }
	    else if(parts[p].polar)
	      {
		pos[0] += sin(parts[p].xyz[0])*parts[p].xyz[2]*cos(parts[p].xyz[1]);
		pos[1] += cos(parts[p].xyz[0])*parts[p].xyz[2]*cos(parts[p].xyz[1]);
		pos[2] += sin(parts[p].xyz[1])*parts[p].xyz[2];
	      }
	    else
	      {
		pos[0] += parts[p].xyz[0];
		pos[1] += parts[p].xyz[1];
		pos[2] += parts[p].xyz[2];
	      }
	    glVertex3fv(pos);
	  }
	glEnd();
      }

}; // namespace Particle

