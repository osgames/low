
#include "md.H"
#include <signal.h>
#include <sys/time.h>


static volatile int	missed_timers = 0;

static void timer_signal_handler(int)
  {
    missed_timers++;
  }

void* Timer::timer_function(void* vp)
  {
    Timer*		that = reinterpret_cast<Timer*>(vp);
    struct sigaction	sa;
    sigset_t		ss;
    itimerval		it = { { 0, that->micros }, { 0, that->micros } };

    sigemptyset(&ss);
    sigaddset(&ss, SIGALRM);
    sa.sa_handler = timer_signal_handler;
    sa.sa_flags = 0;
    sa.sa_mask = ss;
    sigaction(SIGALRM, &sa, 0);

    setitimer(ITIMER_REAL, &it, 0);

    for(;;) {
	int signum;

	sigwait(&ss, &signum);

	int ticks = missed_timers;
	missed_timers -= ticks;
	do {
	    that->tick();
	} while(ticks--);
    }
  }

void Timer::start(int us, void (*tf)(void))
  {
    micros = us;
    tick = tf;
    Thread::start(timer_function, this);
  }

