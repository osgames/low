////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#ifndef LOW_CONSOLE_H__
#define LOW_CONSOLE_H__

enum CLevel {
    Debug, Info, Warning, Error
};

extern void cprintf(CLevel level, const char* fmt, ...);
extern void ccommand(const char*);
extern void rcfile(const char*);

#endif

