////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2005 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////

#include "setup.H"
#include "map.H"
#include "objects.H"
#include "game.H"
#include "light.H"
#include "solids.H"
#include "fileio.H"

#include <string.h>

OProp			Object::oprop[512];
OProp::Melee		Object::mprop[16];
OProp::Armor		Object::aprop[32];
OProp::Container	Object::cprop[16];
OProp::Light		Object::lprop[16];

void Object::initialize(void)
  {
    File	co = openuw2("data/comobj.dat");

    size_t	len = co.size();

    if(len != 11*512+2)
	throw "comobj.dat has a strange length";

    co.seek(2);
    for(int o=0; o<512; o++)
	co.read(oprop+o, 11);
    co.close();
  }

Object::Object(void)
  {
    light = 0;
    mobile = 0;
    where = Limbo;
  }

Object::~Object()
  {
    if(light)
	delete light;
    if(mobile)
	delete mobile;
    if(mob)
	delete[] mob;
  }


// I know.  Returing a pointer to static data is Bad, but I'm being
// careful to use it quickly and it's much, much less cruddy when used
// in vararg context.
static const char* tname(bool art, bool plur, int id, bool& coll)
  {
    static char	pad[128];
    const char*	raw = Game::str(4, id);

    coll = false;
    if(plur) {
	if(char* p=strchr(raw, '&'))
	    return p+1;
	else
	    art = false;
    }
    strcpy(pad, raw);
    if(char* p=strchr(pad, '&'))
	*p = 0;
    if(plur)
	strcat(pad, "s");
    if(char* u=strchr(pad, '_'))
      {
	if(art)
	    *u = ' ';
	else
	    return u+1;
      }
    else
	coll = true;
    return pad;
  }

const char* Object::name(bool art, bool plur) const
  {
    bool	coll;

    if(!movable()) {
	int id = type;
	if((id&0x1F0)==0x140)
	    id = 0x14F;

	static char	pad[192];
	char*		d = pad + 3;
	const char*	bname = tname(false, quantity!=1, id, coll);

	if(coll)
	    art = false;
	if(art && quantity!=1) {
	    if(quantity) {
		sprintf(d, "%d ", quantity);
		d += strlen(d);
	    } else {
		strcpy(d, "no ");
		d += 3;
	    }
	    art = false;
	}

	strcpy(d, bname);
	d += strlen(d);

	d = pad + 3;
	if(art) {
	    *--d = ' ';
	    if(pad[3]=='a' || pad[3]=='e' || pad[3]=='i' || pad[3]=='o' || pad[3]=='u')
		*--d = 'n';
	    *--d = 'a';
	}
	return d;
    }

    return tname(art, plur, type, coll);
  }

void Object::tick(void)
  {
  }

void Object::think(void)
  {
  }

static void delink(unsigned short num, unsigned short& list)
  {
    if(list == num) {
	list = Game::map.o[num]->next;
	return;
    }
    for(unsigned short o=list; o; o=Game::map.o[o]->next)
	if(Game::map.o[o]->next == num) {
	    Game::map.o[o]->next = Game::map.o[num]->next;
	    return;
	}
  }


void Object::unlist(void)
  {
    close_gumps();

    switch(where) {
	case Limbo:
	    break;
	case Tile:
	    if(mobile) {
		delete mobile;
		mobile = 0;
	    }
	    delink(num, Game::map(tile.x, tile.y).objects);
	    Game::map(tile.x, tile.y).flags |= Tile::Dirty;
	    break;
	case Inside:
	case HeldInside:
	    delink(num, Game::map.o[container]->link);
	    break;
	case Held:
	    Game::player.inv[slot] = 0;
	    break;
    }
    where = Limbo;
  }

void Object::close_gumps(void)
  {
  }

void Object::to_world(float x, float y, float z, int face)
  {
  }

void Object::to_held(int)
  {
  }

void Object::consume(int)
  {
  }

void Object::die(bool)
  {
  }

void Object::act(void)
  {
  }

void Object::pickup(void)
  {
  }

void Object::use(bool)
  {
  }

void Object::talk(void)
  {
  }

void Object::look(void) const
  {
    char	pad[256];
    strcpy(pad, Game::str(1, 276));
    strcat(pad, name(true));
    msg(pad);
  }

