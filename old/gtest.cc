////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "tex.H"
#include "granny.H"


Granny::Model*		m = 0;
Granny::Animation*	a = 0;

void main_key(unsigned char k, int, int)
  {
    if(k==27)
	exit(0);
    else
	glutPostRedisplay();
  }

void main_reshape(int w, int h)
  {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, float(w)/h, 0.01, 8.0);
  }

float angle=0;
bool second = false;

static void render_skel(Granny::Bone* b)
  {
    glPushMatrix();
    glBegin(GL_POINTS);
      glVertex3f(0, 0, 0);
    glEnd();

    glMultMatrixf(b->cmatrix);

    for(Granny::Bone* c=b->children; c; c=c->sibling)
	render_skel(c);
    glPopMatrix();
  }

float	time = 0;

static void main_display(void)
  {
    if(second)
      {
	//if(m->apply(&m->anim, time))
	if(m->apply(a, time))
	    time = 0;
	else
	    time += .05;
	//m->bones.byid(26)->xlate[0] += .01;
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	m->bones.root->calculate();
	m->meshes.first->apply_bones();
      }
    second = true;

    float	lcol[] = { 1.0, 1.0, 1.0, 1.0 };
    float	lpos[] = { 1.0, -3.0, .7, 1.0 };
    float	kol[]  = { 1.0, 1.0, 1.0, 1.0 };

    glClearColor(0.2, 0.1, 0.1, 0.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(1, -3, .5, 0, 0, .5, 0, 0, 1);

    glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, kol);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, lcol);
    glLightfv(GL_LIGHT0, GL_POSITION, lpos);

    //glRotatef(angle, 0, 0, 1);
    if((angle += 1) > 359)
	angle = 0;

#if 1
    m->render();
#else
    glPointSize(3);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);
    glColor3fv(lcol);
    render_skel(m->bones.root);
#endif
    glutSwapBuffers();
  }

struct cname_t {
    unsigned long	id;
    const char*		name;
} cnames[] = {
	{ 0xCA5E0103, "Object_Chunk" },
	{ 0xCA5E0200, "Text_Chunk" },
	{ 0xCA5E0F03, "Object_List" },
	{ 0xCA5E0F00, "Object_Object" },
	{ 0xCA5E0F05, "Object_Keys" },
	{ 0xCA5E0F01, "Object_Key" },
	{ 0xCA5E0F06, "Object_Value_Container" },
	{ 0xCA5E0F02, "Object_Value" },
	{ 0xCA5E0602, "Mesh_Chunk" },
	{ 0xCA5E0601, "Object_Mesh" },
	{ 0xCA5E0604, "Points" },
	{ 0xCA5E0603, "Point_Container" },
	{ 0xCA5E0801, "Point_Data" },
	{ 0xCA5E0802, "Normal_Data" },
	{ 0xCA5E0804, "TexMap_Container" },
	{ 0xCA5E0803, "TexMap_Data" },
	{ 0xCA5E0901, "Polygon_Data" },
	{ 0xCA5E0F04, "Object_ID" },
	{ 0xCA5E0507, "Bone_Chunk" },
	{ 0xCA5E0505, "Skeleton" },
	{ 0xCA5E0508, "Bone_List" },
	{ 0xCA5E0506, "Bone_Data" },
	{ 0xCA5E0304, "Texture_Info_Chunk" },
	{ 0xCA5E0301, "Texture_Info_List" },
	{ 0xCA5E0305, "Texture_Container" },
	{ 0xCA5E0303, "Texture_Geometry" },
	{ 0xCA5E0E01, "Texture_Chunk" },
	{ 0xCA5E0E00, "Texture_Texture" },
	{ 0xCA5E0E07, "Texture_List" },
	{ 0xCA5E0E02, "Texture_Poly" },
	{ 0xCA5E0E03, "Texture_Poly_Datas" },
	{ 0xCA5E0E05, "Texture_Poly_Data" },
	{ 0xCA5E0E04, "Texture_Poly_Data_2" },
	{ 0xCA5E0E06, "Texture_Polys" },
	{ 0xCA5E1205, "Animation_Chunk" },
	{ 0xCA5E1200, "Animation_List" },
	{ 0xCA5E1201, "Animation_Data" },
	{ 0xCA5E1203, "Animation_Container" },
	{ 0xCA5E1204, "Animation_Animation" },
	{ 0, 0 },
};

void showchunk(int indent, Granny::Chunk* c)
  {
    cname_t*	cn = cnames;

    while(cn->name)
      {
	if(cn->id == c->id)
	    break;
	cn++;
      }

    if(cn->name)
	printf("%.*s%s", indent*2, "                  ", cn->name);
    else
	printf("%.*s%X", indent*2, "                  ", c->id);
    int len = c->estimate_len();
    if(len)
	printf(" %d@%d", len, c->start);
    if(len == 1)
	printf(" [%d]", c->dword(0));
    printf("\n");
    if(c->children)
	showchunk(indent+1, c->children);
    if(c->sibling)
	showchunk(indent, c->sibling);
  }

int main(int argc, char** argv)
  {
    glutInitDisplayMode(GLUT_RGBA|GLUT_DOUBLE|GLUT_DEPTH);
    glutInitWindowSize(800, 600);
    glutInit(&argc, argv);

    setvbuf(stdout, 0, _IONBF, 0);
    glutCreateWindow("Model test");

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glShadeModel(GL_SMOOTH);
    glEnable(GL_LIGHTING);
    glEnable(GL_CULL_FACE);
    //glCullFace(GL_BACK);

    glutDisplayFunc(main_display);
    glutReshapeFunc(main_reshape);
    glutKeyboardFunc(main_key);

    glutPostRedisplay();

    if(argc>1)
      {
	Granny::File		mf(argv[1]);
	m = new Granny::Model(mf);
	
	showchunk(0, mf.chunks);
	if(argc>2)
	  {
	    Granny::File	ma(argv[2]);
	    a = new Granny::Animation(ma);
	  }
      }

#if 0
    for(Granny::Object* o=m->keys.first; o; o=o->next)
      {
	printf("%d %d {\n", o->id, o->n2);
	for(Granny::KeyPair* kp=o->first; kp; kp=kp->next)
	    printf("  %s=\"%s\"\n", m->keys.s(kp->key), m->keys.s(kp->value));
	puts("}");
      }
#endif
    printf("%s\n", m->texture);

    if(argc>1)
	glutMainLoop();
  }


namespace Granny {

    void Model::render(void)
      {
	Mesh*		m = meshes.first;
	Texture*	tx = textures.tex;

	if(!m || !tx)
	    return;

	glEnable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, .3);

	if(!tex)
	    tex = ::Texture::byname(texture);
	tex->select();

	glTranslatef(bones.root->xlate[0], bones.root->xlate[1], bones.root->xlate[2]);

	glBegin(GL_TRIANGLES);
	for(int t=0; t<m->num_tri; t++)
	  {
	    for(int v=0; v<3; v++)
	      {
		glNormal3fv(m->normals+m->tri_norm[t*3+v]*3);
		glTexCoord2fv(m->texcoords+tx->tri_tc[t*3+v]*2);
		glVertex3fv(m->deformed+m->tri_verts[t*3+v]*3);
	      }
	  }
	glEnd();
      }

    bool FullModel::render(bool fem, int seq, float time, float* col)
      {
	static float	black[] = { 0.0, 0.0, 0.0, 1.0 };
	Model*		m = ((fem&&female) || !male)? female: male;

	if(!m)
	    return true;

	// apply animation to model
	
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, col);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, black);
	m->render();

	return true; // no anim is always finished
      }

    void RenderModels::render(void)
      {
	static float	white[] = { 1.0, 1.0, 1.0, 1.0 };

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(pos[0], pos[1], pos[2]);
	glRotatef(heading, 0, 0, 1);
	glScalef(scale, scale, -scale);
	render_ani();
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, white);
	glPopMatrix();
      }

}; // namespace Granny

