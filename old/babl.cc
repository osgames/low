////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"
#include "fileio.H"
#include "game.H"
#include "console.H"
#include "entity.H"
#include "bablpanel.H"
#include "textures.H"
#include "map.H"

namespace Babl {

#define ARG(n)	(c.data[c.sp-((n)+1)])
#define PTR(n)	(c.data[n])
#define ARGV(n)	(PTR(ARG(n).v).v)

    namespace builtin {
	enum PType {
	    ptPrompt, ptAvatar, ptSpeech, ptNarrate,
	};

	static const char*	dynstr[16];
	static int		dsn = 0;

	static int newstr(const char* s)
	  {
	    if(dynstr[dsn])
		delete[] dynstr[dsn];
	    char*	ds = new char[strlen(s)+1];
	    strcpy(ds, s);
	    int		sn = dsn+1000;
	    dynstr[dsn++] = ds;
	    if(dsn==16)
		dsn = 8;
	    return sn;
	  }

	static const char* str(Conv& c, int mid)
	  {
	    char	pad[32];

	    switch(mid)
	      {
		case 1000 ... 1015:
		    return dynstr[mid-1000]?: "(empty)";
		case 1016:
		    return c.panels->avatar->ask_buf;
		default:
		    if(!mid)
			return "";
		    sprintf(pad, "[%d/%d]", c.strbase, mid);
		    return Game::str(c.strbase, mid)?: pad;
	      }
	  }

	static void prep_menu(Conv& c)
	  {
	    for(int i=0; i<c.panels->npc->num; i++)
		if(c.panels->npc->l[i].old)
		    c.panels->npc->l[i].fade = 20;
	    c.panels->avatar->clear();
	    c.state = Conv::cWait;
	  }

	static void sstrcpy(Conv& c, char*& d, const char* s)
	  {
	    while(*s)
	      {
		if(*s=='\\')
		  {
		    s++;
		    switch(*s++)
		      {
			case 'm': // pause
			    prep_menu(c);
			    break;
		      }
		  }
		else if(*s=='@')
		  {
		    ++s;
		    char	c1 = *s++;
		    char	c2 = *s++;
		    bool	neg = false;
		    int		mid = 0;

		    if(*s=='-')
			neg = true, s++;
		    while(*s>='0' && *s<='9')
			mid = (mid*10) + (*s++-'0');
		    if(neg)
			mid = -mid;
		    switch(c1)
		      {
			case 'G':
			    mid = c.data[mid].v;
			    break;
			case 'S':
			    mid = c.data[c.bp+mid-1];
			    break;
		      }
		    switch(c2)
		      {
			case 'S':
			    sstrcpy(c, d, str(c, mid)?: "(null)");
			    break;
			case 'I':
			    sprintf(d, "%d", mid);
			    d += strlen(d);
			    break;
		      }
		  }
		else
		    *d++ = *s++;
	      }
	  }

	static void print_(Conv& c, int mid, PType pt, int mo=0)
	  {
	    char	msg[1024];

	    const char*	s = str(c, mid);

	    if(mo)
		sprintf(msg, "%d - ", mo);
	    else
		msg[0] = 0;

	    char*	d = msg+strlen(msg);

	    sstrcpy(c, d, s);
	    *d = 0;

	    s = msg;

	    Panel*	p = c.panels->npc;
	    int		col = 0;
	    switch(pt)
	      {
		case ptPrompt:
		    p = c.panels->avatar;
		    break;
		case ptAvatar:
		    break;
		case ptSpeech:
		    break;
		case ptNarrate:
		    col = 2;
		    break;
	      }

	    char	line[128];
	    const char*	eow;
	    bool	mto = false;

	    d = line;
	    do {
		// beginning of line
		d = line;
		if(mo && mto)
		  { *d++ = ' '; *d++ = ' '; *d++ = ' '; *d++ = ' '; }
		else
		    mto = true;

		while(*s) // word
		  {
		    for(eow = s+1; *eow && *eow!=' '; eow++)
			;
		    if((TextImage::width(s, eow-s)+TextImage::width(line, d-line)) > .49)
		      {
			if(d==line)
			    eow = s + 48 - (d-line);
			else
			    break;
		      }
		    while(*s && s<=eow)
			*d++ = *s++;
		  }
		if(*s && *s==' ')
		    s++;
		*d = 0;
		p->l[0].ti.set("");
		int	last = p->num-1;
		for(int i=0; i<last; i++)
		    p->l[i] = p->l[i+1];
		p->l[last].ti.buf = new char[d-line+1];
		strcpy(p->l[last].ti.buf, line);
		p->l[last].ti.w = d-line;
		p->l[last].ti.alloced = true;
		p->l[last].fade = 0;
		p->l[last].old = false;
		p->l[last].ital = (pt==ptNarrate);
		p->l[last].col = col;
		if(line[0])
		    c.output = true;
		while(*s && *s==' ')
		    s++;
	      } while(*s);
	  }

	static void ni(Conv& c, const char* fname)
	  {
	    char	pad[128];

	    sprintf(pad, "unimplemented call: %s(", fname);
	    for(int i=1; i<=ARG(0).v; i++)
	      {
		if(i>1)
		    strcat(pad, ", ");
		sprintf(pad+strlen(pad), "%d", c.data[ARG(i).v].v);
	      }
	    strcat(pad, ")");
	    cprintf(Warning, "babl: %s", pad);
	  }

	static void babl_menu(Conv& c)
	  {
	    int	mo = 1;
	    int	ae = ARG(1).v;

	    c.menu = 0;
	    prep_menu(c);
	    while(PTR(ae).v)
	      {
		c.menukey[c.menu] = '0'+mo;
		c.menuval[c.menu++] = mo;
		print_(c, PTR(ae).v, ptPrompt, mo);
		ae++;
		mo++;
	      }

	    c.rv = 1;
	  }
	static void babl_fmenu(Conv& c)
	  {
	    int	mo = 1;
	    int	ae = ARG(1).v;
	    int ax = ARG(2).v;

	    c.menu = 0;
	    prep_menu(c);
	    while(PTR(ae).v)
	      {
		if(PTR(ax).v)
		  {
		    c.menukey[c.menu] = '0'+mo;
		    c.menuval[c.menu++] = PTR(ae).v;
		    print_(c, PTR(ae).v, ptPrompt, mo);
		    mo++;
		  }
		ae++;
		ax++;
	      }

	    c.rv = ARGV(1);
	  }
	static void print(Conv& c)
	  {
	    print_(c, ARGV(1), ptNarrate);
	  }
	static void babl_ask(Conv& c)
	  {
	    c.panels->avatar->ask = true;
	    c.panels->avatar->ask_buf[c.panels->avatar->ask_len=0] = 0;
	    c.state = Conv::cWait;
	    c.panels->avatar->clear();
	  }
	static void compare(Conv& c)
	  {
	    const char*	s1 = str(c, ARGV(1));
	    const char*	s2 = str(c, ARGV(2));

	    if(strcasecmp(s1, s2))
		c.rv = 0;
	    else
		c.rv = 1;
	  }
	static void random(Conv& c)
	  {
	    c.rv = ::random() % (ARGV(1)+1);
	  }
	static void plural(Conv& c)
	  {
	    ni(c, "plural");
	  }
	static void contains(Conv& c)
	  {
	    const char*	s1 = str(c, ARGV(1));
	    const char*	s2 = str(c, ARGV(2));
	    int		l = strlen(s1);
	    int		n = strlen(s2)-l;

	    while(n>=0)
	      {
		if(!strncasecmp(s2+n, s1, l))
		  {
		    c.rv = 1;
		    return;
		  }
		--n;
	      }
	    c.rv = 0;
	  }
	static void append(Conv& c)
	  {
	    ni(c, "append");
	  }
	static void copy(Conv& c)
	  {
	    ni(c, "copy");
	  }
	static void find(Conv& c)
	  {
	    ni(c, "find");
	  }
	static void length(Conv& c)
	  {
	    const char*	s1 = str(c, ARGV(1));

	    c.rv = strlen(s1);
	  }
	static void val(Conv& c)
	  {
	    const char*	s1 = str(c, ARGV(1));

	    c.rv = atoi(s1);
	  }
	static void say(Conv& c)
	  {
	    ni(c, "say");
	  }
	static void respond(Conv& c)
	  {
	    ni(c, "respond");
	  }

	static void get_quest(Conv& c)
	  {
	    int	qfn = ARGV(1);
	    c.rv = Game::player.qflag[qfn];
	    cprintf(Info, "babl: get_quest(%d) = %d", qfn, c.rv.v);
	  }
	static void set_quest(Conv& c)
	  {
	    int	qfn = ARGV(2);
	    int	qfv = ARGV(1);
	    Game::player.qflag[qfn] = qfv;
	    cprintf(Info, "babl: set_quest(%d) = %d", qfn, qfv);
	  }
	static void sex(Conv& c)
	  {
	    if(Game::player.stats.female)
		c.rv = ARGV(1);
	    else
		c.rv = ARGV(2);
	  }
	static void show_inv(Conv& c)
	  {
	    ni(c, "show_inv");
	  }
	static void give_to_npc(Conv& c)
	  {
	    ni(c, "give_to_npc");
	  }
	static void give_ptr_npc(Conv& c)
	  {
	    ni(c, "give_ptr_npc");
	  }
	static void take_from_npc(Conv& c)
	  {
	    for(EIndex e=c.npc->f_contents; e; e=e->next)
		if(e->type==ARGV(1))
		  {
		    if(Game::player.inv[0])
			e->to_world(Game::player.p_x, Game::player.p_y, Game::player.p_z, 0);
		    else
			e->to_held(0);
		    return;
		  }
	  }
	static void take_id_from_npc(Conv& c)
	  {
	    ni(c, "take_id_from_npc");
	  }
	static void identify_inv(Conv& c)
	  {
	    ni(c, "identify_inv");
	  }
	static void do_offer(Conv& c)
	  {
	    ni(c, "do_offer");
	  }
	static void do_demand(Conv& c)
	  {
	    ni(c, "do_demand");
	  }
	static void do_inv_create(Conv& c)
	  {
	    EIndex		e = Entity::Entity::create(ARGV(1));

	    e->to_inside(c.npc);
	    c.rv = e->index();
	  }
	static void do_inv_delete(Conv& c)
	  {
	    ni(c, "do_inv_delete");
	  }
	static void check_inv_quality(Conv& c)
	  {
	    ni(c, "check_inv_quality");
	  }
	static void set_inv_quality(Conv& c)
	  {
	    ni(c, "set_inv_quality");
	  }
	static void count_inv(Conv& c)
	  {
	    ni(c, "count_inv");
	  }
	static void setup_to_barter(Conv& c)
	  {
	    ni(c, "setup_to_barter");
	  }
	static void end_barter(Conv& c)
	  {
	    ni(c, "end_barter");
	  }
	static void do_judgement(Conv& c)
	  {
	    ni(c, "do_judgement");
	  }
	static void do_decline(Conv& c)
	  {
	    ni(c, "do_decline");
	  }
	static void pause(Conv& c)
	  {
	    ni(c, "pause");
	  }
	static void set_likes_dislikes(Conv& c)
	  {
	    ni(c, "set_likes_dislikes");
	  }
	static void gronk_door(Conv& c)
	  {
	    int	y = ARGV(2);
	    int	x = ARGV(3);

	    if(x!=1 && y!=1)
	      {
		Game::adjust_xy(x, y);
		for(EIndex e=Game::map.tile(x, y).f_ent; e; e=e->next)
		    if(e->type==320 || e->type==321)
			e->use(true);
	      }
	  }
	static void set_race_attitude(Conv& c)
	  {
	    ni(c, "set_race_attitude");
	  }
	static void place_object(Conv& c)
	  {
	    ni(c, "place_object");
	  }
	static void take_from_npc_inv(Conv& c)
	  {
	    ni(c, "take_from_npc_inv");
	  }
	static void add_to_npc_inv(Conv& c)
	  {
	    ni(c, "add_to_npc_inv");
	  }
	static void remove_talker(Conv& c)
	  {
	    ni(c, "remove_talker");
	  }
	static void set_attitude(Conv& c)
	  {
	    ni(c, "set_attitude");
	  }
	static void x_skills(Conv& c)
	  {
	    ni(c, "x_skills");
	  }
	static void x_traps(Conv& c)
	  {
	    ni(c, "x_traps");
	  }
	static void x_obj_pos(Conv& c)
	  {
	    ni(c, "x_obj_pos");
	  }
	static void x_obj_stuff(Conv& c)
	  {
	    EIndex		e = ARGV(9);

	    for(int i=1; i<9; i++)
		if(ARGV(i)>-1) switch(i)
		  {
		    case 6:
			e->special = ARGV(i);
			break;
		    default:
			cprintf(Warning, "babl: x_obj_stuff() param %d=%d", i, ARGV(i));
		  }
	  }
	static void find_inv(Conv& c)
	  {
	    ni(c, "find_inv");
	  }
	static void find_barter(Conv& c)
	  {
	    ni(c, "find_barter");
	  }
	static void find_barter_total(Conv& c)
	  {
	    ni(c, "find_barter_total");
	  }
	static void babl_hack(Conv& c)
	  {
	    ni(c, "babl_hack");
	  }
	static void give_all_stuff(Conv& c)
	  {
	    ni(c, "give_all_stuff");
	  }
	static void do_input_wait(Conv& c)
	  {
	    ni(c, "do_input_wait");
	  }
	static void gronk_trigger(Conv& c)
	  {
	    ni(c, "gronk_trigger");
	  }
	static void transform_talker(Conv& c)
	  {
	    ni(c, "transform_talker");
	  }
	static void x_clock(Conv& c)
	  {
	    int	a1 = ARGV(1);
	    int	a2 = ARGV(2);

	    c.rv = 0;
	    cprintf(Info, "babl: x_clock(%d, %d) = %d", a1, a2, c.rv.v);
	  }
	static void x_exp(Conv& c)
	  {
	    ni(c, "x_exp");
	  }
	static void teleport_player(Conv& c)
	  {
	    ni(c, "teleport_player");
	  }
	static void add_event(Conv& c)
	  {
	    ni(c, "add_event");
	  }
	static void teleport_talker(Conv& c)
	  {
	    ni(c, "teleport_talker");
	  }
	static void switch_pic(Conv& c)
	  {
	    c.panels->npc->portrait = ARGV(1);
	  }
	static void set_sequence(Conv& c)
	  {
	    ni(c, "set_sequence");
	  }

	typedef void builtin(Conv&);
	static builtin*	func[] = {
		/*00*/	babl_menu, babl_fmenu, print, babl_ask,
			compare, random, plural, contains,
			append, copy, find, length,
			val, say, respond, babl_hack,
		/*10*/	give_all_stuff, do_input_wait, get_quest, set_quest,
			sex, show_inv, give_to_npc, give_ptr_npc,
			take_from_npc, take_id_from_npc, identify_inv, do_offer,
			do_demand, do_inv_create, do_inv_delete, check_inv_quality,
		/*20*/	set_inv_quality, count_inv, setup_to_barter, end_barter,
			do_judgement, do_decline, pause, set_likes_dislikes,
			gronk_door, gronk_trigger, set_race_attitude, place_object,
			take_from_npc_inv, add_to_npc_inv, transform_talker, remove_talker,
		/*30*/	set_attitude, x_skills, x_traps, x_clock,
			x_exp, teleport_player, add_event, x_obj_pos,
			x_obj_stuff, find_inv, find_barter, find_barter_total,
			teleport_talker, switch_pic, set_sequence,
	};
    };

    const char* vimports[] = {
	/*0*/	"play_hunger", "play_health", "play_arms", "play_power",
		"play_hp", "play_mana", "play_level", "new_player_exp",
	/*8*/	"play_name", "play_poison", "play_drawn", "play_sex",
		"npc_xhome", "npc_yhome", "npc_whoami", "npc_hunger",
	/*16*/	"npc_health", "npc_hp", "npc_arms", "npc_power",
		"npc_goal", "npc_attitude", "npc_gtarg", "npc_talkedto",
	/*24*/	"npc_level", "npc_name", "dungeon_level", "riddlecounter",
		"game_time", "game_days", "game_mins",
		0,
    };

    int Conv::gv(int vi)
      {
	// XXX: Doing this at every gv() is an ugly hack that needs to be
	//      fixed eventually...
	int wn;
	for(wn=0; wn<Game::num_worlds; wn++)
	    if(Game::worlds[wn].num==Game::player.p_level)
		break;
	Game::World&	w = Game::worlds[wn];

	switch(vi)
	  {
	    case  1: return Game::player.stats.hp;
	    case  3: return Game::player.stats.mp;
	    case  4: return Game::player.stats.hp_max;
	    case  5: return Game::player.stats.mp_max;
	    case  6: return Game::player.stats.xp_lev;
	    case  7: return 100;
	    case  8: return builtin::newstr(Game::player.name);
	    case  9: return Game::player.stats.poison;
	    case 11: return Game::player.stats.female;
	    case 12: return npc->home_x + w.xo;
	    case 13: return 63 - (npc->home_y + w.yo);
	    case 16: return npc->stats.hp;
	    case 17: return npc->stats.mp;
	    case 19: return npc->stats.hp_max;
	    case 21: return 3 - int(npc->mood);
	    case 23: return npc->talked;
	    case 25: return builtin::newstr("*happy camper*");
	    case 26: return Game::player.p_level;
	    default: return 0;
	  }
      }

    void Conv::sv(int vi, int val)
      {
	if(vi==8 || vi==25) // names are never written to
	    return;
	if(gv(vi) == val)
	    return;
#ifndef RELEASE
	cprintf(Debug, "babl: %s = %d", vimports[vi], val);
#endif
	switch(vi)
	  {
	    case 21: npc->mood = Entity::Mood(3-val); break;
	    default:
		cprintf(Warning, "babl: write (%d) to readonly global '%s'", val, vimports[vi]);
		break;
	  }
      }

    Conv::~Conv()
      {
	if(code)
	    delete[] code;
	if(data)
	    delete[] data;
      }

#define S(n)	data[sp-(n+1)]
#define POP	(sp--)
#define PUSH	(++sp)

    Conv::State Conv::run(unsigned short key)
      {
	output = false;
	try
	  {
	    if(state==cReady)
	      {
		for(int i=0; i<nglob; i++)
		    data[glob[i].index] = gv(glob[i].glob);
		panels->npc->portrait = npc->spec-0x40;
		panels->avatar->portrait = -15;
		cprintf(Debug, "babl: portrait %d", panels->npc->portrait);
		state = cRunning;
	      }
	    if(state==cWait)
	      {
		if(menu)
		  {
		    for(int i=0; i<menu; i++)
			if(key==menukey[i])
			  {
			    rv.v = menuval[i];
			    menu = 0;
			    break;
			  }
		    if(menu)
			return cWait;
		  }
		else if(panels->avatar->ask)
		  {
		    if(key>=' ' && key<127)
		      {
			if(panels->avatar->ask_len<32)
			  {
			    panels->avatar->ask_buf[panels->avatar->ask_len++] = key;
			    panels->avatar->ask_buf[panels->avatar->ask_len] = 0;
			  }
			return cWait;
		      }
		    if(key==8 || key==127)
		      {
			if(panels->avatar->ask_len)
			    panels->avatar->ask_buf[--panels->avatar->ask_len] = 0;
		      }
		    if(key!=10 && key!=13)
			return cWait;
		    state = cRunning;
		    rv = 1016;
		    panels->avatar->ask = false;
		  }
		for(int i=0; i<panels->npc->num; i++)
		    panels->npc->l[i].old = true;
		state = cRunning;
		builtin::print_(*this, 0, builtin::ptNarrate);
	      }

	    while(state==cRunning)
	      {
		if(sp >= (dsize-16))
		    throw "stack overflow";
//#define CHECK_CODE
#if defined(CHECK_CODE) && !defined(RELEASE)
		static const char*	pnames[] = {
		    "nop", "add", "mul", "sub", "div", "mod", "or", "and",
		    "not", "gt", "ge", "lt", "le", "eq", "ne", "jmp",
		    "0branch", "1branch", "branch", "call", "extern", "exit", "push", "pea",
		    "pop", "swap", "pushbp", "popbp", "enter", "leave", "addsp", "load",
		    "store", "addi", "start", "sreg", "result", "strcmp", "exit", "emit",
		    "input", "neg",
		};
		char	insn[16];
		char	arg[16];

		if(code[ip]>=0 && code[ip]<0x2A)
		    strcpy(insn, pnames[code[ip]]);
		else
		    sprintf(insn, "#%x", code[ip]);
		arg[0] = 0;
		switch(code[ip])
		  {
		    case 0x0F:
		    case 0x13:
			sprintf(arg, " %x", code[ip+1]);
			break;
		    case 0x10 ... 0x12:
			sprintf(arg, " %x", code[ip+1]+ip+1);
			break;
		    case 0x14:
			sprintf(arg, " %x", code[ip+1]);
			break;
		    case 0x16:
			sprintf(arg, " #%d", code[ip+1]);
			break;
		    case 0x17:
			sprintf(arg, " (%d)bp", code[ip+1]);
			break;
		  }
		strcat(insn, arg);
		insn[15] = 0;
		fprintf(stderr, "%03x: %15s [", ip, insn);
		for(int s=sp-1; s>=(gsize>?bp); --s)
		    fprintf(stderr, " %d", data[s].v);
		fprintf(stderr, " ]\n");
#endif
		switch(code[ip++])
		  {
		    case 0x00: break;
		    case 0x01: S(1).v += S(0).v; POP; break;
		    case 0x02: S(1).v *= S(0).v; POP; break;
		    case 0x03: S(1).v -= S(0).v; POP; break;
		    case 0x04: S(1).v /= S(0).v; POP; break;
		    case 0x05: S(1).v %= S(0).v; POP; break;
		    case 0x06: S(1).v |= S(0).v; POP; break;
		    case 0x07: S(1).v &= S(0).v; POP; break;
		    case 0x08: S(0).v = !S(0).v; break;
		    case 0x09: S(1).v = S(1).v >  S(0).v; POP; break;
		    case 0x0A: S(1).v = S(1).v >= S(0).v; POP; break;
		    case 0x0B: S(1).v = S(1).v <  S(0).v; POP; break;
		    case 0x0C: S(1).v = S(1).v <= S(0).v; POP; break;
		    case 0x0D: S(1).v = S(1).v == S(0).v; POP; break;
		    case 0x0E: S(1).v = S(1).v != S(0).v; POP; break;
		    case 0x0F: ip = code[ip]; break;
		    case 0x10: POP; ip += S(-1).v? 1: code[ip]; break;
		    case 0x11: POP; ip += S(-1).v? code[ip]: 1; break;
		    case 0x12: ip += code[ip]; break;
		    case 0x13: PUSH; S(0).v = ip+1; ip = code[ip]; break;
		    case 0x14: builtin::func[code[ip++]](*this); break;
		    case 0x15: ip = S(0).v; POP; break;
		    case 0x16: PUSH; S(0).v = code[ip++]; break;
		    case 0x17: PUSH; S(0).v = bp + code[ip++] - 1; break;
		    case 0x18: POP; break;
		    case 0x19: { Value t=S(0); S(0)=S(1); S(1)=t; } break;
		    case 0x1A: PUSH; S(0).v = bp; break;
		    case 0x1B: bp = S(0).v; POP; break;
		    case 0x1C: bp = sp; break;
		    case 0x1D: sp = bp; break;
		    case 0x1E: POP; sp += S(-1).v; break;
		    case 0x1F: S(0).v = data[S(0).v].v; break;
		    case 0x20: data[S(1).v].v = S(0).v; POP; POP; break;
		    case 0x21: S(1).v += S(0).v-1; POP; break;
		    case 0x22: /* start */ break;
		    case 0x23: rv = S(0); POP; break;
		    case 0x24: PUSH; S(0) = rv; break;
		    case 0x26: builtin::prep_menu(*this); state = cDone; break;
		    case 0x27: builtin::print_(*this, S(0).v, builtin::ptSpeech); POP; break;
		    case 0x29: S(0).v = -S(0).v; break;
		    default:
			       throw "unknown primitive";
			       abort();
			       break;
		  }
	      }
	  }
	catch(const char* msg)
	  {
	    state = cError;
	    cprintf(Error, "babl: %s", msg);
	  }

	if(state==cDone)
	  {
	    for(int i=0; i<nglob; i++)
		sv(glob[i].glob, data[glob[i].index].v);
	    if(!npc->glob)
		npc->glob = new short[npc->num_globs=gsize];
	    for(int i=0; i<gsize; i++)
		npc->glob[i] = data[i].v;
	    if(!output)
		state = cFinished;
	  }
	return state;
      }

    File		cfd;
    size_t		cflen;
    short		numc;
    unsigned long*	coff;

    Conv* load(EIndex mob)
      {
	int	num = mob->spec - 0x3F;

	builtin::dsn = 0;

	if(!cfd)
	  {
	    cfd = openuw2("data/cnv.ark");
	    cflen = cfd.size();
	    cfd.read(&numc, 2);
	    coff = new unsigned long[numc];
	    cfd.read(coff, 4*numc);
	  }

	if(num<0 || num>=numc || !coff[num])
	    return 0;

	off_t		start = coff[num];
	off_t		end = cflen;
	for(int i=num+1; i<numc; i++)
	    if(coff[i])
	      {
		end = coff[i];
		break;
	      }

	unsigned char*	cbuf = new unsigned char[end-start];
	cfd.seek(start);
	cfd.read(cbuf, end-start);

	int		ulen = *((int*)(cbuf));
	unsigned char*	ubuf = new unsigned char[ulen+256];
	unsigned char*	cp = cbuf+4;
	unsigned char*	up = ubuf;
	unsigned char*	ue = ubuf+ulen;

	while(up<ue)
	  {
	    unsigned char	bits = *cp++;
	    for(int i=0; i<8; i++, bits>>=1)
	      {
		if(bits&1)
		    *up++ = *cp++;
		else
		  {
		    int	m1 = *cp++;
		    int m2 = *cp++;

		    m1 |= (m2&0xF0) << 4;
		    if(m1&0x800)
			m1 |= 0xFFFFF000;
		    m2 =  (m2&0x0F) + 3;
		    m1 += 18;
		    if(m1 > up-ubuf)
			throw "evil run!";
		    while(m1 < (up-ubuf-0x1000))
			m1 += 0x1000;
		    while(m2--)
			*up++ = ubuf[m1++];
		  }
		if(up>=ue)
		    break;
	      }
	  }
	if(up>ue)
	    throw "conversation failed unpacking";

	struct ch_t {
	    unsigned long		unkonwn1;
	    unsigned long		size;
	    unsigned short		unknown2;
	    unsigned short		strings;
	    unsigned short		gdepth;
	    unsigned short		globals;
	    unsigned char		data[0];
	}&			h = *((ch_t*)ubuf);

	Conv*			c = new Conv;

	memset(c, 0, sizeof(c));
	c->npc = mob;
	c->num = num;
	c->strbase = h.strings;
	c->nglob = 0;
	c->glob = new Conv::glob_[h.globals];
	c->code = new short[c->csize = h.size];
	c->gsize = h.gdepth;
	c->data = new Value[c->dsize = 2048];
	
	if(c->npc->glob)
	    for(int i=0; i<c->gsize; i++)
		c->data[i] = c->npc->glob[i];

	const unsigned char*	cd = h.data;

	for(int i=0; i<h.globals; i++)
	  {
	    char	pad[64];
	    int		namelen = *cd++;

	    memcpy(pad, ++cd, namelen<?63);
	    pad[namelen] = 0;
	    cd += namelen;
	    if(cd[4]!=17)	// global variable
	      {
		int	in = 0;

		while(vimports[in])
		    if(!strcmp(vimports[in], pad))
		      {
			c->glob[c->nglob].index = cd[0];
			c->glob[c->nglob++].glob = in;
			break;
		      }
		    else
			in++;
		if(!vimports[in])
		    cprintf(Warning, "babl: unknown global '%s' in conversation", pad);
	      }
	    cd += 8;
	  }

	memcpy(c->code, cd, 2*c->csize);

	c->bp = c->sp = c->gsize;
	c->ip = 0;
	c->state = Conv::cReady;

	cprintf(Debug, "babl: conversation %d ready", num);

	return c;
      }

    Panel::Panel(float y, int nl): ScrollPanel(.01, y, .68, nl)
      {
	panels = 0;
	trade = 0;
	portrait = 0;
	ask = 0;
      }

    Panel::~Panel()
      {
      }

    void Panel::render(bool select)
      {
	begin_render(select);
	if(portrait)
	  {
	    tex(portrait+0x40f);
	    glColor3f(1, 1, 1);
	    glBegin(GL_QUADS);
	      glTexCoord2f(0, 1); glVertex2d(.01, h*1.3-.01);
	      glTexCoord2f(1, 1); glVertex2d(.12, h*1.3-.01);
	      glTexCoord2f(1, 0); glVertex2d(.12, h*1.3-.35);
	      glTexCoord2f(0, 0); glVertex2d(.01, h*1.3-.35);
	    glEnd();
	  }
	framed = true;

	tex_none();
	float oh=h;
	h *= 1.3;
	render_frame();
	h = oh;
	glScalef(1.1, 1.3, 1);
	glTranslatef(.125, .005, 0);
	for(int i=num-1; i>=0; i--)
	  {
	    float   alpha = 1.0;

	    if(l[i].fade)
	      {
		if(l[i].fade < 50)
		    alpha = .02 * l[i].fade;
		if(!--l[i].fade)
		    l[i].ti.set("");
	      }
	    if(ask && i==(num-1))
	      {
		TextImage	ti;
		char		pad[40];

		sprintf(pad, "You say? %s_", ask_buf);
		ti.set(pad);
		ti.render(1, 1, 1, 1, 1.0);
	      }
	    else
		l[i].ti.render(1, 1, 1, 1, alpha, l[i].ital);
	    glTranslatef(0, .025, 0);
	  }
	end_render();
      }

    void Panel::clear(void)
      {
	for(int i=0; i<num; i++)
	    l[i].ti.set("");
      }

}; // namespace Babl

