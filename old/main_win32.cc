////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"

#define WIN32_LEAN_AND_MEAN	// Lean/er/ anyways.  Somewhat.
#include <windows.h>		// uck.  Ugly bag of mostly bugs.

#include "main.H"

int	want_w = 800;
int	want_h = 600;
int	want_screen = 0;
FILE*	logfile;

#if 0
FILE*	logfile;

int main(int argc, char** argv)
  {
    logfile = fopen("log.txt", "w");
    //int	arg = 1;
    // parse options
    try
      {
	return game();
      }
    catch(const char* msg)
      {
	MessageBox(NULL, msg, "Oops!", MB_OK);
	    return 1;
      }
  }
#endif

HINSTANCE	hinst;

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR, int)
  {
    logfile = fopen("log.txt", "w");
    hinst = hInstance;
    try {
	return game();
    } catch(const char* msg) {
	MessageBox(NULL, msg, "Oops!", MB_OK);
	    return 1;
    }
    return 0;
  }

