////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#define WANT_SDL
#include "setup.H"

#include "game.H"
#include "map.H"
#include "entity.H"
#include "model.H"
#include "granny.H"
#include "render.H"
#include "textures.H"

namespace Entity {


    void Entity::texture(short i)
      {
	tex(0x200+i);
      }

    void Entity::render(void)
      {
	if(where!=Tile)
	    return;

	if(trap() || trigger())
	    if(!Game::setup.martians_debug)
		return;

	if(models.num_rm)
	  {
	    models.heading = phys.aim_h;
	    models.pos[0] = x-tx;
	    models.pos[1] = y-ty;
	    models.pos[2] = z;
	    models.render();
	    return;
	  }

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(x-floor(x+.5), y-floor(y+.5), z);
	glRotatef(facing*45, 0, 0, 1);

	if(mat)
	  {
	    if(mat<0x100)
		texture_hook(mat);
	    else
		tex(mat);
	  }
	else
	    tex_none();
	glDisable(GL_BLEND);
	glEnable(GL_LIGHTING);
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, t().color);

	if(!t().render_pre || t().render_pre(index()))
	  {
	    if(t().model==511) // stand-in
	      {
		glPopMatrix();
		glPushMatrix();
		glTranslatef(x-floor(x+.5), y-floor(y+.5), z);
		double	a = atan2(y-Game::player.p_y, x-Game::player.p_x);
		glRotatef(90+a*180/M_PI, 0, 0, 1);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_ALPHA_TEST);
		glAlphaFunc(GL_GREATER, .3);
		if(type>=0xE8 && type<=0xFF) // runestones
		    Entity::texture(0xE0);
		else
		    Entity::texture(type);
		glTranslatef(t().r_xlate[0], t().r_xlate[1], t().r_xlate[2]);
		glNormal3f(0, 1, 0);
		glBegin(GL_QUADS);
		  {
		    float	iw = .25;
		    float	ih = .25;
		    float	hw = .5/32;
		    float	hh = .5/32;
		    float	tw = 1 - hw;
		    float	th = 1 - hh;

		    glTexCoord2f(hw, th); glVertex3f(-iw/2, 0, -ih);
		    glTexCoord2f(tw, th); glVertex3f(+iw/2, 0, -ih);
		    glTexCoord2f(tw, hh); glVertex3f(+iw/2, 0,  0);
		    glTexCoord2f(hw, hh); glVertex3f(-iw/2, 0,  0);
		  }
		glEnd();
		glDisable(GL_ALPHA_TEST);
	      }
	    else
		if(t().model)
		  {
		    glTranslatef(t().r_xlate[0], t().r_xlate[1], t().r_xlate[2]);
		    glCallList(::models[t().model]);
		  }
	  }

	if(t().render_post)
	    t().render_post(index());

	glPopMatrix();
      }


}; // namespace Entity

