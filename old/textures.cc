////
////  Labyrinth of Worlds
////  Original Copyright (c) 1992 Looking Glass Studios
////  Rewrite Copyright (c) 2002 Marc A. Pelletier
////
////  THIS SOFTWARE COMES WITH ABSOLUTELY NO WARRANTY! USE AT YOUR OWN RISK!
////
////  See the file COPYING for licensing information.
////


#include "setup.H"
#include "md.H"

#include "fileio.H"
#include "png.H"
#include "gr.H"
#include "textures.H"


static const int	lmtw = 64;
static const double	lmtc = 31.5;
unsigned char		lum[lmtw*lmtw*lmtw*3];

#include "data_em.cc"

namespace Textures
  {

    static const int	total_tex = 0x681;

    static unsigned int	tid[total_tex];
    static bool		no_nmap[0x100];
    static const char*	tex_name[128];
    static int		num_tex_name = 0;

    static Gr::File*	doortex_gr = 0;
    static Gr::File*	objects_gr = 0;
    static Gr::File*	curtex_gr = 0;
    static Gr::File*	portraits_gr = 0;
    static Gr::File*	avatar_gr = 0;
    static Gr::File*	spells_gr = 0;
    static Gr::File*	tm_flat_gr = 0;
    static Gr::File*	tm_obj_gr = 0;
    static File		tex_file;

    void load_png(int id, const char* name)
      {
	char	fname[128];

	snprintf(fname, 128, "textures/%s.png", name);

	try
	  {
	    PNG_Context	png(new PNG_reader_stdio(fname));
	    PNG_Image*	img = png.read();

	    glBindTexture(GL_TEXTURE_2D, tid[id]);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->w, img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->rgba);
	    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, img->w, img->h, GL_RGBA, GL_UNSIGNED_BYTE, img->rgba);
	    delete img;
	  }
	catch(const PNG_Error&)
	  {
	    return;
	  }
      }

    void load(int id)
      {
	if(id<0x100)
	  {
	    if(tex_file)
	      {
		int		offs;
		unsigned char	raw[64*64];
		unsigned char	rgba[64*64*4];
		unsigned char*	d = rgba;

		tex_file.seek((id+1)*4);
		tex_file.read(&offs, sizeof(int));
		tex_file.seek(offs);
		tex_file.read(raw, 64*64);

		for(int i=0; i<64*64; i++)
		    if(raw[i])
		      {
			*d++ = Gr::pals->color[raw[i]][0];
			*d++ = Gr::pals->color[raw[i]][1];
			*d++ = Gr::pals->color[raw[i]][2];
			if(alpha_values_[id].p)
			    *d++ = alpha_values_[id].p[i];
			else
			    *d++ = alpha_values_[id].a;
		      }
		    else for(int j=0; j<4; j++)
			*d++ = 0;
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, rgba);
		gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, 64, 64, GL_RGBA, GL_UNSIGNED_BYTE, rgba);
#if 0
		  {
		    char	pad[64];
		    sprintf(pad, "tmp/t%d.raw", id);
		    int f = open(pad, O_WRONLY|O_CREAT|O_TRUNC, 0660);
		    write(f, rgba, 64*64*4);
		    close(f);
		  }
#endif
	      }
	  }
	else if(id<0x120)
	  {
	    if(doortex_gr)
	      {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		doortex_gr->texture(id-0x100);
	      }
	  }
	else if(id<0x140)
	  {
	    if(curtex_gr)
	      {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		curtex_gr->texture(id-0x120);
	      }
	  }
	else if(id<0x180)
	  {
	    char	pad[32];

	    sprintf(pad, "t%x", id);
	    load_png(id, pad);
	  }
	else if(id<0x200)
	  {
	    if(tex_name[id-0x180])
		load_png(id, tex_name[id-0x180]);
	  }
	else if(id<0x400)
	  {
	    if(objects_gr)
	      {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		objects_gr->texture(id-0x200, 0);
	      }
	  }
	else if(id<0x410)
	  {
	    if(avatar_gr)
	      {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		avatar_gr->texture(id-0x400);
	      }
	  }
	else if(id<0x500)
	  {
	    if(portraits_gr)
	      {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		portraits_gr->texture(id-0x410);
	      }
	  }
	else if(id<0x600)
	  {
	    char	pad[32];

	    sprintf(pad, "n%d", id-0x500);
	    load_png(id, pad);
	  }
	else if(id<0x620)
	  {
	    if(spells_gr) {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		spells_gr->texture(id-0x600);
	    }
	  }
	else if(id<0x640)
	  {
	    if(tm_flat_gr) {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		tm_flat_gr->texture(id-0x620, 0);
	    }
	  }
	else if(id<0x680)
	  {
	    if(tm_obj_gr) {
		glBindTexture(GL_TEXTURE_2D, tid[id]);
		tm_obj_gr->texture(id-0x640, 0);
	    }
	  }
	else if(id==0x680)
	  {
	    glBindTexture(GL_TEXTURE_3D, tid[id]);

	    for(int x=0; x<lmtw; x++)
		for(int y=0; y<lmtw; y++)
		    for(int z=0; z<lmtw; z++)
		      {
			double	xx = (x-lmtc) / lmtc;
			double	yy = (y-lmtc) / lmtc;
			double	zz = (z-lmtc) / lmtc;
			double	de = sqrt(xx*xx + yy*yy + zz*zz);
			double	da = (.95 - sqrt(de)) >? 0;

			lum[(x+y*lmtw+z*lmtw*lmtw)*3]   = int(127.5 + 127.5*(-xx/de)*da);
			lum[(x+y*lmtw+z*lmtw*lmtw)*3+1] = int(127.5 + 127.5*(-yy/de)*da);
			lum[(x+y*lmtw+z*lmtw*lmtw)*3+2] = int(127.5 + 127.5*(-zz/de)*da);
		      }

	    glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_PRIORITY, 1.0);
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, lmtw, lmtw, lmtw, 0, GL_RGB, GL_UNSIGNED_BYTE, lum);
#ifndef WIN32
	    gluBuild3DMipmaps(GL_TEXTURE_3D, GL_RGB, lmtw, lmtw, lmtw, GL_RGB, GL_UNSIGNED_BYTE, lum);
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
#else
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
#endif
	    glDisable(GL_TEXTURE_3D);
	  }
      }

    bool initialize(void)
      {
	objects_gr = new Gr::File("data/objects.gr");
	doortex_gr = new Gr::File("data/doors.gr");
	curtex_gr = new Gr::File("data/cursors.gr");
	portraits_gr = new Gr::File("data/charhead.gr");
	avatar_gr = new Gr::File("data/heads.gr");
	spells_gr = new Gr::File("data/spells.gr");
	tm_flat_gr = new Gr::File("data/tmflat.gr");
	tm_obj_gr = new Gr::File("data/tmobj.gr");
	tex_file = openuw2("data/t64.tr");
	glGenTextures(total_tex, tid);
#if 0
	for(int i=0; i<0x100; i++) {
	    load(i);
	}
#endif
	return false;
      }

  };

void tex_none(void)
  {
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_3D);
  }

bool tex(int id)
  {
    if(id>=0x500 && id<0x600 && Textures::no_nmap[id-0x500])
	return false;

    if(!glIsTexture(Textures::tid[id]))
	Textures::load(id);

    if(glIsTexture(Textures::tid[id]))
      {
	if(id==0x680) {
	    glDisable(GL_TEXTURE_2D);
	    glEnable(GL_TEXTURE_3D);
	    glBindTexture(GL_TEXTURE_3D, Textures::tid[id]);
	    return true;
	}
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_3D);
	glBindTexture(GL_TEXTURE_2D, Textures::tid[id]);
#if 0
	switch(id)
	  {
	    case 0x148 ... 0x3FF:
	    case 0x09A ... 0x09F:
	    case 0x0EC ... 0x0EF:
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		break;
	    case 0x070 ... 0x071:
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		break;
	    default:
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		break;
	  }
#endif
	return true;
      }

    if(id>=0x500 && id<0x600)
	Textures::no_nmap[id-0x500] = true;
    tex_none();
    return false;
  }

int tex_named(const char* name)
  {
    int	id;

    for(id=0; id<Textures::num_tex_name; id++)
	if(!strcmp(Textures::tex_name[id], name))
	    return id + 0x180;

    char*	tn = new char[strlen(name)+1];

    strcpy(tn, name);
    Textures::tex_name[Textures::num_tex_name++] = tn;
    return id + 0x180;
  }

