// OSI Isn't quite able to generate every keysym we support, but all
// of the important ones are there.

 { OIS::KC_BACK,	K_BackSpace, },
 { OIS::KC_TAB,		K_Tab, },
 { OIS::KC_RETURN,	K_Return, },
 { OIS::KC_PAUSE,	K_Pause, },
 { OIS::KC_ESCAPE,	K_Escape, },
 { OIS::KC_DELETE,	K_Delete, },
 { OIS::KC_HOME,	K_Home, },
 { OIS::KC_LEFT,	K_Left, },
 { OIS::KC_UP,		K_Up, },
 { OIS::KC_RIGHT,	K_Right, },
 { OIS::KC_DOWN,	K_Down, },
 { OIS::KC_PGUP,	K_Page_Up, },
 { OIS::KC_PGDOWN,	K_Page_Down, },
 { OIS::KC_END,		K_End, },
 { OIS::KC_INSERT,	K_Insert, },
 { OIS::KC_NUMPADENTER,	K_KP_Enter, },
 { OIS::KC_NUMPADEQUALS,K_KP_Equal, },
 { OIS::KC_MULTIPLY,	K_KP_Multiply, },
 { OIS::KC_ADD,		K_KP_Add, },
 { OIS::KC_SUBTRACT,	K_KP_Subtract, },
 { OIS::KC_DECIMAL,	K_KP_Decimal, },
 { OIS::KC_DIVIDE,	K_KP_Divide, },
 { OIS::KC_NUMPAD0,	K_KP_0, },
 { OIS::KC_NUMPAD1,	K_KP_1, },
 { OIS::KC_NUMPAD2,	K_KP_2, },
 { OIS::KC_NUMPAD3,	K_KP_3, },
 { OIS::KC_NUMPAD4,	K_KP_4, },
 { OIS::KC_NUMPAD5,	K_KP_5, },
 { OIS::KC_NUMPAD6,	K_KP_6, },
 { OIS::KC_NUMPAD7,	K_KP_7, },
 { OIS::KC_NUMPAD8,	K_KP_8, },
 { OIS::KC_NUMPAD9,	K_KP_9, },
 { OIS::KC_F1,		K_F1, },
 { OIS::KC_F2,		K_F2, },
 { OIS::KC_F3,		K_F3, },
 { OIS::KC_F4,		K_F4, },
 { OIS::KC_F5,		K_F5, },
 { OIS::KC_F6,		K_F6, },
 { OIS::KC_F7,		K_F7, },
 { OIS::KC_F8,		K_F8, },
 { OIS::KC_F9,		K_F9, },
 { OIS::KC_F10,		K_F10, },
 { OIS::KC_F11,		K_F11, },
 { OIS::KC_F12,		K_F12, },
 { OIS::KC_F13,		K_F13, },
 { OIS::KC_F14,		K_F14, },
 { OIS::KC_F15,		K_F15, },
 { OIS::KC_SCROLL,	K_Scroll_Lock, },
 { OIS::KC_SYSRQ,	K_Sys_Req, },
 { OIS::KC_LSHIFT,	K_Shift_L, },
 { OIS::KC_RSHIFT,	K_Shift_R, },
 { OIS::KC_LCONTROL,	K_Control_L, },
 { OIS::KC_RCONTROL,	K_Control_R, },
 { OIS::KC_CAPITAL,	K_Caps_Lock, },
 { OIS::KC_RMENU,	K_Meta_R, },
 { OIS::KC_LWIN,	K_Super_L, },
 { OIS::KC_RWIN,	K_Super_R, },
 { OIS::KC_APPS,	K_Hyper_R, },
 { OIS::KC_NUMLOCK,	K_Num_Lock, },
 { OIS::KC_SPACE,	K_Space, },
 { OIS::KC_APOSTROPHE,	K_Apostrophe, },
 { OIS::KC_COMMA,	K_Comma, },
 { OIS::KC_MINUS,	K_Minus, },
 { OIS::KC_PERIOD,	K_Period, },
 { OIS::KC_SLASH,	K_Slash, },
 { OIS::KC_0,		K_0, },
 { OIS::KC_1,		K_1, },
 { OIS::KC_2,		K_2, },
 { OIS::KC_3,		K_3, },
 { OIS::KC_4,		K_4, },
 { OIS::KC_5,		K_5, },
 { OIS::KC_6,		K_6, },
 { OIS::KC_7,		K_7, },
 { OIS::KC_8,		K_8, },
 { OIS::KC_9,		K_9, },
 { OIS::KC_SEMICOLON,	K_Semicolon, },
 { OIS::KC_EQUALS,	K_Equal, },
 { OIS::KC_LBRACKET,	K_Bracketleft, },
 { OIS::KC_BACKSLASH,	K_Backslash, },
 { OIS::KC_RBRACKET,	K_Bracketright, },
 { OIS::KC_GRAVE,	K_Grave, },
 { OIS::KC_A,		K_A, },
 { OIS::KC_B,		K_B, },
 { OIS::KC_C,		K_C, },
 { OIS::KC_D,		K_D, },
 { OIS::KC_E,		K_E, },
 { OIS::KC_F,		K_F, },
 { OIS::KC_G,		K_G, },
 { OIS::KC_H,		K_H, },
 { OIS::KC_I,		K_I, },
 { OIS::KC_J,		K_J, },
 { OIS::KC_K,		K_K, },
 { OIS::KC_L,		K_L, },
 { OIS::KC_M,		K_M, },
 { OIS::KC_N,		K_N, },
 { OIS::KC_O,		K_O, },
 { OIS::KC_P,		K_P, },
 { OIS::KC_Q,		K_Q, },
 { OIS::KC_R,		K_R, },
 { OIS::KC_S,		K_S, },
 { OIS::KC_T,		K_T, },
 { OIS::KC_U,		K_U, },
 { OIS::KC_V,		K_V, },
 { OIS::KC_W,		K_W, },
 { OIS::KC_X,		K_X, },
 { OIS::KC_Y,		K_Y, },
 { OIS::KC_Z,		K_Z, },
 { OIS::KC_UNASSIGNED,  K_No_Key, },
