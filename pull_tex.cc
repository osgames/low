

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "uw2files.H"


int main(int, char** argv)
{
    UW2::StringFile	str("../../uw2/data/strings.pak");
    UW2::DataFile	pal("../../uw2/data/pals.dat");
    UW2::DataFile	tex("../../uw2/data/t64.tr");
    char		pad[64];
    char		fname[128];

    for(int tx=0; tx<tex.u16(2); tx++) {
	if(str(10, tx)) {
	    strcpy(pad, str(10, tx));
	    if(pad[0]=='a') {
		if(pad[1]==' ')
		    strcpy(pad, str(10, tx)+2);
		else if(pad[1]=='n' && pad[2]==' ')
		    strcpy(pad, str(10, tx)+3);
	    }
	    for(char* s=pad; *s; s++)
		if(*s==' ')
		    *s = '_';
	} else {
	    strcpy(pad, "unk");
	}
	sprintf(fname, "textures/uw2/%s_%d.xpm", pad, tx);
	FILE* f = fopen(fname, "w");
	fprintf(f,	"/* XPM */\n"
		"static char * t_%s[] = {\n"
		"\"64 64 256 2\",\n", pad);
	fprintf(f, "\"aa c none\",\n");
	for(int t=1; t<256; t++)
	    fprintf(f, "\"%c%c c #%02x%02x%02x\",\n", 'a'+t/26, 'a'+t%26,
		    (pal.u8(t*3+0)<<2)|(pal.u8(t*3+0)&3),
		    (pal.u8(t*3+1)<<2)|(pal.u8(t*3+1)&3),
		    (pal.u8(t*3+2)<<2)|(pal.u8(t*3+2)&3));
	const unsigned char*	tt = tex.data() + tex.u32(4+4*tx);
	for(int y=0; y<64; y++) {
	    fprintf(f, "\"");
	    for(int x=0; x<64; x++) {
		int px = tt[x+y*64];
		fprintf(f, "%c%c", 'a'+px/26, 'a'+px%26);
	    }
	    fprintf(f, "\",\n");
	}
	fprintf(f, "};\n");
    }
}

