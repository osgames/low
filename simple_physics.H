


#include "game.H"


class SimplePhysics: virtual public Game
{
  public:
    					SimplePhysics(void);
					~SimplePhysics();

    void				phy_prepare_world(int level);
    void				phy_simulate(double);
    void				phy_place_player(float, float, float, float);
};


